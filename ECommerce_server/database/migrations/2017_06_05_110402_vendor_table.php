<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VendorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_table', function (Blueprint $table) {
            $table->increments('id');
            $table->string('vendorId');
            $table->string('vendorName');
            $table->string('vendorEmailId');
            $table->string('vendorPhoneNumber');
            $table->text('vendorAddress');
            $table->text('vendorDomainName');
            $table->text('vendorProfilePicture')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vendor_table');
    }
}
