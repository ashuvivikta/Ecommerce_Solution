<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SuggestionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suggestion_table', function (Blueprint $table) {
            $table->increments('id');
            $table->string('vendorId');
            $table->string('endUserName');
            $table->string('endUserEmailId');
            $table->string('suggestionId');
            $table->text('suggestion');
            $table->text('adminReply')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('suggestion_table');
    }
}
