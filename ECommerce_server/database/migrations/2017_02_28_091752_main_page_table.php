<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MainPageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('main_page_table', function (Blueprint $table) {
            $table->increments('id');
            $table->string('vendorId');
            $table->string('mainPageId');
            $table->text('sliderImageDetails');
            $table->string('vendorLogo');
            $table->string('facebookLink')->nullable();
            $table->string('twitterLink')->nullable();
            $table->string('googlePlusLink')->nullable();
            $table->string('instagramLink')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('main_page_table');
    }
}
