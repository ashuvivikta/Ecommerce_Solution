<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_table', function (Blueprint $table) {
            $table->increments('id');
            $table->string('vendorId');
            $table->string('productName');
            $table->text('productDescription');
            $table->string('categoryId');
            $table->string('subcategoryId');
            $table->string('genderId');
            $table->string('productColorId');
            $table->string('productId');
            $table->string('productPrice');
            $table->string('productModel');
            $table->string('productTaxAmount')->nullable();
            $table->string('productShippingAmount')->nullable();
            $table->string('productSize')->nullable();
            $table->string('productColor');
            $table->string('productCount');
            $table->string('productImage');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('product_table');
    }
}
