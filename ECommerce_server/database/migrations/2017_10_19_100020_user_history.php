<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserHistory extends Migration
{
    public function up()
    {
        Schema::create('user_history', function (Blueprint $table) {
            $table->increments('id');
            $table->string('vendorId');
            $table->integer('count')->default('0');
            $table->string('productId');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('user_history');
    }
}
