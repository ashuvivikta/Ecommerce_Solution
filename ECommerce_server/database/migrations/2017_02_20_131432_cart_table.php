<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart_table', function (Blueprint $table) {
            $table->increments('id');
            $table->string('vendorId');
            $table->string('userId');
            $table->string('cartId');
            $table->string('addressId');
            $table->string('orderRecievedDate')->nullable();
            $table->string('paymentRecievedDate')->nullable();
            $table->string('shippingInProgressDate')->nullable();
            $table->string('shippedDate')->nullable();
            $table->string('orderRejectedDate')->nullable();
            $table->text('productDetails');
            $table->string('productStatus');
            $table->string('paymentStatus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cart_table');
    }
}
