<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ComplaintTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('complaint_table', function (Blueprint $table) {
            $table->increments('id');
            $table->string('vendorId');
            $table->string('userId');
            $table->string('cartId');
            $table->string('complaintId');
            $table->text('userComplaint')->nullable();
            $table->text('adminAnswer')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('complaint_table');
    }
}
