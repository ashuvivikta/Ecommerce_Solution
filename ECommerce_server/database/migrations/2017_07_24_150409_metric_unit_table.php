<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MetricUnitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metric_unit_table', function (Blueprint $table) {
            $table->increments('id');
            $table->string('unitId');
            $table->string('unitName');
            $table->text('unitDescription')->nullable();
            $table->string('metricUnit');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('metric_unit_table');
    }
}
