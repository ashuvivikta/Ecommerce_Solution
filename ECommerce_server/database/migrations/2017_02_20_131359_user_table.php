<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_table', function (Blueprint $table) {
            $table->increments('id');
            $table->string('userName');
            $table->string('userEmailId');
            $table->string('userPhoneNumber');
            $table->string('userId');
            $table->text('userVerificationStatus')->nullable();
            $table->text('userProfilePicturePath')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_table');
    }
}
