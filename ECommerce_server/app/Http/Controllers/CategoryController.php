<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Middleware\AuthUser;
use App\Http\Middleware\Configuration;
use App\Http\Middleware\GenerateUUID;
use App\Login;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;


class CategoryController extends Controller
{
    public function add (Request $request)
    {
        $authenticate = new AuthUser();
        $authenticateUser  = $authenticate->authenticateUser();
        if($authenticateUser == 400)
        {
            $getLoginId = Login::where('remember_token',$request->input('token'))->first();

            $flagValidateInputs = $this->validateAddCategory($request);
            if($flagValidateInputs == false)
            {
                $returnValues = new ReturnController("2001","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $checkCategoryName = Category::where('vendorId',$getLoginId['loginId'])
                                             ->where('categoryName',$request->input('categoryName'))->first();
                if($checkCategoryName != null || $checkCategoryName != "")
                {
                    $returnValues = new ReturnController("2002","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $generateUniqueId = new GenerateUUID();
                    $categoryId = $generateUniqueId->getUniqueId();

                    $category = new Category();
                    $category->vendorId = $getLoginId['loginId'];
                    $category->categoryId = $categoryId;
                    $category->categoryName = $request->input('categoryName');
                    $category->save();

                    if(!$category->save())
                    {
                        $returnValues = new ReturnController("2003","FAILURE","");
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                    else
                    {
                        $returnValues = new ReturnController("2000","SUCCESS","");
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                }
            }

        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }

    protected function validateAddCategory(Request $request)
    {
        $rules = array(
            'categoryName' => 'required');

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails())
            return false;
        else
            return true;
    }
    public function listAll (Request $request)
    {
        $getDefaultVariables = new Configuration();
        $paginationCount = $getDefaultVariables->getPaginationcount();

        $getLoginId = Login::where('remember_token',$request->input('token'))->first();


        $filterType = $request->input('filterType');
        switch ($filterType)
        {
            case "0":$authenticate = new AuthUser();
                $authenticateUser  = $authenticate->authenticateUser();
                if($authenticateUser == 400)
                {

                    $listCategoryCount = Category::where('vendorId',$getLoginId['loginId'])->count();

                    if($listCategoryCount <= $paginationCount)
                    {
                        $listCategory = Category::where('vendorId',$getLoginId['loginId'])->orderby('id','desc')->get();

                        if($listCategory == null || $listCategory == "")
                        {
                            $returnValues = new ReturnController("3002","FAILURE","");
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                        else
                        {
                            $data = [
                                "lastPage" => "NULL",
                                "data" => $listCategory];

                            $returnValues = new ReturnController("3000","SUCCESS",$data);
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                    }
                    else
                    {
                        $listCategory = Category::where('vendorId',$getLoginId['loginId'])->orderby('id','desc')->paginate($paginationCount);

                        if($listCategory == null || $listCategory == "")
                        {
                            $returnValues = new ReturnController("3002","FAILURE","");
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                        else
                        {
                            $data=[
                                "total" => $listCategory->total(),
                                "nextPageUrl" => $listCategory->nextPageUrl(),
                                "previousPageUrl" => $listCategory->previousPageUrl(),
                                "currentPage" => $listCategory->currentPage(),
                                "lastPage" => $listCategory->lastPage(),
                                "data" => $listCategory
                            ];

                            $returnValues = new ReturnController("3000","SUCCESS",$data);
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                    }

                }
                else
                {
                    switch($authenticateUser)
                    {
                        case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                            $return = $returnValues->returnValues();
                            return $return;
                            break;
                        case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                            $return = $returnValues->returnValues();
                            return $return;
                            break;
                        case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                            $return = $returnValues->returnValues();
                            return $return;
                            break;
                        case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                            $return = $returnValues->returnValues();
                            return $return;
                            break;
                    }
                }
            break;
            case "1":$listCategory = Category::where('vendorId',$getLoginId['loginId'])->get();
                if($listCategory == null || $listCategory == "")
                {
                    $returnValues = new ReturnController("3002","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $returnValues = new ReturnController("3000","SUCCESS",$listCategory);
                    $return = $returnValues->returnValues();
                    return $return;
                }
             break;
            case "2":$listCategory = Category::where('vendorId',$request->input('vendorId'))->get();
                if($listCategory == null || $listCategory == "")
                {
                    $returnValues = new ReturnController("3002","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $returnValues = new ReturnController("3000","SUCCESS",$listCategory);
                    $return = $returnValues->returnValues();
                    return $return;
                }
                break;

        }
    }
    public function update (Request $request)
    {
        $authenticate = new AuthUser();
        $authenticateUser  = $authenticate->authenticateUser();
        if($authenticateUser == 400)
        {
            $getLoginId = Login::where('remember_token',$request->input('token'))->first();

            $flagValidateInputs = $this->validateUpdateCategory($request);
            if($flagValidateInputs == false)
            {
                $returnValues = new ReturnController("4001","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $updateCategory = Category::where('vendorId',$getLoginId['loginId'])
                                          ->where('categoryId',$request->input('categoryId'))
                                           ->update(['categoryName' => $request->input('categoryName')]);
                if($updateCategory == 0)
                {
                    $returnValues = new ReturnController("4002","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $returnValues = new ReturnController("4000","SUCCESS","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
            }

        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }
    protected function validateUpdateCategory(Request $request)
    {
        $rules = array(
            'categoryId' => 'required',
            'categoryName' => 'required');

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails())
            return false;
        else
            return true;
    }

    public function deleteCategory (Request $request)
    {
        $authenticate = new AuthUser();
        $authenticateUser  = $authenticate->authenticateUser();
        if($authenticateUser == 400)
        {
            $flagValidateInputs = $this->validateUpdateCategory($request);
            if($flagValidateInputs == false)
            {
                $returnValues = new ReturnController("5001","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $deleteCategory = Category::where('categoryId',$request->input('categoryId'))->delete();
                if($deleteCategory < 0)
                {
                    $returnValues = new ReturnController("5002","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $returnValues = new ReturnController("5000","SUCCESS","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
            }

        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }

}
