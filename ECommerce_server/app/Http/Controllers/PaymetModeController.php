<?php

namespace App\Http\Controllers;

use App\Http\Middleware\AuthUser;
use App\Http\Middleware\Configuration;
use App\Http\Middleware\GenerateUUID;
use App\Login;
use App\PaymentMode;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class PaymetModeController extends Controller
{
    public function add (Request $request)
    {
        $authenticate = new AuthUser();
        $authenticateUser  = $authenticate->authenticateUser();
        if($authenticateUser == 400)
        {
            $getLoginId = Login::where('remember_token',$request->input('token'))->first();

            $flagValidateInputs = $this->validateAddCategory($request);
            if($flagValidateInputs == false)
            {
                $returnValues = new ReturnController("58001","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $checkPaymentName = PaymentMode::where('paymentName',$request->input('paymentName'))->first();
                if($checkPaymentName != null || $checkPaymentName != "")
                {
                    $returnValues = new ReturnController("58002","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $generateUniqueId = new GenerateUUID();
                    $paymentId = $generateUniqueId->getUniqueId();

                    $payment = new PaymentMode();
                    $payment->paymentId = $paymentId;
                    $payment->paymentName = $request->input('paymentName');
                    $payment->paymentDescription = $request->input('paymentDescription');
                    $payment->save();

                    if(!$payment->save())
                    {
                        $returnValues = new ReturnController("58003","FAILURE","");
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                    else
                    {
                        $returnValues = new ReturnController("58000","SUCCESS","");
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                }
            }

        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }
    protected function validateAddCategory(Request $request)
    {
        $rules = array(
            'paymentName' => 'required');

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails())
            return false;
        else
            return true;
    }
    public function listAll (Request $request)
    {
        $getDefaultVariables = new Configuration();
        $paginationCount = $getDefaultVariables->getPaginationcount();


        $filterType = $request->input('filterType');
        switch ($filterType)
        {
            case "0":$authenticate = new AuthUser();
                $authenticateUser  = $authenticate->authenticateUser();
                if($authenticateUser == 400)
                {

                    $listPaymentModeCount = PaymentMode::count();

                    if($listPaymentModeCount <= $paginationCount)
                    {
                        $listPayment = PaymentMode::all();

                        if($listPayment == null || $listPayment == "")
                        {
                            $returnValues = new ReturnController("59002","FAILURE","");
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                        else
                        {
                            $data = [
                                "lastPage" => "NULL",
                                "data" => $listPayment];

                            $returnValues = new ReturnController("59000","SUCCESS",$data);
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                    }
                    else
                    {
                        $listPayment = PaymentMode::paginate($paginationCount);

                        if($listPayment == null || $listPayment == "")
                        {
                            $returnValues = new ReturnController("59002","FAILURE","");
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                        else
                        {
                            $data=[
                                "total" => $listPayment->total(),
                                "nextPageUrl" => $listPayment->nextPageUrl(),
                                "previousPageUrl" => $listPayment->previousPageUrl(),
                                "currentPage" => $listPayment->currentPage(),
                                "lastPage" => $listPayment->lastPage(),
                                "data" => $listPayment
                            ];

                            $returnValues = new ReturnController("59000","SUCCESS",$data);
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                    }

                }
                else
                {
                    switch($authenticateUser)
                    {
                        case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                            $return = $returnValues->returnValues();
                            return $return;
                            break;
                        case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                            $return = $returnValues->returnValues();
                            return $return;
                            break;
                        case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                            $return = $returnValues->returnValues();
                            return $return;
                            break;
                        case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                            $return = $returnValues->returnValues();
                            return $return;
                            break;
                    }
                }
                break;
            case "1":$listPayment = PaymentMode::all();
                if($listPayment == null || $listPayment == "")
                {
                    $returnValues = new ReturnController("59002","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $returnValues = new ReturnController("59000","SUCCESS",$listPayment);
                    $return = $returnValues->returnValues();
                    return $return;
                }
                break;

        }
    }

    public function update (Request $request)
    {
        $authenticate = new AuthUser();
        $authenticateUser  = $authenticate->authenticateUser();
        if($authenticateUser == 400)
        {
            $getLoginId = Login::where('remember_token',$request->input('token'))->first();

            $flagValidateInputs = $this->validateUpdatePayment($request);
            if($flagValidateInputs == false)
            {
                $returnValues = new ReturnController("61001","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $updatePaymentMode = PaymentMode::where('paymentId',$request->input('paymentId'))
                                                ->update(['paymentName' => $request->input('paymentName'),
                            'paymentDescription' => $request->input('paymentDescription')]);
                if($updatePaymentMode == 0)
                {
                    $returnValues = new ReturnController("61002","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $returnValues = new ReturnController("61000","SUCCESS","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
            }

        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }
    protected function validateUpdatePayment(Request $request)
    {
        $rules = array(
            'paymentId' => 'required',
            'paymentName' => 'required');

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails())
            return false;
        else
            return true;
    }

    public function delete (Request $request)
    {
        $authenticate = new AuthUser();
        $authenticateUser  = $authenticate->authenticateUser();
        if($authenticateUser == 400)
        {
            $flagValidateInputs = $this->validateDeletePayment($request);
            if($flagValidateInputs == false)
            {
                $returnValues = new ReturnController("62001","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $deleteCategory = PaymentMode::where('paymentId',$request->input('paymentId'))->delete();
                if($deleteCategory < 0)
                {
                    $returnValues = new ReturnController("62002","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $returnValues = new ReturnController("62000","SUCCESS","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
            }

        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }
    protected function validateDeletePayment(Request $request)
    {
        $rules = array(
            'paymentId' => 'required');

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails())
            return false;
        else
            return true;
    }
}
