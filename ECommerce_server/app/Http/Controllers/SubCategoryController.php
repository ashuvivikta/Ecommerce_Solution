<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Middleware\AuthUser;
use App\Http\Middleware\Configuration;
use App\Http\Middleware\GenerateUUID;
use App\Login;
use App\SubCategory;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
class SubCategoryController extends Controller
{
    public function add (Request $request)
    {
        $authenticate = new AuthUser();
        $authenticateUser  = $authenticate->authenticateUser();
        if($authenticateUser == 400)
        {
            $getLoginId = Login::where('remember_token',$request->input('token'))->first();

            $flagValidateInputs = $this->validateAddSubCategory($request);
            if($flagValidateInputs == false)
            {
                $returnValues = new ReturnController("6001","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $checkSubCategoryName = SubCategory::where('vendorId',$getLoginId['loginId'])
                                                   ->where('subcategoryName',$request->input('subcategoryName'))->first();

                if($checkSubCategoryName != null || $checkSubCategoryName != "")
                {
                    $returnValues = new ReturnController("6002","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $generateUniqueId = new GenerateUUID();
                    $SubCategoryId = $generateUniqueId->getUniqueId();

                    $subcategory = new SubCategory();
                    $subcategory->vendorId = $getLoginId['loginId'];
                    $subcategory->subcategoryId = $SubCategoryId;
                    $subcategory->categoryId = $request->input('categoryId');
                    $subcategory->subcategoryName = $request->input('subcategoryName');
                    $subcategory->save();

                    if(!$subcategory->save())
                    {
                        $returnValues = new ReturnController("6003","FAILURE","");
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                    else
                    {
                        $returnValues = new ReturnController("6000","SUCCESS","");
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                }
            }

        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }

    protected function validateAddSubCategory(Request $request)
    {
        $rules = array(
            'subcategoryName' => 'required',
            'categoryId' => 'required');

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails())
            return false;
        else
            return true;
    }
    public function listAll (Request $request)
    {
        $getDefaultVariables = new Configuration();
        $paginationCount = $getDefaultVariables->getPaginationcount();

        $getLoginId = Login::where('remember_token',$request->input('token'))->first();

        $filterType = $request->input('filterType');
        switch ($filterType)
        {
            case "0":$authenticate = new AuthUser();
                $authenticateUser  = $authenticate->authenticateUser();
                if($authenticateUser == 400)
                {

                    $listSubCategoryCount = SubCategory::where('subcategory_table.vendorId',$getLoginId['loginId'])
                    ->join('category_table','subcategory_table.categoryId','=','category_table.categoryId')->count();

                    if($listSubCategoryCount <= $paginationCount)
                    {
                        $listSubCategory = SubCategory::where('subcategory_table.vendorId',$getLoginId['loginId'])
                        ->join('category_table','subcategory_table.categoryId','=','category_table.categoryId')->get();

                        if($listSubCategory == null || $listSubCategory == "")
                        {
                            $returnValues = new ReturnController("7002","FAILURE","");
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                        else
                        {
                            $data = [
                                "lastPage" => "NULL",
                                "data" => $listSubCategory];

                            $returnValues = new ReturnController("7000","SUCCESS",$data);
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                    }
                    else
                    {
                        $listSubCategory = SubCategory::where('subcategory_table.vendorId',$getLoginId['loginId'])
                        ->join('category_table','subcategory_table.categoryId','=','category_table.categoryId')->paginate($paginationCount);

                        if($listSubCategory == null || $listSubCategory == "")
                        {
                            $returnValues = new ReturnController("7002","FAILURE","");
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                        else
                        {
                            $data=[
                                "total" => $listSubCategory->total(),
                                "nextPageUrl" => $listSubCategory->nextPageUrl(),
                                "previousPageUrl" => $listSubCategory->previousPageUrl(),
                                "currentPage" => $listSubCategory->currentPage(),
                                "lastPage" => $listSubCategory->lastPage(),
                                "data" => $listSubCategory
                            ];

                            $returnValues = new ReturnController("7000","SUCCESS",$data);
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                    }

                }
                else
                {
                    switch($authenticateUser)
                    {
                        case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                            $return = $returnValues->returnValues();
                            return $return;
                            break;
                        case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                            $return = $returnValues->returnValues();
                            return $return;
                            break;
                        case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                            $return = $returnValues->returnValues();
                            return $return;
                            break;
                        case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                            $return = $returnValues->returnValues();
                            return $return;
                            break;
                    }
                }
                break;
            case "1":$listSubCategory = SubCategory::where('vendorId',$getLoginId['loginId'])
                                                    ->join('category_table','subcategory_table.categoryId','=','category_table.categoryId')->get();
                if($listSubCategory == null || $listSubCategory == "")
                {
                    $returnValues = new ReturnController("7002","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $returnValues = new ReturnController("7000","SUCCESS",$listSubCategory);
                    $return = $returnValues->returnValues();
                    return $return;
                }
                break;
            case "2":$listSubCategory = SubCategory::where('vendorId',$request->input('vendorId'))
                                             ->join('category_table','subcategory_table.categoryId','=','category_table.categoryId')->get();
                if($listSubCategory == null || $listSubCategory == "")
                {
                    $returnValues = new ReturnController("7002","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $returnValues = new ReturnController("7000","SUCCESS",$listSubCategory);
                    $return = $returnValues->returnValues();
                    return $return;
                }
                break;

        }
    }
    public function update (Request $request)
    {
        $authenticate = new AuthUser();
        $authenticateUser  = $authenticate->authenticateUser();
        if($authenticateUser == 400)
        {
            $getLoginId = Login::where('remember_token',$request->input('token'))->first();

            $flagValidateInputs = $this->validateUpdateSubCategory($request);
            if($flagValidateInputs == false)
            {
                $returnValues = new ReturnController("8001","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $updateSubCategory = SubCategory::where('vendorId',$getLoginId['loginId'])
                                               ->where('subcategoryId',$request->input('subcategoryId'))
                                            ->update(['subcategoryName' => $request->input('subcategoryName'),
                                                     'categoryId' => $request->input('categoryId')]);
                if($updateSubCategory == 0)
                {
                    $returnValues = new ReturnController("8002","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $returnValues = new ReturnController("8000","SUCCESS","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
            }

        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }
    protected function validateUpdateSubCategory(Request $request)
    {
        $rules = array(
            'subcategoryId' => 'required',
            'categoryId' => 'required',
            'subcategoryName' => 'required');

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails())
            return false;
        else
            return true;
    }

    public function deleteSubCategory (Request $request)
    {
        $authenticate = new AuthUser();
        $authenticateUser  = $authenticate->authenticateUser();
        if($authenticateUser == 400)
        {
            $flagValidateInputs = $this->validateUpdateSubCategory($request);
            if($flagValidateInputs == false)
            {
                $returnValues = new ReturnController("9001","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $deleteSubCategory = Category::where('subcategoryId',$request->input('subcategoryId'))->delete();
                if($deleteSubCategory < 0)
                {
                    $returnValues = new ReturnController("9002","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $returnValues = new ReturnController("9000","SUCCESS","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
            }

        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }
    public function listSubCategoryForCategory(Request $request)
    {
        $categoryArray = array();
        $getCategoryName = Category::where('vendorId',$request->input('vendorId'))->get();

        foreach($getCategoryName as $categoryName)
        {
            $listSubCategory = SubCategory::where('subcategory_table.vendorId',$request->input('vendorId'))
                ->where('subcategory_table.categoryId',$categoryName['categoryId'])
                ->join('category_table','subcategory_table.categoryId','=','category_table.categoryId')->get();
            if($listSubCategory == null || $listSubCategory == "")
            {
                $returnValues = new ReturnController("47002","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $tempCategoryNameArray = [];
                $tempCategoryNameArray['categoryName'] = $categoryName['categoryName'];
                $tempCategoryNameArray['subCategories'] = [];
                foreach ($listSubCategory as $category)
                {
                    $tempArray = [];
                    $tempArray['subcategoryName'] = $category['subcategoryName'];
                    $tempArray['subcategoryId'] = $category['subcategoryId'];
                    array_push($tempCategoryNameArray['subCategories'],$tempArray);

                }
                array_push($categoryArray,$tempCategoryNameArray);
            }
        }
        $returnValues = new ReturnController("47000","SUCCESS",$categoryArray);
        $return = $returnValues->returnValues();
        return $return;

    }
    public function listSubCategoryForCategoryId(Request $request)
    {
        $authenticate = new AuthUser();
        $authenticateUser  = $authenticate->authenticateUser();
        if($authenticateUser == 400)
        {
            $flagValidateInputs = $this->validateSubCategory($request);
            if($flagValidateInputs == false)
            {
                $returnValues = new ReturnController("47001","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $listSubCategory = SubCategory::where('subcategory_table.categoryId',$request->input('categoryId'))
                    ->join('category_table','subcategory_table.categoryId','=','category_table.categoryId')->get();
                if($listSubCategory == null || $listSubCategory == "")
                {
                    $returnValues = new ReturnController("47002","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $returnValues = new ReturnController("47000","SUCCESS",$listSubCategory);
                    $return = $returnValues->returnValues();
                    return $return;

                }
            }
        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }

    }
    protected function validateSubCategory(Request $request)
    {
        $rules = array(
            'categoryId' => 'required');

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails())
            return false;
        else
            return true;
    }
}
