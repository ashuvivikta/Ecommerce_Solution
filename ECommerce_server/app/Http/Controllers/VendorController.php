<?php

namespace App\Http\Controllers;

use App\Http\Middleware\AuthUser;
use App\Http\Middleware\Configuration;
use App\Http\Middleware\GenerateUUID;
use App\Http\Middleware\MailEngine;
use App\Login;
use App\Vendor;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class VendorController extends Controller
{
    public function register(Request $request)
    {
        $flagValidateInputs = $this->validateVendor($request);
        if($flagValidateInputs == false)
        {
            $returnValues = new ReturnController("52001","FAILURE","");
            $return = $returnValues->returnValues();
            return $return;
        }
        else
        {
            $validateEmailid = Vendor::where('vendorEmailId',$request->input('vendorEmailId'))->first();

            if($validateEmailid != null || $validateEmailid != "" || $validateEmailid != 0)
            {
                $returnValues = new ReturnController("52002","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $generateUniqueId = new GenerateUUID();
                $vendorId = $generateUniqueId->getUniqueId();

                $addVendor = new Vendor();
                $addVendor->vendorId = $vendorId;
                $addVendor->vendorName = $request->input('vendorName');
                $addVendor->vendorEmailId = $request->input('vendorEmailId');
                $addVendor->vendorPhoneNumber = $request->input('vendorPhoneNumber');
                $addVendor->vendorAddress = $request->input('vendorAddress');
                $addVendor->vendorDomainName = $request->input('vendorDomainName');
                $addVendor->vendorProfilePicture = "N/A";
                $addVendor->save();

                if (!$addVendor->save())
                {
                    $returnValues = new ReturnController("52003", "FAILURE", "");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $mail = new MailEngine();
                    $mail->vendorRegisterMail($request->input('vendorEmailId'), $vendorId, $request->input('vendorName'));

                    $returnValues = new ReturnController("52000", "SUCCESS", "");
                    $return = $returnValues->returnValues();
                    return $return;
                }
            }
        }

    }
    private function validateVendor(Request $request)
    {
        $rules = array(
            'vendorEmailId'  => 'required',
            'vendorName' => 'required',
            'vendorPhoneNumber'  => 'required',
            'vendorAddress' => 'required');

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails())
            return false;
        else
            return true;
    }
    public function createPassword(Request $request)
    {
        $flagValidate = $this->validateadminId($request);
        if ($flagValidate == false)
        {
            $returnValues = new ReturnController("53001","FAILURE","");
            $return = $returnValues->returnValues();
            return $return;
        }
        else
        {
            $admin = Vendor::where('vendorId', $request->input('vendorId'))->first();
            if ($admin == null || $admin == "")
            {
                $returnValues = new ReturnController("53002","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $checkAdminEmailId = Login::where('loginId',$request->input('vendorId'))->first();

                if($checkAdminEmailId != null || $checkAdminEmailId != "" || count($checkAdminEmailId) > 0)
                {
                    $returnValues = new ReturnController("53003","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $login = new Login();
                    $login->name = $admin['vendorName'];
                    $login->emailId = $admin['vendorEmailId'];
                    $login->password = Hash::make($request->input('password'));
                    $login->typeofuser = 1;
                    $login->loginId = $request->input('vendorId');
                    $login->status = 0;
                    $login->save();

                    if (!$login->save())
                    {
                        $returnValues = new ReturnController("53004", "FAILURE", "");
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                    else
                    {

                        $returnValues = new ReturnController("53000", "SUCCESS", "");
                        $return = $returnValues->returnValues();
                        return $return;

                    }

                }
            }
        }

    }
    protected function validateadminId(Request $request)
    {
        $rules = array(
            'vendorId' => 'required',
            'password' => 'required');

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails())
            return false;
        else
            return true;
    }
    public function listAll(Request $request)
    {
        $authenticate = new AuthUser();
        $authenticateUser = $authenticate->authenticateUser();
        if($authenticateUser == 400)
        {
            $getDefaultVariables = new Configuration();
            $paginationCount = $getDefaultVariables->getPaginationcount();

            $getVendorCount = Login::where('typeofuser',1)
                ->join('vendor_table','login_table.loginId','=','vendor_table.vendorId')
                ->count();

            if($getVendorCount <= $paginationCount)
            {
                $getVendor = Login::where('typeofuser',1)
                    ->join('vendor_table','login_table.loginId','=','vendor_table.vendorId')
                    ->get();

                if($getVendor == null || $getVendor == "")
                {
                    $returnValues = new ReturnController("54002","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $listVendorFound = [];
                    $k = 0;

                    foreach ($getVendor as $vendor)
                    {
                        $tempArray = [];
                        $tempArray['vendorId'] = $vendor['vendorId'];
                        $tempArray['vendorName'] = $vendor['vendorName'];
                        $tempArray['vendorEmailId'] = $vendor['vendorEmailId'];
                        $tempArray['vendorPhoneNumber'] = $vendor['vendorPhoneNumber'];
                        $tempArray['vendorAddress'] = $vendor['vendorAddress'];
                        $tempArray['vendorStatus'] = $vendor['status'];
                        $tempArray['vendorProfilePicture'] = $vendor['vendorProfilePicture'];
                        $listVendorFound[$k] = $tempArray;
                        $k++;

                    }

                    $data = [
                        "lastPage" => "NULL",
                        "data" => $listVendorFound];
                    $returnValues = new ReturnController("54000","SUCCESS",$data);
                    $return = $returnValues->returnValues();
                    return $return;
                }
            }
            else
            {
                $getVendor = Login::where('typeofuser',1)
                    ->join('vendor_table','login_table.loginId','=','vendor_table.vendorId')
                    ->paginate($paginationCount);

                if($getVendor == null || $getVendor == "")
                {
                    $returnValues = new ReturnController("54002","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $listVendorFound = [];
                    $k = 0;

                    foreach ($getVendor as $vendor)
                    {
                        $tempArray = [];
                        $tempArray['vendorId'] = $vendor['vendorId'];
                        $tempArray['vendorName'] = $vendor['vendorName'];
                        $tempArray['vendorEmailId'] = $vendor['vendorEmailId'];
                        $tempArray['vendorPhoneNumber'] = $vendor['vendorPhoneNumber'];
                        $tempArray['vendorAddress'] = $vendor['vendorAddress'];
                        $tempArray['vendorStatus'] = $vendor['status'];
                        $tempArray['vendorProfilePicture'] = $vendor['vendorProfilePicture'];
                        $listVendorFound[$k] = $tempArray;
                        $k++;

                    }

                    $data=[
                        "total" => $getVendor->total(),
                        "nextPageUrl" => $getVendor->nextPageUrl(),
                        "previousPageUrl" => $getVendor->previousPageUrl(),
                        "currentPage" => $getVendor->currentPage(),
                        "lastPage" => $getVendor->lastPage(),
                        "data" => $listVendorFound
                    ];

                    $returnValues = new ReturnController("54000","SUCCESS",$data);
                    $return = $returnValues->returnValues();
                    return $return;
                }
            }

        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }
    public function updateStatus(Request $request)
    {
        $authenticate = new AuthUser();
        $authenticateUser = $authenticate->authenticateUser();
        if($authenticateUser == "400")
        {
            $flagValidateInputs = $this->validateApartmentStatus($request);
            if($flagValidateInputs == false)
            {
                $returnValues = new ReturnController("55001","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $updateStatus = Login::where('loginId', $request->input('vendorId'))
                    ->update(["status" => $request->input('status')]);

                if ($updateStatus == 0)
                {
                    $returnValues = new ReturnController("55002","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $getVendorEmailId = Login::where('loginId',$request->input('vendorId'))->first();

                    if($request->input('status') == 1)
                    {
                        $vendorStatusFlag = $request->input('vendorStatusFlag');

                        if($vendorStatusFlag == 1)
                        {
                            $mail = new MailEngine();
                            $mail->vendorApprovedMail($getVendorEmailId['emailId'],$getVendorEmailId['name']);

                            $returnValues = new ReturnController("55000", "SUCCESS", "");
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                        else
                        {
                            $mail = new MailEngine();
                            $mail->vendorApprovedMail($getVendorEmailId['emailId'],$getVendorEmailId['name']);

                            $returnValues = new ReturnController("55000", "SUCCESS", "");
                            $return = $returnValues->returnValues();
                            return $return;
                        }

                    }
                    elseif($request->input('status') == 2)
                    {
                        $mail = new MailEngine();
                        $mail->vendorRejectedMail($getVendorEmailId['emailId'],$getVendorEmailId['name']);

                        $returnValues = new ReturnController("55000", "SUCCESS", "");
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                    else
                    {
                        $mail = new MailEngine();
                        $mail->vendorDisabledMail($getVendorEmailId['emailId'],$getVendorEmailId['name']);

                        $returnValues = new ReturnController("55000", "SUCCESS", "");
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                }

            }
        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }
    protected function validateApartmentStatus(Request $request)
    {
        $rules = array(
            'vendorId' => 'required',
            'status' => 'required');

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails())
            return false;
        else
            return true;
    }

    public function getVendorId (Request $request)
    {
        $getVendorId = Vendor::where('vendorDomainName',$request->input('vendorDomainName'))->first();
        $vendorId =  $getVendorId['vendorId'];

        $returnValues = new ReturnController("57000", "SUCCESS", $vendorId);
        $return = $returnValues->returnValues();
        return $return;
    }
}
