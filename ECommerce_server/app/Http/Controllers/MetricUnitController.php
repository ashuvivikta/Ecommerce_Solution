<?php

namespace App\Http\Controllers;

use App\Http\Middleware\AuthUser;
use App\Http\Middleware\Configuration;
use App\Http\Middleware\GenerateUUID;
use App\Login;
use App\MetricUnit;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class MetricUnitController extends Controller
{
    public function add (Request $request)
    {
        $authenticate = new AuthUser();
        $authenticateUser  = $authenticate->authenticateUser();
        if($authenticateUser == 400)
        {
            $flagValidateInputs = $this->validateAddCategory($request);
            if($flagValidateInputs == false)
            {
                $returnValues = new ReturnController("63001","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $checkUnitName = MetricUnit::where('unitName',$request->input('unitName'))->first();
                if($checkUnitName != null || $checkUnitName != "")
                {
                    $returnValues = new ReturnController("63002","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $generateUniqueId = new GenerateUUID();
                    $unitId = $generateUniqueId->getUniqueId();

                    $payment = new MetricUnit();
                    $payment->unitId = $unitId;
                    $payment->unitName = $request->input('unitName');
                    $payment->unitDescription = $request->input('unitDescription');
                    $payment->metricUnit = $request->input('metricUnit');
                    $payment->save();

                    if(!$payment->save())
                    {
                        $returnValues = new ReturnController("63003","FAILURE","");
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                    else
                    {
                        $returnValues = new ReturnController("63000","SUCCESS","");
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                }
            }

        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }
    protected function validateAddCategory(Request $request)
    {
        $rules = array(
            'unitName' => 'required');

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails())
            return false;
        else
            return true;
    }
    public function listAll (Request $request)
    {
        $getDefaultVariables = new Configuration();
        $paginationCount = $getDefaultVariables->getPaginationcount();


        $filterType = $request->input('filterType');
        switch ($filterType)
        {
            case "0":$authenticate = new AuthUser();
                $authenticateUser  = $authenticate->authenticateUser();
                if($authenticateUser == 400)
                {

                    $listUnitsCount = MetricUnit::count();

                    if($listUnitsCount <= $paginationCount)
                    {
                        $listUnits = MetricUnit::all();

                        if($listUnits == null || $listUnits == "")
                        {
                            $returnValues = new ReturnController("64002","FAILURE","");
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                        else
                        {
                            $data = [
                                "lastPage" => "NULL",
                                "data" => $listUnits];

                            $returnValues = new ReturnController("64000","SUCCESS",$data);
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                    }
                    else
                    {
                        $listUnits = MetricUnit::paginate($paginationCount);

                        if($listUnits == null || $listUnits == "")
                        {
                            $returnValues = new ReturnController("64002","FAILURE","");
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                        else
                        {
                            $data=[
                                "total" => $listUnits->total(),
                                "nextPageUrl" => $listUnits->nextPageUrl(),
                                "previousPageUrl" => $listUnits->previousPageUrl(),
                                "currentPage" => $listUnits->currentPage(),
                                "lastPage" => $listUnits->lastPage(),
                                "data" => $listUnits
                            ];

                            $returnValues = new ReturnController("64000","SUCCESS",$data);
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                    }

                }
                else
                {
                    switch($authenticateUser)
                    {
                        case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                            $return = $returnValues->returnValues();
                            return $return;
                            break;
                        case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                            $return = $returnValues->returnValues();
                            return $return;
                            break;
                        case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                            $return = $returnValues->returnValues();
                            return $return;
                            break;
                        case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                            $return = $returnValues->returnValues();
                            return $return;
                            break;
                    }
                }
                break;
            case "1":$listUnits = MetricUnit::all();
                if($listUnits == null || $listUnits == "")
                {
                    $returnValues = new ReturnController("64002","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $returnValues = new ReturnController("64000","SUCCESS",$listUnits);
                    $return = $returnValues->returnValues();
                    return $return;
                }
                break;

        }
    }

    public function update (Request $request)
    {
        $authenticate = new AuthUser();
        $authenticateUser  = $authenticate->authenticateUser();
        if($authenticateUser == 400)
        {
            $flagValidateInputs = $this->validateUpdateUnits($request);
            if($flagValidateInputs == false)
            {
                $returnValues = new ReturnController("65001","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $updateUnits = MetricUnit::where('unitId',$request->input('unitId'))
                    ->update(['unitName' => $request->input('unitName'),
                        'unitDescription' => $request->input('unitDescription'),
                        'metricUnit' => $request->input('metricUnit')]);
                if($updateUnits == 0)
                {
                    $returnValues = new ReturnController("65002","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $returnValues = new ReturnController("65000","SUCCESS","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
            }

        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }
    protected function validateUpdateUnits(Request $request)
    {
        $rules = array(
            'unitId' => 'required',
            'unitName' => 'required');

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails())
            return false;
        else
            return true;
    }

    public function delete (Request $request)
    {
        $authenticate = new AuthUser();
        $authenticateUser  = $authenticate->authenticateUser();
        if($authenticateUser == 400)
        {
            $flagValidateInputs = $this->validateDeleteUnits($request);
            if($flagValidateInputs == false)
            {
                $returnValues = new ReturnController("66001","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $deleteUnits = MetricUnit::where('unitId',$request->input('unitId'))->delete();
                if($deleteUnits < 0)
                {
                    $returnValues = new ReturnController("66002","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $returnValues = new ReturnController("66000","SUCCESS","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
            }

        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }
    protected function validateDeleteUnits(Request $request)
    {
        $rules = array(
            'unitId' => 'required');

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails())
            return false;
        else
            return true;
    }
}
