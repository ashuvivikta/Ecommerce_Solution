<?php

namespace App\Http\Controllers;

use App\Complaint;
use App\Http\Middleware\AuthUser;
use App\Http\Middleware\Configuration;
use App\Http\Middleware\GenerateUUID;
use App\Http\Middleware\MailEngine;
use App\Login;
use App\MainPage;
use App\Product;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class ComplaintController extends Controller
{
    public function add (Request $request)
    {
        $authenticate = new AuthUser();
        $authenticateUser  = $authenticate->authenticateUser();
        if($authenticateUser == 400)
        {
            $getLoginId = Login::where('remember_token',$request->input('token'))->first();

            $flagValidateInputs = $this->validateAddComplaint($request);
            if($flagValidateInputs == false)
            {
                $returnValues = new ReturnController("44001","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $generateUniqueId = new GenerateUUID();
                $complaintId = $generateUniqueId->getUniqueId();

                $complaint = new Complaint();
                $complaint->vendorId = $request->input('vendorId');
                $complaint->complaintId = $complaintId;
                $complaint->userId = $getLoginId['loginId'];
                $complaint->cartId = $request->input('cartId');
                $complaint->userComplaint = $request->input('userComplaint');
                $complaint->adminAnswer = "N/A";
                $complaint->save();

                if(!$complaint->save())
                {
                    $returnValues = new ReturnController("44003","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $productId = $request->input('cartId');
                    $userComplaint = $request->input('userComplaint');
                    $userName = $getLoginId['name'];

                    $adminEmailId = Vendor::where('vendorId',$request->input('vendorId'))->first();

                    $getVendorLogo = MainPage::where('vendorId',$request->input('vendorId'))->first();
                    $vendorLogo = $getVendorLogo['vendorLogo'];

                    $mail = new MailEngine();
                    $mail->complaintRecieved($adminEmailId['vendorEmailId'],$productId,$adminEmailId['name'],$complaintId,$userComplaint,$vendorLogo);

                    $mail = new MailEngine();
                    $mail->userComplaint($getLoginId['emailId'],$productId,$userName,$complaintId,$userComplaint,$vendorLogo);

                    $returnValues = new ReturnController("44000","SUCCESS","");
                    $return = $returnValues->returnValues();
                    return $return;
                }

            }

        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }

    protected function validateAddComplaint(Request $request)
    {
        $rules = array(
            'cartId' => 'required');

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails())
            return false;
        else
            return true;
    }

    public function listAll (Request $request)
    {
        $getDefaultVariables = new Configuration();
        $paginationCount = $getDefaultVariables->getPaginationcount();

        $getLoginId = Login::where('remember_token',$request->input('token'))->first();


        $filterType = $request->input('filterType');
        switch ($filterType)
        {
            case "0":$authenticate = new AuthUser();
                $authenticateUser  = $authenticate->authenticateUser();
                if($authenticateUser == 400)
                {
                    $listComplaintCount = Complaint::where('complaint_table.vendorId',$getLoginId['loginId'])
                        ->join('user_table','complaint_table.userId','=','user_table.userId')
                        ->join('cart_table','complaint_table.cartId','=','cart_table.cartId')->count();

                    if($listComplaintCount <= $paginationCount)
                    {
                        $listComplaint = Complaint::where('complaint_table.vendorId',$getLoginId['loginId'])
                            ->join('user_table','complaint_table.userId','=','user_table.userId')
                            ->join('cart_table','complaint_table.cartId','=','cart_table.cartId')->orderby('complaint_table.id','desc')->get();

                        if($listComplaint == null || $listComplaint == "")
                        {
                            $returnValues = new ReturnController("44002","FAILURE","");
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                        else
                        {
                            $listComplaintFound = [];
                            $k = 0;
                            foreach ($listComplaint as $complaint)
                            {
                                $productDetails = $complaint['productDetails'];
                                $decodeProductDetails = json_decode($productDetails,true);
                                $getProductDetails = json_encode($decodeProductDetails['productDetails']);
                                $details = json_decode($getProductDetails);
                                $tempProductIdArray = [];

                                for($i=0;$i<count($details);$i++)
                                {
                                    $getProductDetails = Product::where('productId',$details[$i]->productId)->first();
                                    array_push($tempProductIdArray,$getProductDetails);

                                }
                                $tempComplaintArray = [];
                                $tempComplaintArray['userName'] = $complaint['userName'];
                                $tempComplaintArray['userId'] = $complaint['userId'];
                                $tempComplaintArray['cartId'] = $complaint['cartId'];
                                $tempComplaintArray['complaintId'] = $complaint['complaintId'];
                                $tempComplaintArray['userComplaint'] = $complaint['userComplaint'];
                                $tempComplaintArray['adminAnswer'] = $complaint['adminAnswer'];
                                $tempComplaintArray['productDetails'] = $tempProductIdArray;
                                $listComplaintFound[$k] = $tempComplaintArray;
                                $k++;

                            }
                            $data = [
                                "lastPage" => "NULL",
                                "data" => $listComplaintFound];

                            $returnValues = new ReturnController("44000","SUCCESS",$data);
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                    }
                    else
                    {
                        $listComplaint = Complaint::where('complaint_table.vendorId',$getLoginId['loginId'])
                            ->join('user_table','complaint_table.userId','=','user_table.userId')
                            ->join('cart_table','complaint_table.cartId','=','cart_table.cartId')->orderby('complaint_table.id','desc')->paginate($paginationCount);

                        if($listComplaint == null || $listComplaint == "")
                        {
                            $returnValues = new ReturnController("44002","FAILURE","");
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                        else
                        {
                            $listComplaintFound = [];
                            $k = 0;
                            foreach ($listComplaint as $complaint)
                            {
                                $productDetails = $complaint['productDetails'];
                                $decodeProductDetails = json_decode($productDetails,true);
                                $getProductDetails = json_encode($decodeProductDetails['productDetails']);
                                $details = json_decode($getProductDetails);
                                $tempProductIdArray = [];

                                for($i=0;$i<count($details);$i++)
                                {
                                    $getProductDetails = Product::where('productId',$details[$i]->productId)->first();
                                    array_push($tempProductIdArray,$getProductDetails);

                                }
                                $tempComplaintArray = [];
                                $tempComplaintArray['userName'] = $complaint['userName'];
                                $tempComplaintArray['userId'] = $complaint['userId'];
                                $tempComplaintArray['cartId'] = $complaint['cartId'];
                                $tempComplaintArray['complaintId'] = $complaint['complaintId'];
                                $tempComplaintArray['userComplaint'] = $complaint['userComplaint'];
                                $tempComplaintArray['adminAnswer'] = $complaint['adminAnswer'];
                                $tempComplaintArray['productDetails'] = $tempProductIdArray;
                                $listComplaintFound[$k] = $tempComplaintArray;
                                $k++;

                            }
                            $data=[
                                "total" => $listComplaint->total(),
                                "nextPageUrl" => $listComplaint->nextPageUrl(),
                                "previousPageUrl" => $listComplaint->previousPageUrl(),
                                "currentPage" => $listComplaint->currentPage(),
                                "lastPage" => $listComplaint->lastPage(),
                                "data" => $listComplaintFound
                            ];

                            $returnValues = new ReturnController("44000","SUCCESS",$data);
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                    }

                }
                else
                {
                    switch($authenticateUser)
                    {
                        case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                            $return = $returnValues->returnValues();
                            return $return;
                            break;
                        case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                            $return = $returnValues->returnValues();
                            return $return;
                            break;
                        case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                            $return = $returnValues->returnValues();
                            return $return;
                            break;
                        case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                            $return = $returnValues->returnValues();
                            return $return;
                            break;
                    }
                }
                break;
            case "1":$listComplaint = Complaint::where('complaint_table.vendorId',$getLoginId['loginId'])
                ->join('user_table','complaint_table.userId','=','user_table.userId')
                ->join('cart_table','complaint_table.cartId','=','cart_table.cartId')->get();
                if($listComplaint == null || $listComplaint == "")
                {
                    $returnValues = new ReturnController("44002","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $returnValues = new ReturnController("44000","SUCCESS",$listComplaint);
                    $return = $returnValues->returnValues();
                    return $return;
                }
                break;
            case "2":$listComplaint = Complaint::where('complaint_table.vendorId',$request->input('vendorId'))
                         ->join('user_table','complaint_table.userId','=','user_table.userId')
                       ->join('cart_table','complaint_table.cartId','=','cart_table.cartId')->get();
                if($listComplaint == null || $listComplaint == "")
                {
                    $returnValues = new ReturnController("44002","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $returnValues = new ReturnController("44000","SUCCESS",$listComplaint);
                    $return = $returnValues->returnValues();
                    return $return;
                }
                break;

        }
    }
    public function update (Request $request)
    {
        $authenticate = new AuthUser();
        $authenticateUser  = $authenticate->authenticateUser();
        if($authenticateUser == 400)
        {
            $getLoginId = Login::where('remember_token',$request->input('token'))->first();

            $flagValidateInputs = $this->validateUpdateComplaint($request);
            if($flagValidateInputs == false)
            {
                $returnValues = new ReturnController("45001","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $getComplaintDetails = Complaint::where('complaint_table.complaintId',$request->input('complaintId'))
                                        ->join('user_table','complaint_table.userId','=','user_table.userId')
                                        ->first();

                $updateComplaint = Complaint::where('complaintId',$request->input('complaintId'))
                                        ->update(['adminAnswer' => $request->input('adminAnswer')]);

                if($updateComplaint == 0)
                {
                    $returnValues = new ReturnController("45002","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $productId = $getComplaintDetails['cartId'];
                    $userName = $getComplaintDetails['userName'];
                    $complaintId = $request->input('complaintId');
                    $userComplaint = $getComplaintDetails['userComplaint'];
                    $adminAnswer = $request->input('adminAnswer');

                    $getVendorLogo = MainPage::where('vendorId',$getLoginId['loginId'])->first();
                    $vendorLogo = $getVendorLogo['vendorLogo'];

                    $mail = new MailEngine();
                    $mail->complaintReply($getComplaintDetails['userEmailId'],$productId,$userName,$complaintId,$userComplaint,$adminAnswer,$vendorLogo);

                    $returnValues = new ReturnController("45000","SUCCESS","");
                    $return = $returnValues->returnValues();
                    return $return;
                }

            }

        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }

    protected function validateUpdateComplaint(Request $request)
    {
        $rules = array(
            'complaintId' => 'required');

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails())
            return false;
        else
            return true;
    }
}
