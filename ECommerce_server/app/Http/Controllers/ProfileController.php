<?php

namespace App\Http\Controllers;

use App\EndUser;
use App\Http\Middleware\AuthUser;
use App\Login;
use App\PaymentMode;
use App\UserAddress;
use App\Vendor;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function listAll(Request $request)
    {
        $authenticate=new AuthUser();
        $authenticateUser = $authenticate->authenticateUser();
        if ($authenticateUser == "400")
        {
            $getLoginId = Login::where('remember_token',$request->input('token'))->first();
            $typeofuser = $getLoginId['typeofuser'];
            switch ($typeofuser)
            {
                case "1":$vendorDetails = Vendor::where('vendorId',$getLoginId['loginId'])->first();
                    if($vendorDetails == null || $vendorDetails == "")
                    {
                        $returnValues = new ReturnController("37001","FAILURE","");
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                    else
                    {

                        $tempArray = [];
                        $tempArray['vendorId'] = $vendorDetails['vendorId'];
                        $tempArray['vendorName'] = $vendorDetails['vendorName'];
                        $tempArray['vendorEmailId'] = $vendorDetails['vendorEmailId'];
                        $tempArray['vendorPhoneNumber'] = $vendorDetails['vendorPhoneNumber'];
                        $tempArray['vendorAddress'] = $vendorDetails['vendorAddress'];
                        $tempArray['vendorDomainName'] = $vendorDetails['vendorDomainName'];
                        $tempArray['vendorProfilePicture'] = $vendorDetails['vendorProfilePicture'];
                        $tempArray['vendorConfigurationDetails'] = json_decode($vendorDetails['vendorConfigurationDetails']);

                        $returnValues = new ReturnController("37000","SUCCESS",$tempArray);
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                break;
                case "2":$userDetails = EndUser::where('userId',$getLoginId['loginId'])->first();
                    if($userDetails == null || $userDetails == "")
                    {
                        $returnValues = new ReturnController("37002","FAILURE","");
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                    else
                    {
                        $returnValues = new ReturnController("37000","SUCCESS",$userDetails);
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                break;
                //profile for index page
                case "3":$vendorDetails = Vendor::where('vendorId',$request->input('vendorId'))->first();
                    if($vendorDetails == null || $vendorDetails == "")
                    {
                        $returnValues = new ReturnController("37001","FAILURE","");
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                    else
                    {
                        $returnValues = new ReturnController("37000","SUCCESS",$vendorDetails);
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                    break;
            }
        }
        else
        {
            switch ($authenticateUser)
            {
                case "404":
                    $returnValues = new ReturnController("404", "FAILURE", "INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":
                    $returnValues = new ReturnController("405", "FAILURE", "TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":
                    $returnValues = new ReturnController("406", "FAILURE", "INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":
                    $returnValues = new ReturnController("407", "FAILURE", "TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }

    }
    public function update(Request $request)
    {
        $authenticate=new AuthUser();
        $authenticateUser = $authenticate->authenticateUser();
        if ($authenticateUser == "400")
        {
            $getLoginId = Login::where('remember_token',$request->input('token'))->first();
            $typeofuser = $getLoginId['typeofuser'];
            switch ($typeofuser)
            {
                case "1":$listPaymentName = PaymentMode::where('paymentId',$request->input('paymentId'))->first();
                    $data = [
                    "defaultTaxAmount" => $request->input('defaultTaxAmount'),
                    "defaultShippingAmount" => $request->input('defaultShippingAmount'),
                    "paymentMode" => [
                        "paymentId" => $request->input('paymentId'),
                        "paymentName" => $listPaymentName['paymentName']
                    ],
                ];
                    $updateVendorDetails = Vendor::where('vendorId',$getLoginId['loginId'])
                    ->update(['vendorName'=>$request->input('vendorName'),
                        'vendorPhoneNumber' => $request->input('vendorPhoneNumber'),
                        'vendorAddress' => $request->input('vendorAddress'),
                        'vendorConfigurationDetails' => json_encode($data)]);
                         if($updateVendorDetails == 0)
                         {
                             $returnValues = new ReturnController("38002","FAILURE","");
                             $return = $returnValues->returnValues();
                             return $return;
                         }
                         else
                         {
                             $returnValues = new ReturnController("38000","SUCCESS","");
                             $return = $returnValues->returnValues();
                             return $return;
                         }
                break;
                case "2":$userDetails = EndUser::where('userId',$getLoginId['loginId'])
                    ->update(['userName'=>$request->input('userName'),
                        'userPhoneNumber' => $request->input('userPhoneNumber')]);
                    if($userDetails == 0)
                    {
                        $returnValues = new ReturnController("38002","FAILURE","");
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                    else
                    {
                        $returnValues = new ReturnController("38000","SUCCESS","");
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                break;
            }
        }
        else
        {
            switch ($authenticateUser)
            {
                case "404":
                    $returnValues = new ReturnController("404", "FAILURE", "INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":
                    $returnValues = new ReturnController("405", "FAILURE", "TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":
                    $returnValues = new ReturnController("406", "FAILURE", "INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":
                    $returnValues = new ReturnController("407", "FAILURE", "TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }

    }
    public function updateConfiguration(Request $request)
    {
        $authenticate=new AuthUser();
        $authenticateUser = $authenticate->authenticateUser();
        if ($authenticateUser == "400")
        {
            $getLoginId = Login::where('remember_token',$request->input('token'))->first();

            $checkConfiguration = Vendor::where('vendorId',$getLoginId['loginId'])->first();

            if($checkConfiguration['vendorConfigurationDetails'] != null)
            {
                $returnValues = new ReturnController("60002","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $listPaymentName = PaymentMode::where('paymentId',$request->input('paymentId'))->first();

                $data = [
                    "defaultTaxAmount" => $request->input('defaultTaxAmount'),
                    "defaultShippingAmount" => $request->input('defaultShippingAmount'),
                    "paymentMode" => [
                        "paymentId" => $request->input('paymentId'),
                        "paymentName" => $listPaymentName['paymentName']
                    ],
                ];

                $updateConfiguration = Vendor::where('vendorId',$getLoginId['loginId'])
                    ->update(['vendorConfigurationDetails' => json_encode($data)]);

                if($updateConfiguration == 0)
                {
                    $returnValues = new ReturnController("60003","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $returnValues = new ReturnController("60000","SUCCESS","");
                    $return = $returnValues->returnValues();
                    return $return;
                }

            }

        }
        else
        {
            switch ($authenticateUser)
            {
                case "404":
                    $returnValues = new ReturnController("404", "FAILURE", "INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":
                    $returnValues = new ReturnController("405", "FAILURE", "TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":
                    $returnValues = new ReturnController("406", "FAILURE", "INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":
                    $returnValues = new ReturnController("407", "FAILURE", "TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }

    }
}
