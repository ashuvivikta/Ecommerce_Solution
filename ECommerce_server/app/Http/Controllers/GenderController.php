<?php

namespace App\Http\Controllers;

use App\Category;
use App\Gender;
use App\Http\Middleware\AuthUser;
use App\Http\Middleware\GenerateUUID;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class GenderController extends Controller
{
    public function add (Request $request)
    {
        $authenticate = new AuthUser();
        $authenticateUser  = $authenticate->authenticateUser();
        if($authenticateUser == 400)
        {
            $flagValidateInputs = $this->validateAddGender($request);
            if($flagValidateInputs == false)
            {
                $returnValues = new ReturnController("18001","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $checkGender = Gender::where('gender',$request->input('gender'))->first();
                if($checkGender != null || $checkGender != "")
                {
                    $returnValues = new ReturnController("18002","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $generateUniqueId = new GenerateUUID();
                    $genderId = $generateUniqueId->getUniqueId();

                    $gender = new Gender();
                    $gender->genderId = $genderId;
                    $gender->gender = $request->input('gender');
                    $gender->save();

                    if(!$gender->save())
                    {
                        $returnValues = new ReturnController("18003","FAILURE","");
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                    else
                    {
                        $returnValues = new ReturnController("18000","SUCCESS","");
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                }
            }

        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }

    protected function validateAddGender(Request $request)
    {
        $rules = array(
            'gender' => 'required');

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails())
            return false;
        else
            return true;
    }
    public function listAll (Request $request)
    {
        $lisGender = Gender::all();
        if($lisGender == null || $lisGender == "")
        {
            $returnValues = new ReturnController("19001","FAILURE","");
            $return = $returnValues->returnValues();
            return $return;
        }
        else
        {
            $returnValues = new ReturnController("19000","SUCCESS",$lisGender);
            $return = $returnValues->returnValues();
            return $return;
        }
    }
    public function update (Request $request)
    {
        $authenticate = new AuthUser();
        $authenticateUser  = $authenticate->authenticateUser();
        if($authenticateUser == 400)
        {
            $flagValidateInputs = $this->validateUpdateGender($request);
            if($flagValidateInputs == false)
            {
                $returnValues = new ReturnController("20001","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $updateGender = Gender::where('genderId',$request->input('genderId'))
                                      ->update(['gender' => $request->input('gender')]);
                if($updateGender == 0)
                {
                    $returnValues = new ReturnController("20002","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $returnValues = new ReturnController("20000","SUCCESS","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
            }

        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }
    protected function validateUpdateGender(Request $request)
    {
        $rules = array(
            'genderId' => 'required');

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails())
            return false;
        else
            return true;
    }

    public function delete(Request $request)
    {
        $authenticate = new AuthUser();
        $authenticateUser  = $authenticate->authenticateUser();
        if($authenticateUser == 400)
        {
            $flagValidateInputs = $this->validateUpdateGender($request);
            if($flagValidateInputs == false)
            {
                $returnValues = new ReturnController("21001","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $deleteGender = Gender::where('genderId',$request->input('genderId'))->delete();
                if($deleteGender < 0)
                {
                    $returnValues = new ReturnController("21002","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $returnValues = new ReturnController("21000","SUCCESS","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
            }

        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }
}
