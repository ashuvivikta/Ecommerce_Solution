<?php

namespace App\Http\Controllers;
use App\Cart;
use App\Http\Middleware\AuthUser;
use App\Http\Middleware\Configuration;
use App\Http\Middleware\GenerateUUID;
use App\Http\Middleware\MailEngine;
use App\Login;
use App\MainPage;
use App\Product;
use App\Vendor;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class CartController extends Controller
{
    public function add (Request $request)
    {
        $authenticate = new AuthUser();
        $authenticateUser  = $authenticate->authenticateUser();
        if($authenticateUser == 400)
        {
            $getLoginId = Login::where('remember_token',$request->input('token'))->first();

            $flagValidateInputs = $this->validateAddCart($request);
            if($flagValidateInputs == false)
            {
                $returnValues = new ReturnController("26001","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $checkUserId = Cart::where('vendorId',$request->input('vendorId'))
                                    ->where('userId',$getLoginId['loginId'])
                                    ->where('productStatus',0)
                                    ->first();

                if($checkUserId != null || $checkUserId != "" || count($checkUserId) > 0)
                {
                    $updateCart = Cart::where('vendorId',$request->input('vendorId'))
                    ->where('userId',$getLoginId['loginId'])
                        ->where('productStatus',0)
                        ->update(['productDetails'=> $request->input('cartDetails')]) ;

                    if($updateCart == 0)
                    {
                        $returnValues = new ReturnController("26003","FAILURE","");
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                    else
                    {
                        $returnValues = new ReturnController("26000","SUCCESS",$checkUserId['cartId']);
                        $return = $returnValues->returnValues();
                        return $return;
                    }

                }
                else
                {
                    $generateUniqueId = new GenerateUUID();
                    $cartId = $generateUniqueId->getUniqueId();

                    $cart = new Cart();
                    $cart->vendorId = $request->input('vendorId');
                    $cart->userId = $getLoginId['loginId'];
                    $cart->cartId = $cartId;
                    $cart->addressId = "N/A";
                    $cart->productDetails = $request->input('cartDetails');
                    $cart->productStatus = 0;
                    $cart->paymentStatus = "N/A";
                    $cart->save();

                    if(!$cart->save())
                    {
                        $returnValues = new ReturnController("26004","FAILURE","");
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                    else
                    {
                        $returnValues = new ReturnController("26000","SUCCESS",$cartId);
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                }
            }

        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }

    protected function validateAddCart(Request $request)
    {
        $rules = array(
            'cartDetails' => 'required',
            'vendorId' => 'required');

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails())
            return false;
        else
            return true;
    }
    public function listCartItemDetails(Request $request)
    {
        $getProductId = json_decode($request->input('cartItems'));
        $this->totalAmount = 0;
        $productIdCount = 0;
        $listCartItemsFound = [];
        $k =0;
        foreach ($getProductId->productDetails as $details)
        {
            $productIdCount++;
            $productId = $details->productId;
            $quantity = $details->quantity;
            $listProducts = Product::where('productId',$productId)
                                    ->join('product_color_table','product_table.productColorId','=','product_color_table.productColorId')
                                    ->get();

            foreach ($listProducts as $product)
            {
                $tempArray = [];
                $tempArray['productName'] = $product['productName'];
                $tempArray['productModel'] = $product['productModel'];
                $tempArray['productColor'] = $product['productColor'];
                $tempArray['productId'] = $product['productId'];
                $tempArray['quantity'] = $quantity;
                $tempArray['productPrice'] = $product['productPrice'];
                $tempArray['productTaxAmount'] = $product['productTaxAmount'];
                $tempArray['productShippingAmount'] = $product['productShippingAmount'];
                $productTotalPrice = $product['productPrice']+$product['productTaxAmount']+ $product['productShippingAmount'];
                $tempArray['productTotalPrice'] = ($productTotalPrice * $quantity);
                $this->totalAmount += ($productTotalPrice * $quantity);
                $tempArray['productImages'] = [];
                $splitOptions = explode(';',$product->productImage);

                for($i=1;$i<count($splitOptions);$i++)
                {
                    if($splitOptions[$i] == "" || $splitOptions[$i] == null)
                        continue;
                    else
                    {
                        $tempProductImage = [];
                        if($i == 1)
                        {
                            $tempProductImage['thumbnail'] = $splitOptions[$i];
                        }
                        array_push($tempArray['productImages'],$tempProductImage);
                        break;
                    }
                }
                $listCartItemsFound[$k] = $tempArray;
                $k++;
            }
        }
        $data = [
            "cartItemDetails" => $listCartItemsFound,
            "totalAmount" => $this->totalAmount
        ];
        $returnValues = new ReturnController("30000","SUCCESS",$data);
        $return = $returnValues->returnValues();
        return $return;
    }
    public function buy (Request $request)
    {
        $authenticate = new AuthUser();
        $authenticateUser  = $authenticate->authenticateUser();
        if($authenticateUser == 400)
        {
            $getLoginId = Login::where('remember_token',$request->input('token'))->first();

            $flagValidateInputs = $this->validateBuy($request);
            if($flagValidateInputs == false)
            {
                $returnValues = new ReturnController("34001","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $getProductId = Cart::where('cart_table.vendorId',$request->input('vendorId'))
                    ->where('cart_table.cartId',$request->input('cartId'))
                    ->where('cart_table.userId',$getLoginId['loginId'])
                    ->where('cart_table.productStatus','=',0)
                    ->first();
                Log::info("product".$getProductId);

                if($getProductId == null || $getProductId == "" || count($getProductId) < 0)
                {
                    $returnValues = new ReturnController("34002","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;

                }
                else
                {
                    $productId = '';
                    $quantity = '';
                    $productDetails = json_decode($getProductId['productDetails']);
                    foreach ($productDetails->productDetails as $details)
                    {
                        $productId = $details->productId;
                        $quantity = $details->quantity;
                        Log::info("ProductId ".$productId);

                        $getProductCount = Product::where('vendorId',$request->input('vendorId'))
                            ->where('productId',$productId)->first();

                        $oldCount = $getProductCount['productCount'];
                        $updateCount = $oldCount-$quantity;

                        $updateProductCount = Product::where('vendorId',$request->input('vendorId'))
                            ->where('productId',$productId)
                            ->update(['productCount' => $updateCount]);
                    }
                    $currentDate = Carbon::now();
                    $currentDate = date_create($currentDate);
                    $currentDate = $currentDate->format('Y-m-d H:i:s');

                    $updateCart = Cart::where('cart_table.vendorId',$request->input('vendorId'))
                        ->where('cart_table.cartId',$request->input('cartId'))
                        ->where('userId',$getLoginId['loginId'])
                        ->where('productStatus',0)
                        ->update(['productStatus' => 1,
                            'orderRecievedDate' => $currentDate,
                            'addressId' => $request->input('addressId')]);

                    if($updateCart == 0)
                    {
                        $returnValues = new ReturnController("34003","FAILURE","");
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                    else
                    {
                        $productId = $getProductId['cartId'];
                        $userName = $getLoginId['name'];
                        $orderDate = $getProductId['orderRecievedDate'];
                        $userAddress = $getProductId['userAddress'];
                        $status = "Order Recieved";

                        $adminEmailId = Vendor::where('vendorId',$request->input('vendorId'))->first();

                        $getVendorLogo = MainPage::where('vendorId',$request->input('vendorId'))->first();
                        $vendorLogo = $getVendorLogo['vendorLogo'];

                        $getVendorDomainName = Vendor::where('vendorId',$request->input('vendorId'))->first();
                        $vendorDomainName = $getVendorDomainName['vendorDomainName'];

                        $mail = new MailEngine();
                        $mail->orderRecieved($adminEmailId['vendorEmailId'],$productId,$userName,$currentDate,$userAddress,$status,$vendorLogo);

                        $mail = new MailEngine();
                        $mail->userBought($getLoginId['emailId'],$productId,$userName,$currentDate,$userAddress,$status,$vendorLogo,$vendorDomainName);

                        $returnValues = new ReturnController("34000","SUCCESS","");
                        $return = $returnValues->returnValues();
                        return $return;
                    }

                }
            }

        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }
    protected function validateBuy(Request $request)
    {
        $rules = array('addressId' => 'required');

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails())
            return false;
        else
            return true;
    }
    public function updateOrderDetails (Request $request)
    {
        $authenticate = new AuthUser();
        $authenticateUser  = $authenticate->authenticateUser();
        if($authenticateUser == 400)
        {
            $getLoginId = Login::where('remember_token',$request->input('token'))->first();

            $getVendorDomainName = Vendor::where('vendorId',$request->input('vendorId'))->first();
            $vendorDomainName = $getVendorDomainName['vendorDomainName'];

            $getVendorLogo = MainPage::where('vendorId',$getLoginId['loginId'])->first();
            $vendorLogo = $getVendorLogo['vendorLogo'];

            $flagValidateInputs = $this->validateUpdateOrderDetails($request);
            if($flagValidateInputs == false)
            {
                $returnValues = new ReturnController("48001","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $productStatus = $request->input('productStatus');
                switch($productStatus)
                {
                    case "2": $currentDate = Carbon::now();
                        $currentDate = date_create($currentDate);
                        $currentDate = $currentDate->format('Y-m-d H:i:s');

                        $getProductId = Cart::where('cart_table.cartId',$request->input('cartId'))
                            ->join('user_address_table','cart_table.addressId','=','user_address_table.addressId')
                            ->join('user_table','cart_table.userId','=','user_table.userId')
                            ->first();

                        $updateCart = Cart::where('cartId',$request->input('cartId'))
                            ->update(['productStatus' => 2,
                                'paymentRecievedDate' => $currentDate]);

                        if($updateCart == 0)
                        {
                            $returnValues = new ReturnController("48002","FAILURE","");
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                        else
                        {
                            Log::info($getProductId['userName']);
                            $productId = $getProductId['cartId'];
                            $userName = $getProductId['userName'];
                            $orderDate = $currentDate;
                            $userAddress = $getProductId['userAddress'];
                            $status = "Payment Recieved";

                            $mail = new MailEngine();
                            $mail->paymentRecieved($getProductId['userEmailId'],$productId,$userName,$orderDate,$userAddress,$status,$vendorLogo,$vendorDomainName);

                            $returnValues = new ReturnController("48000","SUCCESS","");
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                    break;
                    case "3": $currentDate = Carbon::now();
                        $currentDate = date_create($currentDate);
                        $currentDate = $currentDate->format('Y-m-d H:i:s');

                        $getProductId = Cart::where('cart_table.cartId',$request->input('cartId'))
                            ->join('user_address_table','cart_table.addressId','=','user_address_table.addressId')
                            ->join('user_table','cart_table.userId','=','user_table.userId')
                            ->first();

                        $updateCart = Cart::where('cartId',$request->input('cartId'))
                            ->update(['productStatus' => 3,
                                'shippingInProgressDate' => $currentDate]);

                        if($updateCart == 0)
                        {
                            $returnValues = new ReturnController("48002","FAILURE","");
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                        else
                        {
                            $productId = $getProductId['cartId'];
                            $userName = $getProductId['userName'];
                            $orderDate = $currentDate;
                            $userAddress = $getProductId['userAddress'];
                            $status = "Shipping In Progress";

                            $mail = new MailEngine();
                            $mail->shippingInProgress($getProductId['userEmailId'],$productId,$userName,$orderDate,$userAddress,$status,$vendorLogo,$vendorDomainName);

                            $returnValues = new ReturnController("48000","SUCCESS","");
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                    break;
                    case "4": $currentDate = Carbon::now();
                        $currentDate = date_create($currentDate);
                        $currentDate = $currentDate->format('Y-m-d H:i:s');

                        $getProductId = Cart::where('cart_table.cartId',$request->input('cartId'))
                            ->join('user_address_table','cart_table.addressId','=','user_address_table.addressId')
                            ->join('user_table','cart_table.userId','=','user_table.userId')
                            ->first();

                        $updateCart = Cart::where('cartId',$request->input('cartId'))
                            ->update(['productStatus' => 4,
                                'shippedDate' => $currentDate]);

                        if($updateCart == 0)
                        {
                            $returnValues = new ReturnController("48002","FAILURE","");
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                        else
                        {

                            $productId = $getProductId['cartId'];
                            $userName = $getProductId['userName'];
                            $orderDate = $currentDate;
                            $userAddress = $getProductId['userAddress'];
                            $status = "Order Shipped";


                            $mail = new MailEngine();
                            $mail->orderShipped($getProductId['userEmailId'],$productId,$userName,$orderDate,$userAddress,$status,$vendorLogo,$vendorDomainName);

                            $returnValues = new ReturnController("48000","SUCCESS","");
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                    break;
                    case "5": $currentDate = Carbon::now();
                        $currentDate = date_create($currentDate);
                        $currentDate = $currentDate->format('Y-m-d H:i:s');

                        $getProductId = Cart::where('cart_table.cartId',$request->input('cartId'))
                            ->join('user_address_table','cart_table.addressId','=','user_address_table.addressId')
                            ->join('user_table','cart_table.userId','=','user_table.userId')
                            ->first();

                        $updateCart = Cart::where('cartId',$request->input('cartId'))
                            ->update(['productStatus' => 5,
                                'orderRejectedDate' => $currentDate]);

                        if($updateCart == 0)
                        {
                            $returnValues = new ReturnController("48002","FAILURE","");
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                        else
                        {
                            $productId = $getProductId['cartId'];
                            $userName = $getProductId['userName'];
                            $orderDate = $currentDate;
                            $userAddress = $getProductId['userAddress'];
                            $status = "Order Rejected";


                            $mail = new MailEngine();
                            $mail->orderRejected($getProductId['userEmailId'],$productId,$userName,$orderDate,$userAddress,$status,$vendorLogo,$vendorDomainName);

                            $returnValues = new ReturnController("48000","SUCCESS","");
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                        break;
                }
            }

        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }
    protected function validateUpdateOrderDetails(Request $request)
    {
        $rules = array('cartId' => 'required');

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails())
            return false;
        else
            return true;
    }

}
