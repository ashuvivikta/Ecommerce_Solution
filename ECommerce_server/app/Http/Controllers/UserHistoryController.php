<?php
/*
namespace App\Http\Controllers;

use App\Product;
use App\UserHistory;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Log;

class UserHistoryController extends Controller
{
    public function CountProductForId(Request $request)
    {

        $listProducts = UserHistory::where('user_history.vendorId', $request->input('vendorId'))
            ->where('user_history.productId', $request->input('productId'))
            ->join('product_color_table', 'user_history.productColorId', '=', 'product_color_table.productColorId')
            ->join('category_table', 'user_history.categoryId', '=', 'category_table.categoryId')
            ->join('subcategory_table', 'user_history.subcategoryId', '=', 'subcategory_table.subcategoryId')->get();
        Log::info($listProducts);
        if ($listProducts == null || $listProducts == "")
        {
            $returnValues = new ReturnController("23001", "FAILURE", "");
            $return = $returnValues->returnValues();
            return $return;
        }
        else
        {
            $listProductFound = [];
            $k = 0;
            foreach ($listProducts as $products)
            {
                $tempArray = [];
                $tempArray['productName'] = $products->productName;
                $tempArray['productPrice'] = $products->productPrice;
                $tempArray['productModel'] = $products->productModel;
                $tempArray['productId'] = $products->productId;
                $tempArray['productColor'] = $products->productColor;
                $tempArray['categoryName'] = $products->categoryName;
                $tempArray['subcategoryName'] = $products->subcategoryName;
                $tempArray['productImages'] = [];
                $tempArray['similarProducts'] = [];
                $splitOptions = explode(';', $products->productImage);

                for ($i = 1; $i < count($splitOptions); $i++)
                {
                    if ($splitOptions[$i] == "" || $splitOptions[$i] == null)
                        continue;
                    else
                    {
                        $tempProductImage = [];
                        if ($i == 1)
                        {
                            $tempProductImage['thumbnail'] = $splitOptions[$i];
                        }
                        $tempProductImage['productImage'] = $splitOptions[$i];
                        array_push($tempArray['productImages'], $tempProductImage);
                    }
                }

                /*$getProductsForModelId = Product::where('product_table.productModel', $products->productModel)
                    ->where('product_table.productId', '!=', $products->productId)
                    ->where('product_table.subcategoryId', '=', $products->subcategoryId)
                    ->join('product_color_table', 'product_table.productColorId', '=', 'product_color_table.productColorId')
                    ->join('category_table', 'product_table.categoryId', '=', 'category_table.categoryId')
                    ->join('subcategory_table', 'product_table.subcategoryId', '=', 'subcategory_table.subcategoryId')->get();

                foreach ($getProductsForModelId as $modelId)
                {
                    $tempSimilarProductArray = [];
                    $tempSimilarProductArray['productName'] = $modelId->productName;
                    $tempSimilarProductArray['productDescription'] = $modelId->productDescription;
                    $tempSimilarProductArray['productPrice'] = $modelId->productPrice;
                    $tempSimilarProductArray['productModel'] = $modelId->productModel;
                    $tempSimilarProductArray['productId'] = $modelId->productId;
                    $tempSimilarProductArray['productColor'] = $modelId->productColor;
                    $tempSimilarProductArray['categoryName'] = $modelId->categoryName;
                    $tempSimilarProductArray['subcategoryName'] = $modelId->subcategoryName;
                    $tempSimilarProductArray['productImages'] = [];
                    $splitOptions = explode(';', $modelId->productImage);

                    for ($i = 1; $i < count($splitOptions); $i++)
                    {
                        if ($splitOptions[$i] == "" || $splitOptions[$i] == null)
                            continue;
                        else
                        {
                            $tempProductImage = [];
                            if ($i == 1)
                            {
                                $tempProductImage['thumbnail'] = $splitOptions[$i];
                            }
                            array_push($tempSimilarProductArray['productImages'], $tempProductImage);
                        }
                    }
                    array_push($tempArray['similarProducts'], $tempSimilarProductArray);
                }*/
               /* $listProductFound[$k] = $tempArray;
                $k++;

            }

            $returnValues = new ReturnController("23000", "SUCCESS", $listProductFound);

            UserHistory::where('user_history.vendorId', $request->input('vendorId'))->increment('count');

            $return = $returnValues->returnValues();
            return $return;
        }
    }
}*/
