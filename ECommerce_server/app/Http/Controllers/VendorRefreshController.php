<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Category;
use App\Complaint;
use App\Gender;
use App\Login;
use App\Product;
use App\ProductColor;
use App\SubCategory;
use App\Suggestion;
use App\Vendor;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Log;

class VendorRefreshController extends Controller
{
    public function clearVendorDetails(Request $request)
    {
        $getVendorId = Vendor::where('vendorId',"0D5D1FF9")->first();

        $deleteCart = Cart::where('vendorId',$getVendorId['vendorId'])->delete();
        $deleteCategory = Category::where('vendorId',$getVendorId['vendorId'])->delete();
        $deleteLogin = Login::where('loginId',$getVendorId['vendorId'])->delete();
        $deleteComplaint = Complaint::where('vendorId',$getVendorId['vendorId'])->delete();
        $deleteGender = Gender::where('vendorId',$getVendorId['vendorId'])->delete();
        $deleteProductColor = ProductColor::where('vendorId',$getVendorId['vendorId'])->delete();
        $deleteProduct = Product::where('vendorId',$getVendorId['vendorId'])->delete();
        $deleteSubCategory = SubCategory::where('vendorId',$getVendorId['vendorId'])->delete();
        $deleteZSuggestion = Suggestion::where('vendorId',$getVendorId['vendorId'])->delete();
        $deleteVendor = Vendor::where('vendorId',$getVendorId['vendorId'])->delete();

        $returnValues = new ReturnController("52000","SUCCESS","");
        $return = $returnValues->returnValues();
        return $return;
    }
}
