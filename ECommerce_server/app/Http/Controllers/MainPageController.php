<?php

namespace App\Http\Controllers;
use App\Http\Middleware\AuthUser;
use App\Http\Middleware\Configuration;
use App\Http\Middleware\GenerateUUID;
use App\Login;
use App\MainPage;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class MainPageController extends Controller
{
    public function add (Request $request)
    {
        $getDefaultVariables = new Configuration();
        $PROJECT_PATH = $getDefaultVariables->getDefaultProjectPath();

        $authenticate = new AuthUser();
        $authenticateUser  = $authenticate->authenticateUser();
        if($authenticateUser == 400)
        {
            $getLoginId = Login::where('remember_token',$request->input('token'))->first();
            $vendorId = $getLoginId['loginId'];

            $flagValidateInputs = $this->validateAddMainImage($request);
            if($flagValidateInputs == false)
            {
                $returnValues = new ReturnController("25001","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $path = public_path();
                $vendorLogo = '';
                $generateUniqueId = new GenerateUUID();
                $mainPageId = $generateUniqueId->getUniqueId();


                $imageArray = Input::file('sliderImage');
                $nameArray = Input::get('sliderImageHeading');
                $productUrlArray = Input::get('productUrl');
                $descriptionArray = Input::get('sliderImageDescription');
                $sliderImageArray = [];
                $k = 0;

                for($i=0;$i<count($imageArray);$i++)
                {
                    $fileExtension = pathinfo($imageArray[$i]->getClientOriginalName(),PATHINFO_EXTENSION);

                    $sliderImage = "$PROJECT_PATH/$vendorId/sliderImageDirectory/images/".$mainPageId."_$i".".".$fileExtension;

                    Storage::disk('s3')->put($sliderImage, file_get_contents($imageArray[$i]));
                    Storage::disk('s3')->setVisibility($sliderImage, 'public');

                    $sliderImagePath = Storage::disk('s3')->url($sliderImage);

                    $tempArray = [];
                    $tempArray['sliderImageHeading'] = $nameArray[$i];
                    $tempArray['sliderImageDescription'] = $descriptionArray[$i];
                    $tempArray['productUrl'] = $productUrlArray[$i];
                    $tempArray['sliderImagePath'] = $sliderImagePath;
                    $sliderImageArray[$k] = $tempArray;
                    $k++;
                }
                if(Input::file('vendorLogo') != "" || Input::file('vendorLogo') != null)
                {
                    $vendorLogoFile = Input::file('vendorLogo');
                    $fileExtension = pathinfo($vendorLogoFile->getClientOriginalName(),PATHINFO_EXTENSION);

                    $vendorLogoPath = "$PROJECT_PATH/$vendorId/sliderImageDirectory/logo/".$mainPageId.".".$fileExtension;

                    Storage::disk('s3')->put($vendorLogoPath, file_get_contents($vendorLogoFile));
                    Storage::disk('s3')->setVisibility($vendorLogoPath, 'public');

                    $vendorLogo = Storage::disk('s3')->url($vendorLogoPath);
                }
                $product = new MainPage();
                $product->vendorId = $vendorId;
                $product->mainPageId = $mainPageId;
                $product->sliderImageDetails = json_encode($sliderImageArray);
                $product->facebookLink = $request->input('facebookLink');
                $product->twitterLink = $request->input('twitterLink');
                $product->googlePlusLink = $request->input('googlePlusLink');
                $product->instagramLink = $request->input('instagramLink');
                $product->vendorLogo = $vendorLogo;
                $product->save();

                if(!$product->save())
                {
                    $returnValues = new ReturnController("25002","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $returnValues = new ReturnController("25000","SUCCESS","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
            }

        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }

    protected function validateAddMainImage(Request $request)
    {
        $rules = array(
            'sliderImageHeading' => 'required');

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails())
            return false;
        else
            return true;
    }

    public function listAll (Request $request)
    {
        $getLoginId = Login::where('remember_token',$request->input('token'))->first();
        $filterType = $request->input('filterType');
        switch ($filterType)
        {
            case "0":$listMainImage = MainPage::where('vendorId',$getLoginId['loginId'])->get();
                if($listMainImage == null || $listMainImage == "")
                {
                    $returnValues = new ReturnController("28001","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $listImageFound = [];
                    $k =0;
                    foreach ($listMainImage as $image)
                    {
                        $tempArray =[];
                        $tempArray['vendorId'] = $image->vendorId;
                        $tempArray['mainPageId'] = $image->mainPageId;
                        $tempArray['facebookLink'] = $image->facebookLink;
                        $tempArray['twitterLink'] = $image->twitterLink;
                        $tempArray['googlePlusLink'] = $image->googlePlusLink;
                        $tempArray['instagramLink'] = $image->instagramLink;
                        $tempArray['vendorLogo'] = $image->vendorLogo;
                        $tempArray['sliderImageDetails'] = json_decode($image['sliderImageDetails']);
                        $listImageFound[$k] = $tempArray;
                        $k++;
                    }
                    $returnValues = new ReturnController("24000","SUCCESS",$listImageFound);
                    $return = $returnValues->returnValues();
                    return $return;
                }
            break;
            case "1":$listMainImage = MainPage::where('main_page_table.vendorId',$request->input('vendorId'))
                ->join('vendor_table','main_page_table.vendorId','=','vendor_table.vendorId')
                ->get();
                if($listMainImage == null || $listMainImage == "")
                {
                    $returnValues = new ReturnController("28001","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $listImageFound = [];
                    $k =0;
                    foreach ($listMainImage as $image)
                    {

                        $tempArray =[];
                        $tempArray['vendorId'] = $image->vendorId;
                        $tempArray['mainPageId'] = $image->mainPageId;
                        $tempArray['facebookLink'] = $image->facebookLink;
                        $tempArray['twitterLink'] = $image->twitterLink;
                        $tempArray['googlePlusLink'] = $image->googlePlusLink;
                        $tempArray['instagramLink'] = $image->instagramLink;
                        $tempArray['vendorLogo'] = $image->vendorLogo;
                        $tempArray['vendorName'] = $image['vendorName'];
                        $tempArray['vendorEmailId'] = $image['vendorEmailId'];
                        $tempArray['vendorPhoneNumber'] = $image['vendorPhoneNumber'];
                        $tempArray['vendorAddress'] = $image['vendorAddress'];
                        $tempArray['vendorLogo'] = $image->vendorLogo;
                        $tempArray['sliderImageDetails'] = json_decode($image['sliderImageDetails']);
                        $listImageFound[$k] = $tempArray;
                        $k++;
                    }
                    $returnValues = new ReturnController("24000","SUCCESS",$listImageFound);
                    $return = $returnValues->returnValues();
                    return $return;
                }
            break;
        }
    }
    public function update (Request $request)
    {
        $getDefaultVariables = new Configuration();
        $PROJECT_PATH = $getDefaultVariables->getDefaultProjectPath();

        $authenticate = new AuthUser();
        $authenticateUser  = $authenticate->authenticateUser();
        if($authenticateUser == 400)
        {
            $getLoginId = Login::where('remember_token',$request->input('token'))->first();
            $vendorId = $getLoginId['loginId'];

            $flagValidateInputs = $this->validateUpdateMainImage($request);
            if($flagValidateInputs == false)
            {
                $returnValues = new ReturnController("29001","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $path = public_path();
                $vendorLogo = '';
                $mainPageId = $request->input('mainPageId');

                $imageArray = Input::file('sliderImage');
                $nameArray = Input::get('sliderImageHeading');
                $productUrlArray = Input::get('productUrl');
                Log::info("Name Array".count($imageArray));
                $descriptionArray = Input::get('sliderImageDescription');
                $sliderImageArray = [];
                $k = 0;

                for($i=0;$i<count($nameArray);$i++)
                {
                    $fileExtension = pathinfo($imageArray[$i]->getClientOriginalName(),PATHINFO_EXTENSION);

                    $sliderImage = "$PROJECT_PATH/$vendorId/sliderImageDirectory/images/".$mainPageId."_$i".".".$fileExtension;

                    Storage::disk('s3')->put($sliderImage, file_get_contents($imageArray[$i]));
                    Storage::disk('s3')->setVisibility($sliderImage, 'public');

                    $sliderImagePath = Storage::disk('s3')->url($sliderImage);

                    $tempArray = [];
                    $tempArray['sliderImageHeading'] = $nameArray[$i];
                    $tempArray['sliderImageDescription'] = $descriptionArray[$i];
                    $tempArray['productUrl'] = $productUrlArray[$i];
                    $tempArray['sliderImagePath'] = $sliderImagePath;
                    $sliderImageArray[$k] = $tempArray;
                    $k++;
                }
                if(Input::file('vendorLogo') != "" || Input::file('vendorLogo') != null)
                {
                    $vendorLogoFile = Input::file('vendorLogo');
                    $fileExtension = pathinfo($vendorLogoFile->getClientOriginalName(),PATHINFO_EXTENSION);

                    $vendorLogoPath = "$PROJECT_PATH/$vendorId/sliderImageDirectory/logo/".$mainPageId.".".$fileExtension;

                    Storage::disk('s3')->put($vendorLogoPath, file_get_contents($vendorLogoFile));
                    Storage::disk('s3')->setVisibility($vendorLogoPath, 'public');

                    $vendorLogo = Storage::disk('s3')->url($vendorLogoPath);
                }

                $updateMainImage = MainPage::where('vendorId',$vendorId)
                     ->where('mainPageId',$request->input('mainPageId'))
                    ->update(['sliderImageDetails' => json_encode($sliderImageArray),
                            'facebookLink' => $request->input('facebookLink'),
                            'twitterLink' => $request->input('twitterLink'),
                            'googlePlusLink' => $request->input('googlePlusLink'),
                            'instagramLink' => $request->input('instagramLink'),
                             'vendorLogo' => $vendorLogo]);

                if($updateMainImage == 0)
                {
                    $returnValues = new ReturnController("29002","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $returnValues = new ReturnController("29000","SUCCESS","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
            }

        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }
    protected function validateUpdateMainImage(Request $request)
    {
        $rules = array(
            'mainPageId' => 'required');

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails())
            return false;
        else
            return true;
    }
}
