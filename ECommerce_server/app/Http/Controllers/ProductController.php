<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Category;
use App\Http\Middleware\AuthUser;
use App\Http\Middleware\Configuration;
use App\Http\Middleware\GenerateUUID;
use App\Login;
use App\MainPage;
use App\Product;
use App\SubCategory;
use App\UserHistory;
use App\Vendor;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use phpDocumentor\Reflection\Types\Null_;

class ProductController extends Controller
{

    public function add(Request $request)
    {
        $getDefaultVariables = new Configuration();
        $PROJECT_PATH = $getDefaultVariables->getDefaultProjectPath();

        $authenticate = new AuthUser();
        $authenticateUser = $authenticate->authenticateUser();
        if ($authenticateUser == 400)
        {
            $getLoginId = Login::where('remember_token', $request->input('token'))->first();
            $vendorId = $getLoginId['loginId'];

            $flagValidateInputs = $this->validateAddProduct($request);
            if ($flagValidateInputs == false)
            {
                $returnValues = new ReturnController("11001", "FAILURE", "");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $checkProduct = Product::where('vendorId', $getLoginId['loginId'])
                    ->where('productModel', $request->input('productModel'))
                    ->where('productColorId', $request->input('productColorId'))->first();
                if ($checkProduct != null || $checkProduct != "")
                {
                    $returnValues = new ReturnController("11002", "FAILURE", "");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $path = public_path();
                    $productImagePathArray = '';
                    $productImagePath = '';
                    $generateUniqueId = new GenerateUUID();
                    $productId = $generateUniqueId->getUniqueId();

                    if ($request->hasFile('productImage'))
                    {
                        $files = $request->file('productImage');
                        for ($i = 0; $i < count($files); $i++)
                        {
                            $fileExtension = pathinfo($files[$i]->getClientOriginalName(), PATHINFO_EXTENSION);

                            $productImages = "$PROJECT_PATH/$vendorId/productsDirectory/images/" . $productId . "_$i" . "." . $fileExtension;

                            Storage::disk('s3')->put($productImages, file_get_contents($files[$i]));
                            Storage::disk('s3')->setVisibility($productImages, 'public');

                            $productImagePath = Storage::disk('s3')->url($productImages);
                            $productImagePathArray .= ";" . $productImagePath;

                        }
                    }
                    $productTaxAmount = $request->input('productTaxAmount');
                    $productShippingAmount = $request->input('productShippingAmount');
                    if ($productTaxAmount == null)
                    {
                        $vendorList = Vendor::where('vendorId', $getLoginId['loginId'])->first();
                        if ($vendorList['vendorConfigurationDetails'] == null)
                        {
                            $productTaxAmount = 0;
                        }
                        else
                        {
                            $configurationDetails = json_decode($vendorList['vendorConfigurationDetails']);
                            $productTaxAmount = $configurationDetails->defaultTaxAmount;

                        }
                    }
                    if ($productShippingAmount == null)
                    {
                        $vendorList = Vendor::where('vendorId', $getLoginId['loginId'])->first();
                        if ($vendorList['vendorConfigurationDetails'] == null)
                        {
                            $productShippingAmount = 0;
                        }
                        else
                        {
                            $configurationDetails = json_decode($vendorList['vendorConfigurationDetails']);
                            $productShippingAmount = $configurationDetails->defaultShippingAmount;

                        }
                    }

                    $product = new Product();
                    $product->vendorId = $getLoginId['loginId'];
                    $product->productName = $request->input('productName');
                    $product->productDescription = $request->input('productDescription');
                    $product->categoryId = $request->input('categoryId');
                    $product->subcategoryId = $request->input('subcategoryId');
                    $product->productColorId = $request->input('productColorId');
                    $product->productId = $productId;
                    $product->productPrice = $request->input('productPrice');
                    $product->productTaxAmount = $productTaxAmount;
                    $product->productShippingAmount = $productShippingAmount;
                    $product->productSize = $request->input('productSize');
                    $product->productPrice = $request->input('productPrice');
                    $product->productModel = $request->input('productModel');
                    $product->productCount = $request->input('productCount');
                    $product->productImage = $productImagePathArray;
                    $product->save();

                    if (!$product->save())
                    {
                        $returnValues = new ReturnController("11002", "FAILURE", "");
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                    else
                    {
                        $returnValues = new ReturnController("11000", "SUCCESS", "");
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                }

            }

        }
        else
        {
            switch ($authenticateUser)
            {
                case "404":
                    $returnValues = new ReturnController("404", "FAILURE", "INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":
                    $returnValues = new ReturnController("405", "FAILURE", "TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":
                    $returnValues = new ReturnController("406", "FAILURE", "INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":
                    $returnValues = new ReturnController("407", "FAILURE", "TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }

    protected function validateAddProduct(Request $request)
    {
        $rules = array(
            'productName' => 'required',
            'categoryId' => 'required',
            'subcategoryId' => 'required');

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails())
            return false;
        else
            return true;
    }

    public function listAllProductsForSubCategory(Request $request)
    {
        $getDefaultVariables = new Configuration();
        $paginationCount = $getDefaultVariables->getPaginationcount();

        $minimumProductPrice = $request->input('minimumProductPrice');
        $maximumProductPrice = $request->input('maximumProductPrice');
       // $productName = explode(',',Input::get('productName'));
        $productColorId = explode(',',Input::get('productColorId'));
        Log::info(count($productColorId));
        Log::info($productColorId);

        $listProductsCount = Product::where('product_table.vendorId', $request->input('vendorId'))
            ->where('product_table.subcategoryId', $request->input('subCategoryId'))
            ->join('product_color_table', 'product_table.productColorId', '=', 'product_color_table.productColorId')
            ->join('category_table', 'product_table.categoryId', '=', 'category_table.categoryId')
            ->join('subcategory_table', 'product_table.subcategoryId', '=', 'subcategory_table.subcategoryId')->count();

        if ($listProductsCount <= $paginationCount)
        {

            if (($minimumProductPrice =="" || $minimumProductPrice==null || $maximumProductPrice==""|| $maximumProductPrice==null))
            {
                $listProducts = Product::where('product_table.vendorId', $request->input('vendorId'))
                    ->where('product_table.subcategoryId', $request->input('subCategoryId'))
                    ->join('product_color_table', 'product_table.productColorId', '=', 'product_color_table.productColorId')
                    ->join('category_table', 'product_table.categoryId', '=', 'category_table.categoryId')
                    ->join('subcategory_table', 'product_table.subcategoryId', '=', 'subcategory_table.subcategoryId')
                    ->whereIn('product_table.productColorId',$productColorId)
                    ->get();
            }
            else
            {
                $listProducts = Product::where('product_table.vendorId', $request->input('vendorId'))
                    ->where('product_table.subcategoryId', $request->input('subCategoryId'))
                    ->join('product_color_table', 'product_table.productColorId', '=', 'product_color_table.productColorId')
                    ->join('category_table', 'product_table.categoryId', '=', 'category_table.categoryId')
                    ->join('subcategory_table', 'product_table.subcategoryId', '=', 'subcategory_table.subcategoryId')
                    ->whereBetween('product_table.productPrice', [$minimumProductPrice, $maximumProductPrice])
                    ->whereIn('product_table.productColorId',$productColorId)
                    ->get();
            }

            if ($listProducts == null || $listProducts == "") {
                $returnValues = new ReturnController("24001", "FAILURE", "");
                $return = $returnValues->returnValues();
                return $return;
            } else {
                $listProductFound = [];
                $k = 0;
                foreach ($listProducts as $products) {
                    $tempArray = [];
                    $tempArray['productName'] = $products->productName;
                    $tempArray['productDescription'] = $products->productDescription;
                    $tempArray['productPrice'] = $products->productPrice;
                    $tempArray['productModel'] = $products->productModel;
                    $tempArray['productCount'] = $products->productCount;
                    $tempArray['productId'] = $products->productId;
                    $tempArray['categoryId'] = $products->catgoryId;
                    $tempArray['productColor'] = $products->productColor;
                    $tempArray['categoryName'] = $products->categoryName;
                    $tempArray['subcategoryName'] = $products->subcategoryName;
                    $tempArray['productImages'] = [];
                    $splitOptions = explode(';', $products->productImage);


                    //if ($products->productPrice > $minPrice && $products->productPrice < $maxPrice && $products->productName == $name && ($products->productColor == $color || $products->productColor =="" || $products->productColor ==null))
                    //{
                    for ($i = 1; $i < count($splitOptions); $i++) {

                        Log::info($splitOptions[1]);
                        if ($splitOptions[$i] == "" || $splitOptions[$i] == null)
                            continue;
                        else {
                            $tempProductImage = [];
                            if ($i == 1) {
                                $tempProductImage['thumbnail'] = $splitOptions[$i];
                            }
                            $tempProductImage['productImage'] = $splitOptions[$i];
                            array_push($tempArray['productImages'], $tempProductImage);
                        }
                    }
                    for ($j = 0; $j < count($productColorId); $j++) {
                        Log::info(count($productColorId));
                        if ($productColorId[$j] == "" || $productColorId[$j] == null)
                            continue;
                        else {
                            $color['productColorId'] = $productColorId[$j];
                            //Log::info($color);
                            $productColorId[$j] = $color;

                        }
                    }
                        $listProductFound[$k] = $tempArray;
                        $k++;
                    //}
                }
                $data = [
                    "lastPage" => "NULL",
                    "data" => $listProductFound];
                $returnValues = new ReturnController("24000", "SUCCESS", $data);
                $return = $returnValues->returnValues();
                return $return;
            }
        }

        else
        {
            if (($minimumProductPrice =="" || $minimumProductPrice==null || $maximumProductPrice==""|| $maximumProductPrice==null) || ($productColorId=="" ||$productColorId==null ||count($productColorId)<2))
            {
                $listProducts = Product::where('product_table.vendorId', $request->input('vendorId'))
                    ->where('product_table.subcategoryId', $request->input('subCategoryId'))
                    ->join('product_color_table', 'product_table.productColorId', '=', 'product_color_table.productColorId')
                    ->join('category_table', 'product_table.categoryId', '=', 'category_table.categoryId')
                    ->join('subcategory_table', 'product_table.subcategoryId', '=', 'subcategory_table.subcategoryId')
                    ->whereIn('product_table.productColorId',$productColorId)
                    ->paginate($paginationCount);
            }
            else
            {
                $listProducts = Product::where('product_table.vendorId', $request->input('vendorId'))
                    ->where('product_table.subcategoryId', $request->input('subCategoryId'))
                    ->join('product_color_table', 'product_table.productColorId', '=', 'product_color_table.productColorId')
                    ->join('category_table', 'product_table.categoryId', '=', 'category_table.categoryId')
                    ->join('subcategory_table', 'product_table.subcategoryId', '=', 'subcategory_table.subcategoryId')
                    ->whereIn('product_table.productColorId',$productColorId)
                    ->whereBetween('product_table.productPrice', [$minimumProductPrice, $maximumProductPrice])
                    ->paginate($paginationCount);
            }

            if (count($listProducts) == 0)
            {
                $returnValues = new ReturnController("24001", "FAILURE", "");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $listProductFound = [];
                $k = 0;
                foreach ($listProducts as $products) {
                    $tempArray = [];
                    $tempArray['productName'] = $products->productName;
                    $tempArray['productDescription'] = $products->productDescription;
                    $tempArray['productPrice'] = $products->productPrice;
                    $tempArray['productModel'] = $products->productModel;
                    $tempArray['productCount'] = $products->productCount;
                    $tempArray['productId'] = $products->productId;
                    $tempArray['productColor'] = $products->productColor;
                    $tempArray['categoryName'] = $products->categoryName;
                    $tempArray['subcategoryName'] = $products->subcategoryName;
                    $tempArray['productImages'] = [];
                    $splitOptions = explode(';', $products->productImage);
                   /* if (($products->productPrice > $minPrice && $products->productPrice < $maxPrice ||$minPrice =="" ||$minPrice==null ||$maxPrice==""||$maxPrice==null) && ($products->productName==$name ||$name =="" ||$name ==null) && ($products->productColor == $color|| $color == "" || $color == null))
                    {*/

                        for ($i = 1; $i < count($splitOptions); $i++) {

                            Log::info($splitOptions[1]);
                            if ($splitOptions[$i] == "" || $splitOptions[$i] == null)
                                continue;
                            else {
                                $tempProductImage = [];
                                if ($i == 1) {
                                    $tempProductImage['thumbnail'] = $splitOptions[$i];
                                }
                                $tempProductImage['productImage'] = $splitOptions[$i];
                                array_push($tempArray['productImages'], $tempProductImage);
                            }
                        }


                        $listProductFound[$k] = $tempArray;
                        $k++;
                   //}
                }

                $data = [
                    "total" => $listProducts->total(),
                    "nextPageUrl" => $listProducts->nextPageUrl(),
                    "previousPageUrl" => $listProducts->previousPageUrl(),
                    "currentPage" => $listProducts->currentPage(),
                    "lastPage" => $listProducts->lastPage(),
                    "data" => $listProductFound
                ];
                $returnValues = new ReturnController("24000", "SUCCESS", $data);
                $return = $returnValues->returnValues();
                return $return;
            }
        }

    }

    public function listAll(Request $request)
    {
        $getDefaultVariables = new Configuration();
        $paginationCount = $getDefaultVariables->getPaginationcount();

        $getLoginId = Login::where('remember_token', $request->input('token'))->first();

        $filterType = $request->input('filterType');
        switch ($filterType) {
            case "0":
                $authenticate = new AuthUser();
                $authenticateUser = $authenticate->authenticateUser();
                if ($authenticateUser == 400)
                {

                    $listProductsCount = DB::table('product_table')
                        ->where('product_table.vendorId', $getLoginId['loginId'])
                        ->join('product_color_table', 'product_table.productColorId', '=', 'product_color_table.productColorId')
                        ->join('category_table', 'product_table.categoryId', '=', 'category_table.categoryId')
                        ->join('subcategory_table', 'product_table.subcategoryId', '=', 'subcategory_table.subcategoryId')->count();

                    if ($listProductsCount <= $paginationCount)
                    {
                        $listProducts = DB::table('product_table')
                            ->where('product_table.vendorId', $getLoginId['loginId'])
                            ->join('product_color_table', 'product_table.productColorId', '=', 'product_color_table.productColorId')
                            ->join('category_table', 'product_table.categoryId', '=', 'category_table.categoryId')
                            ->join('subcategory_table', 'product_table.subcategoryId', '=', 'subcategory_table.subcategoryId')->orderby('product_table.id', 'desc')->get();

                        if ($listProducts == null || $listProducts == "")
                        {
                            $returnValues = new ReturnController("12002", "FAILURE", "");
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                        else
                        {

                            $listProductFound = [];
                            $k = 0;

                            foreach ($listProducts as $products) {
                                $tempArray = [];
                                $tempArray['productName'] = $products->productName;
                                $tempArray['productDescription'] = $products->productDescription;
                                $tempArray['productPrice'] = $products->productPrice;
                                $tempArray['productModel'] = $products->productModel;
                                $tempArray['productId'] = $products->productId;
                                $tempArray['productColorId'] = $products->productColorId;
                                $tempArray['productColor'] = $products->productColor;
                                $tempArray['productCount'] = $products->productCount;
                                $tempArray['categoryName'] = $products->categoryName;
                                $tempArray['subcategoryName'] = $products->subcategoryName;
                                $tempArray['productImages'] = [];
                                $splitOptions = explode(';', $products->productImage);

                                for ($i = 1; $i < count($splitOptions); $i++) {
                                    Log::info($splitOptions[1]);
                                    if ($splitOptions[$i] == "" || $splitOptions[$i] == null)
                                        continue;
                                    else {
                                        $tempProductImage = [];
                                        if ($i == 1) {
                                            $tempProductImage['thumbnail'] = $splitOptions[$i];
                                        }
                                        $tempProductImage['productImage'] = $splitOptions[$i];
                                        array_push($tempArray['productImages'], $tempProductImage);
                                    }
                                }
                                $listProductFound[$k] = $tempArray;
                                $k++;
                            }
                            $data = [
                                "lastPage" => "NULL",
                                "data" => $listProductFound];

                            $returnValues = new ReturnController("12000", "SUCCESS", $data);
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                    }
                    else
                    {
                        $listProducts = DB::table('product_table')
                            ->where('product_table.vendorId', $getLoginId['loginId'])
                            ->join('product_color_table', 'product_table.productColorId', '=', 'product_color_table.productColorId')
                            ->join('category_table', 'product_table.categoryId', '=', 'category_table.categoryId')
                            ->join('subcategory_table', 'product_table.subcategoryId', '=', 'subcategory_table.subcategoryId')->orderby('product_table.id', 'desc')->paginate($paginationCount);

                        if ($listProducts == null || $listProducts == "") {
                            $returnValues = new ReturnController("3002", "FAILURE", "");
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                        else
                        {
                            $listProductFound = [];
                            $k = 0;
                            foreach ($listProducts as $products) {
                                $tempArray = [];
                                $tempArray['productName'] = $products->productName;
                                $tempArray['productDescription'] = $products->productDescription;
                                $tempArray['productPrice'] = $products->productPrice;
                                $tempArray['productModel'] = $products->productModel;
                                $tempArray['productId'] = $products->productId;
                                $tempArray['productColor'] = $products->productColor;
                                $tempArray['productCount'] = $products->productCount;
                                $tempArray['categoryName'] = $products->categoryName;
                                $tempArray['subcategoryName'] = $products->subcategoryName;
                                $tempArray['productImages'] = [];
                                $splitOptions = explode(';', $products->productImage);

                                for ($i = 1; $i < count($splitOptions); $i++) {
                                    Log::info($splitOptions[1]);
                                    if ($splitOptions[$i] == "" || $splitOptions[$i] == null)
                                        continue;
                                    else {
                                        $tempProductImage = [];
                                        if ($i == 1) {
                                            $tempProductImage['thumbnail'] = $splitOptions[$i];
                                        }
                                        $tempProductImage['productImage'] = $splitOptions[$i];
                                        array_push($tempArray['productImages'], $tempProductImage);
                                    }
                                }
                                $listProductFound[$k] = $tempArray;
                                $k++;
                            }

                            $data = [
                                "total" => $listProducts->total(),
                                "nextPageUrl" => $listProducts->nextPageUrl(),
                                "previousPageUrl" => $listProducts->previousPageUrl(),
                                "currentPage" => $listProducts->currentPage(),
                                "lastPage" => $listProducts->lastPage(),
                                "data" => $listProductFound
                            ];

                            $returnValues = new ReturnController("12000", "SUCCESS", $data);
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                    }

                } else {
                    switch ($authenticateUser) {
                        case "404":
                            $returnValues = new ReturnController("404", "FAILURE", "INVALID_USER");
                            $return = $returnValues->returnValues();
                            return $return;
                            break;
                        case "405":
                            $returnValues = new ReturnController("405", "FAILURE", "TOKEN_EXPIRED");
                            $return = $returnValues->returnValues();
                            return $return;
                            break;
                        case "406":
                            $returnValues = new ReturnController("406", "FAILURE", "INVALID_TOKEN");
                            $return = $returnValues->returnValues();
                            return $return;
                            break;
                        case "407":
                            $returnValues = new ReturnController("407", "FAILURE", "TOKEN_ABSENT");
                            $return = $returnValues->returnValues();
                            return $return;
                            break;
                    }
                }
                break;

            case "1":
                $listProducts = DB::table('product_table')
                    ->where('product_table.vendorId', $getLoginId['loginId'])
                    ->join('product_color_table', 'product_table.productColorId', '=', 'product_color_table.productColorId')
                    ->join('category_table', 'product_table.categoryId', '=', 'category_table.categoryId')
                    ->join('subcategory_table', 'product_table.subcategoryId', '=', 'subcategory_table.subcategoryId')
                    ->orderby('product_table.id', 'desc')
                    ->get();
                if ($listProducts == null || $listProducts == "") {
                    $returnValues = new ReturnController("12002", "FAILURE", "");
                    $return = $returnValues->returnValues();
                    return $return;
                } else {
                    $listProductFound = [];
                    $k = 0;
                    foreach ($listProducts as $products) {
                        $tempArray = [];
                        $tempArray['productName'] = $products->productName;
                        $tempArray['productDescription'] = $products->productDescription;
                        $tempArray['productPrice'] = $products->productPrice;
                        $tempArray['productModel'] = $products->productModel;
                        $tempArray['productId'] = $products->productId;
                        $tempArray['productColor'] = $products->productColor;
                        $tempArray['productCount'] = $products->productCount;
                        $tempArray['categoryName'] = $products->categoryName;
                        $tempArray['subcategoryName'] = $products->subcategoryName;
                        $tempArray['productImages'] = [];
                        $splitOptions = explode(';', $products->productImage);

                        for ($i = 1; $i < count($splitOptions); $i++) {
                            Log::info($splitOptions[1]);
                            if ($splitOptions[$i] == "" || $splitOptions[$i] == null)
                                continue;
                            else {
                                $tempProductImage = [];
                                if ($i == 1) {
                                    $tempProductImage['thumbnail'] = $splitOptions[$i];
                                }
                                $tempProductImage['productImage'] = $splitOptions[$i];
                                array_push($tempArray['productImages'], $tempProductImage);
                            }
                        }
                        $listProductFound[$k] = $tempArray;
                        $k++;
                    }
                    $returnValues = new ReturnController("12000", "SUCCESS", $listProductFound);
                    $return = $returnValues->returnValues();
                    return $return;
                }
                break;

            case "2":$listProducts = DB::table('product_table')
                ->where('product_table.vendorId',$request->input('vendorId'))
                ->join('product_color_table','product_table.productColorId','=','product_color_table.productColorId')
                ->join('category_table','product_table.categoryId','=','category_table.categoryId')
                ->join('subcategory_table','product_table.subcategoryId','=','subcategory_table.subcategoryId')
                ->orderby('product_table.id','desc')
                ->get();

                if($listProducts == null || $listProducts == "")
                {
                    $returnValues = new ReturnController("12002","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }

                else
                {
                    $listProductFound = [];
                    $k = 0;
                    foreach ($listProducts as $products) {
                        $tempArray = [];
                        $tempArray['productName'] = $products->productName;
                        $tempArray['productDescription'] = $products->productDescription;
                        $tempArray['productPrice'] = $products->productPrice;
                        $tempArray['productModel'] = $products->productModel;
                        $tempArray['productId'] = $products->productId;
                        $tempArray['productColor'] = $products->productColor;
                        $tempArray['productCount'] = $products->productCount;
                        $tempArray['categoryName'] = $products->categoryName;
                        $tempArray['subcategoryName'] = $products->subcategoryName;
                        $tempArray['productImages'] = [];
                        $splitOptions = explode(';', $products->productImage);

                        for ($i = 1; $i < count($splitOptions); $i++)
                        {
                                Log::info($splitOptions[1]);
                                if ($splitOptions[$i] == "" || $splitOptions[$i] == null)
                                    continue;
                                else {
                                    $tempProductImage = [];
                                    if ($i == 1) {
                                        $tempProductImage['thumbnail'] = $splitOptions[$i];
                                    }
                                    $tempProductImage['productImage'] = $splitOptions[$i];
                                    array_push($tempArray['productImages'], $tempProductImage);
                                }
                        }
                        $listProductFound[$k] = $tempArray;
                        $k++;

                    }

                    $returnValues = new ReturnController("12000", "SUCCESS", $listProductFound);
                    $return = $returnValues->returnValues();
                    return $return;
                }

                break;


        }
    }


    public function listProductForId(Request $request)
    {

        $listProducts = Product::where('product_table.vendorId', $request->input('vendorId'))
            ->where('product_table.productId', $request->input('productId'))
            ->join('product_color_table', 'product_table.productColorId', '=', 'product_color_table.productColorId')
            ->join('category_table', 'product_table.categoryId', '=', 'category_table.categoryId')
            ->join('subcategory_table', 'product_table.subcategoryId', '=', 'subcategory_table.subcategoryId')->get();
        Log::info($listProducts);
        if ($listProducts == null || $listProducts == "") {
            $returnValues = new ReturnController("23001", "FAILURE", "");
            $return = $returnValues->returnValues();
            return $return;
        } else {
            $listProductFound = [];
            $k = 0;
            foreach ($listProducts as $products) {
                $tempArray = [];
                $tempArray['productName'] = $products->productName;
                $tempArray['productDescription'] = $products->productDescription;
                $tempArray['productPrice'] = $products->productPrice;
                $tempArray['productModel'] = $products->productModel;
                $tempArray['productId'] = $products->productId;
                $tempArray['productColor'] = $products->productColor;
                $tempArray['categoryName'] = $products->categoryName;
                $tempArray['subcategoryName'] = $products->subcategoryName;
                $tempArray['productImages'] = [];
                $tempArray['similarProducts'] = [];
                $splitOptions = explode(';', $products->productImage);

                for ($i = 1; $i < count($splitOptions); $i++) {
                    if ($splitOptions[$i] == "" || $splitOptions[$i] == null)
                        continue;
                    else {
                        $tempProductImage = [];
                        if ($i == 1) {
                            $tempProductImage['thumbnail'] = $splitOptions[$i];
                        }
                        $tempProductImage['productImage'] = $splitOptions[$i];
                        array_push($tempArray['productImages'], $tempProductImage);
                    }
                }
                $getProductsForModelId = Product::where('product_table.productModel', $products->productModel)
                    ->where('product_table.productId', '!=', $products->productId)
                    ->where('product_table.subcategoryId', '=', $products->subcategoryId)
                    ->join('product_color_table', 'product_table.productColorId', '=', 'product_color_table.productColorId')
                    ->join('category_table', 'product_table.categoryId', '=', 'category_table.categoryId')
                    ->join('subcategory_table', 'product_table.subcategoryId', '=', 'subcategory_table.subcategoryId')->get();

                foreach ($getProductsForModelId as $modelId) {
                    $tempSimilarProductArray = [];
                    $tempSimilarProductArray['productName'] = $modelId->productName;
                    $tempSimilarProductArray['productDescription'] = $modelId->productDescription;
                    $tempSimilarProductArray['productPrice'] = $modelId->productPrice;
                    $tempSimilarProductArray['productModel'] = $modelId->productModel;
                    $tempSimilarProductArray['productId'] = $modelId->productId;
                    $tempSimilarProductArray['productColor'] = $modelId->productColor;
                    $tempSimilarProductArray['categoryName'] = $modelId->categoryName;
                    $tempSimilarProductArray['subcategoryName'] = $modelId->subcategoryName;
                    $tempSimilarProductArray['productImages'] = [];
                    $splitOptions = explode(';', $modelId->productImage);

                    for ($i = 1; $i < count($splitOptions); $i++) {
                        if ($splitOptions[$i] == "" || $splitOptions[$i] == null)
                            continue;
                        else {
                            $tempProductImage = [];
                            if ($i == 1) {
                                $tempProductImage['thumbnail'] = $splitOptions[$i];
                            }
                            array_push($tempSimilarProductArray['productImages'], $tempProductImage);
                        }
                    }
                    array_push($tempArray['similarProducts'], $tempSimilarProductArray);
                }
                $listProductFound[$k] = $tempArray;
                $k++;

            }

            $countView = DB::table('user_history')->increment('count');
            //$countView = UserHistory::where('vendorId',$request->input('vendorId'))->increment('count');
            Log::info($countView);

            $returnValues = new ReturnController("23000", "SUCCESS", $listProductFound);

            $return = $returnValues->returnValues();
            return $return;
        }
    }

    public function listAllUserBoughtProducts(Request $request)
    {
        $authenticate = new AuthUser();
        $authenticateUser = $authenticate->authenticateUser();
        if ($authenticateUser == "400") {
            $getLoginId = Login::where('remember_token', $request->input('token'))->first();
            $vendorId = $getLoginId['loginId'];

            $filterType = $request->input('filterType');
            switch ($filterType) {
                case "0";//get the cart details based on loginId
                    $cartDetailsForUser = Cart::where('vendorId', $vendorId)
                        ->where('cart_table.productStatus', '!=', 0)
                        ->join('user_table', 'cart_table.userId', '=', 'user_table.userId')
                        ->join('user_address_table', 'cart_table.addressId', '=', 'user_address_table.addressId')
                        ->get();
                    $mainCartDetailsArray = [];
                    $k = 0;
                    foreach ($cartDetailsForUser as $cartDetail) {
                        $tempArray = [];
                        $tempArray['cartId'] = $cartDetail['cartId'];
                        $tempArray['userName'] = $cartDetail['userName'];
                        $tempArray['userAddress'] = $cartDetail['userAddress'];
                        $tempArray['userPhoneNumber'] = $cartDetail['userPhoneNumber'];
                        $tempArray['paymentStatus'] = $cartDetail['paymentStatus'];
                        $tempArray['productStatus'] = $cartDetail['productStatus'];
                        $tempArray['orderRecievedDate'] = $cartDetail['orderRecievedDate'];
                        $tempArray['paymentRecievedDate'] = $cartDetail['paymentRecievedDate'];
                        $tempArray['shippingInProgressDate'] = $cartDetail['shippingInProgressDate'];
                        $tempArray['shippedDate'] = $cartDetail['shippedDate'];
                        $productDetailsJSON = json_decode($cartDetail['productDetails']);
                        $tempArray['productDetails'] = [];
                        //now parse the JSON to get each product detail
                        for ($i = 0; $i < count($productDetailsJSON->productDetails); $i++) {
                            $tempProductDetailsArray = [];
                            $tempProductId = $productDetailsJSON->productDetails[$i]->productId;
                            $tempQuantity = $productDetailsJSON->productDetails[$i]->quantity;
                            //get the product name, price etc.. based on productID
                            Log::info($tempProductId);
                            $getproductDetails = Product::where('productId', $tempProductId)
                                ->join('product_color_table', 'product_table.productColorId', '=', 'product_color_table.productColorId')
                                ->join('category_table', 'product_table.categoryId', '=', 'category_table.categoryId')
                                ->join('subcategory_table', 'product_table.subcategoryId', '=', 'subcategory_table.subcategoryId')
                                ->first();
                            $tempProductDetailsArray['productName'] = $getproductDetails['productName'];
                            $tempProductDetailsArray['productPrice'] = $getproductDetails['productPrice'];
                            $tempProductDetailsArray['productModel'] = $getproductDetails['productModel'];
                            $tempProductDetailsArray['productColor'] = $getproductDetails['productColor'];
                            $tempProductDetailsArray['categoryName'] = $getproductDetails['categoryName'];
                            $tempProductDetailsArray['subcategoryName'] = $getproductDetails['subcategoryName'];
                            $tempProductDetailsArray['thumbnail'] = '';
                            //get the product thumbnails
                            $imageSplitArray = explode(';', $getproductDetails['productImage']);
                            for ($j = 1; $j < count($imageSplitArray); $j++) {
                                if ($imageSplitArray[$j] == "" || $imageSplitArray[$j] == null)
                                    continue;
                                else {
                                    if ($j == 1) {
                                        $tempProductDetailsArray['thumbnail'] = $imageSplitArray[$j];
                                    }
                                }
                            }
                            $tempProductDetailsArray['productId'] = $tempProductId;
                            $tempProductDetailsArray['quantity'] = $tempQuantity;
                            array_push($tempArray['productDetails'], $tempProductDetailsArray);
                        }
                        $mainCartDetailsArray[$k] = $tempArray;
                        $k++;
                    }
                    $returnValues = new ReturnController("35000", "SUCCESS", $mainCartDetailsArray);
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "1";
                    $filterDate = $request->input('date');
                    $filterDate = date_create($filterDate);
                    $filterDate = $filterDate->format('Y-m-d H:i:s');
                    $filterDate = explode(' ', $filterDate);
                    $filterDate = $filterDate[0];
                    $cartDetailsForUser = Cart::where('vendorId', $vendorId)
                        ->where('cart_table.productStatus', '!=', 0)
                        ->where('cart_table.productStatus', '=', $request->input('productStatus'))
                        ->join('user_table', 'cart_table.userId', '=', 'user_table.userId')
                        ->join('user_address_table', 'cart_table.addressId', '=', 'user_address_table.addressId')
                        ->get();
                    $mainCartDetailsArray = [];
                    $k = 0;
                    foreach ($cartDetailsForUser as $cartDetail) {
                        $tempArray = [];
                        $tempArray['cartId'] = $cartDetail['cartId'];
                        $tempArray['userName'] = $cartDetail['userName'];
                        $tempArray['userAddress'] = $cartDetail['userAddress'];
                        $tempArray['userPhoneNumber'] = $cartDetail['userPhoneNumber'];
                        $tempArray['paymentStatus'] = $cartDetail['paymentStatus'];
                        $tempArray['productStatus'] = $cartDetail['productStatus'];
                        $tempArray['orderRecievedDate'] = $cartDetail['orderRecievedDate'];
                        $tempArray['paymentRecievedDate'] = $cartDetail['paymentRecievedDate'];
                        $tempArray['shippingInProgressDate'] = $cartDetail['shippingInProgressDate'];
                        $tempArray['shippedDate'] = $cartDetail['shippedDate'];
                        $productDetailsJSON = json_decode($cartDetail['productDetails']);
                        $tempArray['productDetails'] = [];
                        //now parse the JSON to get each product detail
                        for ($i = 0; $i < count($productDetailsJSON->productDetails); $i++) {
                            $tempProductDetailsArray = [];
                            $tempProductId = $productDetailsJSON->productDetails[$i]->productId;
                            $tempQuantity = $productDetailsJSON->productDetails[$i]->quantity;
                            //get the product name, price etc.. based on productID
                            $getproductDetails = Product::where('product_table.productId', $request->input('productId'))
                                ->join('product_color_table', 'product_table.productColorId', '=', 'product_color_table.productColorId')
                                ->join('category_table', 'product_table.categoryId', '=', 'category_table.categoryId')
                                ->join('subcategory_table', 'product_table.subcategoryId', '=', 'subcategory_table.subcategoryId')
                                ->first();
                            $tempProductDetailsArray['productName'] = $getproductDetails['productName'];
                            $tempProductDetailsArray['productPrice'] = $getproductDetails['productPrice'];
                            $tempProductDetailsArray['productModel'] = $getproductDetails['productModel'];
                            $tempProductDetailsArray['productColor'] = $getproductDetails['productColor'];
                            $tempProductDetailsArray['categoryName'] = $getproductDetails['categoryName'];
                            $tempProductDetailsArray['subcategoryName'] = $getproductDetails['subcategoryName'];
                            $tempProductDetailsArray['thumbnail'] = '';
                            //get the product thumbnails
                            $imageSplitArray = explode(';', $getproductDetails['productImage']);
                            for ($j = 1; $j < count($imageSplitArray); $j++) {
                                if ($imageSplitArray[$j] == "" || $imageSplitArray[$j] == null)
                                    continue;
                                else {
                                    if ($j == 1) {
                                        $tempProductDetailsArray['thumbnail'] = $imageSplitArray[$j];
                                    }
                                }
                            }
                            $tempProductDetailsArray['productId'] = $tempProductId;
                            $tempProductDetailsArray['quantity'] = $tempQuantity;
                            array_push($tempArray['productDetails'], $tempProductDetailsArray);
                        }
                        $mainCartDetailsArray[$k] = $tempArray;
                        $k++;
                    }
                    $returnValues = new ReturnController("35000", "SUCCESS", $mainCartDetailsArray);
                    $return = $returnValues->returnValues();
                    return $return;
                    break;

            }
        } else {
            switch ($authenticateUser) {
                case "404":
                    $returnValues = new ReturnController("404", "FAILURE", "INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":
                    $returnValues = new ReturnController("405", "FAILURE", "TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":
                    $returnValues = new ReturnController("406", "FAILURE", "INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":
                    $returnValues = new ReturnController("407", "FAILURE", "TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }

    }

    public function delete(Request $request)
    {
        $authenticate = new AuthUser();
        $authenticateUser = $authenticate->authenticateUser();
        if ($authenticateUser == 400) {
            $flagValidateInputs = $this->validateDeleteProduct($request);
            if ($flagValidateInputs == false) {
                $returnValues = new ReturnController("13001", "FAILURE", "");
                $return = $returnValues->returnValues();
                return $return;
            } else {
                $deleteProduct = Product::where('productId', $request->input('productId'))->delete();
                if ($deleteProduct < 0) {
                    $returnValues = new ReturnController("13002", "FAILURE", "");
                    $return = $returnValues->returnValues();
                    return $return;
                } else {
                    $returnValues = new ReturnController("13000", "SUCCESS", "");
                    $return = $returnValues->returnValues();
                    return $return;
                }
            }

        } else {
            switch ($authenticateUser) {
                case "404":
                    $returnValues = new ReturnController("404", "FAILURE", "INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":
                    $returnValues = new ReturnController("405", "FAILURE", "TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":
                    $returnValues = new ReturnController("406", "FAILURE", "INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":
                    $returnValues = new ReturnController("407", "FAILURE", "TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }

    protected function validateDeleteProduct(Request $request)
    {
        $rules = array(
            'productId' => 'required');

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails())
            return false;
        else
            return true;
    }


    public function search(Request $request)
    {
        $search = $request->input('search');

        $listProducts = DB::table('product_table')
            ->where('product_table.vendorId',$request->input('vendorId'))
            ->join('product_color_table', 'product_table.productColorId', '=', 'product_color_table.productColorId')
            ->join('category_table', 'product_table.categoryId', '=', 'category_table.categoryId')
            ->join('subcategory_table', 'product_table.subcategoryId', '=', 'subcategory_table.subcategoryId')
            ->where('productName', 'LIKE', '%' . $search . '%')
            ->orderby('product_table.id', 'desc')
            ->get();

        Log::info($listProducts);

        if (count($listProducts) < 1) {
            $returnValues = new ReturnController("67003", "FAILURE", "");
            $return = $returnValues->returnValues();
            return $return;
        } else {
            $returnValues = new ReturnController("67000", "SUCCESS", $listProducts);
            $return = $returnValues->returnValues();
            return $return;
        }
    }


    public function subCategorySearch(Request $request)
    {
        $search = $request->input('search');

        $listProducts = Product::
             where('product_table.subcategoryId', $request->input('subCategoryId'))
            ->where('product_table.vendorId',$request->input('vendorId'))
            ->join('product_color_table', 'product_table.productColorId', '=', 'product_color_table.productColorId')
            ->join('category_table', 'product_table.categoryId', '=', 'category_table.categoryId')
            ->join('subcategory_table', 'product_table.subcategoryId', '=', 'subcategory_table.subcategoryId')
            ->where('productName', 'LIKE', '%' . $search . '%')
                ->orderby('product_table.id', 'desc')
            ->get();

        Log::info($listProducts);

        if (count($listProducts) < 1) {
            $returnValues = new ReturnController("68003", "FAILURE", "");
            $return = $returnValues->returnValues();
            return $return;
        } else {
            $returnValues = new ReturnController("68000", "SUCCESS", $listProducts);
            $return = $returnValues->returnValues();
            return $return;
        }
    }
}

