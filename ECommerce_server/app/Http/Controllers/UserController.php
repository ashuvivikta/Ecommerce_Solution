<?php

namespace App\Http\Controllers;

use App\Cart;
use App\EndUser;
use App\Http\Middleware\AuthUser;
use App\Http\Middleware\GenerateUUID;
use App\Http\Middleware\MailEngine;
use App\Login;
use App\Product;
use App\UserAddress;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function register (Request $request)
    {
        $flagValidateInputs = $this->validateRegisterUser($request);
        if($flagValidateInputs == false)
        {
            $returnValues = new ReturnController("22001","FAILURE","");
            $return = $returnValues->returnValues();
            return $return;
        }
        else
        {
            $checkUserEmailId = EndUser::where('userEmailId',$request->input('userEmailId'))->first();
            if($checkUserEmailId != null || $checkUserEmailId != "")
            {
                $returnValues = new ReturnController("22002","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $verificationStatus = "Active";
                $generateUniqueId = new GenerateUUID();
                $userId = $generateUniqueId->getUniqueId();

                $subAdmin = new EndUser();
                $subAdmin->userName = $request->input('userName');
                $subAdmin->userEmailId = $request->input('userEmailId');
                $subAdmin->userPhoneNumber = $request->input('userPhoneNumber');
                $subAdmin->userId = $userId;
                $subAdmin->userVerificationStatus = $verificationStatus;
                $subAdmin->userProfilePicturePath = "N/A";
                $subAdmin->save();

                if(!$subAdmin->save())
                {
                    $returnValues = new ReturnController("22003","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $userAddress = $request->input('userAddress');
                    if($userAddress != null || $userAddress != "")
                    {
                        $generateUniqueId = new GenerateUUID();
                        $addressId = $generateUniqueId->getUniqueId();

                        $userAddress = new UserAddress();
                        $userAddress->userId = $userId;
                        $userAddress->addressId = $addressId;
                        $userAddress->userAddress = $request->input('userAddress');
                        $userAddress->save();

                        if(!$userAddress->save())
                        {
                            $returnValues = new ReturnController("22004","FAILURE","");
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                        else
                        {
                            $mail = new MailEngine();
                            $mail->userRegister($request->input('userEmailId'),$userId);

                            $returnValues = new ReturnController("22000","SUCCESS","");
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                    }
                    $mail = new MailEngine();
                    $mail->userRegister($request->input('userEmailId'),$userId);

                    $returnValues = new ReturnController("22000","SUCCESS","");
                    $return = $returnValues->returnValues();
                    return $return;

                }
            }
        }
    }

    protected function validateRegisterUser(Request $request)
    {
        $rules = array(
            'userName' => 'required',
            'userEmailId' => 'required',
            'userPhoneNumber' => 'required');

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails())
            return false;
        else
            return true;
    }
    public function createPassword(Request $request)
    {
        $flagValidate = $this->validateSubAdminId($request);
        if ($flagValidate == false)
        {
            $returnValues = new ReturnController("27001","FAILURE","");
            $return = $returnValues->returnValues();
            return $return;
        }
        else
        {
            $checkUser = EndUser::where('userId',$request->input('id'))->first();
            if ($checkUser == null || $checkUser == "")
            {
                $returnValues = new ReturnController("27002","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $verificationStatus = "Active";

                $userUpdate = EndUser::where('userId',$request->input('id'))
                                  ->update(['userVerificationStatus' => $verificationStatus]);


                if ($userUpdate == 0 )
                {
                    $returnValues = new ReturnController("27003", "FAILURE", "");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $checkLogin = Login::where('loginId',$request->input('id'))->first();

                    if($checkLogin != null || $checkLogin != "" || count($checkLogin) > 0)
                    {
                        $returnValues = new ReturnController("27004", "FAILURE", "");
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                    else
                    {
                        $name = $checkUser['userName'];
                        $emailId = $checkUser['userEmailId'];
                        $userId = $checkUser['userId'];
                        $status = 1;
                        $typeOfUser = "2";

                        $updateLogin = new Login();
                        $updateLogin->name = $name;
                        $updateLogin->emailId = $emailId;
                        $updateLogin->password = Hash::make($request->input('password'));
                        $updateLogin->status = $status;
                        $updateLogin->typeofuser = $typeOfUser;
                        $updateLogin->loginId = $userId;
                        $updateLogin->save();

                        if (!$updateLogin->save())
                        {
                            $returnValues = new ReturnController("27005", "FAILURE", "");
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                        else
                        {
                            $returnValues = new ReturnController("27000","SUCCESS","");
                            $return = $returnValues->returnValues();
                            return $return;

                        }
                    }
                }
            }
        }
    }
    protected function validateSubAdminId(Request $request)
    {
        $rules = array(
            'id' => 'required',
            'password' => 'required',
            'confirmPassword' => 'required');
        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails())
            return false;
        else
            return true;
    }

    public function addAddress(Request $request)
    {
        $authenticate = new AuthUser();
        $authenticateUser = $authenticate->authenticateUser();
        if ($authenticateUser == "400")
        {
            $getLoginId = Login::where('remember_token',$request->input('token'))->first();

            $flagValidate = $this->validateShippingAddress($request);
            if ($flagValidate == false)
            {
                $returnValues = new ReturnController("32001","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $generateUniqueId = new GenerateUUID();
                $addressId = $generateUniqueId->getUniqueId();

                $userAddress = new UserAddress();
                $userAddress->userId = $getLoginId['loginId'];
                $userAddress->addressId = $addressId;
                $userAddress->userAddress = $request->input('userAddress');
                $userAddress->save();

                if(!$userAddress->save())
                {
                    $returnValues = new ReturnController("32002","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $returnValues = new ReturnController("32000","SUCCESS","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
            }
        }
        else
        {
            switch ($authenticateUser)
            {
                case "404":
                    $returnValues = new ReturnController("404", "FAILURE", "INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":
                    $returnValues = new ReturnController("405", "FAILURE", "TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":
                    $returnValues = new ReturnController("406", "FAILURE", "INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":
                    $returnValues = new ReturnController("407", "FAILURE", "TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }
    protected function validateShippingAddress(Request $request)
    {
        $rules = array(
            'userAddress' => 'required');
        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails())
            return false;
        else
            return true;
    }
    public function listAddress(Request $request)
    {
        $authenticate=new AuthUser();
        $authenticateUser = $authenticate->authenticateUser();
        if ($authenticateUser == "400")
        {
            $getLoginId = Login::where('remember_token',$request->input('token'))->first();

            $checkEmailId = Login::where('emailId',$getLoginId['emailId'])->first();
            if($checkEmailId == null || $checkEmailId == "")
            {
                $returnValues = new ReturnController("31001","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            $userDetails = UserAddress::where('userId',$getLoginId['loginId'])->get();
            if($userDetails == null || $userDetails == "")
            {
                $returnValues = new ReturnController("31002","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $returnValues = new ReturnController("31000","SUCCESS",$userDetails);
                $return = $returnValues->returnValues();
                return $return;
            }

        }
        else
        {
            switch ($authenticateUser)
            {
                case "404":
                    $returnValues = new ReturnController("404", "FAILURE", "INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":
                    $returnValues = new ReturnController("405", "FAILURE", "TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":
                    $returnValues = new ReturnController("406", "FAILURE", "INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":
                    $returnValues = new ReturnController("407", "FAILURE", "TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }

    }
    public function updateAddress(Request $request)
    {
        $authenticate=new AuthUser();
        $authenticateUser = $authenticate->authenticateUser();
        if ($authenticateUser == "400")
        {
            $getLoginId = Login::where('remember_token',$request->input('token'))->first();

            $checkEmailId = Login::where('logiId',$getLoginId['loginId'])->first();

            if($checkEmailId == null || $checkEmailId == "")
            {
                $returnValues = new ReturnController("42001","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            $userUserAddress = EndUser::where('userId',$getLoginId['loginId'])
                             ->where('addressId',$request->input('addressId'))
                             ->update(['userAddress'=>$request->input('userAddress')]);

            if($userUserAddress == 0)
            {
                $returnValues = new ReturnController("42002","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $returnValues = new ReturnController("42000","SUCCESS","");
                $return = $returnValues->returnValues();
                return $return;
            }
        }
        else
        {
            switch ($authenticateUser)
            {
                case "404":
                    $returnValues = new ReturnController("404", "FAILURE", "INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":
                    $returnValues = new ReturnController("405", "FAILURE", "TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":
                    $returnValues = new ReturnController("406", "FAILURE", "INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":
                    $returnValues = new ReturnController("407", "FAILURE", "TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }

    }
    public function deleteAddress(Request $request)
    {
        $authenticate=new AuthUser();
        $authenticateUser = $authenticate->authenticateUser();
        if ($authenticateUser == "400")
        {
            $getLoginId = Login::where('remember_token',$request->input('token'))->first();

            $checkEmailId = Login::where('loginId',$getLoginId['loginId'])->first();

            if($checkEmailId == null || $checkEmailId == "")
            {
                $returnValues = new ReturnController("43001","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            $userUserAddress = UserAddress::where('addressId',$request->input('addressId'))->delete();

            if($userUserAddress < 0)
            {
                $returnValues = new ReturnController("43002","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $returnValues = new ReturnController("43000","SUCCESS","");
                $return = $returnValues->returnValues();
                return $return;
            }
        }
        else
        {
            switch ($authenticateUser)
            {
                case "404":
                    $returnValues = new ReturnController("404", "FAILURE", "INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":
                    $returnValues = new ReturnController("405", "FAILURE", "TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":
                    $returnValues = new ReturnController("406", "FAILURE", "INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":
                    $returnValues = new ReturnController("407", "FAILURE", "TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }

    }
    public function myOrders(Request $request)
    {
        $authenticate=new AuthUser();
        $authenticateUser = $authenticate->authenticateUser();
        if ($authenticateUser == "400")
        {
            $getLoginId = Login::where('remember_token',$request->input('token'))->first();

            //get the cart details based on loginId
            $cartDetailsForUser = Cart::where('vendorId',$request->input('vendorId'))
                                        ->where('userId',$getLoginId['loginId'])
                                        ->where('productStatus','!=',0)
                                        ->orderby('created_at','desc')
                                        ->get();
            $mainCartDetailsArray = [];
            $k = 0;
            foreach($cartDetailsForUser as $cartDetail)
            {
                $tempArray = [];
                $tempArray['cartId'] = $cartDetail['cartId'];
                $tempArray['paymentStatus'] = $cartDetail['paymentStatus'];
                $tempArray['orderDate'] = $cartDetail['created_at'];
                $productDetailsJSON = json_decode($cartDetail['productDetails']);
                $tempArray['productDetails'] = [];
                //now parse the JSON to get each product detail
                for($i=0;$i<count($productDetailsJSON->productDetails);$i++)
                {
                    $tempProductDetailsArray = [];
                    $tempProductId = $productDetailsJSON->productDetails[$i]->productId;
                    $tempQuantity = $productDetailsJSON->productDetails[$i]->quantity;
                    //get the product name, price etc.. based on productID
                    $getproductDetails = Product::where('product_table.productId',$tempProductId)
                        ->join('product_color_table','product_table.productColorId','=','product_color_table.productColorId')
                        ->join('subcategory_table','product_table.subcategoryId','=','subcategory_table.subcategoryId')
                        ->first();
                    $tempProductDetailsArray['productName'] = $getproductDetails['productName'];
                    $tempProductDetailsArray['productPrice'] = $getproductDetails['productPrice'];
                    $tempProductDetailsArray['productModel'] = $getproductDetails['productModel'];
                    $tempProductDetailsArray['productColor'] = $getproductDetails['productColor'];
                    $tempProductDetailsArray['subcategoryName'] = $getproductDetails['subcategoryName'];
                    $tempProductDetailsArray['thumbnail'] = '';
                    //get the product thumbnails
                    $imageSplitArray = explode(';',$getproductDetails['productImage']);
                    for($j=1;$j<count($imageSplitArray);$j++)
                    {
                        if($imageSplitArray[$j] == "" || $imageSplitArray[$j] == null)
                            continue;
                        else
                        {
                            if($j == 1)
                            {
                                $tempProductDetailsArray['thumbnail'] = $imageSplitArray[$j];
                            }
                        }
                    }
                    $tempProductDetailsArray['productId'] = $tempProductId;
                    $tempProductDetailsArray['quantity'] = $tempQuantity;
                    array_push($tempArray['productDetails'],$tempProductDetailsArray);
                }
                $mainCartDetailsArray[$k] = $tempArray;
                $k++;
            }
            $returnValues = new ReturnController("36000", "SUCCESS", $mainCartDetailsArray);
            $return = $returnValues->returnValues();
            return $return;
        }
        else
        {
            switch ($authenticateUser)
            {
                case "404":
                    $returnValues = new ReturnController("404", "FAILURE", "INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":
                    $returnValues = new ReturnController("405", "FAILURE", "TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":
                    $returnValues = new ReturnController("406", "FAILURE", "INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":
                    $returnValues = new ReturnController("407", "FAILURE", "TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }

    }
}
