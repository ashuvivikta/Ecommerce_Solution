<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Middleware\AuthUser;
use App\Http\Middleware\Configuration;
use App\Http\Middleware\GenerateUUID;
use App\Login;
use App\ProductColor;
use App\UserAddress;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class ProductColorController extends Controller
{
    public function add (Request $request)
    {
        $authenticate = new AuthUser();
        $authenticateUser  = $authenticate->authenticateUser();
        if($authenticateUser == 400)
        {
            $getLoginId = Login::where('remember_token',$request->input('token'))->first();

            $flagValidateInputs = $this->validateAddColor($request);
            if($flagValidateInputs == false)
            {
                $returnValues = new ReturnController("14001","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $checkProductColor = ProductColor::where('vendorId',$getLoginId['loginId'])
                                                 ->where('productColor',$request->input('productColor'))->first();
                if($checkProductColor != null || $checkProductColor != "")
                {
                    $returnValues = new ReturnController("14002","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $generateUniqueId = new GenerateUUID();
                    $productColorId = $generateUniqueId->getUniqueId();

                    $color = new ProductColor();
                    $color->vendorId = $getLoginId['loginId'];
                    $color->productColorId = $productColorId;
                    $color->productColor = $request->input('productColor');
                    $color->save();

                    if(!$color->save())
                    {
                        $returnValues = new ReturnController("14003","FAILURE","");
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                    else
                    {
                        $returnValues = new ReturnController("14000","SUCCESS","");
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                }
            }

        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }

    protected function validateAddColor(Request $request)
    {
        $rules = array(
            'productColor' => 'required');

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails())
            return false;
        else
            return true;
    }
    public function listAll (Request $request)
    {
        $getDefaultVariables = new Configuration();
        $paginationCount = $getDefaultVariables->getPaginationcount();

        $getLoginId = Login::where('remember_token',$request->input('token'))->first();

        $filterType = $request->input('filterType');
        switch ($filterType)
        {
            case "0":$authenticate = new AuthUser();
                $authenticateUser  = $authenticate->authenticateUser();
                if($authenticateUser == 400)
                {

                    $listColorCount = ProductColor::where('vendorId',$getLoginId['loginId'])->count();

                    if($listColorCount <= $paginationCount)
                    {
                        $listColor = ProductColor::where('vendorId',$getLoginId['loginId'])->orderby('id','desc')->get();

                        if($listColor == null || $listColor == "")
                        {
                            $returnValues = new ReturnController("15002","FAILURE","");
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                        else
                        {
                            $data = [
                                "lastPage" => "NULL",
                                "data" => $listColor];

                            $returnValues = new ReturnController("15000","SUCCESS",$data);
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                    }
                    else
                    {
                        $listColor = ProductColor::where('vendorId',$getLoginId['loginId'])->orderby('id','desc')->paginate($paginationCount);

                        if($listColor == null || $listColor == "")
                        {
                            $returnValues = new ReturnController("15002","FAILURE","");
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                        else
                        {
                            $data=[
                                "total" => $listColor->total(),
                                "nextPageUrl" => $listColor->nextPageUrl(),
                                "previousPageUrl" => $listColor->previousPageUrl(),
                                "currentPage" => $listColor->currentPage(),
                                "lastPage" => $listColor->lastPage(),
                                "data" => $listColor
                            ];

                            $returnValues = new ReturnController("15000","SUCCESS",$data);
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                    }

                }
                else
                {
                    switch($authenticateUser)
                    {
                        case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                            $return = $returnValues->returnValues();
                            return $return;
                            break;
                        case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                            $return = $returnValues->returnValues();
                            return $return;
                            break;
                        case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                            $return = $returnValues->returnValues();
                            return $return;
                            break;
                        case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                            $return = $returnValues->returnValues();
                            return $return;
                            break;
                    }
                }
                break;
            case "1":$listColor = ProductColor::where('vendorId',$getLoginId['loginId'])->orderby('id','desc')->get();
                if($listColor == null || $listColor == "")
                {
                    $returnValues = new ReturnController("15002","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $returnValues = new ReturnController("15000","SUCCESS",$listColor);
                    $return = $returnValues->returnValues();
                    return $return;
                }
                break;
            case "2":$listColor = ProductColor::where('vendorId',$request->input('vendorId'))->orderby('id','desc')->get();
                if($listColor == null || $listColor == "")
                {
                    $returnValues = new ReturnController("15002","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $returnValues = new ReturnController("15000","SUCCESS",$listColor);
                    $return = $returnValues->returnValues();
                    return $return;
                }
                break;

        }
    }

    public function listAllColorForUsers (Request $request)
    {
        $listColor = ProductColor::where('vendorId',$request->input('vendorId'))->orderby('id','desc')->get();
        if($listColor == null || $listColor == "")
        {
            $returnValues = new ReturnController("15002","FAILURE","");
            $return = $returnValues->returnValues();
            return $return;
        }
        else
        {
            $data = [
                "lastPage" => "NULL",
                "data" => $listColor];
            $returnValues = new ReturnController("15000","SUCCESS",$data);
            $return = $returnValues->returnValues();
            return $return;
        }
    }

    public function update (Request $request)
    {
        $authenticate = new AuthUser();
        $authenticateUser  = $authenticate->authenticateUser();
        if($authenticateUser == 400)
        {
            $getLoginId = Login::where('remember_token',$request->input('token'))->first();

            $flagValidateInputs = $this->validateUpdateColor($request);
            if($flagValidateInputs == false)
            {
                $returnValues = new ReturnController("16001","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $updateColor = ProductColor::where('vendorId',$getLoginId['loginId'])
                                            ->where('productColorId',$request->input('productColorId'))
                                          ->update(['productColor' => $request->input('productColor')]);
                if($updateColor == 0)
                {
                    $returnValues = new ReturnController("16002","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $returnValues = new ReturnController("16000","SUCCESS","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
            }

        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }
    protected function validateUpdateColor(Request $request)
    {
        $rules = array(
            'productColorId' => 'required');

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails())
            return false;
        else
            return true;
    }

    public function delete(Request $request)
    {
        $authenticate = new AuthUser();
        $authenticateUser  = $authenticate->authenticateUser();
        if($authenticateUser == 400)
        {
            $flagValidateInputs = $this->validateUpdateColor($request);
            if($flagValidateInputs == false)
            {
                $returnValues = new ReturnController("17001","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $deleteColor = ProductColor::where('productColorId',$request->input('productColorId'))->delete();
                if($deleteColor < 0)
                {
                    $returnValues = new ReturnController("17002","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $returnValues = new ReturnController("17000","SUCCESS","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
            }

        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }
    public function listUserAddress(Request $request)
    {
        $authenticate=new AuthUser();
        $authenticateUser = $authenticate->authenticateUser();
        if ($authenticateUser == "400")
        {
            $getLoginId = Login::where('remember_token',$request->input('token'))->first();

            $checkEmailId = Login::where('emailId',$getLoginId['emailId'])->first();
            if($checkEmailId == null || $checkEmailId == "")
            {
                $returnValues = new ReturnController("31001","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            $userDetails = UserAddress::where('userId',$getLoginId['loginId'])->get();
            if($userDetails == null || $userDetails == "")
            {
                $returnValues = new ReturnController("31002","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $returnValues = new ReturnController("31000","SUCCESS",$userDetails);
                $return = $returnValues->returnValues();
                return $return;
            }

        }
        else
        {
            switch ($authenticateUser)
            {
                case "404":
                    $returnValues = new ReturnController("404", "FAILURE", "INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":
                    $returnValues = new ReturnController("405", "FAILURE", "TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":
                    $returnValues = new ReturnController("406", "FAILURE", "INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":
                    $returnValues = new ReturnController("407", "FAILURE", "TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }

    }

}
