<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Category;
use App\Complaint;
use App\Http\Middleware\AuthUser;
use App\Login;
use App\Product;
use App\ProductColor;
use App\SubCategory;
use App\Suggestion;
use Illuminate\Http\Request;

use App\Http\Requests;

class VendorDashboardController extends Controller
{
    private $totalCategories;
    private $totalSubCategories;
    private $totalProducts;
    private $totalOrders;
    private $totalColors;
    private $totalComplaintsCount;
    private $totalSuggestionsCount;
    private $vendorId;


    public function getData(Request $request)
    {
        $authenticate = new AuthUser();
        $authenticateUser = $authenticate->authenticateUser();
        if($authenticateUser == "400")
        {
            $getLoginId = Login::where('remember_token',$request->input('token'))->first();
            $this->vendorId = $getLoginId['loginId'];

            $this->totalCategories = $this->getCategoryCount($request);
            $this->totalSubCategories = $this->getSubCategoryCount($request);
            $this->totalProducts = $this->getProductsCount($request);
            $this->totalOrders = $this->getOrderCount($request);
            $this->totalColors = $this->getColorCount($request);
            $this->totalComplaintsCount = $this->getComplaintCount($request);
            $this->totalSuggestionsCount = $this->getSuggestionCount($request);

            $dashboardData = ["totalCategoriesCount" => $this->totalCategories,
                "totalSubCategoriesCount" => $this->totalSubCategories,
                "totalProductsCount" => $this->totalProducts,
                "totalOrdersCount" => $this->totalOrders,
                "totalComplaintsCount" => $this->totalComplaintsCount,
                "totalSuggestionsCount" => $this->totalSuggestionsCount,
                "totalColorsCount" => $this->totalColors];

            $returnValues = new ReturnController("10000","SUCCESS",$dashboardData);
            $return = $returnValues->returnValues();
            return $return;
        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }

    public function getCategoryCount(Request $request)
    {
        $categoryCount = Category::where('vendorId',$this->vendorId )->count();
        if($categoryCount == "" || $categoryCount == null)
            return $categoryCount = 0;
        else
            return $categoryCount;
    }
    public function getSubCategoryCount(Request $request)
    {
        $subCategoryCount = SubCategory::where('vendorId',$this->vendorId )->count();
        if($subCategoryCount == "" || $subCategoryCount == null)
            return $subCategoryCount = 0;
        else
            return $subCategoryCount;
    }
    public function getProductsCount(Request $request)
    {
        $productsCount = Product::where('vendorId',$this->vendorId )->count();
        if($productsCount == "" || $productsCount == null)
            return $productsCount = 0;
        else
            return $productsCount;
    }
    public function getOrderCount(Request $request)
    {
        $ordersCount = Cart::where('vendorId',$this->vendorId )->count();
        if($ordersCount == "" || $ordersCount == null)
            return $ordersCount = 0;
        else
            return $ordersCount;
    }
    public function getColorCount(Request $request)
    {
        $colorCount = ProductColor::where('vendorId',$this->vendorId )->count();
        if($colorCount == "" || $colorCount == null)
            return $colorCount = 0;
        else
            return $colorCount;
    }
    public function getComplaintCount(Request $request)
    {
        $complaintCount = Complaint::where('vendorId',$this->vendorId )->count();
        if($complaintCount == "" || $complaintCount == null)
            return $complaintCount = 0;
        else
            return $complaintCount;
    }
    public function getSuggestionCount(Request $request)
    {
        $suggestionCount = Suggestion::where('vendorId',$this->vendorId )->count();
        if($suggestionCount == "" || $suggestionCount == null)
            return $suggestionCount = 0;
        else
            return $suggestionCount;
    }
}
