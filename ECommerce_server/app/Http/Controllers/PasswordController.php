<?php

namespace App\Http\Controllers;

use App\ForgotPassword;
use App\Http\Middleware\AuthUser;
use App\Http\Middleware\GenerateUUID;
use App\Http\Middleware\MailEngine;
use App\Login;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class PasswordController extends Controller
{
    public function change(Request $request)
    {
        $authenticate=new AuthUser();
        $authenticateUser = $authenticate->authenticateUser();
        if ($authenticateUser == "400")
        {
            $getLoginId = Login::where('remember_token',$request->input('token'))->first();

            $flagValidateInputs = $this->validateChangePassword($request);
            if($flagValidateInputs == false)
            {
                $returnValues = new ReturnController("39001","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $checkLoginId = Login::where('loginId', $getLoginId['loginId'])->first();
                if($checkLoginId == null || $checkLoginId == "")
                {
                    $returnValues = new ReturnController("39002","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $typeofuser = $checkLoginId['typeofuser'];
                    if($typeofuser == 1)
                    {
                        $changePassword = Login::where('loginId',$getLoginId['loginId'])
                            ->update(['password' => Hash::make($request->input('password'))]);
                        if($changePassword == 0)
                        {
                            $returnValues = new ReturnController("39003","FAILURE","");
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                        else
                        {
                            $returnValues = new ReturnController("39000","SUCCESS","");
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                    }
                    else
                    {
                        $changePassword = Login::where('loginId',$getLoginId['loginId'])
                            ->update(['password' => Hash::make($request->input('password'))]);
                        if($changePassword == 0)
                        {
                            $returnValues = new ReturnController("39003","FAILURE","");
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                        else
                        {
                            $returnValues = new ReturnController("39000","SUCCESS","");
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                    }
                }

            }
        }
        else
        {
            switch ($authenticateUser)
            {
                case "404":
                    $returnValues = new ReturnController("404", "FAILURE", "INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":
                    $returnValues = new ReturnController("405", "FAILURE", "TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":
                    $returnValues = new ReturnController("406", "FAILURE", "INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":
                    $returnValues = new ReturnController("407", "FAILURE", "TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }

    }
    protected function validateChangePassword(Request $request)
    {
        // create the validation rules
        $rules = array('password' => 'required',
            'confirmPassword' => 'required');

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails())
            return false;
        else
            return true;
    }
    public function forgot(Request $request)
    {
        $flagValidateInputs = $this->validateForgotPassword($request);
        if($flagValidateInputs == false)
        {
            $returnValues = new ReturnController("40001","FAILURE","");
            $return = $returnValues->returnValues();
            return $return;
        }
        else
        {
            $registerEmailid = Login::where('emailId',$request->input('emailId'))
                                    ->where('typeofuser',$request->input('typeofuser'))
                                    ->first();

            if($registerEmailid == null || $registerEmailid =="" || count($registerEmailid) < 0)
            {
                $returnValues = new ReturnController("40002","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {

                $typeofuser = $request->input('typeofuser');
                if($typeofuser == 1)
                {
                    $generateUniqueId = new GenerateUUID();
                    $resetToken = $generateUniqueId->generate32bitId();

                    $forgotPassword = new ForgotPassword();
                    $forgotPassword->emailId = $request->input('emailId');
                    $forgotPassword->resettoken = $resetToken;
                    $forgotPassword->save();
                    if (!$forgotPassword->save())
                    {
                        $returnValues = new ReturnController("40003", "FAILURE", "");
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                    else
                    {
                        $typeofuser=1;
                        $mail = new MailEngine();
                        $mail->forgotPassword($request->input('emailId'),$request->input('emailId'), $resetToken,$typeofuser);

                        $returnValues = new ReturnController("40000", "SUCCESS", "");
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                }
                else
                {
                    $generateUniqueId = new GenerateUUID();
                    $resetToken = $generateUniqueId->generate32bitId();

                    $forgotPassword = new ForgotPassword();
                    $forgotPassword->emailId = $request->input('emailId');
                    $forgotPassword->resettoken = $resetToken;
                    $forgotPassword->save();
                    if (!$forgotPassword->save())
                    {
                        $returnValues = new ReturnController("40003", "FAILURE", "");
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                    else
                    {
                        $typeofuser=2;
                        $mail = new MailEngine();
                        $mail->forgotPasswordForUser($request->input('emailId'),$request->input('emailId'), $resetToken,$typeofuser);

                        $returnValues = new ReturnController("40000", "SUCCESS", "");
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                }

            }
        }
    }

    private function validateForgotPassword(Request $request)
    {
        $rules = array(
            'emailId'  => 'required',
            'typeofuser' => 'required');

        $validator = Validator::make(\Input::all(), $rules);
        if($validator->fails())
            return false;
        else
            return true;
    }
    public function reset(Request $request)
    {
        $flagValidateStatus = $this->validateResetPassword($request);
        if ($flagValidateStatus == false)
        {
            $returnValues = new ReturnController("41001", "FAILURE", "");
            $return = $returnValues->returnValues();
            return $return;
        }
        else
        {
            $registerEmailId = Login::where('emailId',$request->input('emailId'))
                                    ->where('typeofuser',$request->input('typeofuser'))
                                    ->first();
            if($registerEmailId == null || $registerEmailId == "")
            {
                $returnValues = new ReturnController("41002", "FAILURE", "");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $typeofuser = $request->input('typeofuser');
                if($typeofuser == 1)
                {
                    $checkResetToken = ForgotPassword::where('resetToken',$request->input('resetToken'))
                        ->where('emailId',$request->input('emailId'))->first();

                    if($checkResetToken == null || $checkResetToken == "")
                    {
                        $returnValues = new ReturnController("41003", "FAILURE", "");
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                    else
                    {
                        $updatePassword = Login::where('emailId',$registerEmailId['emailId'])
                            ->where('typeofuser',1)
                            ->update(['password' => Hash::make($request->input('password')) ]);

                        if($updatePassword == 0)
                        {
                            $returnValues = new ReturnController("41004", "FAILURE", "");
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                        else
                        {
                            $returnValues = new ReturnController("41000", "SUCCESS", "");
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                    }

                }
                else
                {
                    $checkResetToken = ForgotPassword::where('resetToken',$request->input('resetToken'))
                        ->where('emailId',$request->input('emailId'))->first();

                    if($checkResetToken == null || $checkResetToken == "")
                    {
                        $returnValues = new ReturnController("41003", "FAILURE", "");
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                    else
                    {
                        $updatePassword = Login::where('emailId',$registerEmailId['emailId'])
                            ->where('typeofuser',2)
                            ->update(['password' => Hash::make($request->input('password')) ]);

                        if($updatePassword == 0)
                        {
                            $returnValues = new ReturnController("41004", "FAILURE", "");
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                        else
                        {
                            $returnValues = new ReturnController("41000", "SUCCESS", "");
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                    }
                }
            }

        }

    }

    protected function validateResetPassword(Request $request)
    {
        $rules = array('emailId' => 'required',
            'password' => 'required',
            'typeofuser' => 'required');

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails())
            return false;
        else
            return true;
    }
}
