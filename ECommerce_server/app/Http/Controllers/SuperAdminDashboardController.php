<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Category;
use App\Complaint;
use App\Http\Middleware\AuthUser;
use App\Http\Middleware\GenerateUUID;
use App\Login;
use App\Product;
use App\ProductColor;
use App\SubCategory;
use App\Suggestion;
use App\Vendor;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class SuperAdminDashboardController extends Controller
{
    private $totalVendorsCount;

    public function get(Request $request)
    {
        $authenticate = new AuthUser();
        $authenticateUser = $authenticate->authenticateUser();
        if($authenticateUser == "400")
        {

            $this->totalVendorsCount = $this->getVendorsCount($request);

            $dashboardData = ["totalVendorsCount" => $this->totalVendorsCount
            ];

            $returnValues = new ReturnController("56000","SUCCESS",$dashboardData);
            $return = $returnValues->returnValues();
            return $return;
        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }

    public function getVendorsCount(Request $request)
    {
        $vendorCount = Login::where('typeofuser',1)->count();
        if($vendorCount == "" || $vendorCount == null)
            return $vendorCount = 0;
        else
            return $vendorCount;
    }
}
