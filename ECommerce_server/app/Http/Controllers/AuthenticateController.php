<?php

namespace App\Http\Controllers;

use App\Cart;
use App\EndUser;
use App\Http\Middleware\Configuration;
use App\Login;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Log;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;

class AuthenticateController extends Controller
{
    public function index()
    {
        // TODO: show users
    }

    public function authenticate(Request $request)
    {
        $startTime = microtime();
        $credentials = $request->only('emailId', 'password');

        $getDefaultVariables = new Configuration();
        $DEFAULT_PATH = $getDefaultVariables->getDefaultPaths();

        $checkUser =  User::where('emailId', $request->input('emailId'))
            ->where('typeofuser',$request->input('typeofuser'))->first();

        if($checkUser == "" || $checkUser == null)
        {

            $returnValues = new ReturnController("1001", "FAILURE", "");
            $return = $returnValues->returnValues();
            return $return;
        }
        else
        {
            switch($checkUser['status'])
            {
                case "0":$returnValues = new ReturnController("1002", "FAILURE", "");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "2":$returnValues = new ReturnController("1003", "FAILURE", "");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case"3":$returnValues = new ReturnController("1004", "FAILURE", "");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }

        }

        try {
            // verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                $returnValues = new ReturnController("401", "FAILURE", "");
                $return = $returnValues->returnValues();
                return $return;
            }
        } catch (JWTException $e) {
            // something went wrong
            $returnValues = new ReturnController("500", "FAILURE", "");
            $return = $returnValues->returnValues();
            return $return;
        }

        // if no errors are encountered we can return a JWT
        $checkCredientials = User::where('emailId', $request->input('emailId'))
                                    ->where('typeofuser',$request->input('typeofuser'))
                                    ->where('status',1)
                                    ->first();

        if($checkCredientials == "" || $checkCredientials == null)
        {

            $returnValues = new ReturnController("1001", "FAILURE", "");
            $return = $returnValues->returnValues();
            return $return;
        }
        $profilePicturePath = '';
        $typeOfUser = '';
        $status = $checkCredientials['status'];
        $name = $checkCredientials['name'];
        $emailId = $checkCredientials['emailId'];
        $loginId = $checkCredientials['loginId'];
        $productDetails = '';

        $typeOfUser = $checkCredientials['typeofuser'];

        switch ($typeOfUser)
        {
            case "0":$profilePicturePath = $DEFAULT_PATH."defaultImages/avatar.png";
            break;
            case "1":$profilePicturePath = $DEFAULT_PATH."defaultImages/avatar.png";
            break;
            case "2":$userDetails = EndUser::where('userEmailId',$request->input('emailId'))->first();
                $userProfilePicturePath = $userDetails['userProfilePicturePath'];
                if($userProfilePicturePath == 'N/A')
                {
                    $profilePicturePath = $DEFAULT_PATH."defaultImages/avatar.png";
                }
                else
                {
                    $profilePicturePath = $userProfilePicturePath;
                }
            break;
        }
        $getCartDetails = Cart::where('cartId',$checkCredientials['loginId'])
                              ->where('productStatus',0)
                              ->first();
        $productDetails = $getCartDetails['productDetails'];
        $token = compact('token');
        $data = [
            "token"=>$token,
            "typeofuser" => $typeOfUser,
            "loginId" => $loginId,
            "status" => $status,
            "name" => $name,
            "emailId" => $emailId,
            "cartDetails" => $productDetails,
            "profilePicturePath" => $profilePicturePath];

        $updateToken = User::where('emailId',$request->input('emailId'))
                            ->where('typeofuser',$request->input('typeofuser'))
                            ->update(['remember_token' => $token['token']]);



        $endTime = microtime();
        $duration = $endTime - $startTime;
        Log::Info("Duration time taken to login".$duration);
        $returnValues = new ReturnController("1000", "SUCCESS", $data);
        $return = $returnValues->returnValues();
        return $return->withCookie(cookie('ecommerce_session','true',60,"","",false,true));


    }

}
