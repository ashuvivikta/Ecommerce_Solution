<?php

namespace App\Http\Controllers;

use App\Http\Middleware\AuthUser;
use App\Http\Middleware\Configuration;
use App\Http\Middleware\GenerateUUID;
use App\Http\Middleware\MailEngine;
use App\Login;
use App\MainPage;
use App\Suggestion;
use App\Vendor;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class SuggestionController extends Controller
{
    public function add (Request $request)
    {
        $flagValidateInputs = $this->validateAddSuggestion($request);
        if($flagValidateInputs == false)
        {
            $returnValues = new ReturnController("49001","FAILURE","");
            $return = $returnValues->returnValues();
            return $return;
        }
        else
        {
            $generateUniqueId = new GenerateUUID();
            $suggestionId = $generateUniqueId->getUniqueId();

            $suggestion = new Suggestion();
            $suggestion->vendorId = $request->input('vendorId');
            $suggestion->suggestionId = $suggestionId;
            $suggestion->endUserName = $request->input('endUserName');
            $suggestion->endUserEmailId = $request->input('endUserEmailId');
            $suggestion->suggestion = $request->input('suggestion');
            $suggestion->adminReply = "N/A";
            if(!$suggestion->save())
            {
                $returnValues = new ReturnController("49002","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $userName = $request->input('endUserName');
                $userEmailId = $request->input('endUserEmailId');
                $suggestion = $request->input('suggestion');

                $adminEmailId = Vendor::where('vendorId',$request->input('vendorId'))->first();

                $getVendorLogo = MainPage::where('vendorId',$request->input('vendorId'))->first();
                $vendorLogo = $getVendorLogo['vendorLogo'];

                $mail = new MailEngine();
                $mail->suggestionRecieved($adminEmailId['vendorEmailId'],$adminEmailId['name'],$suggestionId,$suggestion,$vendorLogo);

                $mail = new MailEngine();
                $mail->userSuggestion($userEmailId,$userName,$suggestionId,$suggestion,$vendorLogo);

                $returnValues = new ReturnController("49000","SUCCESS","");
                $return = $returnValues->returnValues();
                return $return;
            }

        }
    }

    protected function validateAddSuggestion(Request $request)
    {
        $rules = array(
            'endUserName' => 'required',
            'endUserEmailId' => 'required',
            'suggestion' => 'required');

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails())
            return false;
        else
            return true;
    }
    public function listAll (Request $request)
    {
        $getDefaultVariables = new Configuration();
        $paginationCount = $getDefaultVariables->getPaginationcount();

        $getLoginId = Login::where('remember_token',$request->input('token'))->first();


        $filterType = $request->input('filterType');
        switch ($filterType)
        {
            case "0":$authenticate = new AuthUser();
                $authenticateUser  = $authenticate->authenticateUser();
                if($authenticateUser == 400)
                {
                    $listSuggestionCount = Suggestion::where('vendorId',$getLoginId['loginId'])->count();

                    if($listSuggestionCount <= $paginationCount)
                    {
                        $listSuggestion = Suggestion::where('vendorId',$getLoginId['loginId'])->orderby('id','desc')->get();

                        if($listSuggestion == null || $listSuggestion == "")
                        {
                            $returnValues = new ReturnController("50002","FAILURE","");
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                        else
                        {
                            $data = [
                                "lastPage" => "NULL",
                                "data" => $listSuggestion];

                            $returnValues = new ReturnController("50000","SUCCESS",$data);
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                    }
                    else
                    {
                        $listSuggestion = Suggestion::where('vendorId',$getLoginId['loginId'])->orderby('id','desc')->paginate($paginationCount);

                        if($listSuggestion == null || $listSuggestion == "")
                        {
                            $returnValues = new ReturnController("50002","FAILURE","");
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                        else
                        {
                            $data=[
                                "total" => $listSuggestion->total(),
                                "nextPageUrl" => $listSuggestion->nextPageUrl(),
                                "previousPageUrl" => $listSuggestion->previousPageUrl(),
                                "currentPage" => $listSuggestion->currentPage(),
                                "lastPage" => $listSuggestion->lastPage(),
                                "data" => $listSuggestion
                            ];

                            $returnValues = new ReturnController("50000","SUCCESS",$data);
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                    }

                }
                else
                {
                    switch($authenticateUser)
                    {
                        case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                            $return = $returnValues->returnValues();
                            return $return;
                            break;
                        case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                            $return = $returnValues->returnValues();
                            return $return;
                            break;
                        case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                            $return = $returnValues->returnValues();
                            return $return;
                            break;
                        case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                            $return = $returnValues->returnValues();
                            return $return;
                            break;
                    }
                }
                break;
            case "1":$listSuggestion = Suggestion::where('vendorId',$getLoginId['loginId'])->get();
                if($listSuggestion == null || $listSuggestion == "")
                {
                    $returnValues = new ReturnController("50002","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $returnValues = new ReturnController("50000","SUCCESS",$listSuggestion);
                    $return = $returnValues->returnValues();
                    return $return;
                }
                break;
            case "2":$listSuggestion = Suggestion::where('vendorId',$request->input('vendorId'))->get();
                if($listSuggestion == null || $listSuggestion == "")
                {
                    $returnValues = new ReturnController("50002","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $returnValues = new ReturnController("50000","SUCCESS",$listSuggestion);
                    $return = $returnValues->returnValues();
                    return $return;
                }
                break;

        }
    }
    public function update (Request $request)
    {
        $getLoginId = Login::where('remember_token',$request->input('token'))->first();

        $authenticate = new AuthUser();
        $authenticateUser  = $authenticate->authenticateUser();
        if($authenticateUser == 400)
        {
            $flagValidateInputs = $this->validateUpdateSuggestion($request);
            if($flagValidateInputs == false)
            {
                $returnValues = new ReturnController("51001","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $getSuggestionDetails = Suggestion::where('suggestionId',$request->input('suggestionId'))
                    ->first();

                $updateSuggestion = Suggestion::where('suggestionId',$request->input('suggestionId'))
                    ->update(['adminReply' => $request->input('adminReply')]);

                if($updateSuggestion == 0)
                {
                    $returnValues = new ReturnController("51002","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $userName = $getSuggestionDetails['endUserName'];
                    $userEmailId = $getSuggestionDetails['endUserEmailId'];
                    $suggestionId = $request->input('suggestionId');
                    $suggestion = $getSuggestionDetails['suggestion'];
                    $adminReply = $request->input('adminReply');

                    $getVendorLogo = MainPage::where('vendorId',$getLoginId['loginId'])->first();
                    $vendorLogo = $getVendorLogo['vendorLogo'];


                    $mail = new MailEngine();
                    $mail->suggestionReply($userEmailId,$userName,$suggestionId,$suggestion,$adminReply,$vendorLogo);

                    $returnValues = new ReturnController("51000","SUCCESS","");
                    $return = $returnValues->returnValues();
                    return $return;
                }

            }

        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }

    protected function validateUpdateSuggestion(Request $request)
    {
        $rules = array(
            'suggestionId' => 'required');

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails())
            return false;
        else
            return true;
    }
}
