<?php

namespace App\Http\Middleware;

use Closure;

class Configuration
{
    public function getDefaultPaths()
    {
        $DEV_MODE = 0;
        $DEV_PATH = "http://192.168.1.2:8001/";
        $DEFAULT_PATH = "https://shoppingmall.vivikta.in/";

        if ($DEV_MODE == 1) {
            $DEFAULT_PATH = $DEV_PATH;
        }
        return $DEFAULT_PATH;
    }
    public function getDefaultS3Path()
    {
        $S3_PATH = "https://s3-us-west-2.amazonaws.com/vivikta/";
        return $S3_PATH;
    }
    public function getDefaultProjectPath()
    {
        $PROJECT_PATH = "ecommerce";
        return $PROJECT_PATH;
    }

    public function getPaginationcount()
    {
        $paginationCount = 10;
        return $paginationCount;
    }

}
