<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Closure;

class MailEngine
{
    private $recepient;
    public function userRegister($to,$userId)
    {
        $this->recepient = $to;
        $data = [
            "id" => $userId
        ];
        Mail::send('emails.userRegister',$data, function($message)
        {
            $message->from('donotreply_anantya@vivikta.in');
            $message->to($this->recepient)->subject('User Account Creation');
        });
    }
    public function userBought($to,$productId,$userName,$orderDate,$userAddress,$status,$vendorLogo,$vendorDomainName)
    {
        $this->recepient = $to;
        $data = [
            "productId" => $productId,
            "userName" => $userName,
            "orderDate" => $orderDate,
            "userAddress" => $userAddress,
            "status" => $status,
            "vendorLogo" => $vendorLogo,
            "vendorDomainName" => $vendorDomainName
        ];
        Mail::send('emails.userBought',$data, function($message)
        {
            $message->from('donotreply_anantya@vivikta.in');
            $message->to($this->recepient)->subject('Product Bought Successfully');
        });
    }
    public function paymentRecieved($to,$productId,$userName,$orderDate,$userAddress,$status,$vendorLogo,$vendorDomainName)
    {
        $this->recepient = $to;
        $data = [
            "productId" => $productId,
            "userName" => $userName,
            "orderDate" => $orderDate,
            "userAddress" => $userAddress,
            "status" => $status,
            "vendorLogo" => $vendorLogo,
            "vendorDomainName" => $vendorDomainName
        ];
        Mail::send('emails.userBought',$data, function($message)
        {
            $message->from('donotreply_anantya@vivikta.in');
            $message->to($this->recepient)->subject('Payment Recieved Successfully');
        });
    }
    public function shippingInProgress($to,$productId,$userName,$orderDate,$userAddress,$status,$vendorLogo,$vendorDomainName)
    {
        $this->recepient = $to;
        $data = [
            "productId" => $productId,
            "userName" => $userName,
            "orderDate" => $orderDate,
            "userAddress" => $userAddress,
            "status" => $status,
            "vendorLogo" => $vendorLogo,
            "vendorDomainName" => $vendorDomainName
        ];
        Mail::send('emails.shippingInProgress',$data, function($message)
        {
            $message->from('donotreply_anantya@vivikta.in');
            $message->to($this->recepient)->subject('Shipping In Progress');
        });
    }
    public function orderShipped($to,$productId,$userName,$orderDate,$userAddress,$status,$vendorLogo,$vendorDomainName)
    {
        $this->recepient = $to;
        $data = [
            "productId" => $productId,
            "userName" => $userName,
            "orderDate" => $orderDate,
            "userAddress" => $userAddress,
            "status" => $status,
            "vendorLogo" => $vendorLogo,
            "vendorDomainName" => $vendorDomainName
        ];
        Mail::send('emails.orderShipped',$data, function($message)
        {
            $message->from('donotreply_anantya@vivikta.in');
            $message->to($this->recepient)->subject('Product Shipped Successfully');
        });
    }
    public function orderRejected($to,$productId,$userName,$orderDate,$userAddress,$status,$vendorLogo,$vendorDomainName)
    {
        $this->recepient = $to;
        $data = [
            "productId" => $productId,
            "userName" => $userName,
            "orderDate" => $orderDate,
            "userAddress" => $userAddress,
            "status" => $status,
            "vendorLogo" => $vendorLogo,
            "vendorDomainName" => $vendorDomainName
        ];
        Mail::send('emails.orderRejected',$data, function($message)
        {
            $message->from('donotreply_anantya@vivikta.in');
            $message->to($this->recepient)->subject('Order Rejected');
        });
    }
    public function userComplaint($to,$productId,$userName,$complaintId,$userComplaint,$vendorLogo)
    {
        $this->recepient = $to;
        $data = [
            "productId" => $productId,
            "userName" => $userName,
            "complaintId" => $complaintId,
            "userComplaint" => $userComplaint,
            "vendorLogo" => $vendorLogo
        ];
        Mail::send('emails.userComplaint',$data, function($message)
        {
            $message->from('donotreply_anantya@vivikta.in');
            $message->to($this->recepient)->subject('User Complaint');
        });
    }
    public function userSuggestion($to,$userName,$suggestionId,$suggestion,$vendorLogo)
    {
        $this->recepient = $to;
        $this->recepient = $to;
        $data = [
            "userName" => $userName,
            "suggestionId" => $suggestionId,
            "suggestion" => $suggestion,
            "vendorLogo" => $vendorLogo
        ];
        Mail::send('emails.userSuggestion',$data, function($message)
        {
            $message->from('donotreply_anantya@vivikta.in');
            $message->to($this->recepient)->subject('User Suggestion');
        });
    }
    public function complaintReply($to,$productId,$userName,$complaintId,$userComplaint,$adminAnswer,$vendorLogo)
    {
        $this->recepient = $to;
        $data = [
            "productId" => $productId,
            "userName" => $userName,
            "complaintId" => $complaintId,
            "userComplaint" => $userComplaint,
            "adminAnswer" => $adminAnswer,
            "vendorLogo" => $vendorLogo
        ];
        Mail::send('emails.complaintReply',$data, function($message)
        {
            $message->from('donotreply_anantya@vivikta.in');
            $message->to($this->recepient)->subject('Complaint Reply');
        });
    }
    public function suggestionReply($to,$userName,$suggestionId,$suggestion,$adminReply,$vendorLogo)
    {
        $this->recepient = $to;
        $data = [
            "userName" => $userName,
            "suggestionId" => $suggestionId,
            "suggestion" => $suggestion,
            "adminReply" => $adminReply,
            "vendorLogo" => $vendorLogo
        ];
        Mail::send('emails.suggestionReply',$data, function($message)
        {
            $message->from('donotreply_anantya@vivikta.in');
            $message->to($this->recepient)->subject('Suggestion Reply');
        });
    }
    public function orderRecieved($to,$productId,$userName,$orderDate,$userAddress,$status,$vendorLogo)
    {
        $this->recepient = $to;
        $data = [
            "productId" => $productId,
            "userName" => $userName,
            "orderDate" => $orderDate,
            "userAddress" => $userAddress,
            "status" => $status,
            "vendorLogo" => $vendorLogo
        ];
        Mail::send('emails.orderRecieved',$data, function($message)
        {
            $message->from('donotreply_anantya@vivikta.in');
            $message->to($this->recepient)->subject('Product Bought Successfully');
        });
    }
    public function complaintRecieved($to,$productId,$userName,$complaintId,$userComplaint,$vendorLogo)
    {
        $this->recepient = $to;
        $data = [
            "productId" => $productId,
            "userName" => $userName,
            "complaintId" => $complaintId,
            "userComplaint" => $userComplaint,
            "vendorLogo" => $vendorLogo
        ];
        Mail::send('emails.complaintRecieved',$data, function($message)
        {
            $message->from('donotreply_anantya@vivikta.in');
            $message->to($this->recepient)->subject('User Complaint');
        });
    }
    public function suggestionRecieved($to,$userName,$suggestionId,$suggestion,$vendorLogo)
    {
        $this->recepient = $to;
        $data = [
            "userName" => $userName,
            "suggestionId" => $suggestionId,
            "suggestion" => $suggestion,
            "vendorLogo" => $vendorLogo
        ];
        Mail::send('emails.suggestionRecieved',$data, function($message)
        {
            $message->from('donotreply_anantya@vivikta.in');
            $message->to($this->recepient)->subject('User Suggestion');
        });
    }
    public function forgotPassword($to,$emailId,$resetToken,$typeofuser)
    {
        $this->recepient = $to;
        $data = [
            "emailId" => $emailId,
            "resetToken" => $resetToken,
            "typeofuser" => $typeofuser
        ];
        Mail::send('emails.forgotPassword',$data, function($message)
        {
            $message->from('donotreply_vlearn@vivikta.in');
            $message->to($this->recepient)->subject('Forgot Password');
        });
    }
    public function forgotPasswordForUser($to,$emailId,$resetToken,$typeofuser)
    {
        $this->recepient = $to;
        $data = [
            "emailId" => $emailId,
            "resetToken" => $resetToken,
            "typeofuser" => $typeofuser
        ];
        Mail::send('emails.forgotPasswordForUser',$data, function($message)
        {
            $message->from('donotreply_vlearn@vivikta.in');
            $message->to($this->recepient)->subject('Forgot Password');
        });
    }
    public function vendorRegisterMail($to,$vendorId,$vendorName)
    {
        $this->recepient = $to;
        $data = [
            "vendorId" => $vendorId,
            "vendorName" => $vendorName
        ];
        Mail::send('emails.vendorRegisterMail',$data, function($message)
        {
            $message->from('donotreply_medha@vivikta.in');
            $message->to($this->recepient)->subject('Account Confirmation');
        });
    }
    public function vendorCreatePasswordMail($to,$vendorId,$vendorName)
    {
        $this->recepient = $to;
        $data = [
            "vendorId" => $vendorId,
            "vendorName" => $vendorName
        ];
        Mail::send('emails.vendorCreatePasswordMail',$data, function($message)
        {
            $message->from('donotreply_medha@vivikta.in');
            $message->to($this->recepient)->subject('Password Created Successfully!');
        });
    }
    public function vendorApprovedMail($to,$vendorName)
    {
        $this->recepient = $to;
        $data = [
            "vendorName" => $vendorName
        ];
        Mail::send('emails.vendorApprovedMail',$data, function($message)
        {
            $message->from('donotreply_medha@vivikta.in');
            $message->to($this->recepient)->subject('Account Approved');
        });
    }
    public function vendorDisabledMail($to,$vendorName)
    {
        $this->recepient = $to;
        $data = [
            "vendorName" => $vendorName
        ];
        Mail::send('emails.vendorDisabledMail',$data, function($message)
        {
            $message->from('donotreply_medha@vivikta.in');
            $message->to($this->recepient)->subject('Account Disabled');
        });
    }
    public function vendorRejectedMail($to,$vendorName)
    {
        $this->recepient = $to;
        $data = [
            "vendorName" => $vendorName
        ];
        Mail::send('emails.vendorRejectedMail',$data, function($message)
        {
            $message->from('donotreply_medha@vivikta.in');
            $message->to($this->recepient)->subject('Account Rejected');
        });
    }

}
