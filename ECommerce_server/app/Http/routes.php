<?php
header('Access-Control-Allow-Origin:*');
header('Access-Control-Allow-Methods:GET,POST,PUT,DELETE');

Route::get('/', function () {
    return view('welcome');
});
Route::group(['prefix' => 'login'],function()
{
    Route::post('authenticate','AuthenticateController@authenticate');

});
Route::group(['prefix' => 'dashboard'],function()
{
    Route::get('get','SuperAdminDashboardController@get');
    Route::get('getData','VendorDashboardController@getData');
    Route::get('userDashboardData','UserDashboardController@userDashboardData');

});
Route::group(['prefix' => 'vendor'],function()
{
    Route::post('register','VendorController@register');
    Route::post('createPassword','VendorController@createPassword');
    Route::get('listAll','VendorController@listAll');
    Route::put('updateStatus','VendorController@updateStatus');
    Route::get('getVendorId','VendorController@getVendorId');

    Route::delete('clearVendorDetails','VendorRefreshController@clearVendorDetails');

});
Route::group(['prefix' => 'user'],function()
{
    Route::post('register','UserController@register');
    Route::post('createPassword','UserController@createPassword');

    Route::post('addAddress','UserController@addAddress');
    Route::get('listAddress','UserController@listAddress');
    Route::put('updateAddress','UserController@updateAddress');
    Route::delete('deleteAddress','UserController@deleteAddress');

    Route::get('myOrders','UserController@myOrders');

    Route::get('userDashboardData','UserDashboardController@userDashboardData');

});
Route::group(['prefix' => 'category'],function()
{
    Route::post('add','CategoryController@add');
    Route::get('listAll','CategoryController@listAll');
    Route::put('update','CategoryController@update');
    Route::delete('delete','CategoryController@delete');
});
Route::group(['prefix' => 'subcategory'],function()
{
    Route::post('add','SubCategoryController@add');
    Route::get('listSubCategoryForCategory','SubCategoryController@listSubCategoryForCategory');
    Route::get('listSubCategoryForCategoryId','SubCategoryController@listSubCategoryForCategoryId');
    Route::get('listAll','SubCategoryController@listAll');
    Route::put('update','SubCategoryController@update');
    Route::delete('delete','SubCategoryController@delete');
});
Route::group(['prefix' => 'genders'],function()
{
    Route::post('add','GenderController@add');
    Route::get('listAll','GenderController@listAll');
    Route::put('update','GenderController@update');
    Route::delete('delete','GenderController@delete');
});
Route::group(['prefix' => 'colors'],function()
{
    Route::post('add','ProductColorController@add');
    Route::get('listAll','ProductColorController@listAll');
    Route::put('update','ProductColorController@update');
    Route::delete('delete','ProductColorController@delete');
    Route::get('listAllColorForUsers','ProductColorController@listAllColorForUsers');
});
Route::group(['prefix' => 'mainPage'],function()
{
    Route::post('add','MainPageController@add');
    Route::get('listAll','MainPageController@listAll');
    Route::post('update','MainPageController@update');

});
Route::group(['prefix' => 'profile'],function()
{
    Route::get('listAll','ProfileController@listAll');
    Route::put('update','ProfileController@update');
    Route::put('updateConfiguration','ProfileController@updateConfiguration');

});
Route::group(['prefix' => 'password'],function()
{
    Route::post('change','PasswordController@change');
    Route::post('forgot','PasswordController@forgot');
    Route::post('reset','PasswordController@reset');

});
Route::group(['prefix' => 'cart'],function()
{
    Route::post('add','CartController@add');
    Route::put('buy','CartController@buy');
    Route::get('listCartItemDetails','CartController@listCartItemDetails');
    Route::put('updateOrderDetails','CartController@updateOrderDetails');
    Route::post('update','CartController@update');

});
Route::group(['prefix' => 'complaint'],function()
{
    Route::post('add','ComplaintController@add');
    Route::get('listAll','ComplaintController@listAll');
    Route::put('update','ComplaintController@update');
    Route::delete('delete','ComplaintController@delete');
});
Route::group(['prefix' => 'suggestion'],function()
{
    Route::post('add','SuggestionController@add');
    Route::get('listAll','SuggestionController@listAll');
    Route::put('update','SuggestionController@update');
});
Route::group(['prefix' => 'products'],function()
{
    Route::post('add','ProductController@add');
    Route::get('listAll','ProductController@listAll');
    Route::get('listAllUserBoughtProducts','ProductController@listAllUserBoughtProducts');
    Route::get('listProductForId','ProductController@listProductForId');
    Route::get('listAllProductsForSubCategory','ProductController@listAllProductsForSubCategory');
    Route::delete('delete','ProductController@delete');
    Route::get('search','ProductController@search');
    Route::get('subCategorySearch','ProductController@subCategorySearch');

});
Route::group(['prefix' => 'paymentMode'],function()
{
    Route::post('add','PaymetModeController@add');
    Route::get('listAll','PaymetModeController@listAll');
    Route::put('update','PaymetModeController@update');
    Route::delete('delete','PaymetModeController@delete');
});
Route::group(['prefix' => 'metricUnits'],function()
{
    Route::post('add','MetricUnitController@add');
    Route::get('listAll','MetricUnitController@listAll');
    Route::put('update','MetricUnitController@update');
    Route::delete('delete','MetricUnitController@delete');
});

Route::group(['prefix' => 'history'],function()
{
    Route::get('CountProductForId','UserHistoryController@CountProductForId');
});

