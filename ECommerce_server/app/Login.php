<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Login extends Model
{
    protected $table = "login_table";
    protected $fillable = [
        'name', 'emailId', 'password',
    ];
}
