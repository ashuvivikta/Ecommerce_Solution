<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $table = "cart_table";
    protected $fillable = [
        'id'
    ];
}
