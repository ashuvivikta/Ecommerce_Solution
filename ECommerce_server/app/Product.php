<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = "product_table";
    protected $fillable = [
        'id', 'productName'
    ];
}
