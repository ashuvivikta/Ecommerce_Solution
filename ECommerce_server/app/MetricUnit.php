<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MetricUnit extends Model
{
    protected $table = "metric_unit_table";
    protected $fillable = [
        'id', 'paymentName'
    ];
}
