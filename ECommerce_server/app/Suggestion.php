<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Suggestion extends Model
{
    protected $table = "suggestion_table";
    protected $fillable = [
        'id', 'suggestionId'
    ];
}
