<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EndUser extends Model
{
    protected $table = "user_table";
    protected $fillable = [
        'id', 'userName'
    ];
}
