<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentMode extends Model
{
    protected $table = "payment_mode_table";
    protected $fillable = [
        'id', 'paymentName'
    ];
}
