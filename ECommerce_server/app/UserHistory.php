<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserHistory extends Model
{
    protected $table = "user_history";
    protected $attributes = array(
        'count' => 0,
    );
}
