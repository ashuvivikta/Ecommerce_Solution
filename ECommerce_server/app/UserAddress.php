<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAddress extends Model
{
    protected $table = "user_address_table";
    protected $fillable = [
        'id', 'userId'
    ];
}
