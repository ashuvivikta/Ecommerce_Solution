<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ForgotPassword extends Model
{
    protected $table = 'forgot_password_table';
    protected $fillable = [
        'emailId', 'resetToken'
    ];
}
