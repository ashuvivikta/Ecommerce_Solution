<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MainPage extends Model
{
    protected $table = "main_page_table";
    protected $fillable = [
        'id'
    ];
}
