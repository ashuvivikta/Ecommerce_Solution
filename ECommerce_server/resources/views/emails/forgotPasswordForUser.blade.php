<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>E-Commerce</title>
</head>
<body>
<div class="content">
    <div class="content-wrapper">
        <div class="email-content">
            <p>Hey,</p>
            <p class="email-message">Please click the link below to reset your password.:</p>
            <div class="confirm-button">
                <a href="https://shoppingmall.vivikta.in/services/resetPassword.php?resetToken={{$resetToken}}&emailId={{$emailId}}"><button class="btn-primary" type="button">Reset Password</button></a>
            </div>
            <p class="email-message">.</p>
            <p class="email-message support-text">For any support and queries please contact us at support@vivikta.in</p>
            <p class="email-message">Thanks,<br/>E-Commerce Team,<br/>Vivikta Technologies</p>
        </div>
    </div>
</div>
<div class="footer">
    <p class="email-message">Powered by Vivikta Technologies</p>
</div>
</body>
</html>
