<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>E-Commerce System</title>
</head>
<body>
<div class="content">
    <div class="content-wrapper">
        <div class="email-content">
            <p>Hey,</p>
            <p class="email-message">Your password has been created successfully.</p>
            <p class="email-message">Please wait for account to be approved by ecommerce admin,After approval you will get an e-mail to the registered emailid:</p>
            <div class="confirm-button">
                <a href="https://shoppingmall.vivikta.in/assets/login.php"><button class="btn-primary" type="button">Confirm Email Address</button></a>
            </div>
            <p class="email-message support-text">For any support and queries please contact us at support@vivikta.in</p>
            <p class="email-message">Thanks,<br/>E-Commerce Team,<br/>Vivikta Technologies</p>
        </div>
    </div>
</div>
<div class="footer">
    <p class="email-message">Powered by Vivikta Technologies</p>
</div>
</body>
