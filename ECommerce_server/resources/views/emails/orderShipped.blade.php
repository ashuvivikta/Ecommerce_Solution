<!DOCTYPE html>
<html>
<head>
    <title>E-Commerce</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <style>
        html, body {
            height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            width: 100%;
            display: table;
            font-weight: 100;
        }

        .container {
            text-align: center;
            display: table-cell;
        }

        .content {
            text-align: center;
            display: inline-block;
        }

        .title {
            font-size: 96px;
        }
    </style>
</head>
<body>
<div class="container">
    <div class='row'>
        <div class="col-lg-12" style="width: 100%;background-color: #000000;padding: 10px;text-align: left;">
            <img src="{{$vendorLogo}}">
        </div>
    </div>
    <div class="row">
        <div class='col-lg-12' style="padding:30px;">
                    <span style="display: block;text-align: left;">
                        <h3 style="font-size: 20px;font-weight: bolder;"><strong>Hi {{$userName}},</strong></h3>
                        <br/>
                        Your order has been successfully shipped!
                        <br/>
                        you can check the status of your order on {{$vendorDomainName}}
                    </span>
            <div style="background-color: #f5f5f5;color: #000000;margin:20px auto;width: 80%;text-align: left;padding: 20px;">
                <p>Order Id : {{$productId}}</p>
                <p>Date : {{$orderDate}}</p>
                <p>Status : {{$status}}</p>
                <hr/>
                <h4>DELIVERY ADDRESS</h4>
                <p>{{$userAddress}}</p>
            </div>
        </div>
    </div>
    <div class='row'>
        <div class="col-lg-12" style="width: 100%;background-color: #000000;padding: 10px;text-align: center;color: #ffffff;">
            <h6>For any queries please visit www.ecommerce.vivikta.in</h6>
        </div>
    </div>
</div>
</body>
</html>
