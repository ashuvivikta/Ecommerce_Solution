<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>E-Commerce System</title>
</head>
<body>
<div class="content">
    <div class="content-wrapper">
        <div class="email-content">
            <p>Hey,</p>
            <p class="email-message"> Congratulations!.><br/></p>
            <p class="email-message">To protect your privacy please confirm your Email by clicking the address below:</p>
            <div class="confirm-button">
                <a href="https://shoppingmall.vivikta.in/services/createPassword.php?id={{$id}}"><button class="btn-primary" type="button">Confirm Email Address</button></a>
            </div>
            <p class="email-message">On clicking the link above you will be allowed to set a password for your account on ecommerce. You can use this email and the password to login into the ecommerce.</p>
            <p class="email-message support-text">For any support and queries please contact us at support@vivikta.in</p>
            <p class="email-message">Thanks,<br/>E-Commerce Team,<br/>Vivikta Technologies</p>
        </div>
    </div>
</div>
<div class="footer">
    <p class="email-message">Powered by Vivikta Technologies</p>
</div>
</body>
