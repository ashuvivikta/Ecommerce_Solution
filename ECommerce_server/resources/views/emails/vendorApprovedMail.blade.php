<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <title>E-Commerce System</title>
</head>
<body>
<div class="content">
    <div class="content-wrapper">
        <div class="email-content">
            <p>Hey,</p>
            <p class="email-message">Your account has been approved by the e-commerce admin.</p>
            <strong class="email-message">Please redirect your domain name to the below IP Address :</strong><br>
            <strong>34.208.146.55</strong>
            <div class="confirm-button">
                <a href="https://shoppingmall.vivikta.in/assets/login.php"><button class="btn-primary" type="button">Login</button></a>
            </div>
            <p class="email-message support-text">For any support and queries please contact us at support@vivikta.in</p>
            <p class="email-message">Thanks,<br/>E-Commerce Team,<br/>Vivikta Technologies</p>
        </div>
    </div>
</div>
<div class="footer">
    <p class="email-message">Powered by Vivikta Technologies</p>
</div>
</body>
