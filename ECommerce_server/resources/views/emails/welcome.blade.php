<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link href="font-awesome/css/font-awesome.css" rel="stylesheet">
        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
            }

            .container {
                text-align: center;
                display: table-cell;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class='row'>
                <div class="col-lg-12" style="width: 100%;background-color: #000000;padding: 10px;text-align: left;">
                    <img src="img/logo.png">
                </div>
            </div>
            <div class="row">
                <div class='col-lg-12' style="padding:30px;">
                    <span style="display: block;text-align: left;">
                        <h3 style="font-size: 20px;font-weight: bolder;"><strong>Hi User,</strong></h3>
                        <br/>
                        Your order has been successfully placed!
                        <br/>
                        Delivery is on track and we will keep you updated as your order is being packed,shipped and delivered. Meanwhile, you can check the status of your order on www.anantya.com
                    </span>
                    <div style="background-color: #f5f5f5;color: #000000;margin:20px auto;width: 80%;text-align: left;padding: 20px;">
                        <p>Order Id : 123</p>
                        <p>Date : 123</p>
                        <p>Status : 123</p>
                        <hr/>
                        <h4>DELIVERY ADDRESS</h4>
                        <p>Address goes here..</p>
                    </div>
                    <div class="col-lg-3" style="text-align: center;background-color: #f5f5f5;padding: 10px;">
                        <span class="fa-stack fa-lg" style="color: green;display: block;margin:0px auto;">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-check fa-stack-1x fa-inverse"></i>
                        </span>
                        <h4 style="color: #616161;">ORDER PLACED</h4>
                    </div>
                    <div class="col-lg-3" style="text-align: center;background-color: #f5f5f5;padding: 10px;">
                        <span class="fa-stack fa-lg" style="color: #616161;display: block;margin:0px auto;">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-rupee fa-stack-1x fa-inverse"></i>
                        </span>
                        <h4 style="color: #616161;">PAYMENT RECEIVED</h4>
                    </div>
                    <div class="col-lg-3" style="text-align: center;background-color: #f5f5f5;padding: 10px;">
                        <span class="fa-stack fa-lg" style="color: #616161;display: block;margin:0px auto;">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-spinner fa-stack-1x fa-inverse"></i>
                        </span>
                        <h4 style="color: #616161;">SHIPPING IN-PROGRESS</h4>
                    </div>
                    <div class="col-lg-3" style="text-align: center;background-color: #f5f5f5;padding: 10px;">
                        <span class="fa-stack fa-lg" style="color: #616161;display: block;margin:0px auto;">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-ship fa-stack-1x fa-inverse"></i>
                        </span>
                        <h4 style="color: #616161;">SHIPPED</h4>
                    </div>
                </div>
            </div>
             <div class='row'>
                <div class="col-lg-12" style="width: 100%;background-color: #000000;padding: 10px;text-align: center;color: #ffffff;">
                    <h6>For any queries please visit www.anantya.com</h6>
                </div>
            </div>
        </div>
    </body>
</html>
