<?php
    include "sidebar.php";
    include "plugins.php";
    include "header.php";
   /* if(!isset($_COOKIE['anantya_session'])){
        header("Location:login.php");
    }*/
?>
<!DOCTYPE html>
<html>
<head>
  <?php echo loadPlugins() ?>
  <title>ECommerce | Colors</title>
  <script src="js/controllers/colorsController.js"></script>
  <script src="js/controllers/sidemenuController.js"></script>
</head>

<body ng-app="anantya" ng-controller="ColorsController" ng-init="loadColors(0,'')">
  <div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation" ng-controller="SidemenuController">
      <div class="sidebar-collapse">
        <?php echo loadSideBar() ?>
      </div>
    </nav>
    <div id="page-wrapper" class="gray-bg">
      <div class="row border-bottom">
        <?php echo loadHeader() ?>
      </div>
        <div class="row wrapper border-bottom white-bg page-heading">
          <div class="col-sm-4">
            <h2><i class="fa fa-paint-brush fa-2x"></i>&nbsp;&nbsp;Colors</h2>
              <ol class="breadcrumb">
                <li>
                  <a href="index.php">Dashboard</a>
                </li>
                <li class="active">
                  <strong>Colors</strong>
                </li>
              </ol>
          </div>
          <div class="col-sm-8">
            <div class="title-action">
              <button type="button" class="btn btn-primary customButton" data-toggle="modal" data-target="#addColorModal"><i class="fa fa-plus"></i>&nbsp;Colors</button>
            </div>
          </div>
        </div>
        <div class="row ng-cloak" ng-show="colorsPresent" id="colorsList">
          <div class="col-lg-11 card cardLayout">
            <div class="ibox float-e-margins">
              <div class="ibox-content">
                <div class="row">
                  <div class="col-sm-3">
                    <div class="input-group">
                      <input type="text" placeholder="Search" class="input-sm form-control" ng-model="search"> <span class="input-group-btn">
                      <button type="button" class="btn btn-sm btn-info"><i class="fa fa-search"></i></button> </span>
                    </div>
                  </div>
                </div>
                <div class="table-responsive ng-cloak">
                  <table class="table table-striped customTable">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr ng-repeat="color in colors|filter:search">
                        <td>{{$index+1}}</td>
                        <td>{{color.productColor}}</td>
                        <td>
                          <button type="button" class="btn btn-info btn-circle btn-outline customButton"  ng-click="loadColorDetails(color)" data-toggle="modal" data-target="#editColorModal"><i class="fa fa-pencil"></i></button>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div> 
        <div class="text-center ng-cloak" ng-show="showPagination">
            <div class="btn-group">
                <button ng-disabled="currentPage == 1" class="btn btn-white customButton" max-size="maxSize" boundary-links="true" ng-click="loadColors(1,previousPageUrl)"><i class="fa fa-chevron-left"></i></button>
                <button class="btn btn-primary ng-cloak customButton" max-size="maxSize" boundary-links="true">{{currentPage}}/{{lastPage}}</button>
                <button ng-disabled="currentPage == lastPage" class="btn btn-white customButton" max-size="maxSize" boundary-links="true" ng-click="loadColors(1,nextPageUrl)"><i class="fa fa-chevron-right"></i></button>
            </div>   
        </div>     
        <div class="row ng-cloak" ng-hide="colorsPresent" id="noCategoriesInfo">
          <div class="col-lg-12">
            <div class="wrapper wrapper-content">
              <div class="middle-box text-center animated fadeInRightBig">
                <h3 class="font-bold">No colors found</h3>
                  <div class="error-desc">
                    Add Color to view them
                  </div>
              </div>
            </div>
          </div>
        </div>
     
    </div>
  </div>
  <div class="modal inmodal" id="editColorModal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content animated fadeIn customModal">
        <div class="modal-header customModalHeading">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <i class="fa fa-paint-brush fa-3x"></i>
            <h4 class="customModal-title">Update Color</h4>
        </div>
        <div class="modal-body">
          <form name="editColorForm">
            <div class="modal-body">
              <div class="form-group">
                <div class="col-sm-2">
                  <label>Name *</label> 
                </div>
                <div class="col-sm-10">
                  <input type="text" placeholder="Name" class="form-control customInputForm" ng-model="editColorData.name" required>
                </div>
              </div>
            </div>
          </form>   
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-white customButton" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-white customButton" data-dismiss="modal" ng-click="updateColor(editColorData)">Update</button>
        </div>
      </div>
    </div>
  </div>
  <div class="modal inmodal" id="addColorModal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content animated fadeIn customModal">
        <div class="modal-header customModalHeading">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <i class="fa fa-paint-brush fa-3x"></i>
            <h4 class="customModal-title">Add Color</h4>
        </div>
        <div class="modal-body">
          <form name="addColorForm">
            <div class="modal-body">
              <div class="form-group">
                <div class="col-sm-2">
                  <label>Name *</label>
                </div>
                <div class="col-sm-10">
                  <input type="text" placeholder="Name" class="form-control customInputForm" ng-model="colorData.name" required>
                </div>
              </div>
            </div>
          </form>   
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-white customButton" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-white customButton" data-dismiss="modal" ng-click="addNewColor(colorData)">Add</button>
        </div>
      </div>
    </div>
  </div>
</body>
</html>
