<?php
    include "sidebar.php";
    include "plugins.php";
    include "header.php";
   /* if(!isset($_COOKIE['anantya_session'])){
        header("Location:login.php");
    }*/
?>
<!DOCTYPE html>
<html>
<head>
  <?php echo loadPlugins() ?>
  <title>ECommerce | Payment Mode</title>
  <script src="../js/controllers/superAdminVendorsController.js"></script>
  <script src="../js/controllers/sidemenuController.js"></script>
  <script src="../js/controllers/superAdminMerticsUnits.js"></script>
</head>

<body ng-app="anantya" ng-controller="MetricUnitsController" ng-init="loadUnits(0,'')">
  <div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation" ng-controller="SidemenuController">
      <div class="sidebar-collapse">
        <?php echo loadSideBar() ?>
      </div>
    </nav>
    <div id="page-wrapper" class="gray-bg">
      <div class="row border-bottom">
        <?php echo loadHeader() ?>
      </div>
        <div class="row wrapper border-bottom white-bg page-heading">
          <div class="col-sm-4">
            <h2><i class="fa fa-tags fa-2x"></i>&nbsp;&nbsp;Metric Units</h2>
              <ol class="breadcrumb">
                <li>
                  <a href="index.php">Dashboard</a>
                </li>
                <li class="active">
                  <strong>Metric Units</strong>
                </li>
              </ol>
          </div>
          <div class="col-sm-8">
            <div class="title-action">
              <button type="button" class="btn btn-primary customButton" data-toggle="modal" data-target="#addUnitsModal"><i class="fa fa-plus"></i>&nbsp;Metric Units</button>
            </div>
          </div>
        </div>
        <div class="row ng-cloak" ng-show="unitsPresent" id="unitsList">
          <div class="col-lg-11 card cardLayout">
            <div class="ibox float-e-margins">
              <div class="ibox-content">
                <div class="row">
                  <div class="col-sm-3">
                    <div class="input-group">
                      <input type="text" placeholder="Search" class="input-sm form-control" ng-model="search"> <span class="input-group-btn">
                      <button type="button" class="btn btn-sm btn-info"><i class="fa fa-search"></i></button> </span>
                    </div>
                  </div>
                </div>
                <div class="table-responsive ng-cloak">
                  <table class="table table-striped customTable">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Units</th>
                        <th>Description</th>
                        <th>Edit</th>
                        <th>Delete</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr ng-repeat="unit in units|filter:search">
                        <td>{{$index+1}}</td>
                        <td>{{unit.unitName}}</td>
                         <td>{{unit.metricUnit}}</td>
                        <td>
                           <span ng-if="unit.unitDescription != null">
                              {{unit.unitDescription}}
                          </span>
                          <span ng-if="unit.unitDescription == null">
                              N/A
                          </span>
                         </td> 
                        <td>
                            <button type="button" class="btn btn-info btn-circle btn-outline customButton"  ng-click="loadUnitDetails(unit)" data-toggle="modal" data-target="#editUnitModal"><i class="fa fa-pencil"></i></button>
                        </td>
                        <td>
                              <a class="btn btn-warning btn-circle btn-outline customButton" ng-click="deleteUnitDetails(unit)" data-toggle="modal" data-target="#deleteUnitModal"><i class="fa fa-trash-o"></i></a>
                          </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>   
        <div class="text-center ng-cloak" ng-show="showPagination">
            <div class="btn-group">
                <button ng-disabled="currentPage == 1" class="btn btn-white customButton" max-size="maxSize" boundary-links="true" ng-click="loadUnits(1,previousPageUrl)"><i class="fa fa-chevron-left"></i></button>
                <button class="btn btn-primary ng-cloak customButton" max-size="maxSize" boundary-links="true">{{currentPage}}/{{lastPage}}</button>
                <button ng-disabled="currentPage == lastPage" class="btn btn-white customButton" max-size="maxSize" boundary-links="true" ng-click="loadUnits(1,nextPageUrl)"><i class="fa fa-chevron-right"></i></button>
            </div>
        </div>      
        <div class="row ng-cloak" ng-hide="unitsPresent" id="noUnitsInfo">
          <div class="col-lg-12">
            <div class="wrapper wrapper-content">
              <div class="middle-box text-center animated fadeInRightBig">
                <h3 class="font-bold">No units found</h3>
                  <div class="error-desc">
                    Add units mode to view them
                  </div>
              </div>
            </div>
          </div>
        </div>
      
    </div>
  </div>
  <div class="modal inmodal" id="editUnitModal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content animated fadeIn customModal">
        <div class="modal-header customModalHeading">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <i class="fa fa-tags fa-3x"></i>
            <h4 class="customModal-title">Update Units</h4>
        </div>
        <div class="modal-body">
          <form name="editPaymentForm">
            <div class="modal-body">
              <div class="form-group">
                <div class="row">
                  <div class="col-sm-3">
                    <label>Name *</label>
                  </div>
                  <div class="col-sm-9">
                    <input type="text" placeholder="Name" class="form-control customInputForm" ng-model="editUnitData.name" required>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-sm-3">
                    <label>Description *</label>
                  </div>
                  <div class="col-sm-9">
                    <input type="text" placeholder="Description" class="form-control customInputForm" ng-model="editUnitData.description" required>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-sm-3">
                    <label>Units *</label>
                  </div>
                  <div class="col-sm-9">
                    <input type="text" placeholder="Name" class="form-control customInputForm" ng-model="editUnitData.metricUnit" required>
                  </div>
                </div>
              </div>
            </div>
          </form>   
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-white customButton" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-white customButton" data-dismiss="modal" ng-click="updateUnit(editUnitData)">Update</button>
        </div>
      </div>
    </div>
  </div>
  <div class="modal inmodal" id="addUnitsModal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content animated fadeIn customModal">
        <div class="modal-header customModalHeading">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <i class="fa fa-tags fa-3x"></i>
            <h4 class="customModal-title">Add Unit</h4>
        </div>
        <div class="modal-body">
          <form name="addCategoryForm">
            <div class="modal-body">
              <div class="form-group">
                <div class="row">
                  <div class="col-sm-3">
                    <label>Name *</label>
                  </div>
                  <div class="col-sm-9">
                    <input type="text" placeholder="Name" class="form-control customInputForm" ng-model="unitData.name" required>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-sm-3">
                    <label>Description </label> 
                  </div>
                  <div class="col-sm-9">
                     <input type="textarea" placeholder="Description" class="form-control customInputForm" ng-model="unitData.description">
                  </div>
                </div>
              </div>
               <div class="form-group">
                <div class="row">
                  <div class="col-sm-3">
                    <label>Unit *</label> 
                  </div>
                  <div class="col-sm-9">
                    <input type="text" placeholder="Unit" class="form-control customInputForm" ng-model="unitData.metricUnit" required>
                  </div>
                </div>                
              </div>
            </div>
          </form>   
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-white customButton" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-white customButton" data-dismiss="modal" ng-click="addNewUnit(unitData)">Add</button>
        </div>
      </div>
    </div>
  </div>
  <div class="modal inmodal" id="deleteUnitModal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content animated fadeIn customModal">
        <div class="modal-header customModalHeading">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <i class="fa fa-cart-plus fa-3x"></i>
        </div>
        <div class="modal-body">
           <h4>Are you sure want to delete this unit mode</h4>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-white customButton" data-dismiss="modal">Close</button>
            <button href="#" class="btn btn-primary customButton" ng-click="deleteUnit(unitDetails)">Delete <i class="mdi-action-done right"></i></button>
        </div>
      </div>
    </div>
  </div>
</body>
</html>
