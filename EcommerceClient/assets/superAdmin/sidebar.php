<?php
  function loadSideBar()
  {
  	$sidebar = "<ul class='nav' id='side-menu'>
      <li class='nav-header'>
        <div class='dropdown profile-element'> 
          <span>
            <img id='profilePicture' src={{profilePicturePath}} alt='image' class='img-circle' style='width:70px;height:70px'/>
          </span>
          <a data-toggle='dropdown' class='dropdown-toggle' href='#' style='color:white !important'>
            <span class='clear'><span class='block m-t-xs'> <strong class='font-bold ng-cloak'>{{name}}</strong>
            </span> 
            <span class='text-muted text-xs block'><label style='color:white !important'>Online</label><b class='caret' style='color:white !important'></b></span> 
            </span>
          </a>
          <ul class='dropdown-menu animated fadeInRight m-t-xs'>
            <li><a href='profile.php'>Profile</a></li>
            <li class='divider'></li>
            <li><a href='login.php'>Logout</a></li>
          </ul>
        </div>
        <div class='logo-element'>
              ANA
        </div>
      </li>
      <li>
        <a href='index.php'><i class='fa fa-th-large'></i> <span class='nav-label'>Dashboard</span></a>
      </li>
      <li>
        <a href='vendors.php'><i class='fa fa-users'></i> <span class='nav-label'>Vendors</span></a>
      </li>
      <li>
        <a href='paymentMode.php'><i class='fa fa-credit-card '></i> <span class='nav-label'>Payment Mode</span></a>
      </li>
      <li>
        <a href='metricUnits.php'><i class='fa fa-sort-numeric-asc'></i> <span class='nav-label'>Metric Units</span></a>
      </li>
      </ul>";
 		return $sidebar;
  }
?>