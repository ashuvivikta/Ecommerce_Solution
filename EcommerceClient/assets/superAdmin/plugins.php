<?php
   function loadPlugins()
   {
  	$plugins = ' <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link href="../css/bootstrap.min.css" rel="stylesheet">
      <link href="../font-awesome/css/font-awesome.css" rel="stylesheet">
      <link href="../css/plugins/iCheck/custom.css" rel="stylesheet">
      <link href="../css/animate.css" rel="stylesheet">
      <link href="../css/style.css" rel="stylesheet">
      <link href="../css/customStyle.css" rel="stylesheet">
      <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
      <script src="../js/controllers/config.js"></script>
      <script src="../js/controllers/debugConsole.js"></script>
      <link rel="shortcut icon" type="image/x-icon" href="../img/favicon.png">
      <script src="../js/controllers/app.js"></script>
      <script src="../js/controllers/toast.js"></script>
      <script src="../js/jquery-2.1.1.js"></script>
      <script src="../js/bootstrap.min.js"></script>
      <script src="../js/plugins/metisMenu/jquery.metisMenu.js"></script>
      <script src="../js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

      <!-- Custom and plugin javascript -->
      <script src="../js/inspinia.js"></script>
      <script src="../js/plugins/pace/pace.min.js"></script>

      <!-- iCheck -->
      <script src="../js/plugins/iCheck/icheck.min.js"></script>
      <link href="../css/plugins/toastr/toastr.min.css" rel="stylesheet">
      <script src="../js/plugins/toastr/toastr.min.js"></script>
      <script src="../js/controllers/toast.js"></script>';
   	return $plugins;
   }
?>