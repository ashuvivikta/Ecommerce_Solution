<?php
    include "sidebar.php";
    include "plugins.php";
    include "header.php";
   /* if(!isset($_COOKIE['anantya_session'])){
        header("Location:login.php");
    }*/
?>
<!DOCTYPE html>
<html>
<head>
  <?php echo loadPlugins() ?>
  <title>ECommerce | Vendors</title>
  <script src="../js/controllers/superAdminVendorsController.js"></script>
  <script src="../js/controllers/sidemenuController.js"></script>
</head>

<body ng-app="anantya" ng-controller="SuperAdminVendorsController" ng-init="loadVendors(0,'')">
  <div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation" ng-controller="SidemenuController">
      <div class="sidebar-collapse">
        <?php echo loadSideBar() ?>
      </div>
    </nav>
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <?php echo loadHeader() ?>
        </div>
        <div class="row wrapper border-bottom white-bg page-heading">
          <div class="col-sm-4">
            <h2><i class="fa fa-users fa-2x"></i>&nbsp;&nbsp;Vendors</h2>
              <ol class="breadcrumb">
                <li>
                  <a href="index.php">Dashboard</a>
                </li>
                <li class="active">
                  <strong>Vendors</strong>
                </li>
              </ol>
          </div>
        </div>
        <div class="row ng-cloak" ng-show="vendorsFound">
            <div class="col-lg-11 card cardLayout" style="margin-bottom:2%;">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Vendors Management</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="input-group"><input type="text" placeholder="Search" class="input-sm form-control" ng-model="search"> <span class="input-group-btn">
                                    <button type="button" class="btn btn-sm btn-primary"><i class="fa fa-search"></i></button> </span>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped customTable">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email Id</th>
                                        <th>Phone Number</th>
                                        <th>Address</th>
                                        <th>Status</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="vendor in vendors|filter:search">
                                        <td>
                                            {{$index+1}}
                                        </td>
                                        <td>
                                            {{vendor.vendorName}}
                                        </td>
                                        <td>
                                            {{vendor.vendorEmailId}}
                                        </td>
                                        <td>
                                            {{vendor.vendorPhoneNumber}}
                                        </td>
                                        <td>
                                            {{vendor.vendorAddress}}
                                        </td>
                                        <td>
                                            <span ng-if="vendor.vendorStatus == 0">
                                                Approval Pending
                                            </span>
                                            <span ng-if="vendor.vendorStatus == 1">
                                                Active
                                            </span>
                                            <span ng-if="vendor.vendorStatus == 2">
                                                Rejected
                                            </span>
                                            <span ng-if="vendor.vendorStatus == 3">
                                                Disabled
                                            </span>
                                        </td>
                                        <td>
                                            <button class="btn btn-warning customButton" ng-if="vendor.vendorStatus == 0" ng-click="approveVendor(vendor,1,0)">
                                                Approve
                                            </button>
                                            <button class="btn btn-danger customButton" ng-if="vendor.vendorStatus == 0" ng-click="approveVendor(vendor,2,0)">
                                                Reject
                                            </button>
                                            <button class="btn btn-primary customButton" ng-if="vendor.vendorStatus == 3 || vendor.vendorStatus == 2" ng-click="approveVendor(vendor,1,1)">
                                                Activate
                                            </button>
                                            <button class="btn btn-danger customButton" ng-if="vendor.vendorStatus == 1" ng-click="approveVendor(vendor,3,0)">
                                                Disable
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="text-center ng-cloak" ng-show="showPagination">
                            <div class="btn-group">
                                <button ng-disabled="currentPage == 1" class="btn btn-white customButton" max-size="maxSize" boundary-links="true" ng-click="loadVendors(1,previousPageUrl)"><i class="fa fa-chevron-left"></i></button>
                                <button class="btn btn-primary ng-cloak customButton" max-size="maxSize" boundary-links="true">{{currentPage}}/{{lastPage}}</button>
                                <button ng-disabled="currentPage == lastPage" class="btn btn-white customButton" max-size="maxSize" boundary-links="true" ng-click="loadVendors(1,nextPageUrl)"><i class="fa fa-chevron-right"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>         
        <div class="row ng-cloak" ng-show="vendorsNotFound">
            <div class="col-lg-12">
                <div class="wrapper wrapper-content">
                    <div class="middle-box text-center animated fadeInRightBig">
                        <h3 class="font-bold">There are no vendors found!</h3>
                        <div class="error-desc">
                           Please add the vendors before continuing.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
</body>
</html>
