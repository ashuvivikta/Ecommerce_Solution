<?php
    include "sidebar.php";
    include "plugins.php";
    include "header.php";
   /* if(!isset($_COOKIE['anantya_session'])){
        header("Location:login.php");
    }*/
?>
<!DOCTYPE html>
<html>
<head>
  <?php echo loadPlugins() ?>
  <title>ECommerce | Payment Mode</title>
  <script src="../js/controllers/superAdminVendorsController.js"></script>
  <script src="../js/controllers/sidemenuController.js"></script>
  <script src="../js/controllers/superAdminPaymentMode.js"></script>
</head>

<body ng-app="anantya" ng-controller="PaymentModeController" ng-init="loadPaymentModes(0,'')">
  <div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation" ng-controller="SidemenuController">
      <div class="sidebar-collapse">
        <?php echo loadSideBar() ?>
      </div>
    </nav>
    <div id="page-wrapper" class="gray-bg">
      <div class="row border-bottom">
        <?php echo loadHeader() ?>
      </div>
        <div class="row wrapper border-bottom white-bg page-heading">
          <div class="col-sm-4">
            <h2><i class="fa fa-tags fa-2x"></i>&nbsp;&nbsp;Payment Modes</h2>
              <ol class="breadcrumb">
                <li>
                  <a href="index.php">Dashboard</a>
                </li>
                <li class="active">
                  <strong>Payment Modes</strong>
                </li>
              </ol>
          </div>
          <div class="col-sm-8">
            <div class="title-action">
              <button type="button" class="btn btn-primary customButton" data-toggle="modal" data-target="#addPaymentModesModal"><i class="fa fa-plus"></i>&nbsp;Payment Modes</button>
            </div>
          </div>
        </div>
        <div class="row ng-cloak" ng-show="paymentsPresent" id="paymentsList">
          <div class="col-lg-11 card cardLayout">
            <div class="ibox float-e-margins">
              <div class="ibox-content">
                <div class="row">
                  <div class="col-sm-3">
                    <div class="input-group">
                      <input type="text" placeholder="Search" class="input-sm form-control" ng-model="search"> <span class="input-group-btn">
                      <button type="button" class="btn btn-sm btn-info"><i class="fa fa-search"></i></button> </span>
                    </div>
                  </div>
                </div>
                <div class="table-responsive ng-cloak">
                  <table class="table table-striped customTable">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Edit</th>
                        <th>Delete</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr ng-repeat="payment in payments|filter:search">
                        <td>{{$index+1}}</td>
                        <td>{{payment.paymentName}}</td>
                        <td>
                           <span ng-if="payment.paymentDescription != null">
                              {{payment.paymentDescription}}
                          </span>
                          <span ng-if="payment.paymentDescription == null">
                              N/A
                          </span>
                         </td> 
                        <td>
                            <button type="button" class="btn btn-info btn-circle btn-outline customButton"  ng-click="loadPaymentDetails(payment)" data-toggle="modal" data-target="#editPaymentModal"><i class="fa fa-pencil"></i></button>
                        </td>
                        <td>
                              <a class="btn btn-warning btn-circle btn-outline customButton" ng-click="deletePaymentDetails(payment)" data-toggle="modal" data-target="#deletePaymentModal"><i class="fa fa-trash-o"></i></a>
                          </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>   
        <div class="text-center ng-cloak" ng-show="showPagination">
            <div class="btn-group">
                <button ng-disabled="currentPage == 1" class="btn btn-white customButton" max-size="maxSize" boundary-links="true" ng-click="loadPaymentModes(1,previousPageUrl)"><i class="fa fa-chevron-left"></i></button>
                <button class="btn btn-primary ng-cloak customButton" max-size="maxSize" boundary-links="true">{{currentPage}}/{{lastPage}}</button>
                <button ng-disabled="currentPage == lastPage" class="btn btn-white customButton" max-size="maxSize" boundary-links="true" ng-click="loadPaymentModes(1,nextPageUrl)"><i class="fa fa-chevron-right"></i></button>
            </div>
        </div>      
        <div class="row ng-cloak" ng-hide="paymentssPresent" id="noPaymentsInfo">
          <div class="col-lg-12">
            <div class="wrapper wrapper-content">
              <div class="middle-box text-center animated fadeInRightBig">
                <h3 class="font-bold">No payment mode found</h3>
                  <div class="error-desc">
                    Add payment mode to view them
                  </div>
              </div>
            </div>
          </div>
        </div>
      
    </div>
  </div>
  <div class="modal inmodal" id="editPaymentModal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content animated fadeIn customModal">
        <div class="modal-header customModalHeading">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <i class="fa fa-tags fa-3x"></i>
            <h4 class="customModal-title">Update Payment Mode</h4>
        </div>
        <div class="modal-body">
          <form name="editPaymentForm">
            <div class="modal-body">
              <div class="form-group">
                <div class="row">
                  <div class="col-sm-3">
                    <label>Name *</label> 
                  </div>
                  <div class="col-sm-9">
                    <input type="text" placeholder="Name" class="form-control customInputForm" ng-model="editPaymentData.name" required>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-sm-3">
                    <label>Description *</label>
                  </div>
                  <div class="col-sm-9">
                    <input type="text" placeholder="Description" class="form-control customInputForm" ng-model="editPaymentData.description" required>
                  </div>
                </div>
              </div>
            </div>
          </form>   
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-white customButton" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-white customButton" data-dismiss="modal" ng-click="updatePayment(editPaymentData)">Update</button>
        </div>
      </div>
    </div>
  </div>
  <div class="modal inmodal" id="addPaymentModesModal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content animated fadeIn customModal">
        <div class="modal-header customModalHeading">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <i class="fa fa-tags fa-3x"></i>
            <h4 class="customModal-title">Add Category</h4>
        </div>
        <div class="modal-body">
          <form name="addCategoryForm">
            <div class="modal-body">
              <div class="form-group">
                <div class="row">
                  <div class="col-sm-3">
                    <label>Name *</label>
                  </div>
                  <div class="col-sm-9">
                    <input type="text" placeholder="Name" class="form-control customInputForm" ng-model="paymentData.name" required>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-sm-3">
                    <label>Description *</label> 
                  </div>
                  <div class="col-sm-9">
                    <input type="textarea" placeholder="Description" class="form-control customInputForm" ng-model="paymentData.description">
                  </div>
                </div>
              </div>
            </div>
          </form>   
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-white customButton" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-white customButton" data-dismiss="modal" ng-click="addNewPayment(paymentData)">Add</button>
        </div>
      </div>
    </div>
  </div>
  <div class="modal inmodal" id="deletePaymentModal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content animated fadeIn customModal">
        <div class="modal-header customModalHeading">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <i class="fa fa-cart-plus fa-3x"></i>
        </div>
        <div class="modal-body">
           <h4>Are you sure want to delete this payment mode</h4>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-white customButton" data-dismiss="modal">Close</button>
            <button href="#" class="btn btn-primary customButton" ng-click="deletePayment(paymentDetails)">Delete <i class="mdi-action-done right"></i></button>
        </div>
      </div>
    </div>
  </div>
</body>
</html>
