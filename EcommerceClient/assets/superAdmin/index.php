<?php
  include "sidebar.php";
  include "plugins.php";
  include "header.php";
  /*if(!isset($_COOKIE['anantya_session'])){
      header("Location:login.php");
  }*/
?>
<!DOCTYPE html>
<html>
<head>
  <?php echo loadPlugins() ?>
  <title>ECommerce | Dashboard</title>
  <script src="../js/controllers/loginController.js"></script>
  <script src="../js/controllers/debugConsole.js"></script>
  <script src="../js/controllers/sidemenuController.js"></script>
  <script src="../js/controllers/superAdminDashboardController.js"></script>
</head>
<body ng-app="anantya" ng-controller="SuperAdminDashboardController" ng-init="loadDashboardData()">
  <div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation" ng-controller="SidemenuController">
      <div class="sidebar-collapse">
        <?php echo loadSideBar() ?>
      </div>
    </nav>
    <div id="page-wrapper" class="gray-bg">
      <div class="row border-bottom">
        <?php echo loadHeader() ?>
      </div>
      <div class="card wrapper wrapper-content">
        <div class="row">
          <a href="vendors.php">
            <div class="col-lg-2" >
              <div class="widget navy-bg p-lg text-center widget-style">
                <div class="m-b-md">
                  <i class="fa fa-users fa-4x"></i>
                  <h1 class="m-xs ng-cloak">{{totalVendorsCount}}</h1>
                  <h3 class="font-bold no-margins">
                    Vendors
                  </h3>
                  <small>Total Count</small>
                </div>
              </div>
            </div>
          </a>
        </div>
      </div>
    </div>
  </div>
</body>
</html>
