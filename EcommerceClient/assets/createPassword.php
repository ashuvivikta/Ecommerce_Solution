<?php
  include_once 'plugins.php';
?>
<!DOCTYPE html>
<html>
<head>
   <title>ECommerce | Create Password</title>
   <?php echo loadPlugins()?>
   <script src="js/controllers/passwordController.js"></script>
</head>
<body class="gray-bg" ng-app="anantya" ng-controller="PasswordController" style="background-image: url('img/img.jpg');background-repeat: no-repeat;background-size: cover;padding-top:10%;">
   <div class="middle-box text-center loginscreen animated fadeInDown" style="padding-left:30%;">
      <div style="background-color:rgba(0,0,0,0.5);padding:20px;width: 400px;color: black">
         <h3 style="color:white">Create Password</h3>
         <p style="color:white">Create a Password for your account</p>
         <form class="m-t" role="form" name="createPasswordForm">
            <div class="form-group">
               <input type="hidden" id="registrationToken" ng-model="passwordData.registrationToken" value="<?php echo $_GET['vendorId'] ?>">
               <input type="password" class="form-control" placeholder="Password" required="" ng-model="passwordData.password" ng-minlength="6" ng-change="checkPassword(passwordData)"><span style="font-size:10px;color:white;">Min.6 characters</span>
            </div>
            <div class="form-group">
               <input type="password" class="form-control" placeholder="Confirm Password" required="" ng-model="passwordData.confirmPassword" ng-change="checkPassword(passwordData)" ng-minlength="6">
            </div>
            <p ng-show="createPasswordError" class="text-center ng-cloak" style="color:white">Unable to create password. Try again!</p>
            <p ng-show="validUserError" class="text-center ng-cloak" style="color:white">Not a valid user. Please register to continue!</p>
            <p ng-show="userAlreadyExistsError" class="text-center ng-cloak" style="color:white">Password already created for this user,Login to proceed!</p>
            <span class="ng-cloak" ng-show="validatePassword" style="color:white;">Password did not match!</span>
            <button type="submit" class="btn btn-danger block full-width m-b" ng-click="createPassword(passwordData)" ng-disabled="createPasswordForm.$invalid">Create</button>
            <p class="text-muted text-center" style="color:white"><small>Already have an account?</small></p>
            <a class="btn btn-sm btn-primary btn-block" href="login.php">Login</a>
         </form>
         <p class="m-t" style="color:white"> <small>Vivikta Technologies &copy; 2016-2017</small> </p>
      </div>
   </div>
</body>
</html>