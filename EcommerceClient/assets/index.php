<?php
  include "sidebar.php";
  include "plugins.php";
  include "header.php";
  /*if(!isset($_COOKIE['anantya_session'])){
      header("Location:login.php");
  }*/
?>
<!DOCTYPE html>
<html>
<head>
  <?php echo loadPlugins() ?>
  <title>ECommerce | Dashboard</title>
  <script src="js/controllers/loginController.js"></script>
  <script src="js/controllers/debugConsole.js"></script>
  <script src="js/controllers/sidemenuController.js"></script>
  <script src="js/controllers/dashboardController.js"></script>
</head>
<body ng-app="anantya" ng-controller="DashboardController" ng-init="loadDashboardData()">
  <div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation" ng-controller="SidemenuController">
      <div class="sidebar-collapse">
        <?php echo loadSideBar() ?>
      </div>
    </nav>
    <div id="page-wrapper" class="gray-bg">
      <div class="row border-bottom">
        <?php echo loadHeader() ?>
      </div>
      <div class=" card wrapper wrapper-content">
        <div class="row">
          <a href="categories.php">
            <div class="col-lg-2" >
              <div class="widget navy-bg p-lg text-center widget-style">
                <div class="m-b-md">
                  <i class="fa fa-tags fa-3x"></i>
                  <h1 class="m-xs ng-cloak">{{totalCategoriesCount}}</h1>
                  <h3 class="font-bold no-margins">
                    Categories
                  </h3>
                  <small>Total Count</small>
                </div>
              </div>
            </div>
          </a>
          <a href="subCategories.php">
            <div class="col-lg-2" >
              <div class="widget blue-bg p-lg text-center widget-style">
                <div class="m-b-md">
                  <i class="fa fa-tags fa-3x"></i>
                  <h1 class="m-xs ng-cloak">{{totalSubCategoriesCount}}</h1>
                  <h3 class="font-bold no-margins">
                    Sub Categories
                  </h3>
                  <small>Total Count</small>
                </div>
              </div>
            </div>
          </a>
          <a href="colors.php">
            <div class="col-lg-2" >
              <div class="widget red-bg p-lg text-center widget-style">
                <div class="m-b-md">
                  <i class="fa fa-paint-brush fa-3x"></i>
                  <h1 class="m-xs ng-cloak">{{totalColorsCount}}</h1>
                  <h3 class="font-bold no-margins">
                    Colors
                  </h3>
                  <small>Total Count</small>
                </div>
              </div>
            </div>
          </a>
          <a href="products.php">
            <div class="col-lg-2" >
              <div class="widget yellow-bg p-lg text-center widget-style">
                <div class="m-b-md">
                  <i class="fa fa-cart-plus fa-3x"></i>
                  <h1 class="m-xs ng-cloak">{{totalProductsCount}}</h1>
                  <h3 class="font-bold no-margins">
                    Products
                  </h3>
                  <small>Total Count</small>
                </div>
              </div>
            </div>
          </a>
          <a href="orders.php">
            <div class="col-lg-2" >
              <div class="widget navy-bg p-lg text-center widget-style">
                <div class="m-b-md">
                  <i class="fa fa-arrow-circle-o-down fa-3x"></i>
                  <h1 class="m-xs ng-cloak">{{totalOrdersCount}}</h1>
                  <h3 class="font-bold no-margins">
                    Orders
                  </h3>
                  <small>Total Count</small>
                </div>
              </div>
            </div>
          </a>
          <a href="complaints.php">
            <div class="col-lg-2" >
              <div class="widget blue-bg p-lg text-center widget-style">
                <div class="m-b-md">
                  <i class="fa fa-exclamation-triangle fa-3x"></i>
                  <h1 class="m-xs ng-cloak">{{totalComplaintsCount}}</h1>
                  <h3 class="font-bold no-margins">
                    Complaints
                  </h3>
                  <small>Total Count</small>
                </div>
              </div>
            </div>
          </a>
          <a href="suggestions.php">
            <div class="col-lg-2" >
              <div class="widget red-bg p-lg text-center widget-style">
                <div class="m-b-md">
                  <i class="fa fa-paper-plane fa-3x"></i>
                  <h1 class="m-xs ng-cloak">{{totalSuggestionsCount}}</h1>
                  <h3 class="font-bold no-margins">
                    Suggestions
                  </h3>
                  <small>Total Count</small>
                </div>
              </div>
            </div>
          </a>
        </div>
      </div>
    </div>
  </div>
</body>
</html>
