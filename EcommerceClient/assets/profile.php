<?php
    include "sidebar.php";
    include "plugins.php";
    include "header.php";
   /* if(!isset($_COOKIE['anantya_session'])){
        header("Location:login.php");
    }*/
?>
<!DOCTYPE html>
<html>
<head>
  <?php echo loadPlugins() ?>
  <title>ECommerce | Profile</title>
  <script src="js/controllers/sidemenuController.js"></script>
  <script src="js/controllers/profileController.js"></script>
    <script src="js/controllers/services.js"></script>
</head>

<body ng-app="anantya" ng-controller="ProfileController" ng-init="loadProfileData()">
  <div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation" ng-controller="SidemenuController">
      <div class="sidebar-collapse">
        <?php echo loadSideBar() ?>
      </div>
    </nav>
    <div id="page-wrapper" class="gray-bg">
      <div class="row border-bottom">
        <?php echo loadHeader() ?>
      </div>
        <div class="row wrapper border-bottom white-bg page-heading">
          <div class="col-sm-4">
            <h2><i class="fa fa-user fa-2x"></i>&nbsp;&nbsp;Profile</h2>
              <ol class="breadcrumb">
                <li>
                  <a href="index.php">Dashboard</a>
                </li>
                <li class="active">
                  <strong>Profile</strong>
                </li>
              </ol>
          </div>
          <div class="col-sm-8">
            <div class="title-action">
              <button type="button" class="btn btn-primary customButton" ng-show="vendorConfigurationDetails==0" data-toggle="modal" data-target="#addConfigurationModal"><i class="fa fa-plus"></i>&nbsp;Configurations</button>
            </div>
          </div>
        </div> 
        <div class="wrapper wrapper-content">
          <div class="row animated fadeInRight">
            <div class="col-md-4 card">
                <div class="ibox float-e-margins">
                    <div class="text-center">
                        <h4>Profile Picture</h4>
                    </div>
                    <div>
                        <div class="ibox-content no-padding border-left-right">
                            <input type="image" class="img-responsive ng-cloak"  id="profilepic" src="https://shoppingmall.vivikta.in/defaultImages/avatar.png"/>
                            <input type="file" file-model="myFile" id="myFile" style="display: none;" name="myFile" onchange="showImage(this)"/>
                        </div>
                        <div class="ibox-content profile-content">
                            <div class='text-center ng-cloak'>
                            <h3 class="ng-cloak"><strong>{{profileDetails.name}}</strong></h3>
                            </div>
                            <div class="user-button">
                                <div class="row">
                                    <div class="col-md-12">
                                       <!--  <button type="button" class="btn btn-primary btn-sm btn-block" ng-click="uploadProfilePicture()">Upload</button> -->
                                        <button type="button" class="btn btn-warning btn-sm btn-block" data-toggle="modal" data-target="#myModal4">Change Password</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-7 card cardLayout">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Profile Details</h5>
                        <div class="ibox-tools">
                            <a>
                                <button  class="btn btn-primary btn-circle btn-outline" data-ng-hide="editMode" data-ng-click="editMode = true; editField(profileDetails)"><i class="fa fa-pencil"></i></button>
                                <button type="submit" data-ng-show="editMode" data-ng-click="editMode = false; cancel()" class="btn btn-danger btn-circle btn-outline"><i class="fa fa-times"></i></button>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div>
                            <form name="profileForm">
                            <div class="feed-activity-list">
                                <div class="feed-element">
                                    <div class="pull-left" style="color:#23c6c8;">
                                        <i class="fa fa-user fa-2x"></i>
                                    </div>
                                    <div class="media-body ">
                                        <small class="pull-right text-navy" style="color:#23c6c8;">
                                        </small>
                                        <strong>Name</strong><br>
                                        <small class="text-muted ng-cloak" style="font-size: 14px;">{{profileDetails.name}}</small>
                                    </div>
                                </div>
                                <div class="feed-element">
                                    <div class="pull-left" style="color:#23c6c8;">
                                        <i class="fa fa-envelope fa-2x"></i>
                                    </div>
                                    <div class="media-body ">
                                        <small class="pull-right text-navy" style="color:#23c6c8;">
                                        </small>
                                        <strong>Email Id</strong><br>
                                        <small class="text-muted ng-cloak" style="font-size: 14px;">{{profileDetails.emailId}}</small>
                                    </div>
                                </div>
                                <div class="feed-element">
                                    <div class="pull-left" style="color:#23c6c8;">
                                        <i class="fa fa-phone fa-2x"></i>
                                    </div>
                                    <div class="media-body ">
                                        <small class="pull-right text-navy" style="color:#23c6c8;"></small>
                                        <strong>Phone Number</strong><br>
                                        <small class="text-muted ng-cloak" style="font-size: 14px;"><span data-ng-hide="editMode">{{profileDetails.phoneNumber}}</span>
                                        <input type="text" class="form-control customInputForm" data-ng-show="editMode" data-ng-model="profileDetails.phoneNumber" numbers-only data-ng-required /></small>
                                    </div>
                                </div>
                                <div class="feed-element">
                                    <div class="pull-left" style="color:#23c6c8;">
                                        <i class="fa fa-map-marker fa-2x"></i>
                                    </div>
                                    <div class="media-body ">
                                        <small class="pull-right text-navy" style="color:#23c6c8;"></small>
                                        <strong>Address</strong><br>
                                        <small class="text-muted ng-cloak" style="font-size: 14px;">
                                            <span data-ng-hide="editMode">{{profileDetails.address1}}</span>
                                            <input type="text" class="form-control customInputForm" data-ng-show="editMode" data-ng-model="profileDetails.address1" data-ng-required />
                                        </small>
                                    </div>
                                </div>
                                <div class="feed-element">
                                    <div class="pull-left" style="color:#23c6c8;">
                                        <i class="fa fa-at fa-2x"></i>
                                    </div>
                                    <div class="media-body ">
                                        <small class="pull-right text-navy" style="color:#23c6c8;"></small>
                                        <strong>Domain Name</strong><br>
                                        <small class="text-muted ng-cloak" style="font-size: 14px;">
                                            <span>{{profileDetails.vendorDomainName}}</span>
                                        </small>
                                    </div>
                                </div>
                                 <div class="feed-element" ng-if="profileDetails.taxAmount!=null">
                                    <div class="pull-left" style="color:#23c6c8;">
                                        <i class="fa fa-rupee fa-2x"></i>
                                    </div>
                                    <div class="media-body ">
                                        <small class="pull-right text-navy" style="color:#23c6c8;"></small>
                                        <strong>Default Tax Amount</strong><br>
                                        <small class="text-muted ng-cloak" style="font-size: 14px;">
                                            <span data-ng-hide="editMode">{{profileDetails.taxAmount}}</span><input type="text" class="form-control" data-ng-show="editMode" data-ng-model="profileDetails.taxAmount" data-ng-required />
                                        </small>
                                    </div>
                                </div>
                                <div class="feed-element" ng-if="profileDetails.shippingAmount!=null">
                                    <div class="pull-left" style="color:#23c6c8;">
                                        <i class="fa fa-ship fa-2x"></i>
                                    </div>
                                    <div class="media-body ">
                                        <small class="pull-right text-navy" style="color:#23c6c8;"></small>
                                        <strong>Default Shipping Amount</strong><br>
                                        <small class="text-muted ng-cloak" style="font-size: 14px;">
                                            <span data-ng-hide="editMode">{{profileDetails.shippingAmount}}</span><input type="text" class="form-control" data-ng-show="editMode" data-ng-model="profileDetails.shippingAmount" data-ng-required />
                                        </small>
                                    </div>
                                </div>
                                <div class="feed-element" ng-if="profileDetails.paymentMode!=null">
                                    <div class="pull-left" style="color:#23c6c8;">
                                        <i class="fa fa-credit-card fa-2x"></i>
                                    </div>
                                    <div class="media-body ">
                                        <small class="pull-right text-navy" style="color:#23c6c8;"></small>
                                        <strong>Payment Mode</strong> <br>
                                        <small class="text-muted ng-cloak" data-ng-model="profileDetails.paymentId" style="font-size: 14px;">
                                            <span>{{profileDetails.paymentMode}}</span>
                                        </small>
                                    </div>
                                </div>

                            </div>
                            <div class="profile-button-alignment">
                              <button class="btn btn-primary customButton" ng-show="editMode" ng-click="updateProfileDetails(profileDetails)">Save</button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
          </div>
          <div class="modal inmodal" id="myModal4" tabindex="-1" role="dialog"  aria-hidden="true">
             <div class="modal-dialog">
                <div class="modal-content animated fadeIn customModal">
                   <div class="modal-header customModalHeading">
                      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                      <i class="fa fa-lock fa-3x"></i>
                      <h4 class="customModal-title">Change Password</h4>
                      <small>Change the password for your account</small>
                   </div>
                   <form name="changePasswordForm">
                      <div class="modal-body">
                         <div class="form-group">
                           <div class="row">
                            <div class="col-sm-4">
                              <label>New Password</label> 
                            </div>
                            <div class="col-sm-8">
                              <input type="password" class="form-control customInputForm" ng-model="password.newPassword" ng-minlength="6" required>
                            </div>
                           </div>
                         </div>
                         <div class="form-group">
                          <div class="row">
                            <div class="col-sm-4">
                              <label>Confirm Password</label> 
                            </div>
                            <div class="col-sm-8">
                              <input type="password" class="form-control customInputForm" ng-model="password.confirmPassword" ng-minlength="6" required>
                            </div>
                          </div>
                         </div>
                         <p ng-show="validatePasswordError">Password did not match</p>
                         <p ng-show="passwordChangeError">Could not change password! try again!</p>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-white customButton" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary customButton" ng-click="changePassword(password)" ng-disabled="changePasswordForm.$invalid">Change</button>
                      </div>
                   </form>
                </div>
             </div>
          </div>
            <div class="modal inmodal" id="addConfigurationModal" tabindex="-1" role="dialog"  aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content animated fadeIn">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-tags modal-icon"></i>
                    <h4 class="modal-title">Add Configurations</h4>
                </div>
                <div class="modal-body">
                  <form name="addCategoryForm">
                    <div class="modal-body">
                      <div class="form-group">
                        <label>Default Tax Amount *</label> 
                        <input type="text" placeholder="Name" class="form-control" ng-model="configurationData.taxAmount" required>
                      </div>
                      <div class="form-group">
                        <label>Default Shipping Charges *</label> 
                        <input type="text" placeholder="Name" class="form-control" ng-model="configurationData.shippingAmount" required>
                      </div>
                      <div class="form-group">
                      <label>Payment Mode *</label>
                        <select class="form-control" ng-model="configurationData.paymentId">
                            <option ng-repeat="payment in payments" value="{{payment.paymentId}}">{{payment.paymentName}}</option>
                        </select>
                    </div>
                  </form>   
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-white" data-dismiss="modal" ng-click="addNewConfiguration(configurationData)">Add</button>
                </div>
              </div>
            </div>
          </div>
        </div>       
      
    </div>
  </div>
</body>
</html>
