<!DOCTYPE html>
<html>
<head>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Forgot Password | ECommerce</title>
   <link rel='shortcut icon' href='img/favicon.png'>
   <link href="css/bootstrap.min.css" rel="stylesheet">
   <link href="font-awesome/css/font-awesome.css" rel="stylesheet">
   <link href="css/plugins/iCheck/custom.css" rel="stylesheet">
   <link href="css/animate.css" rel="stylesheet">
   <link href="css/style.css" rel="stylesheet">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
   <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
   <script src='js/controllers/debugConsole.js'></script>
   <script src="js/controllers/config.js"></script>
   <script src="js/controllers/app.js"></script>
   <script src="js/controllers/loginController.js"></script>
</head>
<body class="gray-bg" ng-app="anantya" ng-controller="LoginController" style="background-image:url('img/img.jpg');background-repeat: no-repeat;background-size: cover;" ng-init="fetchData()">
    <div class="middle-box text-center loginscreen animated fadeInDown" style="padding-top: 10%;padding-left:30%">
      <div style="background-color:rgba(0,0,0,0.5);padding:20px;width: 400px;color: white">
        <h3 style="color: white">Welcome to ECommerce</h3>     
          <div ng-hide="resetSuccessful">
             <h3 style="color:white">Reset Password</h3>
             <p style="color:white">Reset the password of your account</p>
             <form class="m-t" role="form" name="resetPasswordForm" style="color:black">
                <div class="form-group">
                   <input type="hidden" id="emailId" ng-model="emailId" value="<?php echo $_GET['emailId'] ?>">
                   <h1 style="font-size:20px;text-decoration:bold;color:white;">{{emailId}}</h1>
                </div>
                <div class="form-group">
                   <input type="hidden" id="resetToken" ng-model="resetToken" value="<?php echo $_GET['resetToken'] ?>">
                   <input type="password" class="form-control" placeholder="Password" required="" ng-model="password" ng-minlength="6" ng-change="checkPassword()"><span style="font-size:10px;color:white;">Min.6 characters</span>
                </div>
                <div class="form-group">
                   <input type="password" class="form-control" placeholder="Confirm Password" required="" ng-model="confirmPassword" ng-change="checkPassword()" ng-minlength="6">
                </div>
                <p ng-show="createPasswordError" class="text-center" style="color:white">Unable to create password. Try again!</p>
                <p ng-show="validUserError" class="text-center" style="color:white">Not a valid user. Please register to continue!</p>
                <span ng-show="validatePassword" style="color:white">Password did not match</span>
                <button type="submit" class="btn btn-danger block full-width m-b" ng-click="resetPassword()" ng-disabled="resetPasswordForm.$invalid">Reset</button>
             </form>
             <p class="m-t" style="color:white"> <small>Vivikta Technologies &copy; 2016-2017</small> </p>
          </div>
          <div ng-show="resetSuccessful">
            <div>
                <h1 class="logo-name"><img src="img/img.jpg" width="100" height="100"/></h1>
             </div>
            <h3 style="color:white">Password reset successful! Please login to continue.</h3>
            <a class="btn btn-sm btn-primary btn-block" href="login.php">Login</a>
          </div>
        </div>
      </div>
    </div>
    <!-- Mainly scripts -->
    <script src="js/jquery-2.1.1.js"></script>
    <script src="js/bootstrap.min.js"></script>

</body>
</html>