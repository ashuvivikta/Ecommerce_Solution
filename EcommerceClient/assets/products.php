<?php
    include "sidebar.php";
    include "plugins.php";
    include "header.php";
   /* if(!isset($_COOKIE['anantya_session'])){
        header("Location:login.php");
    }*/
?>
<!DOCTYPE html>
<html>
<head>
  <?php echo loadPlugins() ?>
  <title>ECommerce | Products</title>
  <script src="js/controllers/productsController.js"></script>
  <script src="js/controllers/sidemenuController.js"></script>
  <script src="js/controllers/services.js"></script>
</head>

<body ng-app="anantya" ng-controller="ProductsController" ng-init="loadProducts(0,'')">
  <div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation" ng-controller="SidemenuController">
      <div class="sidebar-collapse">
        <?php echo loadSideBar() ?>
      </div>
    </nav>
    <div id="page-wrapper" class="gray-bg">
      <div class="row border-bottom">
        <?php echo loadHeader() ?>
      </div>
        <div class="row wrapper border-bottom white-bg page-heading">
          <div class="col-sm-4">
            <h2><i class="fa fa-cart-plus fa-2x"></i>&nbsp;&nbsp;Products</h2>
              <ol class="breadcrumb">
                <li>
                  <a href="index.php">Dashboard</a>
                </li>
                <li class="active">
                  <strong>Products</strong>
                </li>
              </ol>
          </div>
          <div class="col-sm-8">
            <div class="title-action">
              <button type="button" class="btn btn-primary customButton" data-toggle="modal" data-target="#addProductModal"><i class="fa fa-plus"></i>&nbsp;Products</button>
            </div>
          </div>
        </div>
        <div class="row ng-cloak" ng-show="productsPresent" id="productsList">
          <div class="col-lg-11 card cardLayout">
            <div class="ibox float-e-margins">
              <div class="ibox-content">
                <div class="row">
                  <div class="col-sm-3">
                    <div class="input-group">
                      <input type="text" placeholder="Search" class="input-sm form-control" ng-model="search"> <span class="input-group-btn">
                      <button type="button" class="btn btn-sm btn-info"><i class="fa fa-search"></i></button> </span>
                    </div>
                  </div>
                </div>
                <div class="table-responsive ng-cloak">
                  <table class="table table-striped customTable">
                    <thead>
                      <tr>
                          <th data-field="id">#</th>
                          <th data-field="name" style="width:200px">Name</th>
                          <th data-field="model">Model</th>
                          <th data-field="color">Color</th>
                          <th data-field="price">Price</th>
                          <th data-field="count">Available</th>
                          <th data-field="view">View</th>
                          <!-- <th data-field="delete">Delete</th> -->
                      </tr>
                    </thead>
                    <tbody>
                      <tr class="ng-cloak" ng-repeat="product in products|filter:search">
                          <td>{{$index+1}}</td>
                          <td>{{product.productName}}</td>
                          <td>{{product.productModel}}</td>
                          <td>{{product.productColor}}</td>
                          <td>{{product.productPrice}}</td>
                          <td>{{product.productCount}}</td>
                          <td>
                              <button  type="button" class="btn btn-info btn-circle btn-outline customButton"  ng-click="loadProductDetails(product.productImages[0].thumbnail)" data-toggle="modal" data-target="#viewProductModal"><i class="fa fa-weibo"></i></button>
                          </td>
                          <!-- <td>
                              <a class="btn btn-warning btn-circle btn-outline" ng-click="deleteProductDetails(product)" data-toggle="modal" data-target="#deleteProductModal"><i class="fa fa-trash-o"></i></a>
                          </td> -->
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>   
        <div class="text-center ng-cloak" ng-show="showPagination">
            <div class="btn-group">
                <button ng-disabled="currentPage == 1" class="btn btn-white customButton" max-size="maxSize" boundary-links="true" ng-click="loadProducts(1,previousPageUrl)"><i class="fa fa-chevron-left"></i></button>
                <button class="btn btn-primary ng-cloak customButton" max-size="maxSize" boundary-links="true">{{currentPage}}/{{lastPage}}</button>
                <button ng-disabled="currentPage == lastPage" class="btn btn-white customButton" max-size="maxSize" boundary-links="true" ng-click="loadProducts(1,nextPageUrl)"><i class="fa fa-chevron-right"></i></button>
            </div>
        </div>       
        <div class="row ng-cloak" ng-hide="productsPresent" id="noProductsInfo">
          <div class="col-lg-12">
            <div class="wrapper wrapper-content">
              <div class="middle-box text-center animated fadeInRightBig">
                <h3 class="font-bold">No products found</h3>
                  <div class="error-desc">
                    Add Products to view them
                  </div>
              </div>
            </div>
          </div>
        </div>
      
    </div>
  </div>
  <div class="modal inmodal" id="deleteProductModal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content animated fadeIn customModal">
        <div class="modal-header customModalheading">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <i class="fa fa-cart-plus fa-3x"></i>
        </div>
        <div class="modal-body">
           <h4>Are you sure want to delete this product</h4>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-white customButton" data-dismiss="modal">Close</button>
            <button href="#" class="btn btn-primary customButton" ng-click="deleteProduct(productDetails)">Delete <i class="mdi-action-done right"></i></button>
        </div>
      </div>
    </div>
  </div>
  <div class="modal inmodal" id="viewProductModal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content animated fadeIn customModal">
        <div class="modal-header customModalHeading">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <i class="fa fa-file-image-o modal-icon"></i>
        </div>
        <div class="modal-body">
          <center>
              <h4 class="header2" style="font-size: 15px">Thumbnail</h4> 
              <img src={{productsDetails}} style="width:200px;height: 200px">
          </center>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-white customButton" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <div class="modal inmodal" id="addProductModal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content animated fadeIn customModal">
        <div class="modal-header customModalHeading">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <i class="fa fa-cart-plus fa-3x"></i>
            <h4 class="customModal-title">Add Product</h4>
        </div>
        <div class="modal-body">
          <form name="addCategoryForm">
            <div class="modal-body">
              <div class="form-group">
                <div class="row">
                  <div class="col-sm-3">
                    <label>Name *</label>
                  </div>
                  <div class="col-sm-9">
                    <input type="text" placeholder="Name" class="form-control customInputForm"  ng-model="productData.name" required>
                  </div>
                </div>                
              </div>
              <div class="form-group">
              <div class="row">
                  <div class="col-sm-3">
                    <label>Description *</label> 
                  </div>
                  <div class="col-sm-9">
                    <textarea placeholder="Description" class="form-control customInputForm" ng-model="productData.description" required></textarea>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-sm-3">
                    <label>Category *</label>
                  </div>
                  <div class="col-sm-9">
                    <select class="form-control customInputForm customSelectForm" ng-model="productData.categoryId" ng-change="loadSubCategoriesBasedOnCategory(productData.categoryId)">
                      <option ng-repeat="category in categories" value="{{category.categoryId}}">{{category.categoryName}}</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-sm-3">
                    <label>Sub Category *</label>
                  </div>
                  <div class="col-sm-9">
                    <select class="form-control customInputForm customSelectForm" ng-model="productData.subCategoryId">
                      <option ng-repeat="subcategory in subcategoriesDetails" value="{{subcategory.subcategoryId}}">{{subcategory.subcategoryName}}</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-sm-3">
                     <label>Model *</label> 
                  </div>
                  <div class="col-sm-9">
                    <input type="text" placeholder="Model" class="form-control customInputForm"  ng-model="productData.model" required>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-sm-3">
                    <label>Price *</label> 
                  </div>
                  <div class="col-sm-9">
                    <input type="text" placeholder="Price" class="form-control customInputForm"  ng-model="productData.price" required>
                  </div>
                </div>
              </div>
		          <div class="form-group">
                <div class="row">
                  <div class="col-sm-3">
                    <label>Tax Amount</label> 
                  </div>
                  <div class="col-sm-9">
                    <input type="text" placeholder="Price" class="form-control customInputForm"  ng-model="productData.taxAmount" required>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-sm-3">
                    <label>Shipping Amount</label>
                  </div>
                  <div class="col-sm-9">
                    <input type="text" placeholder="Price" class="form-control customInputForm"  ng-model="productData.shippingAmount" required>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-sm-3">
                    <label>Size</label>
                  </div>
                  <div class="col-sm-9">
                    <input type="checkbox" id="isSize" ng-model="productData.isSize">
                  </div>
                </div>
              </div>
              <div class="form-group" ng-if="productData.isSize==true">
                <div class="row">
                  <div class="col-sm-3">
                    <label>Size</label>
                  </div>
                  <div class="col-sm-9">
                    <input type="text" placeholder="size" class="form-control customInputForm"  ng-model="productData.productSize" required>
                  </div>
                </div>
              </div>
              <div class="form-group" ng-if="productData.isSize==true">
                <div class="row">
                  <div class="col-sm-3">
                    <label>Unit</label>
                  </div>
                  <div class="col-sm-9">
                    <select class="form-control customInputForm customSelectForm" ng-model="productData.metricUnit">
                      <option ng-repeat="metric in metricUnits" value="{{metric.metricUnit}}">{{metric.unitName}}</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-sm-3">
                    <label>Inventory Count *</label> 
                  </div>
                  <div class="col-sm-9">
                    <input type="text" placeholder="count" class="form-control customInputForm"  ng-model="productData.count" required>
                  </div>
                </div>                
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-sm-3">
                    <label>Color *</label>
                  </div>
                  <div class="col-sm-9">
                    <select class="form-control customInputForm customSelectForm" ng-model="productData.productColorId">
                      <option ng-repeat="color in colors" value="{{color.productColorId}}">{{color.productColor}}</option>
                    </select>
                  </div>
                </div>                  
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-sm-3">
                    <label>Images *</label>
                  </div>
                  <div class="col-sm-9">
                    <input type="file" class="form-control customInputForm customSelectForm" file-model="productImage" name="productImage[]" id="productImage" multiple/>
                  </div>
                </div>
              </div>
          </form>   
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-white customButton" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-white customButton" data-dismiss="modal" ng-click="addNewProduct(productData)">Add</button>
        </div>
      </div>
    </div>
  </div>
</body>
</html>
