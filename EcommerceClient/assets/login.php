<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>ECommerce | Login</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/customStyle.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
    <script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
      <script src="js/controllers/config.js"></script>
      <script src="js/controllers/debugConsole.js"></script>
      <script src="js/controllers/app.js"></script>
      <script src="js/controllers/loginController.js"></script>
      <link rel="shortcut icon" type="image/x-icon" href="img/favicon.png">
      <link href="css/plugins/toastr/toastr.min.css" rel="stylesheet">
      <script src="js/plugins/toastr/toastr.min.js"></script>
      <script src="js/controllers/toast.js"></script>

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">


</head>

<body class="gray-bg" ng-app="anantya" ng-controller="LoginController" style="background-image:url('img/img.jpg');background-repeat: no-repeat;background-size: cover;">

    <div class="middle-box text-center loginscreen animated fadeInDown" style="margin-left:65%;margin-top: 5%">
        <div class="middle-box text-center loginscreen animated fadeInDown ng-cloak" ng-hide="registrationStatus">
            <div style="background-color:rgba(0,0,0,0.5);padding:20px;width: 400px;color: white">
                <h3 style="color: white">Welcome to ECommerce</h3>
                <p style="color: white">Login in. To see it in action.</p>
                <form class="m-t" role="form" style="color: black">
                    <div class="form-group">
                        <input type="email" class="form-control" placeholder="Username" ng-model="loginData.username" required="">
                    </div>

                    <div class="form-group">
                        <input type="password" class="form-control" placeholder="Password" ng-model="loginData.password" required="">
                    </div>
                    <a href="forgotPassword.php" style="font-size: 12px;color: white;">Forgot password?</a><br/>
                    <button type="submit" class="btn btn-primary block full-width m-b" ng-click="doLogin(loginData)">Login</button>
                    <p class="text-muted text-center" style="color:white"><small>Dont have an account?</small></p>
                    <a class="btn btn-sm btn-danger btn-block" href="register.php">Create an Account</a>
                </form>
                <p style="color: white" class="m-t"> <small>Vivikta Technologies &copy; 2016-17</small> </p>
            </div>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="js/jquery-2.1.1.js"></script>
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
