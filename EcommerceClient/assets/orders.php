<?php
    include "sidebar.php";
    include "plugins.php";
    include "header.php";
   /* if(!isset($_COOKIE['anantya_session'])){
        header("Location:login.php");
    }*/
?>
<!DOCTYPE html>
<html>
<head>
  <?php echo loadPlugins() ?>
  <title>ECommerce | Orders</title>
  <script src="js/controllers/orderController.js"></script>
  <script src="js/controllers/sidemenuController.js"></script>
</head>

<body ng-app="anantya" ng-controller="OrderController"  ng-init="loadOrders('0','0','0','0')">
  <div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation" ng-controller="SidemenuController">
      <div class="sidebar-collapse">
        <?php echo loadSideBar() ?>
      </div>
    </nav>
    <div id="page-wrapper" class="gray-bg">
      <div class="row border-bottom">
        <?php echo loadHeader() ?>
      </div>
        <div class="row wrapper border-bottom white-bg page-heading">
          <div class="col-sm-4">
            <h2><i class="fa fa-arrow-circle-o-down fa-2x"></i>&nbsp;&nbsp;Orders</h2>
              <ol class="breadcrumb">
                <li>
                  <a href="index.php">Dashboard</a>
                </li>
                <li class="active">
                  <strong>Orders</strong>
                </li>
              </ol>
          </div>
        </div>
        <div class="row ng-cloak" ng-show="ordersPresent" id="ordersList">
          <div class="col-lg-11 card cardLayout">
            <div class="ibox float-e-margins">
              <div class="ibox-content">
                <div class="row">
                <form name="filterForm">
                  <div class="col-sm-3">
                    <div class="input-group">
                      <input type="text" placeholder="Search" class="input-sm form-control" ng-model="search"> <span class="input-group-btn">
                      <button type="button" class="btn btn-sm btn-info"><i class="fa fa-search"></i></button> </span>
                    </div>
                  </div>
                    <div class="col-sm-3" ng-init="loadProducts()">
                        <select class="form-control customInputForm customSelectForm"  placeholder="Product Filter"  id="filter.productId" ng-model="filter.productId" required="">
                            <option value="" disabled selected>Choose Product</option>
                            <option ng-repeat="product in products" value="{{product.productId}}" >
                                {{product.productName}}
                            </option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <select class="form-control customInputForm customSelectForm"  placeholder="Status Filter"  id="filter.productStatus" ng-model="filter.productStatus" required="">
                            <option value="" disabled selected>Choose Status</option>
                            <option  value="1" >Order Recieved</option>
                            <option  value="2" >Payment Recieved</option>
                            <option  value="3" >Shipping In Progress</option>
                            <option  value="4" >Shipped</option>
                        </select>
                    </div>
                    <button data-toggle="tooltip" title="Add filter" ng-disabled="filterForm.$invalid" class="btn btn-info customButton" ng-click="loadOrders(filter.date,filter.productId,filter.productStatus,'1')" style="border:none;margin-left: 2%"><i class="fa fa-filter" aria-hidden="true"></i></button> 

                    <button data-toggle="tooltip" title="Clear filter" class="btn btn-info customButton" ng-click="clearFilter(filter)" ng-disabled="filterForm.$invalid" style="border:none;margin-left: 2%"><i class="fa fa-eraser" aria-hidden="true"></i></button> 
                  </form>
                </div>
                <div class="table-responsive ng-cloak">
                  <table class="table table-striped customTable">
                    <thead>
                        <tr>
                            <th data-field="id">#</th>
                            <th data-field="name">User Name</th>
                            <th data-field="orderId">Phone Number</th>
                            <th data-field="status">Current Status</th>
                            <th data-field="status">Status</th>
                            <th data-field="change">Change</th>
                            <th data-field="address">Details</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="order in ordersList|filter:search">
                            <td>{{$index+1}}</td>
                            <td>{{order.userName}}</td>
                            <td>{{order.userPhoneNumber}}</td>
                            <td ng-if="order.productStatus==1">Order Recieved</td>
                            <td ng-if="order.productStatus==2">Payment Recieved</td>
                            <td ng-if="order.productStatus==3">Shipping in progress </td>
                            <td ng-if="order.productStatus==4">Order Shipped </td>
                            <td ng-if="order.productStatus==5">Order Rejected</td>
                            <td>
                              <select  class="form-control customInputForm customSelectForm" style="width:100px" id="order.changeStatus" ng-model="order.changeStatus">
                                <option value="" disabled selected>Choose Status</option>
                                <option  value="2" >Payment Recieved</option>
                                <option  value="3" >Shipping In Progress</option>
                                <option  value="4" >Shipped</option>
                                <option  value="5" >Rejected</option>
                              </select>
                            </td>
                            <td>
                              <button data-toggle="tooltip" title="Change Status" ng-disabled="order.changeStatus==null" type="button" class="btn btn-info btn-circle customButton" style="color: white"  ng-click="changeStatus(order,order.changeStatus)"><i class="fa fa-pencil-square-o"></i></button>
                            </td>
                            <td>
                                <button type="button" class="btn btn-info btn-circle customButton"  ng-click="loadOrderDetails(order)" data-toggle="modal" data-target="#viewOrdersModal"><i class="fa fa-weibo"></i></button>
                            </td>
                        </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>         
        <div class="row ng-cloak" ng-hide="ordersPresent" id="noOrdersInfo">
          <div class="col-lg-12">
            <div class="wrapper wrapper-content">
              <div class="middle-box text-center animated fadeInRightBig">
                <h3 class="font-bold">No Orders found</h3>
              </div>
            </div>
          </div>
        </div>
      
    </div>
  </div>
  <div class="modal inmodal" id="viewOrdersModal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content animated fadeIn customModal">
        <div class="modal-header customModalHeading">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <i class="fa fa-arrow-circle-o-down fa-3x"></i>
            <h4 class="customModal-title">Order Details</h4>
        </div>
        <div class="modal-body">
          Username :{{orders.userName}}<br/>
          User Address :{{orders.userAddress}}</br/>
          Phone Number : {{orders.userPhoneNumber}}<br/>
          <div ng-if="orders.productStatus==1">
              Order Date:{{orders.orderRecievedDate}}<br/><br/>
          </div>
          <div ng-if="orders.productStatus==2">
              Order Date:{{orders.orderRecievedDate}}<br/>
              Payment Recieved Date :{{orders.paymentRecievedDate}}<br/><br/>
          </div>
          <div ng-if="orders.productStatus==3">
              Order Date:{{orders.orderRecievedDate}}<br/>
              Payment Recieved Date :{{orders.paymentRecievedDate}}<br/>
              Shippind Date :{{orders.shippingInProgressDate}}<br/><br/>
          </div>
          <div ng-if="orders.productStatus==4">
              Order Date:{{orders.orderRecievedDate}}<br/>
              Payment Recieved Date :{{orders.paymentRecievedDate}}<br/>
              Shippind Date :{{orders.shippingInProgressDate}}<br/>
              Delievered Date :{{orders.shippedDate}}<br/><br/>
          </div>
          <div class="table-responsive ng-cloak">
            <table class="table table-striped customTable">
              <thead>
                  <tr>
                      <th data-field="id">#</th>
                      <th data-field="name">Product Name</th>
                      <th data-field="orderId">Order Id</th>
                      <th data-field="color">Color</th>
                      <th data-field="amount">Amount</th>
                      <th data-field="quantity">Quantity</th>
                      <th data-field="image">Image</th>
                  </tr>
              </thead>
              <tbody>
                  <tr ng-repeat="quantityDetails in orders.productDetails">
                      <td style="width:50px">{{$index+1}}</td>
                      <td style="width:170px">{{quantityDetails.productName}}</td>
                      <td style="width:100px">{{orders.cartId}}</td>
                      <td style="width:100px">{{quantityDetails.productColor}}</td>
                      <td style="width:100px">
                      {{quantityDetails.productPrice}}</td>
                      <td style="width:100px">{{quantityDetails.quantity}}</td>
                      <td><img src={{quantityDetails.thumbnail}} style="width:30px;height: 30px"></td>
                  </tr>
              </tbody>
            </table>      
          </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-white customButton" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <script>
    $(document).ready(function(){
     $('[data-toggle="tooltip"]').tooltip();   
    });
  </script>
</body>
</html>
