<!DOCTYPE html>
<html>
<head>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Forgot Password | ECommerce</title>
   <link rel='shortcut icon' href='img/favicon.png'>
   <link href="css/bootstrap.min.css" rel="stylesheet">
   <link href="font-awesome/css/font-awesome.css" rel="stylesheet">
   <link href="css/plugins/iCheck/custom.css" rel="stylesheet">
   <link href="css/animate.css" rel="stylesheet">
   <link href="css/style.css" rel="stylesheet">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
   <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
   <script src='js/controllers/debugConsole.js'></script>
   <script src="js/controllers/config.js"></script>
   <script src="js/controllers/app.js"></script>
   <script src="js/controllers/loginController.js"></script>
</head>
<body class="gray-bg" ng-app="anantya" ng-controller="LoginController" style="background-image:url('img/img.jpg');background-repeat: no-repeat;background-size: cover;">

    <div class="middle-box text-center loginscreen animated fadeInDown" style="padding-top: 10%;padding-left:30%">
      <div style="background-color:rgba(0,0,0,0.5);padding:20px;width: 400px;color: white">
        <h3 style="color: white">Welcome to ECommerce</h3>
          <div ng-hide="passwordResetSuccessful">
             <h3 style="color:white">Forgot Password</h3>
             <form class="m-t" role="form" name="forgotPasswordForm" style="color:black">
                <div class="form-group">
                   <input type="email" class="form-control" placeholder="Email Id" required="" ng-model="forgotPasswordDetails.emailId" ng-pattern="/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/">
                </div>
                <p ng-show="userInvalidError" class="text-center" style="color:#FFFFFF">Invalid Email Id!</p>
                <button type="submit" class="btn btn-danger block full-width m-b" ng-click="forgotPassword(forgotPasswordDetails)" ng-disabled="forgotPasswordForm.$invalid">Submit</button>
             </form>
             <p class="m-t" style="color:white"> <small>Vivikta Technologies &copy; 2016-2017</small> </p>
          </div>
          <div ng-show="passwordResetSuccessful">
            <div>
                <h1 class="logo-name"><img src="img/img.jpg" width="100" height="100"/></h1>
             </div>
            <h3 style="color:white">We have sent you an email to your registered email on ECommerce to reset your password. Please follow the instructions in the email.</h3>
          </div>
        </div>
      </div>
    </div>

    <!-- Mainly scripts -->
    <script src="js/jquery-2.1.1.js"></script>
    <script src="js/bootstrap.min.js"></script>

</body>
</html>