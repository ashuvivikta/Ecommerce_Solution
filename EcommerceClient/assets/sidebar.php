<?php
  function loadSideBar()
  {
  	$sidebar = "<ul class='nav' id='side-menu'>
      <li class='nav-header'>
        <div class='dropdown profile-element'> 
          <span>
            <img id='profilePicture' src={{profilePicturePath}} alt='image' class='img-circle' style='width:70px;height:70px'/>
          </span>
          <a data-toggle='dropdown' class='dropdown-toggle' href='#' style='color:white !important'>
            <span class='clear'><span class='block m-t-xs'> <strong class='font-bold ng-cloak'>{{name}}</strong>
            </span> 
            <span class='text-muted text-xs block'><label style='color:white !important'>Online</label><b class='caret' style='color:white !important'></b></span> 
            </span>
          </a>
          <ul class='dropdown-menu animated fadeInRight m-t-xs'>
            <li><a href='profile.php'>Profile</a></li>
            <li class='divider'></li>
            <li><a href='login.php'>Logout</a></li>
          </ul>
        </div>
        <div class='logo-element'>
              ANA
        </div>
      </li>
      <li>
        <a href='index.php'><i class='fa fa-th-large'></i> <span class='nav-label'>Dashboard</span></a>
      </li>
      <li>
        <a href='categories.php'><i class='fa fa-tags'></i> <span class='nav-label'>Categories</span></a>
      </li>
      <li>
        <a href='subCategories.php'><i class='fa fa-tags'></i> <span class='nav-label'>Sub Categories</span></a>
      </li>
      <li>
        <a href='colors.php'><i class='fa fa-paint-brush'></i> <span class='nav-label'>Colors</span></a>
      </li>
      <li>
        <a href='products.php'><i class='fa fa-cart-plus'></i> <span class='nav-label'>Products</span></a>
      </li>
      <li>
        <a href='images.php'><i class='fa fa-picture-o'></i> <span class='nav-label'>Images</span></a>
      </li>
      <li>
        <a href='orders.php'><i class='fa fa-arrow-circle-o-down'></i> <span class='nav-label'>Orders</span></a>
      </li>
      <li>
        <a href='complaints.php'><i class='fa fa-exclamation-triangle'></i> <span class='nav-label'>Complaints</span></a>
      </li>
      <li>
        <a href='suggestions.php'><i class='fa fa-paper-plane'></i> <span class='nav-label'>Suggestion</span></a>
      </li>
      <li>
        <a href='profile.php'><i class='fa fa-cogs'></i> <span class='nav-label'>Profile</span></a>
      </li>
      </ul>";
 		return $sidebar;
  }
?>