<?php
    include "sidebar.php";
    include "plugins.php";
    include "header.php";
   /* if(!isset($_COOKIE['anantya_session'])){
        header("Location:login.php");
    }*/
?>
<!DOCTYPE html>
<html>
<head>
  <?php echo loadPlugins() ?>
  <title>ECommerce | Complaints</title>
  <script src="js/controllers/complaintsController.js"></script>
  <script src="js/controllers/sidemenuController.js"></script>
</head>

<body ng-app="anantya" ng-controller="ComplaintsController" ng-init="loadComplaints(0,'')">
  <div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation" ng-controller="SidemenuController">
      <div class="sidebar-collapse">
        <?php echo loadSideBar() ?>
      </div>
    </nav>
    <div id="page-wrapper" class="gray-bg">
      <div class="row border-bottom">
        <?php echo loadHeader() ?>
      </div>
        <div class="row wrapper border-bottom white-bg page-heading">
          <div class="col-sm-4">
            <h2><i class="fa fa-exclamation-triangle fa-2x"></i>&nbsp;&nbsp;Complaints</h2>
              <ol class="breadcrumb">
                <li>
                  <a href="index.php">Dashboard</a>
                </li>
                <li class="active">
                  <strong>Complaints</strong>
                </li>
              </ol>
          </div>
        </div>
        <div class="row ng-cloak" ng-show="complaintsPresent" id="complaintsList">
          <div class="col-lg-11 card cardLayout">
            <div class="ibox float-e-margins">
              <div class="ibox-content">
                <div class="row">
                  <div class="col-sm-3">
                    <div class="input-group">
                      <input type="text" placeholder="Search" class="input-sm form-control" ng-model="search"> <span class="input-group-btn">
                      <button type="button" class="btn btn-info btn-sm"><i class="fa fa-search"></i></button> </span>
                    </div>
                  </div>
                </div>
                <div class="table-responsive ng-cloak">
                  <table class="table table-striped customTable">
                    <thead>
                      <tr>
                          <th data-field="id">#</th>
                          <th data-field="name" style="width:250px">Name</th>
                          <th data-field="pname">Product</th>
                          <th data-field="model">Model</th>
                          <th data-field="view">Complaints</th>
                          <th data-field="replay">Replay</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr ng-repeat="complaint in complaints|filter:search">
                          <td>{{$index+1}}</td>
                          <td>{{complaint.userName}}</td>
                          <td><div ng-repeat="product in complaint.productDetails">{{product.productName}}</div></td>
                          <td><div ng-repeat="product in complaint.productDetails">{{product.productModel}}</div></td>
                          <td>{{complaint.userComplaint}}</td>
                          <td ng-if="complaint.adminAnswer=='N/A'">
                              <button type="button" class="btn btn-info btn-circle btn-outline customButton"  data-toggle="modal" ng-click="loadComplaintDetails(complaint)" data-target="#addReplayModal"><i class="fa fa-reply-all"></i></button>
                          </td>
                          <td ng-if="complaint.adminAnswer!='N/A'">
                              {{complaint.adminAnswer}}
                          </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>    
        <div class="text-center ng-cloak" ng-show="showPagination">
            <div class="btn-group">
                <button ng-disabled="currentPage == 1" class="btn btn-white customButton" max-size="maxSize" boundary-links="true" ng-click="loadComplaints(1,previousPageUrl)"><i class="fa fa-chevron-left"></i></button>
                <button class="btn btn-primary ng-cloak customButton" max-size="maxSize" boundary-links="true">{{currentPage}}/{{lastPage}}</button>
                <button ng-disabled="currentPage == lastPage" class="btn btn-white customButton" max-size="maxSize" boundary-links="true" ng-click="loadComplaints(1,nextPageUrl)"><i class="fa fa-chevron-right"></i></button>
            </div>
        </div>      
        <div class="row ng-cloak" ng-hide="complaintsPresent" id="noComplaintsInfo">
          <div class="col-lg-12">
            <div class="wrapper wrapper-content">
              <div class="middle-box text-center animated fadeInRightBig">
                <h3 class="font-bold">No complaints found</h3>
              </div>
            </div>
          </div>
        </div>
      
    </div>
  </div>
  <div class="modal inmodal" id="addReplayModal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content animated fadeIn customModal">
        <div class="modal-header customModalHeading">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <i class="fa fa-exclamation-triangle fa-2x"></i>
            <h4 class="customModal-title">Add Replay</h4>
        </div>
        <div class="modal-body">
          <form name="addReplayForm">
            <div class="modal-body">
              <div class="form-group">
                <div class="col-sm-3">
                  <label>Replay *</label> 
                </div>
                <div class="col-sm-9">
                  <textarea placeholder="Replay" class="form-control customInputForm" ng-model="complaintData.adminAnswer" required></textarea>
                </div>
              </div>
            </div>
          </form>   
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-white customButton" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-white customButton" data-dismiss="modal" ng-click="addNewReplay(complaintData)">Add</button>
        </div>
      </div>
    </div>
  </div>
</body>
</html>
