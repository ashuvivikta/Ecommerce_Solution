<?php
    include_once 'plugins.php';
?>
<!DOCTYPE html>
<html>
<head>
    <title>ECommerce | Register</title>
    <?php echo loadPlugins()?>
    <script src="js/controllers/registrationController.js"></script>
</head>
<body class="gray-bg" ng-app="anantya" ng-controller="RegistrationController" style="background-image: url('img/img.jpg');background-repeat: no-repeat;background-size: cover;">
    <div class="middle-box text-center loginscreen animated fadeInDown ng-cloak" ng-hide="registrationStatus" style="padding-left: 30%">
        <div style="background-color:rgba(0,0,0,0.5);padding:20px;width: 400px;color: white">
            <h3>Register to ECommerce</h3>
            <p>Create account to see it in action.</p>
            <form class="m-t" role="form" name="registrationForm" style="color:black">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Name" ng-model="registrationData.name" required="">
                </div>
                <div class="form-group">
                    <input type="email" class="form-control" placeholder="Email" ng-model="registrationData.emailId" ng-pattern="/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/" required="" spellcheck="false">
                </div>
                <div class="form-group">
                    <input type="number" class="form-control" placeholder="Phone Number" ng-model="registrationData.phoneNumber"  ng-minlength="10" ng-maxlength="10" ng-pattern="/^[0-9]{10}$/" required="">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Address" ng-model="registrationData.address" required="">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="DomainName" ng-model="registrationData.domainName" required="">
                    <p style="color:white;text-align: left">www.example.com</p>
                </div>
                <div class="form-group">
                    <div class="checkbox i-checks" style="color:white"><label> <input type="checkbox" id="agreeCheckBox"><i></i> Agree the terms and policy </label></div>
                    <p ng-show="agreeError" class="text-center ng-cloak" style="color:red">Please agree the terms and conditions</p>
                    <p ng-show="registrationError" class="text-center ng-cloak">Could not be registered. Please try again!</p>
                    <p ng-show="userExistsError" class="text-center ng-cloak" style="color:red;">Email id is already registered. Try login in!</p>
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b" ng-disabled="registrationForm.$invalid" ng-click="doRegistration(registrationData)">Register</button>

                <p class="text-muted text-center" style="color:white"><small>Already have an account?</small></p>
                <a class="btn btn-sm btn-danger btn-block" href="login.php">Login</a>
            </form>
            <p class="m-t"> <small>Vivikta Technologies &copy; 2016-17</small> </p>
        </div>
    </div>
    <div class="text-center loginscreen animated fadeInDown ng-cloak" ng-show="registrationStatus">
        <div style="background-color:rgba(0,0,0,0.7);padding:20px;width: 650px;margin: 20px auto;">
            <br/>
            <h2 class="text-center ng-cloak" style="color: #FFFFFF;">Registration Successful!<br/>A confirmation mail has been sent to your registered Email Id.<br/> Please confirm the account to continue</h2>
            <p class="m-t" style="color: #FFFFFF;"> <small>Vivikta Technologies &copy; 2016</small> </p>
        </div>
    </div>
    <script>
        $(document).ready(function(){
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        });
    </script>
</body>

</html>
