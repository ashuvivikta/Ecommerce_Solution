<?php
    include "sidebar.php";
    include "plugins.php";
    include "header.php";
   /* if(!isset($_COOKIE['anantya_session'])){
        header("Location:login.php");
    }*/
?>
<!DOCTYPE html>
<html>
<head>
  <?php echo loadPlugins() ?>
  <title>ECommerce | Images</title>
  <script src="js/controllers/imagesController.js"></script>
  <script src="js/controllers/editController.js"></script>
  <script src="js/controllers/sidemenuController.js"></script>
  <script src="js/controllers/debugConsole.js"></script>
</head>

<body ng-app="anantya" ng-controller="ImagesController" ng-init="loadImages()">
  <div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation" ng-controller="SidemenuController">
      <div class="sidebar-collapse">
        <?php echo loadSideBar() ?>
      </div>
    </nav>
    <div id="page-wrapper" class="gray-bg">
      <div class="row border-bottom">
        <?php echo loadHeader() ?>
      </div>
      <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-4">
          <h2><i class="fa fa-tags fa-2x"></i>&nbsp;&nbsp;Images</h2>
            <ol class="breadcrumb">
              <li>
                <a href="index.php">Dashboard</a>
              </li>
              <li class="active">
                <strong>Images</strong>
              </li>
            </ol>
        </div>
        <div class="col-sm-8">
        <div class="title-action" ng-if="imagesPresent==false">
          <button type="button" class="btn btn-primary" ng-click="toggleAddImage()"><i class="fa fa-plus"></i>&nbsp;Images</button>
        </div>
        <div class="title-action" ng-if="imagesPresent==true">
          <button type="button" class="btn btn-info btn-circle btn-outline pull-right" ng-click="toggleEditImage(images[0].mainPageId)">
            <i class="fa fa-pencil"></i>
          </button>
        </div>
        </div>
      </div>
      <div class="row ng-cloak" id="ImageList">
        <div class="col-lg-11 card cardLayout" style="margin-bottom:5%;">
          <div class="ibox float-e-margins">
            <div class="ibox-content" ng-if="imagesPresent">
              <center ng-repeat="image in images">
                <div class="row imageCard">
                  <div class="col-lg-12 text-center imageCardHeading">
                    <h5 class="breadcrumbs-title">Logo</h5>
                  </div>
                  <img  src="{{image.vendorLogo}}" style="width:200px;height: 200px;padding-left: 2%;"/>
                </div>
                <div class="row imageCard">
                  <div class="col-lg-12 text-center imageCardHeading">
                    <h5 class="breadcrumbs-title">Images</h5>
                  </div>
                  <div class="col-xs-12">
                      <div id="gallery_01">
                          <div class="col-xs-4 container" ng-repeat="img in image.sliderImageDetails" style="margin-top:2%;">
                              <img class="zoom_03" src="{{img.sliderImagePath}}" alt="" style="height: 250px; width: 250px;">
                              <div class="topleft" style="background-color: black">{{img.sliderImageHeading}}</div>
                              <p>{{img.sliderImageDescription}}</p>
                          </div>
                      </div>
                  </div>
                </div>
              </center>
              <div style="padding-top: 10px;font-size: 20px">
                  <p ng-if="images[0].facebookLink!=null"><i class="fa fa-facebook" aria-hidden="true"></i> : <a ng-href="{{images[0].facebookLink}}">{{images[0].facebookLink}}</a></p><br/>
                  <p ng-if="images[0].googlePlusLink!=null"><i class="fa fa-google-plus" aria-hidden="true"></i> : <a ng-href="{{images[0].googlePlusLink}}">{{images[0].googlePlusLink}}</a></p><br/>
                  <p ng-if="images[0].twitterLink!=null"><i class="fa fa-twitter" aria-hidden="true"></i> : <a ng-href="{{images[0].twitterLink}}">{{images[0].twitterLink}}</a></p><br/>
                  <p ng-if="images[0].instagramLink!=null"><i class="fa fa-instagram" aria-hidden="true"></i> : <a ng-href="{{images[0].instagramLink}}">{{images[0].instagramLink}}</a></p><br/>
              </div>
            </div>
            <div class="row ng-cloak" ng-hide="imagesPresent">
              <div class="col-lg-12">
                <div class="wrapper wrapper-content">
                  <div class="middle-box text-center animated fadeInRightBig">
                    <h3 class="font-bold">No Images found</h3>
                      <div class="error-desc">
                        Add Images to view them
                      </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>         
      </div>
      <div class="row" id="addImageForm" style="display: none;">
        <div class="col-lg-3">
        </div>
        <div class="col-lg-11 card cardLayout">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Add Images here</h5>
                    <div class="ibox-tools">
                        <a>
                            <i class="fa fa-times" ng-click="toggleAddImage()"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form method="get" class="form-horizontal" name="addImageForm">
                      <div class="row">
                        <div class="form-group">
                          <label class="col-sm-3 control-label">Logo *</label>
                          <div class="col-sm-6">
                            <input type="file" class="form-control customInputForm customSelectForm" file-model="vendorLogo" id="vendorLogo" name="vendorLogo" required>
                          </div>
                        </div>
                      </div>
                        <div class="hr-line-dashed"></div>
                        <p style="text-align: center"><strong>Slider Image 1</strong></p>
                      <div class="row">
                        <div class="form-group">
                          <label class="col-sm-3 control-label">Name *</label> 
                          <div class="col-sm-6">
                            <input type="text" placeholder="Name" class="form-control customInputForm" ng-model="image.sliderImageHeading1" required>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-3 control-label">Description *</label> 
                          <div class="col-sm-6">
                            <input type="text" placeholder="Description" class="form-control customInputForm" ng-model="image.sliderImageDescription1" required>
                          </div>
                        </div>
                        <div class="form-group form-inline">
                          <label class="col-sm-3 control-label">Image *</label> 
                          <div class="col-sm-6">
                            <input type="file" class="form-control customInputForm customSelectForm" file-model="sliderImage" id="sliderImage1" style="width:100%;" required>
                          </div>
                        </div>
                        <div class="form-group form-inline">
                          <label class="col-sm-3 control-label">Product *</label> 
                          <div class="col-sm-6" ng-init="loadProducts()">
                            <select class="form-control customInputForm customSelectForm" ng-model="image.productId1" style="width:100%;">
                                <option ng-repeat="product in products" value="{{product.productId}}">{{product.productName}}</option>
                            </select>
                          </div>
                        </div>
                      </div>
                        <div class="hr-line-dashed"></div>
                        <p style="text-align: center"><strong>Slider Image 2</strong></p>
                      <div class="row">
                        <div class="form-group">
                          <label class="col-sm-3 control-label">Name *</label> 
                          <div class="col-sm-6">
                            <input type="text" placeholder="Name" class="form-control customInputForm" id="sliderImageHeading2" ng-model="image.sliderImageHeading2" name="sliderImageHeading[]" required>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-3 control-label">Description *</label> 
                          <div class="col-sm-6">
                            <input type="text" placeholder="Description" class="form-control customInputForm" id="sliderImageDescription2" name="sliderImageHeading[]" ng-model="image.sliderImageDescription2" required>
                          </div>
                        </div>
                        <div class="form-group form-inline">
                          <label class="col-sm-3 control-label">Image *</label> 
                          <div class="col-sm-6">
                            <input type="file" class="form-control customInputForm customSelectForm" file-model="sliderImage" id="sliderImage2" style="width:100%;" required>
                          </div>
                        </div>
                        <div class="form-group form-inline">
                          <label class="col-sm-3 control-label">Product *</label> 
                          <div class="col-sm-6" ng-init="loadProducts()">
                            <select class="form-control customInputForm customSelectForm" ng-model="image.productId2" style="width:100%;">
                                <option ng-repeat="product in products" value="{{product.productId}}">{{product.productName}}</option>
                            </select>
                          </div>
                        </div>
                      </div>
                        <div class="hr-line-dashed"></div>
                        <p style="text-align: center"><strong>Slider Image 3</strong></p>
                      <div class="row">
                        <div class="form-group">
                          <label class="col-sm-3 control-label">Name *</label> 
                          <div class="col-sm-6">
                            <input type="text" placeholder="Name" class="form-control customInputForm"  id="sliderImageHeading3" ng-model="image.sliderImageHeading3" required>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-3 control-label">Description *</label> 
                          <div class="col-sm-6">
                            <input type="text" placeholder="Description" class="form-control customInputForm" id="sliderImageDescription3" ng-model="image.sliderImageDescription3" required>
                          </div>
                        </div>
                        <div class="form-group form-inline">
                          <label class="col-sm-3 control-label">Image *</label> 
                          <div class="col-sm-6">
                            <input type="file" class="form-control customInputForm customSelectForm" file-model="sliderImage" id="sliderImage3" style="width:100%;" required>
                          </div>
                        </div>
                        <div class="form-group form-inline">
                          <label class="col-sm-3 control-label">Product *</label> 
                          <div class="col-sm-6" ng-init="loadProducts()">
                            <select class="form-control customInputForm customSelectForm" ng-model="image.productId3" style="width:100%;">
                                <option ng-repeat="product in products" value="{{product.productId}}">{{product.productName}}</option>
                            </select>
                          </div>
                        </div>
                      </div>
                        <div class="hr-line-dashed"></div>
                      <div class="row">
                        <div class="form-group">
                          <label class="col-sm-3 control-label"><i class="fa fa-facebook" aria-hidden="true"></i>*</label> 
                          <div class="col-sm-6">
                            <input type="text" class="form-control customInputForm" ng-model="image.facebookLink">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-3 control-label"><i class="fa fa-google-plus" aria-hidden="true"></i>*</label> 
                          <div class="col-sm-6">
                            <input type="text" class="form-control customInputForm" ng-model="image.googlePlusLink">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-3 control-label"><i class="fa fa-twitter" aria-hidden="true"></i>*</label> 
                          <div class="col-sm-6">
                            <input type="text" class="form-control customInputForm" ng-model="image.twitterLink">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-3 control-label"><i class="fa fa-instagram" aria-hidden="true"></i>*</label> 
                          <div class="col-sm-6">
                            <input type="text" class="form-control customInputForm" ng-model="image.instagramLink">
                          </div>
                        </div>
                      </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="pull-right" style="padding-right: 20px">
                                <button class="btn btn-primary customButton" ng-disabled=addImageForm.$invalid ng-click="addNewImages(image)">Add Images</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
        </div>
      </div>
      <div class="row" id="editImageForm" style="display: none;">
        <div class="col-lg-3">
        </div>
        <div class="col-lg-11 card cardLayout">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Update Images here</h5>
                    <div class="ibox-tools">
                        <a>
                            <i class="fa fa-times" ng-click="toggleEditImage()"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form method="get" class="form-horizontal" name="editImageForm" ng-controller="EditController">
                        <div class="row">
                          <div class="form-group">
                            <label class="col-sm-3 control-label">Logo *</label>
                            <div class="col-sm-6">
                              <input type="file" class="form-control customInputForm customSelectForm" file-model="vendorLogo" id="vendorLogo1" name="vendorLogo" required>
                            </div>
                          </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <p style="text-align: center"><strong>Slider Image 1</strong></p>
                        <div class="row">
                          <div class="form-group">
                            <label class="col-sm-3 control-label">Name *</label> 
                            <div class="col-sm-6">
                              <input type="text" placeholder="Name" class="form-control customInputForm" ng-model="editImage.sliderImageHeading1" required>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-3 control-label">Description *</label> 
                            <div class="col-sm-6">
                              <input type="text" placeholder="Description" class="form-control customInputForm" ng-model="editImage.sliderImageDescription1" required>
                            </div>
                          </div>
                          <div class="form-group form-inline">
                          <label class="col-sm-3 control-label">Image *</label> 
                            <div class="col-sm-6">
                              <input type="file" class="form-control customInputForm customSelectForm" file-model="sliderImage" id="sliderImage4" style="width:100%" required>
                            </div>
                          </div>
                          <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Product *</label> 
                            <div class="col-sm-6" ng-init="loadProducts()">
                              <select class="form-control customInputForm customSelectForm" ng-model="editImage.productId1" style="width:100%">
                                  <option ng-repeat="product in products" value="{{product.productId}}">{{product.productName}}</option>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <p style="text-align: center"><strong>Slider Image 2</strong></p>
                        <div class="row">
                          <div class="form-group">
                            <label class="col-sm-3 control-label">Name *</label> 
                            <div class="col-sm-6">
                              <input type="text" placeholder="Name" class="form-control customInputForm" id="sliderImageHeading2" ng-model="editImage.sliderImageHeading2" name="sliderImageHeading[]" required>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-3 control-label">Description *</label> 
                            <div class="col-sm-6">
                              <input type="text" placeholder="Description" class="form-control customInputForm" id="sliderImageDescription2" name="sliderImageHeading[]" ng-model="editImage.sliderImageDescription2" required>
                            </div>
                          </div>
                          <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Image *</label> 
                            <div class="col-sm-6">
                              <input type="file" class="form-control customInputForm customSelectForm" file-model="sliderImage" id="sliderImage5" style="width: 100%;" required>
                            </div>
                          </div>
                          <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Product *</label> 
                            <div class="col-sm-6" ng-init="loadProducts()">
                              <select class="form-control customInputForm customSelectForm" ng-model="editImage.productId2" style="width: 100%;">
                                <option ng-repeat="product in products" value="{{product.productId}}">{{product.productName}}</option>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <p style="text-align: center"><strong>Slider Image 3</strong></p>
                        <div class="row">
                          <div class="form-group">
                            <label class="col-sm-3 control-label">Name *</label> 
                            <div class="col-sm-6">
                              <input type="text" placeholder="Name" class="form-control customInputForm"  id="sliderImageHeading3" ng-model="editImage.sliderImageHeading3" required>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-3 control-label">Description *</label> 
                            <div class="col-sm-6">
                              <input type="text" placeholder="Description" class="form-control customInputForm" id="sliderImageDescription3" ng-model="editImage.sliderImageDescription3" required>
                            </div>
                          </div>
                          <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Image *</label> 
                            <div class="col-sm-6">
                              <input type="file" class="form-control customInputForm customSelectForm" file-model="sliderImage" id="sliderImage6" style="width:100%" required>
                            </div>
                          </div>
                          <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Product *</label> 
                            <div class="col-sm-6" ng-init="loadProducts()">
                              <select class="form-control customInputForm customSelectForm" ng-model="editImage.productId3" style="width: 100%;">
                                <option ng-repeat="product in products" value="{{product.productId}}">{{product.productName}}</option>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="row">
                          <div class="form-group">
                            <label class="col-sm-3 control-label"><i class="fa fa-facebook" aria-hidden="true"></i>*</label> 
                            <div class="col-sm-6">
                              <input type="text" class="form-control customInputForm" ng-model="editImage.facebookLink">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-3 control-label"><i class="fa fa-google-plus" aria-hidden="true"></i>*</label> 
                            <div class="col-sm-6">
                              <input type="text" class="form-control customInputForm" ng-model="editImage.googlePlusLink">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-3 control-label"><i class="fa fa-twitter" aria-hidden="true"></i>*</label> 
                            <div class="col-sm-6">
                              <input type="text" class="form-control customInputForm" ng-model="editImage.twitterLink">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-3 control-label"><i class="fa fa-instagram" aria-hidden="true"></i>*</label> 
                            <div class="col-sm-6">
                              <input type="text" class="form-control customInputForm" ng-model="editImage.instagramLink">
                            </div>
                          </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="pull-right" style="padding-right: 2%">
                                <button class="btn btn-primary customButton" ng-disabled=editImageForm.$invalid ng-click="editImages(mainPageId,editImage)">Update Images</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
        </div>
      </div>
    
  </div>
</body>
</html>
