<?php
    include "sidebar.php";
    include "plugins.php";
    include "header.php";
   /* if(!isset($_COOKIE['anantya_session'])){
        header("Location:login.php");
    }*/
?>
<!DOCTYPE html>
<html>
<head>
  <?php echo loadPlugins() ?>
  <title>ECommerce | Sub Categories</title>
  <script src="js/controllers/subCategoryController.js"></script>
  <script src="js/controllers/sidemenuController.js"></script>
  <script src="js/controllers/debugConsole.js"></script>
</head>

<body ng-app="anantya" ng-controller="SubCategoryController" ng-init="loadSubCategories(0,'')">
  <div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation" ng-controller="SidemenuController">
      <div class="sidebar-collapse">
        <?php echo loadSideBar() ?>
      </div>
    </nav>
    <div id="page-wrapper" class="gray-bg">
      <div class="row border-bottom">
        <?php echo loadHeader() ?>
      </div>
        <div class="row wrapper border-bottom white-bg page-heading">
          <div class="col-sm-4">
            <h2><i class="fa fa-tags fa-2x"></i>&nbsp;&nbsp;Sub Categories</h2>
              <ol class="breadcrumb">
                <li>
                  <a href="index.php">Dashboard</a>
                </li>
                <li class="active">
                  <strong>Sub Categories</strong>
                </li>
              </ol>
          </div>
          <div class="col-sm-8">
            <div class="title-action">
              <button type="button" class="btn btn-primary customButton" data-toggle="modal" data-target="#addSubCategoryModal"><i class="fa fa-plus"></i>&nbsp;Sub Categories</button>
            </div>
          </div>
        </div>
        <div class="row ng-cloak" ng-show="subCategoriesPresent" id="subCategoriesList">
          <div class="col-lg-11 card cardLayout">
            <div class="ibox float-e-margins">
              <div class="ibox-content">
                <div class="row">
                  <div class="col-sm-3">
                    <div class="input-group">
                      <input type="text" placeholder="Search" class="input-sm form-control" ng-model="search"> <span class="input-group-btn">
                      <button type="button" class="btn btn-sm btn-info"><i class="fa fa-search"></i></button> </span>
                    </div>
                  </div>
                </div>
                <div class="table-responsive ng-cloak">
                  <table class="table table-striped customTable">
                    <thead>
                      <tr>
                          <th data-field="id">#</th>
                          <th data-field="name" style="width:250px">Name</th>
                          <th style="width:150px" data-field="category">Category</th>
                          <th data-field="edit">Edit</th>
                      </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="subcategory in subcategories|filter:search">
                            <td>{{$index+1}}</td>
                            <td>{{subcategory.subcategoryName}}</td>
                            <td data-field="category">              
                                {{subcategory.categoryName}}
                            </td>
                            <td>
                                <button type="button" class="btn btn-info btn-circle btn-outline customButton"  ng-click="loadSubCategoryDetails(subcategory)" data-toggle="modal" data-target="#editSubCategoryModal"><i class="fa fa-pencil"></i></button>
                            </td>
                        </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>     
        <div class="text-center ng-cloak" ng-show="showPagination">
          <div class="btn-group">
              <button ng-disabled="currentPage == 1" class="btn btn-white customButton" max-size="maxSize" boundary-links="true" ng-click="loadSubCategories(1,previousPageUrl)"><i class="fa fa-chevron-left"></i></button>
              <button class="btn btn-primary ng-cloak customButton" max-size="maxSize" boundary-links="true">{{currentPage}}/{{lastPage}}</button>
              <button ng-disabled="currentPage == lastPage" class="btn btn-white customButton" max-size="maxSize" boundary-links="true" ng-click="loadSubCategories(1,nextPageUrl)"><i class="fa fa-chevron-right"></i></button>
          </div>
        </div>    
        <div class="row ng-cloak" ng-hide="subCategoriesPresent" id="noSubCategoriesInfo">
          <div class="col-lg-12">
            <div class="wrapper wrapper-content">
              <div class="middle-box text-center animated fadeInRightBig">
                <h3 class="font-bold">No Sub Categories found</h3>
                  <div class="error-desc">
                    Add Sub Categories to view them
                  </div>
              </div>
            </div>
          </div>
        </div>
    </div>
  </div>
  <div class="modal inmodal" id="editSubCategoryModal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content animated fadeIn customModal">
        <div class="modal-header customModalHeading">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <i class="fa fa-tags fa-2x"></i>
            <h4 class="customModal-title">Update Sub Category</h4>
        </div>
        <div class="modal-body">
          <form name="editSubCategoryForm">
            <div class="modal-body">
              <div class="form-group">
                <div class="row">
                  <div class="col-sm-3">
                     <label>Name *</label> 
                  </div>
                  <div class="col-sm-9">
                    <input type="text" placeholder="Name" class="form-control customInputForm" ng-model="editSubCategoryData.name" required>
                  </div>
                </div>
              </div>
              <div class="form-group" ng-init="loadCategories()">
                <div class="row">
                  <div class="col-sm-3">
                    <label>Category *</label>
                  </div>
                  <div class="col-sm-9">
                    <select ng-model="editSubCategoryData.categoryId" class="form-control customInputForm customSelectForm" name="categoryId" ng-show="categoriesPresent" required>
                      <option ng-repeat="category in categories" value="{{category.categoryId}}">
                        {{category.categoryName}}
                      </option>
                    </select>
                    <p ng-hide="categoriesPresent">Please add categories before adding subcategories</p>
                  </div>
                </div>
              </div>
            </div>
          </form>   
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-white customButton" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-white customButton" data-dismiss="modal" ng-click="updateSubCategory(editSubCategoryData)">Update</button>
        </div>
      </div>
    </div>
  </div>
  <div class="modal inmodal" id="addSubCategoryModal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content animated fadeIn customModal">
        <div class="modal-header customModalHeading">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <i class="fa fa-tags fa-2x"></i>
            <h4 class="customModal-title">Add Sub Category</h4>
        </div>
        <div class="modal-body">
          <form name="addSubCategoryForm">
            <div class="modal-body">
              <div class="form-group">
                <div class="row">
                  <div class="col-sm-3">
                    <label>Name *</label>
                  </div>
                  <div class="col-sm-9">
                    <input type="text" placeholder="Name" class="form-control customInputForm" ng-model="subCategoryData.name" required>
                  </div>
                  </div>
              </div>
              <div class="form-group" ng-init="loadCategories()">
                <div class="row">
                   <div class="col-sm-3">
                      <label>Category *</label>
                  </div>
                  <div class="col-sm-9">
                    <select ng-model="subCategoryData.categoryId" class="form-control customInputForm customSelectForm" name="categoryId" ng-show="categoriesPresent" required>
                      <option ng-repeat="category in categories" value="{{category.categoryId}}">
                        {{category.categoryName}}
                      </option>
                    </select>
                    <p ng-hide="categoriesPresent">Please add categories before adding subcategories</p>
                  </div>
                </div>
              </div>
            </div>
          </form>   
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-white customButton" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-white customButton" data-dismiss="modal" ng-click="addNewSubCategory(subCategoryData)">Add</button>
        </div>
      </div>
    </div>
  </div>
</body>
</html>
