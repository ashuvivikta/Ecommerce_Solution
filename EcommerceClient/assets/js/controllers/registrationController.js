if(DEV_MODE == 1)
	BASE_URL = DEV_URL;
app
.controller('RegistrationController',function($scope,$http){
	//function to handle the registration
	$scope.errorData = {
		registrationData : false
	};
	$scope.doRegistration = function(registrationData){
        if($("#agreeCheckBox").is(':checked')){
            $scope.agreeError = false;
            $http({
            	method : "POST",
            	url : BASE_URL+REGISTRATION_URL,
            	params : {
            		vendorName : registrationData.name,
            		vendorEmailId : registrationData.emailId,
            		vendorPhoneNumber : registrationData.phoneNumber,
            		vendorAddress : registrationData.address,
                    vendorDomainName : registrationData.domainName
            	}
            }).then(function success(response){
            	viviktaDebugConsole(JSON.stringify(response));
            	if(response.status == HTTP_STATUS_CODE){
            		if(response.data.errorCode == REGISTRATION_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT){
            			$scope.errorData.registrationError = false;
            			$scope.registrationStatus = true;
            		}
                    else if(response.data.errorCode == REGISTRATION_EXISTS_SUCCESS_CODE && response.data.statusText == FAILURE_STATUS_TEXT){
                        $scope.userExistsError = true;
                    }
            		else{
            			$scope.errorData.registrationError = true;
            		}
            	}
            	else{
            		$scope.errorData.registrationError = false;
            		$scope.registrationStatus = false;
            	}
            },function failure(){
            	$scope.errorData.registrationError = false;
            	$scope.registrationStatus = false;
            });
        }
        else{
            $scope.agreeError = true;
        }
	};
})