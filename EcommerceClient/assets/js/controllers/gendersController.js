if(DEV_MODE == 1)
	BASE_URL = DEV_URL;
app
.controller('GendersController',function($scope,$http){
	$scope.authToken = localStorage.getItem("anantya_token");

	//function to load the list of all genders
	$scope.loadGenders = function(){
		$http({
			method : "GET",
			url : BASE_URL+GET_GENDERS_LIST_URL,
			params : {
				token : $scope.authToken
			}
		}).then(function success(response){
			viviktaDebugConsole(JSON.stringify(response));
			if(response.status == HTTP_STATUS_CODE){
				if(response.data.errorCode == GET_GENDERS_LIST_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT && response.data.data != ''){
					$scope.gendersPresent = true;
					$scope.genders = response.data.data;
				}
				else{
					$scope.gendersPresent = false;
					displayToast("No genders found!",4000);
				}
			}
			else{
				$scope.gendersPresent = false;
				displayToast("Unable to load the genders!",4000);
			}
		},function failure(){
			viviktaDebugConsole("GendersController - loadGenders - API not found!");
			$scope.gendersPresent = false;
			displayToast("Unable to load the genders!",4000);
		});
	};

	//function to add a gender
	$scope.addNewGender = function(genderData){
		if(genderData.name == '' || genderData.name == null){
			displayToast("Please provide a name for gender!",4000);
		}
		else{
			$http({
				method : "POST",
				url : BASE_URL+ADD_GENDER_URL,
				params : {
					token : $scope.authToken,
					gender : genderData.name
				}
			}).then(function success(response){
				if(response.status == HTTP_STATUS_CODE){
					viviktaDebugConsole(JSON.stringify(response));
					if(response.data.errorCode == ADD_GENDER_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT){
						displayToast("Gender added successfully!",4000);
						window.location.reload();
					}
					else if(response.data.errorCode == ADD_GENDER_EXISTS_FAILURE_CODE && response.data.statusText == FAILURE_STATUS_TEXT){
						displayToast("Gender already exists!",4000);
					}
					else{
						displayToast("Gender could not be added!",4000);
					}
				}
				else{
					displayToast("Gender could not be added!",4000);
				}
			},function failure(){
				viviktaDebugConsole("GendersController - addNewGender - API not found!");
				displayToast("Gender could not be added!",4000);
			});
		}
	};
	//function to update a gender
	$scope.updateGender = function(editGenderData){
		if(editGenderData.name == '' || editGenderData.name == null){
			displayToast("Please provide a name for gender!",4000);
		}
		else{
			$http({
				method : "PUT",
				url : BASE_URL+UPDATE_GENDER_URL,
				params : {
					token : $scope.authToken,
					gender : editGenderData.name,
					genderId : editGenderData.genderId
				}
			}).then(function success(response){
				if(response.status == HTTP_STATUS_CODE){
					viviktaDebugConsole(JSON.stringify(response));
					if(response.data.errorCode == UPDATE_GENDER_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT){
						displayToast("Gender updated successfully!",4000);
						window.location.reload();
					}
					else{
						displayToast("Gender could not be updated!",4000);
					}
				}
				else{
					displayToast("Gender could not be updated!",4000);
				}
			},function failure(){
				viviktaDebugConsole("GenderController - updateGender - API not found!");
				displayToast("Gender could not be updated!",4000);
			});
		}
	};
	$scope.loadGenderDetails=function(gender){
		$scope.editGenderData = {};
        		$scope.editGenderData.name = angular.copy(gender.gender);
        		$scope.editGenderData.genderId = angular.copy(gender.genderId);
	}
})