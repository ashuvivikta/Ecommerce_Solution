if(DEV_MODE == 1)
	BASE_URL = DEV_URL;
var fd = new FormData();
app.service('AddImageService', ['$http', function ($http) {
    this.addImages = function(authToken,uploadUrl,vendorImagesObject){
    	overlay = $('<div class="col-lg-12"><div class="text-center blockInfoDiv"><i class="fa fa-spinner fa-pulse fa-3x"></i><p class="blockWarning">Uploading!</p></div></div>').prependTo('body').attr('id', 'overlay'); 
    	viviktaDebugConsole("Adding Images");
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined},
            params:{
              	"token": authToken,
                "facebookLink" : vendorImagesObject.facebookLink,
                "googlePlusLink" : vendorImagesObject.googlePlusLink,
                "twitterLink" : vendorImagesObject.twitterLink,
                "instagramLink" : vendorImagesObject.instagramLink
            }
        })
        .success(function(response){
        	viviktaDebugConsole(JSON.stringify(response));
          if(response.errorCode == ADD_IMAGES_SUCCESS_CODE && response.statusText == SUCCESS_STATUS_TEXT){
            showToaster("success","Images added successfully","Images Update");
            overlay.remove();
            window.location.reload();
          }
          else{
            overlay.remove();
            showToaster("error","Images cannot be added","Images Update");
          }
        })
        .error(function(){
        	showToaster("error","Images cannot be added","Images Update");
        });
    }
}])
app.controller('ImagesController',function($scope,$http,AddImageService){
	$scope.authToken = localStorage.getItem("anantya_token");

	//function to list all images
	$scope.loadImages = function(){
		$http({
			method : "GET",
			url : BASE_URL+GET_IMAGES_LIST_URL,
			params : {
				token : $scope.authToken,
				filterType : 0
			}
		}).then(function success(response){
			viviktaDebugConsole(JSON.stringify(response));
			if(response.status == HTTP_STATUS_CODE){
				if(response.data.errorCode == GET_IMAGES_LIST_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT && response.data.data != ''&& response.data.data != null && typeof(response.data.data) !== undefined){
					$scope.imagesPresent = true;
					$scope.images = response.data.data;
				}
				else{
					$scope.imagesPresent = false;
					//showToaster("error","No images found","Images Update");
				}
			}
			else{
				$scope.imagesPresent = false;
				showToaster("error","Unable to load images","Images Update");
			}
		},function failure(){
			viviktaDebugConsole("ImagesController - loadImages - API not found!");
			$scope.imagesPresent = false;
			showToaster("error","Unable to load images","Images Update");
		});
	};

    //function to load the list of all products
    $scope.loadProducts = function(){
        $http({
            method : "GET",
            url : BASE_URL+LIST_PRODUCTS_URL,
            params : {
                token : $scope.authToken,
                filterType : 1
            }
        }).then(function success(response){
            viviktaDebugConsole(JSON.stringify(response));
            if(response.status == HTTP_STATUS_CODE){
                if(response.data.errorCode == LIST_PRODUCTS_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT && response.data.data != ''){
                    $scope.productsPresent = true;
                    $scope.products = response.data.data;
                }
                else{
                    $scope.productsPresent = false;
                    //showToaster("error","No Products found","Products Update");
                }
            }
            else{
                $scope.productsPresent = false;
                showToaster("error","Unable to load Products","Products Update");
            }
        },function failure(){
            viviktaDebugConsole("ProductsController - loadProducts - API not found!");
            $scope.productsPresent = false;
            showToaster("error","Unable to load Products","Products Update");
        });
    };
    
	$scope.showAddImage = false;
	$scope.toggleAddImage = function(){
		if($scope.showAddImage == false){
            $("#ImageList").slideUp();
            $("#addImageForm").slideDown();
            $scope.showAddImage = true;
        }
        else{
            $scope.image="";
            $("#addImageForm").slideUp();
            $("#ImageList").slideDown();
            $scope.showAddImage = false;
        }
	};
	$scope.showEditImage = false;
	$scope.toggleEditImage = function(images){
		$scope.mainPageId=images;
        viviktaDebugConsole($scope.mainPageId);
		if($scope.showEditImage == false){
            $("#ImageList").slideUp();
            $("#editImageForm").slideDown();
            $scope.showEditImage = true;
        }
        else{
            $("#editImageForm").slideUp();
            $("#ImageList").slideDown();
            $scope.showEditImage = false;
        }
	};
	//function to add a new product
  	$scope.addNewImages = function(vendorImagesObject){
  		viviktaDebugConsole(JSON.stringify(vendorImagesObject));
  		$scope.uploadUrl = BASE_URL+ADD_IMAGES_URL;
        	  fd.append('vendorLogo',$("#vendorLogo").prop('files')[0]);
        	  fd.append('sliderImageHeading[]',vendorImagesObject.sliderImageHeading1);
        	  fd.append('sliderImageHeading[]',vendorImagesObject.sliderImageHeading2);
        	  fd.append('sliderImageHeading[]',vendorImagesObject.sliderImageHeading3);
        	  fd.append('sliderImageDescription[]',vendorImagesObject.sliderImageDescription1);
        	  fd.append('sliderImageDescription[]',vendorImagesObject.sliderImageDescription2);
        	  fd.append('sliderImageDescription[]',vendorImagesObject.sliderImageDescription3);
              fd.append('productUrl[]','https://shoppingmall.vivikta.in/services/singleProduct.php?productId='+vendorImagesObject.productId1);
              fd.append('productUrl[]','https://shoppingmall.vivikta.in/services/singleProduct.php?productId='+vendorImagesObject.productId2);
              fd.append('productUrl[]','https://shoppingmall.vivikta.in/services/singleProduct.php?productId='+vendorImagesObject.productId3);
        	  fd.append('sliderImage[]',$("#sliderImage1").prop('files')[0]);
        	  fd.append('sliderImage[]',$("#sliderImage2").prop('files')[0]);
        	  fd.append('sliderImage[]',$("#sliderImage3").prop('files')[0]);
  		AddImageService.addImages($scope.authToken,$scope.uploadUrl,vendorImagesObject);
  	};
})