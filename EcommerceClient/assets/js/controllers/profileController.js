if(DEV_MODE == 1)
	BASE_URL = DEV_URL;
app.controller('ProfileController',['$scope','$http','ListingService',function($scope,$http,ListingService){
	$scope.authToken = localStorage.getItem("anantya_token");
	$scope.loadProfileData = function(){
		viviktaDebugConsole("Loading the profile details");
        $http({
            method : "GET",
            url : BASE_URL+GET_PROFILE_DATA_URL,
            params : {
                token : $scope.authToken,
                typeofuser : 1
            }
        }).then(function success(response){
            viviktaDebugConsole(JSON.stringify(response.data.data));
            if(response.status == HTTP_STATUS_CODE){
                if(response.data == "" || response.data == null){
                    $scope.profileDataFound = false;
                }
                else if(response.data.errorCode == GET_PROFILE_DATA_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT && response.data.data != ''){
                    $scope.profileDataFound = true;
                     $scope.profileDetails = {};
                    if(response.data.data.vendorConfigurationDetails != null)
                    {
                        $scope.profileDetails = {
                        "name" : response.data.data.vendorName,
                        "emailId" : response.data.data.vendorEmailId,
                        "phoneNumber" : response.data.data.vendorPhoneNumber,
                        "address1" : response.data.data.vendorAddress,
                        "vendorDomainName" :  response.data.data.vendorDomainName,
                        "taxAmount": response.data.data.vendorConfigurationDetails.defaultTaxAmount,
                        "shippingAmount": response.data.data.vendorConfigurationDetails.defaultShippingAmount,
                        "paymentMode" : response.data.data.vendorConfigurationDetails.paymentMode.paymentName,
                        "paymentId" : response.data.data.vendorConfigurationDetails.paymentMode.paymentId,
                        };
                        $scope.vendorConfigurationDetails = 1;
                    }
                    else
                    {
                        $scope.profileDetails = {
                        "name" : response.data.data.vendorName,
                        "emailId" : response.data.data.vendorEmailId,
                        "phoneNumber" : response.data.data.vendorPhoneNumber,
                        "address1" : response.data.data.vendorAddress,
                        "vendorDomainName" :  response.data.data.vendorDomainName,
                        };
                        $scope.vendorConfigurationDetails = 0;

                    }
                    
                } 
                else{
                    $scope.profileDataFound = false;
                }
            }
            else{
                $scope.profileDataFound = false;
            }
        },function failure(){
            viviktaDebugConsole("ProfileController - loadProfileData - API not found!");
            $scope.profileDataFound = false;
        });
	};
	//function to change the password
	$scope.changePassword = function(passwordDetails){
		viviktaDebugConsole("passwordDetails : "+JSON.stringify(passwordDetails));
		if(passwordDetails.newPassword === passwordDetails.confirmPassword){
			$scope.validatePasswordError = false;
			$http({
	            method : "POST",
	            url : BASE_URL+CHANGE_PASSWORD_URL,
	            params : {
	                token : $scope.authToken,
	                typeofuser : 1,
	                password : passwordDetails.newPassword,
	                confirmPassword : passwordDetails.confirmPassword
	            }
	        }).then(function success(response){
	            viviktaDebugConsole(JSON.stringify(response));
	            if(response.status == HTTP_STATUS_CODE){
	                if(response.data == "" || response.data == null){
	                    $scope.passwordChangeError = true;
	                }
	                else if(response.data.errorCode == CHANGE_PASSWORD_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT){
	                   $("#myModal4").modal('toggle');
	                   showToaster("success","Password changed successfully!","Profile Update");
	                   $scope.passwordChangeError = false;
	                }
	                else{
	                	$scope.passwordChangeError = true;
	                }
	            }
	            else{
	                $scope.passwordChangeError = true;
	            }
	        },function failure(){
	           $scope.passwordChangeError = true;
	        });
		}
		else{
			$scope.validatePasswordError = true;
		}
	};
    $scope.payments = [];
    ListingService.getData($scope.authToken,BASE_URL+GET_PAYMENT_MODE_LIST_URL,LIST_PAYMENT_MODE_SUCCESS_CODE).then(function(paymentData){
        $scope.payments = paymentData;
    });
    //function to add a configuration
    $scope.addNewConfiguration = function(configurationData){
        $http({
            method : "PUT",
            url : BASE_URL+ADD_VENDOR_CONFIGURATIONs_URL,
            params : {
                token : $scope.authToken,
                defaultTaxAmount : configurationData.taxAmount,
                defaultShippingAmount : configurationData.shippingAmount,
                paymentId : configurationData.paymentId
            }
        }).then(function success(response){
            if(response.status == HTTP_STATUS_CODE){
                viviktaDebugConsole(JSON.stringify(response));
                if(response.data.errorCode == ADD_VENDOR_CONFIGURATIONS_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT){
                    showToaster("success","Configuration added successfully!","Configuration update");
                    window.location.reload();
                }
                else if(response.data.errorCode == ADD_VENDOR_CONFIGURATIONS_FAILURE_CODE && response.data.statusText == FAILURE_STATUS_TEXT){
                    showToaster("error","Configuration already exists!","Configuration update");
                }
                else{
                    showToaster("error","Configuration cannot be added!","try again later");
                }
            }
            else{
                showToaster("error","Configuration cannot be added!","try again later");
            }
        },function failure(){
            viviktaDebugConsole("ProfileController - addNewConfiguration - API not found!");
            showToaster("error","Configuration cannot be added!","try again later");
        });
    };
	//function to update the profileDetails
	$scope.updateProfileDetails = function(profileDetails){
		viviktaDebugConsole("New detials are : "+JSON.stringify(profileDetails));
		$http({
            method : "PUT",
            url : BASE_URL+UPDATE_PROFILE_URL,
            params : {
                token : $scope.authToken,
                typeofuser : 1,
                vendorPhoneNumber : profileDetails.phoneNumber,
                vendorAddress : profileDetails.address1,
                defaultTaxAmount : profileDetails.taxAmount,
                defaultShippingAmount : profileDetails.shippingAmount,
                paymentId : profileDetails.paymentId,

            }
        }).then(function success(response){
            viviktaDebugConsole(JSON.stringify(response));
            if(response.status == HTTP_STATUS_CODE){
                if(response.data == "" || response.data == null){
                    showToaster("error","Profile details could not be updated!","Profile Update");
                }
                else if(response.data.errorCode == UPDATE_PROFILE_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT){
                	$scope.editMode = false;
                    showToaster("success","Profile details updated successfully!","Profile Update");
                    $scope.loadProfileData();
                }
                else{
                    showToaster("error","Profile details could not be updated!","Profile Update");
                }
            }
            else{
                showToaster("error","Profile details could not be updated!","Profile Update");
            }
        },function failure(){
        	showToaster("error","Profile details could not be updated!","Profile Update");
        });
	};
}])