if(DEV_MODE == 1)
	BASE_URL = DEV_URL;
app.controller('LoginController',function($scope,$http){
	//function to handle the login from login page
	$scope.doLogin = function(loginData){
		if(loginData.username=="support@vivikta.in")
			$scope.userType = 0; 
		else
			$scope.userType = 1;
		$http({
			method : "POST",
			url : BASE_URL+LOGIN_URL,
			params : {
				emailId : loginData.username,
				password : loginData.password,
				typeofuser : $scope.userType
			}
		}).then(function success(response){
			console.log(JSON.stringify(response));
			if(response.status == HTTP_STATUS_CODE){
				if(response.data.errorCode == LOGIN_STATUS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT){
					//if the login is successful
					localStorage.setItem("anantya_token",response.data.data.token.token);
					localStorage.setItem("anantya_name",response.data.data.name);
					localStorage.setItem("anantya_username",loginData.username);
					localStorage.setItem("anantya_profilePicturePath",response.data.data.profilePicturePath);
					localStorage.setItem("anantya_typeofuser",response.data.data.typeofuser);
					$scope.typeOfUser=response.data.data.typeofuser;
					viviktaDebugConsole($scope.typeofuser);
					//redirect the superAdmin to index page
					if($scope.typeOfUser==0){
						window.location.href = "superAdmin/index.php";
					}
					//redirect the admin to index page
					else{
						window.location.href = "index.php";
					}
				}
				else if(response.data.errorCode == USER_CREDENTIALS_DONT_MATCH_STATUS_CODE && response.data.statusText == FAILURE_STATUS_TEXT){
					showToaster("error","Username and Password did not match"," Please enter valid credentials!");
				}
				else if(response.data.errorCode == USER_NOT_ACTIVATED_STATUS_CODE && response.data.statusText == FAILURE_STATUS_TEXT){
					showToaster("error","Your account activation is pending"," Please wait for admin approval!");
				}
				else if(response.data.errorCode == USER_REJECTED_STATUS_CODE && response.data.statusText == FAILURE_STATUS_TEXT){
					showToaster("error","Your account has been rejected by admin"," Please contact the administrator!");
				}
				else if(response.data.errorCode == USER_DISABLED_STATUS_CODE && response.data.statusText == FAILURE_STATUS_TEXT){
					showToaster("error","Your account has been disabled by admin"," Please contact the administrator!");
				}
				else{
					showToaster("error","Account does not exist"," Please register and try again!");
				}
			}
			else{
				showToaster("error","Login failed."," Please try again!");
			}
		},function failure(){
			viviktaDebugConsole("LoginController - doLogin - API not found!");
			showToaster("error","Login failed."," Please try again!");
		});
	};
	$scope.forgotPassword = function(forgotPasswordDetails){
		$http({
			method : "POST",
			url : BASE_URL+FORGOT_PASSWORD_URL,
			params : {
				emailId : forgotPasswordDetails.emailId,
				typeofuser : 1
			}
		}).then(function success(response){
			viviktaDebugConsole(JSON.stringify(response));
			if(response.status == HTTP_STATUS_CODE){
				if(response.data.errorCode == FORGOT_PASSWORD_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT){
					$scope.userInvalidError = false;
					$scope.passwordResetSuccessful = true;
				}
				else if(response.data.errorCode == FORGOT_PASSWORD_INVALID_USER_ERROR_CODE && response.data.statusText == FAILURE_STATUS_TEXT){
					$scope.userInvalidError = true;
				}
				else{
					$scope.passwordResetSuccessful = false;
				}
			}else{
				viviktaDebugConsole("API not found!");
				$scope.passwordResetSuccessful = false;
			}
		},function failure(){
			viviktaDebugConsole("ForgotPasswordController - Forgot Password - API not found!");
			$scope.passwordResetSuccessful = false;
		});
	};
	//function to check the password
	$scope.checkPassword = function(){
	    if(angular.equals($scope.password,$scope.confirmPassword))
	      $scope.validatePassword = false;
	    else
	      $scope.validatePassword = true;
	};

  	//function to fetch the data
  	$scope.fetchData = function(){
    	$scope.emailId = document.getElementById("emailId").value;
    	viviktaDebugConsole($scope.emailId);
    	$scope.resetToken = document.getElementById("resetToken").value;
 	};

	//function to reset the password
	$scope.resetPassword = function(){
	    $http({
	      method : "POST",
	      url : BASE_URL+RESET_PASSWORD_URL,
	      params : {
	        emailId : $scope.emailId,
	        typeofuser : 1,
	        resetToken : $scope.resetToken,
	        password : $scope.password,
	        confirmPassword : $scope.confirmPassword
	      }
	    }).then(function success(response){
	    viviktaDebugConsole(JSON.stringify(response));
	      if(response.status == HTTP_STATUS_CODE){
	        if(response.data.errorCode == RESET_PASSWORD_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT){
	          $scope.resetSuccessful = true;
	        }
	        else{
	          $scope.resetSuccessful = false;
	        }
	      }
	      else{
	        viviktaDebugConsole("ResetPasswordController - Http Error");
	        $scope.resetSuccessful = false;
	      }
	    },function failure(){
	      viviktaDebugConsole("ResetPasswordController - Reset Password - API not found!");
	      $scope.resetSuccessful = false;
	    });
	};
})