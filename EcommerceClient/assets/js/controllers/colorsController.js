if(DEV_MODE == 1)
	BASE_URL = DEV_URL;
app
.controller('ColorsController',function($scope,$http){
	$scope.authToken = localStorage.getItem("anantya_token");

	//function to load the list of all colors
	$scope.loadColors = function(paginateFlag,url){
		if(paginateFlag == 0){
            $scope.url = BASE_URL+GET_COLORS_LIST_URL
        }
        else{
            $scope.url = url;
        }
		$http({
			method : "GET",
			url : $scope.url,
			params : {
				token : $scope.authToken,
				filterType : 0
			}
		}).then(function success(response){
			console.log(JSON.stringify(response));
			if(response.status == HTTP_STATUS_CODE){
				if(response.data.errorCode == GET_COLORS_LIST_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT && response.data.data.data != ''){
					$scope.lastPage = response.data.data.lastPage;
			        $scope.showPagination = true;
			        if($scope.lastPage == "NULL"){
			            $scope.showPagination = false;
			            $scope.colors = response.data.data.data;
			        }
			        else{
			            $scope.colors = response.data.data.data.data;
			        }
			        $scope.previousPageUrl = response.data.data.previousPageUrl;
			        $scope.nextPageUrl = response.data.data.nextPageUrl;
			        $scope.currentPage = response.data.data.currentPage;
					$scope.colorsPresent = true;
				}
				else{
					$scope.colorsPresent = false;
					//showToaster("error","No Colors found!","Colors update");
				}
			}
			else{
				$scope.colorsPresent = false;
				showToaster("error","unable to load Colors!","Colors update");
			}
		},function failure(){
			viviktaDebugConsole("ColorsController - loadColors - API not found!");
			$scope.colorsPresent = false;
			showToaster("error","unable to load Colors!","Colors update");
		});
	};

	//function to add a new color
	$scope.addNewColor = function(colorData){
		$http({
			method : "POST",
			url : BASE_URL+ADD_COLOR_URL,
			params : {
				token : $scope.authToken,
				productColor : colorData.name
			}
		}).then(function success(response){
			if(response.status == HTTP_STATUS_CODE){
				viviktaDebugConsole(JSON.stringify(response));
				if(response.data.errorCode == ADD_COLOR_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT){
					showToaster("success","Color added successfully!","Colors update");
					window.location.reload();
				}
				else if(response.data.errorCode == ADD_COLOR_EXISTS_FAILURE_CODE && response.data.statusText == FAILURE_STATUS_TEXT){
					showToaster("error","Color already exists!","Colors update");
				}
				else{
					showToaster("error","Colors cannot be added!","Colors update");
				}
			}
			else{
				showToaster("error","Colors cannot be added!","Colors update");
			}
		},function failure(){
			viviktaDebugConsole("ColorsController - addNewColor - API not found!");
			showToaster("error","Colors cannot be added!","Colors update");
		});
	};
	//function to update a color
	$scope.updateColor = function(editColorData){
		$http({
			method : "PUT",
			url : BASE_URL+UPDATE_COLOR_URL,
			params : {
				token : $scope.authToken,
				productColor : editColorData.name,
				productColorId : editColorData.productColorId
			}
		}).then(function success(response){
			if(response.status == HTTP_STATUS_CODE){
				viviktaDebugConsole(JSON.stringify(response));
				if(response.data.errorCode == UPDATE_COLOR_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT){
					showToaster("success","Colors updated successfully!","Colors update");
					window.location.reload();
				}
				else{
					showToaster("error","Colors cannot be updated!","Colors update");
				}
			}
			else{
				showToaster("error","Colors cannot be updated!","Colors update");
			}
		},function failure(){
			viviktaDebugConsole("ColorController - updateColor - API not found!");
			showToaster("error","Colors cannot be updated!","Colors update");
		});
	};
	$scope.loadColorDetails=function(color){
		$scope.editColorData = {};
        		$scope.editColorData.name = angular.copy(color.productColor);
        		$scope.editColorData.productColorId = angular.copy(color.productColorId);
	}
})