if(DEV_MODE == 1)
	BASE_URL = DEV_URL;
var fe = new FormData();
app.service('EditImageService', ['$http', function ($http) {
    this.editImages = function(authToken,mainPageId,uploadUrl,vendorImagesObject){
      overlay = $('<div class="col-lg-12"><div class="text-center blockInfoDiv"><i class="fa fa-spinner fa-pulse fa-3x"></i><p class="blockWarning">Uploading!</p></div></div>').prependTo('body').attr('id', 'overlay'); 
      viviktaDebugConsole(vendorImagesObject.facebookLink);
        $http.post(uploadUrl, fe, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined},
            params:{
              	"token":authToken,
              	"mainPageId": mainPageId,
                "facebookLink" : vendorImagesObject.facebookLink,
                "googlePlusLink" :vendorImagesObject.googlePlusLink,
                "twitterLink" : vendorImagesObject.twitterLink,
                "instagramLink" : vendorImagesObject.instagramLink
            }
        })
        .success(function(response){
        	viviktaDebugConsole(JSON.stringify(response));
          if(response.errorCode ==EDIT_IMAGES_SUCCESS_CODE && response.statusText == SUCCESS_STATUS_TEXT){
            overlay.remove();
            showToaster("success","Images added successfully","Images Update");
            window.location.reload();
          }
          else{
            showToaster("error","Images cannot be added","Images Update");
            overlay.remove();
           // window.location.reload();
          }
        })
        .error(function(){
        	showToaster("error","Images cannot be added","Images Update");
           // window.location.reload();
        });
    }
}])
app.controller('EditController',function($scope,$http,EditImageService){
	$scope.authToken = localStorage.getItem("anantya_token");

	//function to list all images
	$scope.loadImages = function(){
		$http({
			method : "GET",
			url : BASE_URL+GET_IMAGES_LIST_URL,
			params : {
				token : $scope.authToken,
        filterType : 0
			}
		}).then(function success(response){
			viviktaDebugConsole(JSON.stringify(response));
			if(response.status == HTTP_STATUS_CODE){
				if(response.data.errorCode == GET_IMAGES_LIST_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT && response.data.data != ''&& response.data.data != null && typeof(response.data.data) !== undefined){
				
          $scope.imagesPresent = true;
					$scope.images = response.data.data;
				}
				else{
					$scope.imagesPresent = false;
					showToaster("error","No images found","Images Update");
				}
			}
			else{
				$scope.imagesPresent = false;
				showToaster("error","Unable to load images","Images Update");
			}
		},function failure(){
			viviktaDebugConsole("ImagesController - loadImages - API not found!");
			$scope.imagesPresent = false;
			showToaster("error","Unable to load images","Images Update");
		});
	};
  //function to load the list of all products
  $scope.loadProducts = function(){
    $http({
      method : "GET",
      url : BASE_URL+LIST_PRODUCTS_URL,
      params : {
        token : $scope.authToken,
        filterType : 1
      }
    }).then(function success(response){
      viviktaDebugConsole(JSON.stringify(response));
      if(response.status == HTTP_STATUS_CODE){
        if(response.data.errorCode == LIST_PRODUCTS_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT && response.data.data != ''){
          $scope.productsPresent = true;
          $scope.products = response.data.data;
        }
        else{
          $scope.productsPresent = false;
          //showToaster("error","No Products found","Products Update");
        }
      }
      else{
        $scope.productsPresent = false;
        showToaster("error","Unable to load Products","Products Update");
      }
    },function failure(){
      viviktaDebugConsole("ProductsController - loadProducts - API not found!");
      $scope.productsPresent = false;
      showToaster("error","Unable to load Products","Products Update");
    });
  };
	//function to add a new product
  	$scope.editImages = function(mainPageId,vendorImagesObject){
  		$scope.vendorImagesObject = vendorImagesObject;
      $scope.facebookLink = vendorImagesObject.facebookLink;
      $scope.twitterLink = vendorImagesObject.twitterLink;
      $scope.googlePlusLink = vendorImagesObject.googlePlusLink;
  		$scope.uploadUrl = BASE_URL+EDIT_IMAGES_URL;
		      fe.append('vendorLogo',$("#vendorLogo1").prop('files')[0]);
            fe.append('sliderImageHeading[]',vendorImagesObject.sliderImageHeading1);
            fe.append('sliderImageHeading[]',vendorImagesObject.sliderImageHeading2);
            fe.append('sliderImageHeading[]',vendorImagesObject.sliderImageHeading3);
            fe.append('sliderImageDescription[]',vendorImagesObject.sliderImageDescription1);
            fe.append('sliderImageDescription[]',vendorImagesObject.sliderImageDescription2);
            fe.append('sliderImageDescription[]',vendorImagesObject.sliderImageDescription3);
            fe.append('productUrl[]','https://shoppingmall.vivikta.in/services/singleProduct.php?productId='+vendorImagesObject.productId1);
            fe.append('productUrl[]','https://shoppingmall.vivikta.in/services/singleProduct.php?productId='+vendorImagesObject.productId2);
            fe.append('productUrl[]','https://shoppingmall.vivikta.in/services/singleProduct.php?productId='+vendorImagesObject.productId3);
            fe.append('sliderImage[]',$("#sliderImage4").prop('files')[0]);
            fe.append('sliderImage[]',$("#sliderImage5").prop('files')[0]);
            fe.append('sliderImage[]',$("#sliderImage6").prop('files')[0]);
            for (var pair of fe.entries()) {
                console.log(pair[0]+ ', ' + pair[1]); 
            }
  		EditImageService.editImages($scope.authToken,mainPageId,$scope.uploadUrl,$scope.vendorImagesObject);
  	};
})