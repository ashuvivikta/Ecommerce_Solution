if(DEV_MODE == 1)
	BASE_URL = DEV_URL;
app.controller('SuperAdminVendorsController',function($scope,$http){
	$scope.authToken = localStorage.getItem("anantya_token");
	//function to load the dashboard data
	$scope.loadVendors = function(paginateFlag,url){
		if(paginateFlag == 0){
            $scope.url = BASE_URL+LIST_VENDORS_URL
        }
        else{
            $scope.url = url;
        }
		$http({
			method : "GET",
			url : $scope.url,
			params : {
				token : $scope.authToken
			}
		}).then(function success(response){
			console.log(JSON.stringify(response));
			if(response.status == HTTP_STATUS_CODE){
				if(response.data.errorCode == LIST_VENDORS_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT && response.data.data != ''){
					$scope.lastPage = response.data.data.lastPage;
			        $scope.showPagination = true;
			        if($scope.lastPage == "NULL"){
			            $scope.showPagination = false;
			            $scope.vendors = response.data.data.data;
			        }
			        else{
			            $scope.vendors = response.data.data.data;
			        }
			        $scope.previousPageUrl = response.data.data.previousPageUrl;
			        $scope.nextPageUrl = response.data.data.nextPageUrl;
			        $scope.currentPage = response.data.data.currentPage;
					$scope.vendorsFound = true;
				}
				else{
					showToaster("error","Unable to load vendors","Vendors Update");
				}
			}
			else{
				showToaster("error","Unable to load vendors","Vendors Update");
			}
		},function failure(){
			viviktaDebugConsole("SUperAdminVendorController - loadVendorsData - API not found!");
			showToaster("error","Unable to load data","Vendors Update");
		});
	};
	//function to approve the vendors
	$scope.approveVendor = function(vendorData,status,vendorActivateFlag){
        if(status == 1 && vendorActivateFlag == 0){
            $scope.displayString = "approved";
        }
        else if(status == 1 && vendorActivateFlag == 1){
            $scope.displayString = "activated";
        }
        else if(status == 2 && vendorActivateFlag == 0){
            $scope.displayString = "rejected"; 
        }
        else{
            $scope.displayString = "disbaled";
        }
        $http({
        	method : "PUT",
        	url : BASE_URL+APPROVE_VENDOR_URL,
        	params : {
        		token : $scope.authToken,
        		status : status,
                vendorStatusFlag : vendorActivateFlag,
        		vendorId : vendorData.vendorId
        	}
        }).then(function success(response){
        	viviktaDebugConsole(JSON.stringify(response));
        	if(response.status == HTTP_STATUS_CODE){
        		if(response.data.errorCode == APPROVE_VENDOR_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT){
                    showToaster('success','Vendor '+$scope.displayString+' successfully!','Vendor Update');
                    $scope.vendorsFound = false;
		            $scope.loadVendors(0,'');
        		}
        		else{
                    showToaster('error','Vendor could not be '+$scope.displayString+'!','Vendor Update');
        		}
        	}
        	else{
        		$scope.spinner.close();
                showToaster('error','Vendor could not be '+$scope.displayString+'!','Vendor Update');
	        	
        	}
        },function failure(){
        	showToaster('error','Vendor could not be '+$scope.displayString+'!','Vendor Update');
        });
	};
})