if(DEV_MODE == 1)
	BASE_URL = DEV_URL;
app.controller('ComplaintsController',function($scope,$http){
	$scope.authToken = localStorage.getItem("anantya_token");

	//function to list all complaints
	$scope.loadComplaints = function(paginateFlag,url){
		if(paginateFlag == 0){
            $scope.url = BASE_URL+GET_COMPLAINTS_LIST_URL
        }
        else{
            $scope.url = url;
        }
		$http({
			method : "GET",
			url : $scope.url,
			params : {
				token : $scope.authToken,
				filterType : 0
			}
		}).then(function success(response){
			console.log(JSON.stringify(response));
			if(response.status == HTTP_STATUS_CODE){
				if(response.data.errorCode == GET_COMPLAINTS_LIST_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT && response.data.data.data != ''){
					$scope.lastPage = response.data.data.lastPage;
			        $scope.showPagination = true;
			        if($scope.lastPage == "NULL"){
			            $scope.showPagination = false;
			            $scope.complaints = response.data.data.data;
			        }
			        else{
			            $scope.complaints = response.data.data.data.data;
			        }
			        $scope.previousPageUrl = response.data.data.previousPageUrl;
			        $scope.nextPageUrl = response.data.data.nextPageUrl;
			        $scope.currentPage = response.data.data.currentPage;
					$scope.complaintsPresent = true;
				}
				else{
					$scope.complaintsPresent = false;
					//showToaster("error","Complaints cannot be loaded!","Complaints update");
				}
			}
			else{
				$scope.complaintsPresent = false;
				showToaster("error","Complaints cannot be loaded!","Complaints update");
			}
		},function failure(){
			viviktaDebugConsole("complaintsController - loadcomplaints - API not found!");
			$scope.complaintsPresent = false;
			showToaster("error","Complaints cannot be loaded!","Complaints update");
		});
	};

	//function to add a replay
	$scope.addNewReplay = function(replayData){
		$http({
			method : "PUT",
			url : BASE_URL+ADD_REPLAY_COMPLAINTS_URL,
			params : {
				token : $scope.authToken,
				adminAnswer : replayData.adminAnswer,
				complaintId : replayData.complaintId
			}
		}).then(function success(response){
			if(response.status == HTTP_STATUS_CODE){
				viviktaDebugConsole(JSON.stringify(response));
				if(response.data.errorCode == ADD_REPLAY_COMPLAINTS_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT){
					showToaster("success","Replay added successfully!","Complaints update");
					window.location.reload();
				}
				else{
					showToaster("error","Replay cannot be added!","Complaints update");
				}
			}
			else{
				showToaster("error","Replay cannot be added!","Complaints update");
			}
		},function failure(){
			viviktaDebugConsole("ReplayController - addNewReplay - API not found!");
			showToaster("error","Replay cannot be added!","Complaints update");
		});
	};
	$scope.loadComplaintDetails=function(complaint){
		$scope.complaintData = {};
       	$scope.complaintData.complaintId = angular.copy(complaint.complaintId);
	}
})