if(DEV_MODE == 1)
	BASE_URL = DEV_URL;
app
.controller('PasswordController',function($scope,$http){

    $scope.checkPassword = function(passwordData){
        if(angular.equals(passwordData.password,passwordData.confirmPassword))
          $scope.validatePassword = false;
        else
          $scope.validatePassword = true;
    }

	//function to create the password
	$scope.createPassword = function(passwordData){
        viviktaDebugConsole(JSON.stringify(passwordData));
        if(passwordData.password !== passwordData.confirmPassword){
            $scope.validatePassword = true;
        }
        else{
            $scope.validatePassword = false;
        	$http({
        		method : "POST",
        		url : BASE_URL+CREATE_PASSWORD_URL,
        		params : {
        			vendorId : document.getElementById("registrationToken").value,
        			password : passwordData.password,
        			confirmPassword : passwordData.confirmPassword
        		}
        	}).then(function success(response){
        		viviktaDebugConsole(JSON.stringify(response));
        		if(response.status == HTTP_STATUS_CODE){
        			if(response.data.errorCode == CREATE_PASSWORD_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT){
                        $scope.createPasswordError = false;
                        window.location.href = "login.php";
        			}
        			else if(response.data.errorCode == CREATE_PASSWORD_FAILURE_CODE){
                        $scope.userAlreadyExistsError = true;
        			}
        		}
        		else{
                    $scope.createPasswordError = true;
        		}
        	},function failure(){
                $scope.createPasswordError = true;
        	});
        }
	};
})