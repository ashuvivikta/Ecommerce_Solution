if(DEV_MODE == 1)
	BASE_URL = DEV_URL;
app.controller('SuggestionController',function($scope,$http){
	$scope.authToken = localStorage.getItem("anantya_token");

	//function to list all suggestions
	$scope.loadSuggestions = function(paginateFlag,url){
		if(paginateFlag == 0){
            $scope.url = BASE_URL+GET_SUGGESTIONS_LIST_URL
        }
        else{
            $scope.url = url;
        }
		$http({
			method : "GET",
			url : $scope.url,
			params : {
				token : $scope.authToken,
				filterType : 0
			}
		}).then(function success(response){
			console.log(JSON.stringify(response));
			if(response.status == HTTP_STATUS_CODE){
				if(response.data.errorCode == GET_SUGGESTION_LIST_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT && response.data.data.data != ''){
					$scope.lastPage = response.data.data.lastPage;
			        $scope.showPagination = true;
			        if($scope.lastPage == "NULL"){
			            $scope.showPagination = false;
			            $scope.suggestions = response.data.data.data;
			        }
			        else{
			            $scope.suggestions = response.data.data.data.data;
			        }
			        $scope.previousPageUrl = response.data.data.previousPageUrl;
			        $scope.nextPageUrl = response.data.data.nextPageUrl;
			        $scope.currentPage = response.data.data.currentPage;
					$scope.suggestionsPresent = true;
				}
				else{
					$scope.suggestionsPresent = false;
					//showToaster("error"," No suggestions found","Suggestion Update");
				}
			}
			else{
				$scope.suggestionsPresent = false;
				showToaster("error"," Unable to load suggestions","Suggestion Update");
			}
		},function failure(){
			viviktaDebugConsole("SuggestionController - loadSuggestions - API not found!");
			$scope.suggestionsPresent = false;
			showToaster("error"," Unable to load suggestions","Suggestion Update");
		});
	};

	//function to add a replay
	$scope.addNewReplay = function(replayData){
		$http({
			method : "PUT",
			url : BASE_URL+ADD_REPLAY_SUGGESTION_URL,
			params : {
				token : $scope.authToken,
				adminReply : replayData.adminReply,
				suggestionId : replayData.suggestionId
			}
		}).then(function success(response){
			if(response.status == HTTP_STATUS_CODE){
				viviktaDebugConsole(JSON.stringify(response));
				if(response.data.errorCode == ADD_REPLAY_SUGGESTION_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT){
					showToaster("success"," Replay added successfully","Suggestion Update");
					window.location.reload();
				}
				else{
					showToaster("error"," Replay cannot be added","Suggestion Update");
				}
			}
			else{
				showToaster("error"," Replay cannot be added","Suggestion Update");
			}
		},function failure(){
			viviktaDebugConsole("ReplayController - addNewReplay - API not found!");
			showToaster("error"," Replay cannot be added","Suggestion Update");
		});
	};
	$scope.loadSuggestionDetails=function(suggeston){
		$scope.suggestionData = {};
       	$scope.suggestionData.suggestionId = angular.copy(suggeston.suggestionId);
	}
})