if(DEV_MODE == 1)
	BASE_URL = DEV_URL;
var fd = new FormData();
app.service('AddProductService', ['$http', function ($http) {
    this.addProduct = function(authToken,productData,uploadUrl){
      overlay = $('<div class="col-lg-12"><div class="text-center blockInfoDiv"><i class="fa fa-spinner fa-pulse fa-3x"></i><p class="blockWarning">Uploading!</p></div></div>').prependTo('body').attr('id', 'overlay'); 
    	viviktaDebugConsole("Adding product"+ productData.productSize + productData.metricUnit);
      viviktaDebugConsole("Adding product"+ productData.productSize);
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined},
            params:{
              	"token":authToken,
              	"productName" : productData.name,
              	"productDescription" : productData.description,
              	"categoryId" : productData.categoryId,
              	"subcategoryId" : productData.subCategoryId,
              	"productPrice" : productData.price,
		            "productTaxAmount" : productData.taxAmount,
                "productShippingAmount" : productData.shippingAmount,
              	"productModel" : productData.model,
                "genderId" : productData.genderId,
              	"productColorId" : productData.productColorId,
                "productCount" : productData.count,
                "productSize" : productData.productSize + productData.metricUnit
            }
        })
        .success(function(response){
        	console.log(JSON.stringify(response));
          if(response.errorCode == ADD_PRODUCT_SUCCESS_CODE && response.statusText == SUCCESS_STATUS_TEXT){
            showToaster("success","Product added successfully","Products Update");
            overlay.remove();
            window.location.reload();
          }
          else{
            overlay.remove();
            showToaster("error","Product cannot be added","Products Update");
          }
        })
        .error(function(){
           showToaster("error","Product cannot be added","Products Update");
        });
    }
}])
.controller('ProductsController',['$scope','$http','ListingService','AddProductService',function($scope,$http,ListingService,AddProductService){
	$scope.authToken = localStorage.getItem("anantya_token");

	//function to load the list of all products
	$scope.loadProducts = function(paginateFlag,url){
    if(paginateFlag == 0){
        $scope.url = BASE_URL+LIST_PRODUCTS_URL
    }
    else{
        $scope.url = url;
    }
		$http({
			method : "GET",
			url : $scope.url,
			params : {
				token : $scope.authToken,
        filterType : 0
			}
		}).then(function success(response){
			viviktaDebugConsole(JSON.stringify(response));
			if(response.status == HTTP_STATUS_CODE){
				if(response.data.errorCode == LIST_PRODUCTS_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT && response.data.data.data != ''){
          $scope.lastPage = response.data.data.lastPage;
          $scope.showPagination = true;
          if($scope.lastPage == "NULL"){
              $scope.showPagination = false;
              $scope.products = response.data.data.data;
          }
          else{
              $scope.products = response.data.data.data;
          }
          $scope.previousPageUrl = response.data.data.previousPageUrl;
          $scope.nextPageUrl = response.data.data.nextPageUrl;
          $scope.currentPage = response.data.data.currentPage;
          $scope.productsPresent = true;
				}
				else{
					$scope.productsPresent = false;
					//showToaster("error","There are no Product Found","Products Update");
				}
			}
			else{
				$scope.productsPresent = false;
				showToaster("error","Unable to load Product","Products Update");
			}
		},function failure(){
			viviktaDebugConsole("ProductsController - loadProducts - API not found!");
			$scope.productsPresent = false;
			showToaster("error","Unable to load Product","Products Update");
		});
	};

	//function to toggle the add products section
	$scope.isAddProductToggled = false;
	$scope.toggleAddProduct = function(){
		if($scope.isAddProductToggled == false){
			//show the add products section
			$("#addProductSection").slideDown();
			$("#listProductSection").slideUp();
			$("#noProductPresentSection").slideUp();
			$scope.isAddProductToggled = true;
		}
		else{
			//check if the products list is present or not
			if($scope.productsPresent == true){
				//if there are products in list then show the listProductSection
				$("#listProductSection").slideDown();
				$("#addProductSection").slideUp();
			}
			else{
				//if there are no products present show the noProductPresentSection
				$("#noProductPresentSection").slideDown();
				$("#addProductSection").slideUp();
			}
			$scope.isAddProductToggled = false;
		}
	};
  //function to list all sub categories
  $scope.loadSubCategoriesBasedOnCategory = function(categoryId){
    $http({
      method : "GET",
      url : BASE_URL+GET_SUB_CATEGORIES_BASED_ON_CATEGORY_LIST_URL,
      params : {
        token : $scope.authToken,
        categoryId : categoryId,
        filterType : 1
      }
    }).then(function success(response){
      viviktaDebugConsole(JSON.stringify(response));
      if(response.status == HTTP_STATUS_CODE){
        if(response.data.errorCode == GET_SUB_CATEGORIES_BASED_ON_CATEGORY_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT && response.data.data != ''){
          $scope.subCategoriesPresent = true;
          $scope.subcategoriesDetails = response.data.data;
        }
        else{
          $scope.subCategoriesPresent = false;
        }
      }
      else{
        $scope.subCategoriesPresent = false;
      }
    },function failure(){
      viviktaDebugConsole("ProductController - loadSubCategories - API not found!");
      $scope.subCategoriesPresent = false;
    });
  };

	//call the function to get the categories list from factory service
	$scope.categories = [];
	ListingService.getData($scope.authToken,BASE_URL+GET_CATEGORIES_LIST_URL,GET_CATEGORIES_LIST_SUCCESS_CODE).then(function(categoryData){
    	$scope.categories = categoryData;
  	});

    //call the function to get the genders list from factory service
    $scope.genders = [];
    ListingService.getData($scope.authToken,BASE_URL+GET_GENDERS_LIST_URL,GET_GENDERS_LIST_SUCCESS_CODE).then(function(genderData){
        $scope.genders = genderData;
    });

    $scope.metricUnits = [];
    ListingService.getData($scope.authToken,BASE_URL+GET_UNITS_LIST_URL,LIST_UNITS_SUCCESS_CODE).then(function(metricData){
        $scope.metricUnits = metricData;
    });


    //call the function to get the colors list from factory service
    $scope.colors = [];
    ListingService.getData($scope.authToken,BASE_URL+GET_COLORS_LIST_URL,GET_COLORS_LIST_SUCCESS_CODE).then(function(colorsData){
        $scope.colors = colorsData;
    });

  	//function to add a new product
  	$scope.addNewProduct = function(productData){
  		$scope.uploadUrl = BASE_URL+ADD_PRODUCT_URL;
            var productFiles = $("#productImage")[0].files;
            for (var i = 0; i < productFiles.length; i++){
                    fd.append('productImage[]',productFiles[i]);
        }
  		AddProductService.addProduct($scope.authToken,productData,$scope.uploadUrl);
  	};

  	//function to delete/remove a product
  	$scope.deleteProduct = function(productDetails){
  		$http({
  			method : "DELETE",
  			url : BASE_URL+DELETE_PRODUCT_URL,
  			params : {
  				token : $scope.authToken,
  				productId : productDetails.productId
  			}
  		}).then(function success(response){
  			if(response.status == HTTP_STATUS_CODE){
  				if(response.data.errorCode == DELETE_PRODUCT_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT){
  					showToaster("success"," Product deleted successfully","Products Update");
  					window.location.reload();
  				}
  				else{
  					showToaster("error"," Product cannot be deleted","Products Update");
  				}
  			}
  			else{
  				showToaster("error"," Product cannot be deleted","Products Update");
  			}
  		},function failure(){
  			viviktaDebugConsole("ProductsController - deleteProduct - API not found!");
  			showToaster("error"," Product cannot be deleted","Products Update");
  		});
  	};
      $scope.loadProductDetails = function(products)
      {
          $scope.productsDetails = products;
          viviktaDebugConsole(JSON.stringify($scope.productsDetails));
      };
      $scope.deleteProductDetails = function(products){
        $scope.productDetails = products;
      }
}]);
