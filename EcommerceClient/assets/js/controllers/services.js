app
.factory('ListingService', [ '$http', function($http){
    var listingService = {};
    listingService.getData = function(AUTH_TOKEN,URL,ERROR_CODE){
   	    viviktaDebugConsole("called getData function");
        return $http({
         	method : "GET",
         	url : URL,
         	params : {
         		token : AUTH_TOKEN,
                filterType : 1
         	}
        }).then(function success(response){
     	  viviktaDebugConsole(JSON.stringify(response));
     	    if(response.status == HTTP_STATUS_CODE){
                if(response.data.errorCode == ERROR_CODE && response.data.statusText == SUCCESS_STATUS_TEXT && response.data.data != ''){
     			    return response.data.data;
                }
                else{
                    return [];
                }
            }
            else{
                return [];
            }
        },function failure(){
            return [];
        });
    }
    return listingService;
}])