if(DEV_MODE == 1)
	BASE_URL = DEV_URL;
app.controller('SubCategoryController',function($scope,$http){
	$scope.authToken = localStorage.getItem("anantya_token");

	//function to list all sub categories
	$scope.loadSubCategories = function(paginateFlag,url){
		if(paginateFlag == 0){
            $scope.url = BASE_URL+GET_SUB_CATEGORIES_LIST_URL
        }
        else{
            $scope.url = url;
        }
		$http({
			method : "GET",
			url : $scope.url,
			params : {
				token : $scope.authToken,
				filterType : 0
			}
		}).then(function success(response){
			viviktaDebugConsole(JSON.stringify(response));
			if(response.status == HTTP_STATUS_CODE){
				if(response.data.errorCode == GET_SUB_CATEGORIES_LIST_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT && response.data.data.data != ''){
					$scope.lastPage = response.data.data.lastPage;
			        $scope.showPagination = true;
			        if($scope.lastPage == "NULL"){
			            $scope.showPagination = false;
			            $scope.subcategories = response.data.data.data;
			        }
			        else{
			            $scope.subcategories = response.data.data.data.data;
			        }
			        $scope.previousPageUrl = response.data.data.previousPageUrl;
			        $scope.nextPageUrl = response.data.data.nextPageUrl;
			        $scope.currentPage = response.data.data.currentPage;
					$scope.subCategoriesPresent = true;
				}
				else{
					$scope.subCategoriesPresent = false;
					//showToaster("error"," No subcategories found","Sub Category Update");
				}
			}
			else{
				$scope.subCategoriesPresent = false;
				showToaster("error"," Unable to load subcategories","Sub Category Update");
			}
		},function failure(){
			viviktaDebugConsole("CategoryController - loadSubCategories - API not found!");
			$scope.subCategoriesPresent = false;
			showToaster("error"," Unable to load subcategories","Sub Category Update");
		});
	};

	//function to add a sub category
	$scope.addNewSubCategory = function(subCategoryData){
		$http({
			method : "POST",
			url : BASE_URL+ADD_SUB_CATEGORY_URL,
			params : {
				token : $scope.authToken,
				subcategoryName : subCategoryData.name,
				categoryId : subCategoryData.categoryId
			}
		}).then(function success(response){
			if(response.status == HTTP_STATUS_CODE){
				viviktaDebugConsole(JSON.stringify(response));
				if(response.data.errorCode == ADD_SUB_CATEGORY_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT){
					showToaster("success"," Subcategory added successfully","Sub Category Update");
					window.location.reload();
				}
				else if(response.data.errorCode == ADD_SUB_CATEGORY_EXISTS_FAILURE_CODE && response.data.statusText == FAILURE_STATUS_TEXT){
					showToaster("error"," Subcategory already exists","Sub Category Update");
				}
				else{
					showToaster("error"," Subcategory cannot be added","Sub Category Update");
				}
			}
			else{
				showToaster("error"," Subcategory cannot be added","Sub Category Update");
			}
		},function failure(){
			viviktaDebugConsole("SubCategoryController - addNewSubCategory - API not found!");
			showToaster("error"," Subcategory cannot be added","Sub Category Update");
		});
	};

	//function to update a sub category
	$scope.updateSubCategory = function(editSubCategoryData){
		$http({
			method : "PUT",
			url : BASE_URL+UPDATE_SUB_CATEGORY_URL,
			params : {
				token : $scope.authToken,
				subcategoryName : editSubCategoryData.name,
				subcategoryId : editSubCategoryData.subCategoryId,
				categoryId : editSubCategoryData.categoryId
			}
		}).then(function success(response){
			if(response.status == HTTP_STATUS_CODE){
				viviktaDebugConsole(JSON.stringify(response));
				if(response.data.errorCode == UPDATE_SUB_CATEGORY_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT){
					showToaster("success"," Subcategory updated successfully","Sub Category Update");
					window.location.reload();
				}
				else{
					showToaster("error"," Subcategory cannot be updated","Sub Category Update");
				}
			}
			else{
				showToaster("error"," Subcategory cannot be added","Sub Category Update");
			}
		},function failure(){
			viviktaDebugConsole("SubCategoryController - updateSubCategory - API not found!");
			showToaster("error"," Subcategory cannot be added","Sub Category Update");
		});
	};
	//function to list all categories
	$scope.loadCategories = function(){
		$http({
			method : "GET",
			url : BASE_URL+GET_CATEGORIES_LIST_URL,
			params : {
				token : $scope.authToken,
				filterType : 1
			}
		}).then(function success(response){
			viviktaDebugConsole(JSON.stringify(response));
			if(response.status == HTTP_STATUS_CODE){
				if(response.data.errorCode == GET_CATEGORIES_LIST_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT && response.data.data != ''){
					$scope.categoriesPresent = true;
					$scope.categories = response.data.data;
				}
				else{
					$scope.categoriesPresent = false;
				}
			}
			else{
				$scope.categoriesPresent = false;
			}
		},function failure(){
			viviktaDebugConsole("CategoryController - loadCategories - API not found!");
			$scope.categoriesPresent = false;
		});
	};
	$scope.loadSubCategoryDetails=function(subCategory){
		$scope.editSubCategoryData = {};
        		$scope.editSubCategoryData.name = angular.copy(subCategory.subcategoryName);
        		$scope.editSubCategoryData.subCategoryId = angular.copy(subCategory.subcategoryId);
        		$scope.editSubCategoryData.categoryId = angular.copy(subCategory.categoryId);
	}
})