if(DEV_MODE == 1)
	BASE_URL = DEV_URL;
app.controller('DashboardController',function($scope,$http){
	$scope.authToken = localStorage.getItem("anantya_token");
	//function to load the dashboard data
	$scope.loadDashboardData = function(){
		$http({
			method : "GET",
			url : BASE_URL+GET_DASHBOARD_DATA_URL,
			params : {
				token : $scope.authToken
			}
		}).then(function success(response){
			console.log(JSON.stringify(response));
			if(response.status == HTTP_STATUS_CODE){
				if(response.data.errorCode == GET_DASHBOARD_DATA_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT && response.data.data != ''){
					//showToaster("success","Dashboard loaded successfully!","Dashboard Update");
					$scope.totalCategoriesCount = response.data.data.totalCategoriesCount;
					$scope.totalSubCategoriesCount = response.data.data.totalSubCategoriesCount;
					$scope.totalProductsCount = response.data.data.totalProductsCount;
					$scope.totalColorsCount = response.data.data.totalColorsCount;
					$scope.totalOrdersCount = response.data.data.totalOrdersCount;
					$scope.totalComplaintsCount = response.data.data.totalComplaintsCount;
					$scope.totalSuggestionsCount = response.data.data.totalSuggestionsCount;
				}
				else{
					//showToaster("error","Unable to load data","Dashboard Update");
				}
			}
			else{
				//showToaster("error","Unable to load data","Dashboard Update");
			}
		},function failure(){
			viviktaDebugConsole("DashboardController - loadDashboardData - API not found!");
			showToaster("error","Unable to load data","Dashboard Update");
		});
	};
})