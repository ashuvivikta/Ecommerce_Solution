if(DEV_MODE == 1)
	BASE_URL = DEV_URL;
app.controller('OrderController',function($scope,$http){
	$scope.authToken = localStorage.getItem("anantya_token");

	//function to list all orders
	$scope.loadOrders = function(date,productId,productStatus,type){
		viviktaDebugConsole(JSON.stringify(date));
		$http({
			method : "GET",
			url : BASE_URL+GET_ORDERS_LIST_URL,
			params : {
				token : $scope.authToken,
				date : date,
				productId : productId,
				productStatus : productStatus,
				filterType :type
			}
		}).then(function success(response){
			console.log(JSON.stringify(response));
			if(response.status == HTTP_STATUS_CODE){
				if(response.data.errorCode == GET_ORDERS_LIST_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT && response.data.data != ''&& response.data.data != null && typeof(response.data.data) !== undefined){
					$scope.ordersPresent = true;
					$scope.ordersList = response.data.data;
				}
				else{
					$scope.ordersPresent = false;
					//showToaster("error","No Orders found","Orders Update");
				}
			}
			else{
				$scope.ordersPresent = false;
				showToaster("error","Unable to load Orders","Orders Update");
			}
		},function failure(){
			viviktaDebugConsole("OrderController - loadOrders - API not found!");
			$scope.ordersPresent = false;
			showToaster("error","Unable to load Orders","Orders Update");
		});
	};
	$scope.loadOrderDetails=function(order){
		$scope.orders=order;
		viviktaDebugConsole(JSON.stringify($scope.orders));
	}
	//function to load the list of all products
	$scope.loadProducts = function(){
		$http({
			method : "GET",
			url : BASE_URL+LIST_PRODUCTS_URL,
			params : {
				token : $scope.authToken,
				filterType : 1
			}
		}).then(function success(response){
			viviktaDebugConsole(JSON.stringify(response));
			if(response.status == HTTP_STATUS_CODE){
				if(response.data.errorCode == LIST_PRODUCTS_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT && response.data.data != ''){
					$scope.productsPresent = true;
					$scope.products = response.data.data;
				}
				else{
					$scope.productsPresent = false;
					//showToaster("error","No Products found","Products Update");
				}
			}
			else{
				$scope.productsPresent = false;
				showToaster("error","Unable to load Products","Products Update");
			}
		},function failure(){
			viviktaDebugConsole("ProductsController - loadProducts - API not found!");
			$scope.productsPresent = false;
			showToaster("error","Unable to load Products","Products Update");
		});
	};
	$scope.clearFilter = function(filter){
	        viviktaDebugConsole("inside clearFilter")
	        document.getElementById('filter.productStatus').value = "";
	        document.getElementById('filter.productId').value = "";
	        document.getElementById('filter.date').value = "";
	        $scope.contentsPresent = false;
    };

    //function to change the order status
	$scope.changeStatus = function(orderDetails,filterType){
		$http({
			method : "PUT",
			url : BASE_URL+CHANGE_ORDER_STATUS_URL,
			params : {
				token : $scope.authToken,
				cartId : orderDetails.cartId,
				productStatus : filterType
			}
		}).then(function success(response){
			viviktaDebugConsole(JSON.stringify(response));
			if(response.status == HTTP_STATUS_CODE){
				if(response.data.errorCode == CHANGE_ORDER_STATUS_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT && response.data.data != ''){
					showToaster("success","Status changed Successfully","Orders Update");
					window.location.reload();
				}
				else{
					showToaster("error","Status change unsuccessfull","Orders Update");
				}
			}
			else{
				showToaster("error","Status change unsuccessfull","Orders Update");
			}
		},function failure(){
			viviktaDebugConsole("OrderController - changeStatus - API not found!");
			showToaster("error","Status change unsuccessfull","Orders Update");
		});
	};
})