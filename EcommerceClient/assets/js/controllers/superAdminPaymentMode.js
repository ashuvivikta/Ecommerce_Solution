if(DEV_MODE == 1)
	BASE_URL = DEV_URL;
app.controller('PaymentModeController',function($scope,$http){
	$scope.authToken = localStorage.getItem("anantya_token");

	//function to list all categories
	$scope.loadPaymentModes = function(paginateFlag,url){
		if(paginateFlag == 0){
            $scope.url = BASE_URL+GET_PAYMENT_MODE_LIST_URL
        }
        else{
            $scope.url = url;
        }
		$http({
			method : "GET",
			url : $scope.url,
			params : {
				token : $scope.authToken,
				filterType : 0
			}
		}).then(function success(response){
			console.log(JSON.stringify(response));
			if(response.status == HTTP_STATUS_CODE){
				if(response.data.errorCode == LIST_PAYMENT_MODE_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT && response.data.data.data != ''){
					$scope.lastPage = response.data.data.lastPage;
			        $scope.showPagination = true;
			        if($scope.lastPage == "NULL"){
			            $scope.showPagination = false;
			            $scope.payments = response.data.data.data;
			        }
			        else{
			            $scope.payments = response.data.data.data.data;
			        }
			        $scope.previousPageUrl = response.data.data.previousPageUrl;
			        $scope.nextPageUrl = response.data.data.nextPageUrl;
			        $scope.currentPage = response.data.data.currentPage;
					$scope.paymentsPresent = true;
				}
				else{
					$scope.paymentsPresent = false;
					//showToaster("error","Categories cannot be loaded!","try again later");
				}
			}
			else{
				$scope.paymentsPresent = false;
				showToaster("error","Payments cannot be loaded!","try again later");
			}
		},function failure(){
			viviktaDebugConsole("PaymentController - loadpayments - API not found!");
			$scope.paymentsPresent = false;
			showToaster("error","Payments cannot be loaded!","try again later");
		});
	};

	//function to add a category
	$scope.addNewPayment = function(paymentData){
		$http({
			method : "POST",
			url : BASE_URL+ADD_PAYMENT_MODE_URL,
			params : {
				token : $scope.authToken,
				paymentName : paymentData.name,
				paymentDescription : paymentData.description
			}
		}).then(function success(response){
			if(response.status == HTTP_STATUS_CODE){
				viviktaDebugConsole(JSON.stringify(response));
				if(response.data.errorCode == ADD_PAYMENT_MODE_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT){
					showToaster("success","Payment Mode added successfully!","Payments update");
					window.location.reload();
				}
				else if(response.data.errorCode == ADD_PAYMENT_MODE_FAILURE_CODE && response.data.statusText == FAILURE_STATUS_TEXT){
					showToaster("error","Payment Mode already exists!","Payments update");
				}
				else{
					showToaster("error","Payment Mode cannot be added!","try again later");
				}
			}
			else{
				showToaster("error","Payment Mode cannot be added!","try again later");
			}
		},function failure(){
			viviktaDebugConsole("PaymentController - addNewPayment - API not found!");
			showToaster("error","Payment Mode cannot be added!","try again later");
		});
	};
	//function to update a category
	$scope.updatePayment = function(editPaymentData){
		$http({
			method : "PUT",
			url : BASE_URL+UPDATE_PAYMENT_URL,
			params : {
				token : $scope.authToken,
				paymentId : editPaymentData.paymentId,
				paymentName : editPaymentData.name,
				paymentDescription : editPaymentData.description
			}
		}).then(function success(response){
			if(response.status == HTTP_STATUS_CODE){
				viviktaDebugConsole(JSON.stringify(response));
				if(response.data.errorCode == UPDATE_PAYMENT_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT){
					showToaster("success","Payment Mode updated successfully!","Payments update");
					window.location.reload();
				}
				else{
					showToaster("error","Payment Mode cannot be updated!","try again later");
				}
			}
			else{
				showToaster("error","Payment Mode cannot be updated!","try again later");
			}
		},function failure(){
			viviktaDebugConsole("PaymentController - updatePayment - API not found!");
			showToaster("error","Payment Mode cannot be updated!","try again later");
		});
	};
	$scope.loadPaymentDetails=function(payment){
		$scope.editPaymentData = {};
        		$scope.editPaymentData.name = angular.copy(payment.paymentName);
        		$scope.editPaymentData.paymentId = angular.copy(payment.paymentId);
        		$scope.editPaymentData.description = angular.copy(payment.paymentDescription);
	}
	//function to delete/remove a product
  	$scope.deletePayment = function(paymentDetails){
  		$http({
  			method : "DELETE",
  			url : BASE_URL+DELETE_PAYMENT_URL,
  			params : {
  				token : $scope.authToken,
  				paymentId : paymentDetails.paymentId
  			}
  		}).then(function success(response){
  			if(response.status == HTTP_STATUS_CODE){
  				if(response.data.errorCode == DELETE_PAYMENT_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT){
  					showToaster("success"," payment deleted successfully","Payments Update");
  					window.location.reload();
  				}
  				else{
  					showToaster("error"," payment cannot be deleted","Payments Update");
  				}
  			}
  			else{
  				showToaster("error"," payment cannot be deleted","Payments Update");
  			}
  		},function failure(){
  			viviktaDebugConsole("PaymentsController - deletePayment - API not found!");
  			showToaster("error"," payment cannot be deleted","Payments Update");
  		});
  	};
     
      $scope.deletePaymentDetails = function(payment){
        $scope.paymentDetails = payment;
      }
})