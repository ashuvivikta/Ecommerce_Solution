if(DEV_MODE == 1)
	BASE_URL = DEV_URL;
app.controller('CategoryController',function($scope,$http){
	$scope.authToken = localStorage.getItem("anantya_token");

	//function to list all categories
	$scope.loadCategories = function(paginateFlag,url){
		if(paginateFlag == 0){
            $scope.url = BASE_URL+GET_CATEGORIES_LIST_URL
        }
        else{
            $scope.url = url;
        }
		$http({
			method : "GET",
			url : $scope.url,
			params : {
				token : $scope.authToken,
				filterType : 0
			}
		}).then(function success(response){
			console.log(JSON.stringify(response));
			if(response.status == HTTP_STATUS_CODE){
				if(response.data.errorCode == GET_CATEGORIES_LIST_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT && response.data.data.data != ''){
					$scope.lastPage = response.data.data.lastPage;
			        $scope.showPagination = true;
			        if($scope.lastPage == "NULL"){
			            $scope.showPagination = false;
			            $scope.categories = response.data.data.data;
			        }
			        else{
			            $scope.categories = response.data.data.data.data;
			        }
			        $scope.previousPageUrl = response.data.data.previousPageUrl;
			        $scope.nextPageUrl = response.data.data.nextPageUrl;
			        $scope.currentPage = response.data.data.currentPage;
					$scope.categoriesPresent = true;
				}
				else{
					$scope.categoriesPresent = false;
					//showToaster("error","Categories cannot be loaded!","try again later");
				}
			}
			else{
				$scope.categoriesPresent = false;
				showToaster("error","Categories cannot be loaded!","try again later");
			}
		},function failure(){
			viviktaDebugConsole("CategoryController - loadCategories - API not found!");
			$scope.categoriesPresent = false;
			showToaster("error","Categories cannot be loaded!","try again later");
		});
	};

	//function to add a category
	$scope.addNewCategory = function(categoryData){
		$http({
			method : "POST",
			url : BASE_URL+ADD_CATEGORY_URL,
			params : {
				token : $scope.authToken,
				categoryName : categoryData.name
			}
		}).then(function success(response){
			if(response.status == HTTP_STATUS_CODE){
				viviktaDebugConsole(JSON.stringify(response));
				if(response.data.errorCode == ADD_CATEGORY_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT){
					showToaster("success","Category added successfully!","Category update");
					window.location.reload();
				}
				else if(response.data.errorCode == ADD_CATEGORY_EXISTS_FAILURE_CODE && response.data.statusText == FAILURE_STATUS_TEXT){
					showToaster("error","Category already exists!","Category update");
				}
				else{
					showToaster("error","Category cannot be added!","try again later");
				}
			}
			else{
				showToaster("error","Category cannot be added!","try again later");
			}
		},function failure(){
			viviktaDebugConsole("CategoryController - addNewCategory - API not found!");
			showToaster("error","Category cannot be added!","try again later");
		});
	};
	//function to update a category
	$scope.updateCategory = function(editCategoryData){
		$http({
			method : "PUT",
			url : BASE_URL+UPDATE_CATEGORY_URL,
			params : {
				token : $scope.authToken,
				categoryName : editCategoryData.name,
				categoryId : editCategoryData.categoryId
			}
		}).then(function success(response){
			if(response.status == HTTP_STATUS_CODE){
				viviktaDebugConsole(JSON.stringify(response));
				if(response.data.errorCode == UPDATE_CATEGORY_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT){
					showToaster("success","Category updated successfully!","Category update");
					window.location.reload();
				}
				else{
					showToaster("error","Category cannot be updated!","try again later");
				}
			}
			else{
				showToaster("error","Category cannot be updated!","try again later");
			}
		},function failure(){
			viviktaDebugConsole("CategoryController - updateCategory - API not found!");
			showToaster("error","Category cannot be updated!","try again later");
		});
	};
	$scope.loadCategoryDetails=function(category){
		$scope.editCategoryData = {};
        		$scope.editCategoryData.name = angular.copy(category.categoryName);
        		$scope.editCategoryData.categoryId = angular.copy(category.categoryId);
	}
})