if(DEV_MODE == 1)
	BASE_URL = DEV_URL;
app.controller('SuperAdminDashboardController',function($scope,$http){
	$scope.authToken = localStorage.getItem("anantya_token");
	//function to load the dashboard data
	$scope.loadDashboardData = function(){
		$http({
			method : "GET",
			url : BASE_URL+GET_SUPER_ADMIN_DASHBOARD_DATA_URL,
			params : {
				token : $scope.authToken
			}
		}).then(function success(response){
			console.log(JSON.stringify(response));
			if(response.status == HTTP_STATUS_CODE){
				if(response.data.errorCode == GET_DASHBOARD_SUPER_ADMIN_DATA_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT && response.data.data != ''){
					$scope.totalVendorsCount = response.data.data.totalVendorsCount;
				}
				else{
					showToaster("error","Unable to load data","Dashboard Update");
				}
			}
			else{
				showToaster("error","Unable to load data","Dashboard Update");
			}
		},function failure(){
			viviktaDebugConsole("DashboardController - loadDashboardData - API not found!");
			showToaster("error","Unable to load data","Dashboard Update");
		});
	};
})