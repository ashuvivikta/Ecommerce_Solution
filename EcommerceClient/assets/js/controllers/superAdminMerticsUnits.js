if(DEV_MODE == 1)
	BASE_URL = DEV_URL;
app.controller('MetricUnitsController',function($scope,$http){
	$scope.authToken = localStorage.getItem("anantya_token");

	//function to list all categories
	$scope.loadUnits = function(paginateFlag,url){
		if(paginateFlag == 0){
            $scope.url = BASE_URL+GET_UNITS_LIST_URL
        }
        else{
            $scope.url = url;
        }
		$http({
			method : "GET",
			url : $scope.url,
			params : {
				token : $scope.authToken,
				filterType : 0
			}
		}).then(function success(response){
			console.log(JSON.stringify(response));
			if(response.status == HTTP_STATUS_CODE){
				if(response.data.errorCode == LIST_UNITS_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT && response.data.data.data != ''){
					$scope.lastPage = response.data.data.lastPage;
			        $scope.showPagination = true;
			        if($scope.lastPage == "NULL"){
			            $scope.showPagination = false;
			            $scope.units = response.data.data.data;
			        }
			        else{
			            $scope.units = response.data.data.data.data;
			        }
			        $scope.previousPageUrl = response.data.data.previousPageUrl;
			        $scope.nextPageUrl = response.data.data.nextPageUrl;
			        $scope.currentPage = response.data.data.currentPage;
					$scope.unitsPresent = true;
				}
				else{
					$scope.unitsPresent = false;
					//showToaster("error","Categories cannot be loaded!","try again later");
				}
			}
			else{
				$scope.unitsPresent = false;
				showToaster("error","Units cannot be loaded!","try again later");
			}
		},function failure(){
			viviktaDebugConsole("MetricUnitsController - loadUnits - API not found!");
			$scope.unitsPresent = false;
			showToaster("error","Units cannot be loaded!","try again later");
		});
	};

	//function to add a category
	$scope.addNewUnit = function(unitData){
		$http({
			method : "POST",
			url : BASE_URL+ADD_UNITS_URL,
			params : {
				token : $scope.authToken,
				unitName : unitData.name,
				unitDescription : unitData.description,
				metricUnit : unitData.metricUnit
			}
		}).then(function success(response){
			if(response.status == HTTP_STATUS_CODE){
				viviktaDebugConsole(JSON.stringify(response));
				if(response.data.errorCode == ADD_UNITS_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT){
					showToaster("success","Units added successfully!","Units update");
					window.location.reload();
				}
				else if(response.data.errorCode == ADD_PAYMENT_MODE_FAILURE_CODE && response.data.statusText == FAILURE_STATUS_TEXT){
					showToaster("error","Units Mode already exists!","Units update");
				}
				else{
					showToaster("error","Units Mode cannot be added!","try again later");
				}
			}
			else{
				showToaster("error","Units Mode cannot be added!","try again later");
			}
		},function failure(){
			viviktaDebugConsole("MetricUnitsController- addNewUnits - API not found!");
			showToaster("error","Units Mode cannot be added!","try again later");
		});
	};
	//function to update a category
	$scope.updateUnit = function(editUnitData){
		$http({
			method : "PUT",
			url : BASE_URL+UPDATE_UNITS_URL,
			params : {
				token : $scope.authToken,
				unitId : editUnitData.unitId,
				unitName : editUnitData.name,
				unitDescription : editUnitData.description,
				metricUnit : editUnitData.metricUnit
			}
		}).then(function success(response){
			if(response.status == HTTP_STATUS_CODE){
				viviktaDebugConsole(JSON.stringify(response));
				if(response.data.errorCode == UPDATE_UNITS_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT){
					showToaster("success","Unit Mode updated successfully!","Unit update");
					window.location.reload();
				}
				else{
					showToaster("error","Unit Mode cannot be updated!","try again later");
				}
			}
			else{
				showToaster("error","Unit Mode cannot be updated!","try again later");
			}
		},function failure(){
			viviktaDebugConsole("MetricUnitsController - updatePayment - API not found!");
			showToaster("error","Unit Mode cannot be updated!","try again later");
		});
	};
	$scope.loadUnitDetails=function(unit){
		$scope.editUnitData = {};
        		$scope.editUnitData.name = angular.copy(unit.unitName);
        		$scope.editUnitData.unitId = angular.copy(unit.unitId);
        		$scope.editUnitData.description = angular.copy(unit.unitDescription);
        		$scope.editUnitData.metricUnit = angular.copy(unit.metricUnit);
	}
	//function to delete/remove a product
  	$scope.deleteUnit = function(unitDetails){
  		$http({
  			method : "DELETE",
  			url : BASE_URL+DELETE_UNITS_URL,
  			params : {
  				token : $scope.authToken,
  				unitId : unitDetails.unitId
  			}
  		}).then(function success(response){
  			if(response.status == HTTP_STATUS_CODE){
  				if(response.data.errorCode == DELETE_UNITS_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT){
  					showToaster("success"," Unit deleted successfully","Unit Update");
  					window.location.reload();
  				}
  				else{
  					showToaster("error"," Unit cannot be deleted","Unit Update");
  				}
  			}
  			else{
  				showToaster("error"," Unit cannot be deleted","Unit Update");
  			}
  		},function failure(){
  			viviktaDebugConsole("MetricUnitsController - deletePayment - API not found!");
  			showToaster("error"," Unit cannot be deleted","Payments Update");
  		});
  	};
     
      $scope.deleteUnitDetails = function(unit){
        $scope.unitDetails = unit;
      }
})