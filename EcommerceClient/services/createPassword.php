<?php
    include('header.php');
    include('plugins.php');
    include('footer.php');
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <title>Shopping Mall || Create Password</title>
    <?php echo loadPlugins(); ?>
</head>
<body ng-app="anantya" ng-controller="RegistrationLoginController">
    <div class="wrapper">
        <div id="header">
            <?php echo loadHeader();?>
        </div>
        <div id="page-content" class="page-wrapper">
            <div class="login-section mb-80">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="registered-customers">
                                <h6 class="widget-title border-left mb-50">REGISTERED CUSTOMERS</h6>
                                <form>
                                    <div class="login-account p-30 box-shadow">
                                        <p>Please create the password here</p>
                                        <input type="password" name="name" placeholder="Password" ng-model="passwordData.password">
                                        <input type="password" name="password" placeholder="Confirm Password" ng-model="passwordData.confirmPassword">
                                        <p ng-show="passwordMismatchError" style="color:red">Password and confirm password did not match!</p>
                                        <button class="submit-btn-1 btn-hover-1" type="button" ng-click="createPassword(passwordData)">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="footer">
            <?php echo loadFooter(); ?>
        </div>
    </div>
    <?php echo loadScripts(); ?>
</body>
</html>