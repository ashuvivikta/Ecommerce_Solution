<?php
	function loadFooter(){
		$footer = '<footer id="footer" class="footer-area">
            <div class="footer-top">
                <div class="container-fluid">
                    <div class="plr-185">
                        <div class="footer-top-inner gray-bg">
                            <div class="row">
                                <div class="col-lg-4 col-md-5 col-sm-4">
                                    <div class="single-footer footer-about">
                                        <div class="footer-logo">
                                            <img src="img/logo/logo.png" alt="" style="height: 70px;width: inherit;">
                                        </div>
                                        <div class="footer-brief">
                                            <p>E-commerce is a transaction of buying or selling online. Electronic commerce draws on technologies such as mobile commerce, electronic funds transfer, supply chain management, Internet marketing, online transaction processing, electronic data interchange (EDI), inventory management systems, and automated data collection systems.</p>
                                        </div>
                                        <ul class="footer-social">
                                            <li ng-hide="facebookLink==null">
                                                <a class="facebook" href="{{facebookLink}}" title="Facebook"><i class="zmdi zmdi-facebook"></i></a>
                                            </li>
                                            <li ng-hide="googlePlusLink==null">
                                                <a class="google-plus" href="{{googlePlusLink}}" title="Google Plus"><i class="zmdi zmdi-google-plus"></i></a>
                                            </li>
                                            <li ng-hide="twitterLink==null">
                                                <a class="twitter" href="{{twitterLink}}" title="Twitter"><i class="zmdi zmdi-twitter"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4">
                                    <div class="single-footer">
                                        <h4 class="footer-title border-left">my account</h4>
                                        <ul class="footer-menu">
                                            <li>
                                                <a href="myAccount.php"><i class="zmdi zmdi-circle"></i><span>My Account</span></a>
                                            </li>
                                            <li style="display:none">
                                                <a href="wishlist.php"><i class="zmdi zmdi-circle"></i><span>My Wishlist</span></a>
                                            </li>
                                            <li>
                                                <a href="cart.php"><i class="zmdi zmdi-circle"></i><span>My Cart</span></a>
                                            </li>
                                            <li>
                                                <a href="login.php"><i class="zmdi zmdi-circle"></i><span>Sign In</span></a>
                                            </li>
                                            <li>
                                                <a href="login.php"><i class="zmdi zmdi-circle"></i><span>Registration</span></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="single-footer" ng-controller="FooterController">
                                        <h4 class="footer-title border-left">Get in touch</h4>
                                        <div class="footer-message">
                                            <form>
                                                <input type="text" name="name" placeholder="Your name here..." ng-model="queryData.name">
                                                <input type="text" name="email" placeholder="Your email here..." ng-model="queryData.emailId">
                                                <textarea class="height-80" name="message" placeholder="Your messege here..." ng-model="queryData.message"></textarea>
                                                <p ng-show="messageStatus" style="color:red">Message sent successfully!</p>
                                                <button class="submit-btn-1 mt-20 btn-hover-1" ng-click="raiseQuery(queryData)">submit message</button> 
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="toaster"></div>
            <div class="footer-bottom black-bg">
                <div class="container-fluid">
                    <div class="plr-185">
                        <div class="copyright">
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <div class="copyright-text">
                                        <p>&copy; All Rights Reserved.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>';
		return $footer;
	}
?>