<?php
    include('header.php');
    include('plugins.php');
    include('footer.php');
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <title>Shopping Mall || Contact</title>
    <?php echo loadPlugins();?>
</head>
<body>
    <div class="wrapper">
        <div id="header">
            <?php echo loadHeader(); ?>
        </div>
        <section id="page-content" class="page-wrapper">
            <div class="address-section mb-80">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4 col-xs-12">
                            <div class="contact-address box-shadow">
                                <i class="zmdi zmdi-pin"></i>
                                <h6>House 06, Road 01, Katashur, Mohammadpur,</h6>
                                <h6>Dhaka-1207, Bangladesh</h6>
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <div class="contact-address box-shadow">
                                <i class="zmdi zmdi-phone"></i>
                                <h6>(+880) 1945 082759</h6>
                                <h6>(+880) 1945 082759</h6>
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <div class="contact-address box-shadow">
                                <i class="zmdi zmdi-email"></i>
                                <h6>companyname@gmail.com</h6>
                                <h6>info@domainname.com</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div id="footer">
            <?php echo loadFooter(); ?>
        </div>
    </div>
    <?php echo loadScripts() ?>
</body>
</html>