<?php
    include('header.php');
    include('plugins.php');
    include('footer.php');
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <title>Shopping Mall || Login</title>
    <?php echo loadPlugins(); ?>
</head>
<body ng-app="anantya" ng-controller="RegistrationLoginController">
    <div class="wrapper">
        <div id="header">
            <?php echo loadHeader();?>
        </div>
        <div class="breadcrumbs-section plr-200 mb-80" style="background-color: black;color: white">
            <div class="">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="breadcrumbs-inner">
                                <div class="col-xs-8">
                                    <h1 class="breadcrumbs-title" style="color: white;text-align: left;">Login / Register</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="page-content" class="page-wrapper">
            <div class="login-section mb-80">
                <div class="container">
                    <div class="row" ng-hide="registrationSuccess">
                        <div class="col-md-6">
                            <div class="registered-customers">
                                <h6 class="widget-title border-left mb-50">REGISTERED CUSTOMERS</h6>
                                <form>
                                    <div class="login-account p-30 box-shadow">
                                        <p>If you have an account with us, Please log in.</p>
                                        <input type="text" name="name" placeholder="Email Address" ng-model="loginData.username">
                                        <input type="password" name="password" placeholder="Password" ng-model="loginData.password">
                                        <p><small><a href="#">Forgot our password?</a></small></p>
                                        <p ng-show="loginFailure" style="color:red">Username and password did not match.</p>
                                        <button class="submit-btn-1 btn-hover-1" type="button" ng-click="doLogin(loginData)">login</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- new-customers -->
                        <div class="col-md-6">
                            <div class="new-customers">
                                <form>
                                    <h6 class="widget-title border-left mb-50">NEW CUSTOMERS</h6>
                                    <div class="login-account p-30 box-shadow">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <input type="text"  placeholder="Name" ng-model="registrationData.name">
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text"  placeholder="Phone Number" ng-model="registrationData.phoneNumber">
                                            </div>
                                        </div>
                                        <input type="text"  placeholder="Email address here..." ng-model="registrationData.emailId">
                                        <input type="text"  placeholder="Address" ng-model="registrationData.address">
                                        <div class="row">
                                            <p ng-show="registrationFailure" style="color:red">User already exists! Try log in.</p>
                                            <div class="col-md-6">
                                                <button type="button" class="submit-btn-1 mt-20 btn-hover-1" ng-click="register(registrationData)">Register</button>
                                            </div>
                                            <div class="col-md-6">
                                                <button class="submit-btn-1 mt-20 btn-hover-1 f-right" type="reset">Clear</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="row" ng-show="registrationSuccess">
                         <div class="col-md-12">
                            <h1>Registration successful!</h1>
                            <p>Please check your email id to confirm the account.</p>
                         </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="footer">
            <?php echo loadFooter(); ?>
        </div>
    </div>
    <?php echo loadScripts(); ?>
</body>
</html>
