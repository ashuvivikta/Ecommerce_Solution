<?php
    include('header.php');
    include('plugins.php');
    include('footer.php');
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <title>Shopping Mall || My Account</title>
    <?php echo loadPlugins(); ?>
</head>
<body ng-app="anantya" ng-controller="AccountsController" ng-init="loadAccountDetails()">
    <div class="wrapper">
        <div id="header">
            <?php echo loadHeader(); ?>
        </div>
        <div class="breadcrumbs-section plr-200 mb-80" style="background-color: black;color: white">
            <div class="">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="breadcrumbs-inner">
                                <div class="col-xs-4" style="padding-top: 10px;">
                                    <!--<img src="img/single-watch2.jpg" style="height: 200px;">-->
                                </div>
                                <div class="col-xs-8">
                                    <h1 class="breadcrumbs-title" style="color: white;text-align: left;">Account</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="page-content" class="page-wrapper">
            <div class="login-section mb-80">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="my-account-content" id="accordion2">
                                <!-- My Personal Information -->
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion2" href="#personal_info">My Personal Information</a>
                                        </h4>
                                    </div>
                                    <div id="personal_info" class="panel-collapse collapse in" role="tabpanel">
                                        <div class="panel-body">
                                            <form action="#">
                                                <div class="new-customers">
                                                    <div class="p-30">
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <input type="text"  placeholder="Name" ng-model="profileDetails.userName">
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <input type="text"  placeholder="Email Id" ng-model="profileDetails.userEmailId" disabled>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <input type="text"  placeholder="Phone Number" ng-model="profileDetails.userPhoneNumber">
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <button class="submit-btn-1 mt-20 btn-hover-1" ng-click="updatePersonalInfo(profileDetails)">Save</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- My shipping address -->
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion2" href="#changePassword">Change Password</a>
                                        </h4>
                                    </div>
                                    <div id="changePassword" class="panel-collapse collapse" role="tabpanel" >
                                        <div class="panel-body">
                                            <form action="#">
                                                <div class="new-customers p-30">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <input type="password"  placeholder="Password" ng-model="passwordData.password">
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <input type="password"  placeholder="Confirm Password" ng-model="passwordData.confirmPassword">
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <button class="submit-btn-1 mt-20 btn-hover-1" ng-click="changePassword(passwordData)">Save</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- My billing details -->
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion2" href="#billing_address">My Addresses</a>
                                        </h4>
                                    </div>
                                    <div id="billing_address" class="panel-collapse collapse" role="tabpanel" >
                                        <div class="panel-body">
                                            <form action="#">
                                                <div class="billing-details p-30">
                                                    <textarea class="custom-textarea" placeholder="Your address here..." ng-model="addressData.address"></textarea>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <button class="submit-btn-1 mt-20 btn-hover-1" ng-click="addNewAddress(addressData)">Save</button>
                                                        </div>
                                                    </div>
                                                    <hr/>
                                                    <div class="row">
                                                        <h4>Your saved addresses</h4>
                                                        <div class="col-md-12" ng-repeat="address in addresses">
                                                            <div class="col-md-8"  style="padding: 10px; background-color: #f5f5f5;color: black;border:1px solid #ddd;text-align: left;margin:2px;">
                                                                {{address.userAddress}}
                                                            </div>
                                                            <div class="col-md-2">
                                                                <button class="submit-btn-1 mt-20 btn-hover-1" ng-click="removeAddress(address.addressId)" style="margin-top: 4px;"><i class="fa fa-times"></i></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <div id="footer">
            <?php echo loadFooter();?>
        </div>
        </div>
        <div id="toaster"></div>
    </div>
   <?php echo loadScripts() ?>
</body>
</html>