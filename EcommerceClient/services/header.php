<?php
	function loadHeader(){
		$header = '<header class="header-area header-wrapper" ng-controller="IndexController" ng-init="loadHeaders()">
            <!-- header-top-bar -->
            <div class="header-top-bar plr-185">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-6 hidden-xs">
                            <div class="call-us">
                                <p class="mb-0 roboto"></p>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <div class="top-link clearfix">
                                <ul class="link f-right">
                                    <div class="btn-group" ng-show="isLoggedIn" style="margin-top: 5px;">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background-color:#222222;color:white;border: 0px;">
                                                My Account <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" style="background-color: #f6f6f6">
                                            <li><a href="myAccount.php">My Account</a></li>
                                            <li role="separator" class="divider"></li>
                                            <li><a href="myOrders.php">My Orders</a></li>
                                            <li role="separator" class="divider"></li>
                                            <li><a href="logout.php">Logout</a></li>
                                        </ul>
                                    </div>
                                    <li ng-hide="isLoggedIn">
                                        <a href="login.php">
                                            <i class="zmdi zmdi-lock"></i>
                                            Login
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- header-middle-area -->
            <div id="sticky-header" class="header-middle-area plr-185">
                <div class="container-fluid ng-cloak">
                    <div class="full-width-mega-dropdown">
                        <div class="row">
                            <!-- logo -->
                            <div class="col-md-2 col-sm-6 col-xs-12 ng-cloak">
                                <div class="logo">
                                    <a ng-if="logoPresent">
                                        <img class="ng-cloak" src="{{vendorLogo}}" alt="main logo" style="height: 50px;width: inherit;">
                                    </a>
                                    <a ng-if="logoPresent==false">
                                        <img class="ng-cloak" src="img/default-img/logo-here.png" style="height: 50px;width: inherit;">
                                    </a>
                                </div>
                            </div>
                            <!-- primary-menu -->
                            <div class="col-md-8 hidden-sm hidden-xs" ng-init="loadProductCategories()">
                                <nav id="primary-menu">
                                    <ul class="main-menu text-center">
                                        <li class="ng-cloak" ng-repeat="headerProductDetail in headerProductDetails"><a class="ng-cloak" href="">{{headerProductDetail.categoryName}}</a>
                                            <ul class="dropdwn">
                                                <li ng-repeat="productSubCategory in headerProductDetail.subCategories">
                                                    <a href="products.php?category={{productSubCategory.subcategoryId}}">{{productSubCategory.subcategoryName}}</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                            <!-- header-search & total-cart -->
                            <div class="col-md-2 col-sm-6 col-xs-12">
                                <div class="search-top-cart  f-right">
                                    <!-- header-search -->
                                  
                                    <!-- total-cart -->
                                    <div class="total-cart f-left">
                                        <div class="total-cart-in">
                                            <div class="cart-toggler">
                                                <a href="cart.php">
                                                    <span class="cart-quantity" id="cart-count"></span><br>
                                                    <span class="cart-icon">
                                                        <i class="zmdi zmdi-shopping-cart-plus"></i>
                                                    </span>
                                                </a>                            
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div class="mobile-menu-area hidden-lg hidden-md" ng-init="loadProductCategories()">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="mobile-menu">
                            <nav id="dropdown">
                                <ul>
                                    <li class="ng-cloak" ng-repeat="headerProductDetail in headerProductDetails"><a class="ng-cloak" href="">{{headerProductDetail.categoryName}}</a>
                                        <ul class="dropdwn">
                                            <li ng-repeat="productSubCategory in headerProductDetail.subCategories">
                                                <a href="products.php?category={{productSubCategory.subcategoryId}}">{{productSubCategory.subcategoryName}}</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>';
		return $header;
	}
?>
