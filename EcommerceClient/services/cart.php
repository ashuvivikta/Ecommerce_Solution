<?php
    include('header.php');
    include('plugins.php');
    include('footer.php');
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <title>Shopping Mall || Shopping Cart</title>
    <?php echo loadPlugins();?>
</head>
<body ng-app="anantya" ng-controller="ProductsController" ng-init="loadCartItems()">
    <div class="wrapper">
        <div id="header">
            <?php echo loadHeader(); ?>
        </div>
        <section id="page-content" class="page-wrapper">
            <div class="shop-section mb-80">
                <div class="container ng-cloak" ng-show="itemsFound">
                    <div class="row"  ng-repeat="cartDetails in cartItemDetails">
                        <div class="col-md-2 col-sm-12">
                            <ul class="cart-tab">
                                <li>
                                    <a class="active ng-cloak" href="#shopping-cart" data-toggle="tab">
                                        <span>{{$index+1}}</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-10 col-sm-12">
                            <div class="tab-content">
                                <div class="tab-pane active" id="shopping-cart">
                                    <div class="shopping-cart-content">
                                        <form action="#">
                                            <div class="table-content table-responsive mb-50">
                                                <table class="text-center ng-cloak">
                                                    <thead>
                                                        <tr>
                                                            <th class="product-thumbnail">product</th>
                                                            <th class="product-price">price</th>
                                                            <th class="product-price">Tax</th>
                                                            <th class="product-price">Shipping Amount</th>
                                                            <th class="product-quantity">Quantity</th>
                                                            <th class="product-subtotal">total</th>
                                                            <th class="product-remove">remove</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td class="product-thumbnail">
                                                                <div class="pro-thumbnail-img">
                                                                    <img class="ng-cloak" src="{{cartDetails.productImages[0].thumbnail}}" alt="{{cartDetails.productName}}">
                                                                </div>
                                                                <div class="pro-thumbnail-info text-left">
                                                                    <h6 class="product-title-2 ng-cloak">
                                                                        <a href="singleProduct.php?productId={{cartDetails.productId}}">{{cartDetails.productName}}</a>
                                                                    </h6>
                                                                    <p  class="ng-cloak">Model: {{cartDetails.productModel}}</p>
                                                                    <p  class="ng-cloak">Color: {{cartDetails.productColor}}</p>
                                                                </div>
                                                            </td>
                                                            <td class="product-price ng-cloak">{{cartDetails.productPrice}}</td>
                                                            <td class="product-price ng-cloak">{{cartDetails.productTaxAmount}}</td>
                                                            <td class="product-price ng-cloak">{{cartDetails.productShippingAmount}}</td>
                                                            <td class="product-quantity ng-cloak">
                                                                <div class="cart-plus-minus f-left">
                                                                    <input type="text" name="qtybutton" class="cart-plus-minus-box" ng-model="cartDetails.quantity" ng-change="changeQuantity(cartDetails.productId,cartDetails.quantity)">
                                                                </div> 
                                                            </td>
                                                            <td class="product-subtotal ng-cloak">{{cartDetails.productTotalPrice}}</td>
                                                            <td class="product-remove ng-cloak">
                                                                <a ng-click="removeProductFromCart(cartDetails.productId)"><i class="zmdi zmdi-close"></i></a>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 col-sm-12">
                        </div>
                        <div class="col-md-10 col-sm-12">
                            <div class="table-content table-responsive mb-50">
                                <table class="text-center">
                                    <tbody>
                                        <tr>
                                            <td class="product-price">Grand Total</td>
                                            <td class="product-price">
                                                <i class="fa fa-rupee"></i>&nbsp;{{totalPrice}}
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 col-sm-12">
                        </div>
                        <div class="col-md-10 col-sm-12" style="text-align: right;">
                            <button class="submit-btn-1 btn-hover-1" type="button" ng-click="placeOrder()">place order</button>
                        </div>
                    </div>
                </div>
                <div class='container' ng-hide="itemsFound">
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <h1>There are no items found in cart!</h1>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div id="footer">
            <?php echo loadFooter(); ?>
        </div>
    </div>
    <?php echo loadScripts() ?>
</body>
</html>