<?php
	function loadPlugins(){
		$plugins = '
				<meta charset="utf-8">
    			<meta http-equiv="x-ua-compatible" content="ie=edge">
    			<meta name="description" content="">
			    <meta name="viewport" content="width=device-width, initial-scale=1">
			    <link rel="shortcut icon" type="image/x-icon" href="img/icon/favicon.png">
			    <link rel="stylesheet" href="css/bootstrap.min.css">
			    <link rel="stylesheet" href="lib/css/nivo-slider.css">
			    <link rel="stylesheet" href="css/core.css">
			    <link rel="stylesheet" href="css/shortcode/shortcodes.css">
			    <script src="js/jquery-2.2.0.min.js"></script>
			    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">
			    <script type="text/javascript" src="js/controllers/angular.min.js"></script>
			    <link rel="stylesheet" href="style.css">
			    <link rel="stylesheet" href="css/customStyle.css">
			    <link rel="stylesheet" href="css/responsive.css">
			    <link rel="stylesheet" href="css/custom.css">
			    <link rel="stylesheet" href="css/style-customizer.css">
			    <link href="#" data-style="styles" rel="stylesheet">
			    <script src="js/vendor/modernizr-2.8.3.min.js"></script>
			    <script src="js/controllers/localStorage.js"></script>
			    <script src="js/controllers/config.js"></script>
			    <script src="js/controllers/debugConsole.js"></script>
			    <script src="js/controllers/app.js"></script>
			    <script src="js/controllers/logService.js"></script>
			    <script src="js/controllers/indexController.js"></script>
			    <script src="js/controllers/productsController.js"></script>
			    <script src="js/controllers/registrationLoginController.js"></script>
			    <script src="js/controllers/accountsController.js"></script>
			';
		return $plugins;
	}
	function loadScripts(){
		$scripts = '<script src="js/vendor/jquery-3.1.1.min.js"></script>
				    <script src="js/bootstrap.min.js"></script>
				    <script src="lib/js/jquery.nivo.slider.js"></script>
				    <script src="js/plugins.js"></script>
				    <script src="js/main.js"></script>';
	    return $scripts;
	}
?>