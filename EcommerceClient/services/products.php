<?php
    include('header.php');
    include('plugins.php');
    include('footer.php');
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Shopping Mall || Products</title>
    <?php echo loadPlugins() ?>
 <link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100' rel='stylesheet' type='text/css'>

    <!-- styles -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/owl.theme.css" rel="stylesheet">

    <!-- theme stylesheet -->
    <link href="css/style.default.css" rel="stylesheet" id="theme-stylesheet">

    <!-- your stylesheet with modifications -->
    <link href="css/customobajustyle.css" rel="stylesheet">

    <script src="js/respond.min.js"></script>

    <link rel="shortcut icon" href="favicon.png">


    
</head>
<body ng-app="anantya" ng-controller="ProductsController" ng-init="loadProductsBasedOnProductCategory(0,'')">
    <div class="wrapper">
    <?php echo loadHeader();?>
<div class="container">
    <div class="row">
          <div class="col-md-3" id="vertical">
              <div class="row">
            <div class="panel panel-default sidebar-menu">
                <div class="panel-heading">
                    <h3 class="panel-title">Price Filter <a id="clear" class="btn btn-xs btn-danger pull-right" href="#"><i class="fa fa-times-circle"></i> Clear</a></h3>
                </div>
                <div class="panel-body">
                    <div id="slidecontainer ng-cloak">
                      <p>Custom range slider:</p>
                      <input type="range" min="1" max="50000"  ng-model="priceRange.minimumProductPrice" class="slider ng-cloak" id="myRange">
                        {{priceRange.minimumProductPrice}}
                    </div> 
                    <div id="slidecontainer">
                      <p>Custom range slider:</p>
                      <input type="range" min="1" max="100000"  ng-model="priceRange.maximumProductPrice" class="slider ng-cloak" id="myRange">
                        {{priceRange.maximumProductPrice}}
                    </div> 
                </div>
                 <button class="btn btn-primary" ng-click="getPriceRange(priceRange,paginateFlag,url)">Submit
                    </button>
            </div>
        </div>
              <div class="row" ng-init="loadColorsToFilter(color)">
                <div class="panel panel-default sidebar-menu">
                    <div class="panel-heading">
                        <h3 class="panel-title">Price Filter <a id="clear" class="btn btn-xs btn-danger pull-right" href="#"><i class="fa fa-times-circle"></i> Clear</a></h3>
                    </div>
                    <div class="panel-body">
                        <div class="color" ng-repeat="color in colors" >
                            <input type="checkbox" ng-model="checkboxStatus" ng-change="updateTicketStatus(checkboxStatus,color,productColorId)"/>
                            {{color.productColor}}<br/>
                        </div>
                         <button class="btn btn-default btn-sm btn-primary" ng-click="getColouredProduct(color,paginateFlag,url)"><i class="fa fa-pencil"></i> Apply</button>
                    </div>
                </div>
              </div>
        </div>

        <div class = "col-md-9">
            <div class="row">
                <div class="col-lg-offset-9 col-lg-3">
                      <div class="header-search f-left">
                        <div class="header-search-inner">
                           <button class="search-toggle">
                            <i class="zmdi zmdi-search"></i>
                           </button>
                            <form action="#">
                                <div class="top-search-box">
                                    <input type="text" ng-model="search" minlength="2" ng-change="searchProduct(search,productSubCategory)" placeholder="Search here your product..."><!-- ---->
                                    <button type="submit">
                                        <i class="zmdi zmdi-search"></i>
                                    </button>
                                </div>
                            </form> 
                        </div>
                    </div>
                </div>
            </div>
    <div class="container ng-cloak" ng-if="productsNotPresent" ng-hide="productsPresent">
        <div class="row">
            <div class="col-lg-12 text-center">
            <h3>There are no related contents found! please modify your search</h3>
            </div>
        </div>
    </div>
        <div id="page-content" class="page-wrapper">
            <div class="shop-section mb-80">
                <div class="container">
                    <div class="row">
                        <div class="col-md-9 col-sm-12">
                            <div class="shop-content">
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="grid-view">
                                        <div class="row" ng-if="productsPresent">
                                            <div class="col-md-4 col-sm-4 col-xs-12" ng-repeat="product in products">
                                                <div class="product-item ng-cloak">
                                                    <div class="product-img ng-cloak">
                                                        <a href="singleProduct.php?productId={{product.productId}}">
                                                            <img src="{{product.productImages[0].thumbnail}}" alt="{{product.productName}}" style="min-height: 300px;max-height: 300px;"/>
                                                        </a>
                                                    </div>
                                                    <div class="product-info">
                                                        <h6 class="product-title ng-cloak">
                                                            <a href="single-product.html">{{product.productName}}</a>
                                                        </h6>
                                                        <h3 class="pro-price ng-cloak"><i class="fa fa-rupee"></i> {{product.productPrice}}</h3>
                                                        <ul class="action-button">
                                                            <li>
                                                                <a href="#" data-toggle="modal"  data-target="#productModal" title="Quickview" ng-click="loadProductDetailInModal(product)"><i class="zmdi zmdi-zoom-in"></i></a>
                                                            </li>
                                                            <li>
                                                                <a title="Add to cart" ng-click="addProductToCart(product)"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="text-center ng-cloak" ng-show="showPagination">
                    <div class="btn-group">
                            <button ng-disabled="currentPage == 1" class="btn btn-white" max-size="maxSize" boundary-links="true" ng-click="loadProductsBasedOnProductCategory(1,previousPageUrl)"><span class="glyphicon glyphicon-step-backward"></span></button>
                                
                            <button class="btn btn-primary ng-cloak" max-size="maxSize" boundary-links="true">{{currentPage}}/{{lastPage}}</button>
                                
                            <button ng-disabled="currentPage == lastPage" class="btn btn-white" max-size="maxSize" boundary-links="true" ng-click="loadProductsBasedOnProductCategory(1,nextPageUrl)"><span class="glyphicon glyphicon-step-forward"></span></button>
                    </div>
                </div>
            </div>
        </div>
        <?php echo loadFooter(); ?>
        <div id="quickview-wrapper">
            <div class="modal fade" id="productModal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                        <div class="modal-body">
                            <div class="modal-product clearfix">
                                <div class="product-images">
                                    <div class="main-image images">
                                        <img alt="{{detailsOfProduct.productName}}" src="{{detailsOfProduct.productThumbnail}}">
                                    </div>
                                </div>
                                <div class="product-info">
                                    <h1>{{detailsOfProduct.productName}}</h1>
                                    <div class="price-box-3">
                                        <div class="s-price-box">
                                            <span class="new-price">
                                                <i class="fa fa-rupee"></i>&nbsp;{{detailsOfProduct.productPrice}}
                                            </span>
                                        </div>
                                    </div>
                                    <a href="singleProduct.php?productId={{detailsOfProduct.productId}}" class="see-all">See all features</a>
                                    <div class="quick-add-to-cart">
                                        <form method="post" class="cart">
                                            <button class="single_add_to_cart_button" ng-click="addProductToCart(detailsOfProduct)">Add to cart</button>
                                        </form>
                                    </div>
                                    <div class="quick-desc">
                                        {{detailsOfProduct.productDescription}}
                                    </div>
                                </div><!-- .product-info -->
                            </div><!-- .modal-product -->
                          </div>
                        </div><!-- .modal-body -->
                    </div><!-- .modal-content -->
                </div><!-- .modal-dialog -->
               </div>
            </div>
        </div>
    </div>
    </div>
    <?php echo loadScripts(); ?>
</body>
</html>