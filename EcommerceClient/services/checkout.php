<?php
    include('header.php');
    include('plugins.php');
    include('footer.php');
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <title>Shopping Mall || Checkout</title>
    <?php echo loadPlugins(); ?>
</head>
<body ng-app="anantya" ng-controller="ProductsController" ng-init="loadCheckoutDetails()">
    <div class="wrapper">
        <div id="header">
            <?php echo loadHeader(); ?>
        </div>
        <div id="page-content" class="page-wrapper">
            <div class="login-section mb-80">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="my-account-content" id="accordion2">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion2" href="#personal_info">My Address</a>
                                        </h4>
                                    </div>
                                    <div id="personal_info" class="panel-collapse collapse in" role="tabpanel">
                                        <div class="panel-body">
                                            <form>
                                                <div class="new-customers">
                                                    <div class="p-30">
                                                        <div ng-show="addressFound" ng-hide="addAddress">
                                                            <div class="col-md-6" ng-repeat="details in checkoutDetails">
                                                                <input type="radio" name="shippingAddress" ng-model="addressData.shippingAddress" value="{{details.addressId}}">
                                                                {{details.userAddress}}
                                                            </div>
                                                            <div class="row ng-cloak">
                                                                <div class="col-md-12" style="text-align: right">
                                                                    <button class="submit-btn-1 mt-20 btn-hover-1 f-right" ng-click="toggleAddAddress()">Add new Address</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div ng-hide="addressFound || addAddress">
                                                            <p>No address found!. Please add an address to continue.</p>
                                                            <div class="row ng-cloak">
                                                                <div class="col-md-6">
                                                                </div>
                                                                <div class="col-md-6" style="text-align: right">
                                                                    <button class="submit-btn-1 mt-20 btn-hover-1 f-right" ng-click="toggleAddAddress()">Add new Address</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div ng-show="addAddress">
                           <textarea class="custom-textarea" ng-model="addressData.address" placeholder="Your address here..."></textarea>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <button class="submit-btn-1 mt-20 btn-hover-1" ng-click="addNewAddress(addressData)">Save</button>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <button class="submit-btn-1 mt-20 btn-hover-1 f-right" ng-click="toggleAddAddress()">Cancel</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
				<div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion2" href="#personal_info">Payment Mode</a>
                                        </h4>
                                    </div>
                                    <div id="personal_info" class="panel-collapse collapse in" role="tabpanel">
                                        <div class="panel-body">
                                            <form>
                                                <div class="new-customers">
                                                    <div class="p-30">
                                                        <p><input type="radio" name=""  value="" checked> Cash on Delivery</p> 
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 ng-cloak">
                                        <p ng-show="checkoutError" style="color:red">Please select an address before proceeding to payment.</p>
                                    </div>
                                    <div class="col-md-6" style="text-align: right">
                                        <button class="submit-btn-1 mt-20 btn-hover-1" ng-click="proceedToPayment()">Proceed to Payment</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <div id="footer">
            <?php echo loadFooter();?>
        </div>
        </div>
    </div>
   <?php echo loadScripts() ?>
</body>
</html>
