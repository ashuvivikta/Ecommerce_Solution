<?php
    include('header.php');
    include('plugins.php');
    include('footer.php');
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <title>Shopping Mall || My Orders</title>
    <?php echo loadPlugins(); ?>
</head>
<body ng-app="anantya" ng-controller="ProductsController" ng-init="loadOrders()">
    <div class="wrapper">
        <div id="header">
            <?php echo loadHeader(); ?>
        </div>
        <div class="breadcrumbs-section plr-200 mb-80" style="background-color: #f5f5f5;color: white">
            <div class="">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="breadcrumbs-inner">
                                <div class="col-xs-8">
                                    <h1 class="breadcrumbs-title" style="color: black;text-align: left;padding-left: 10%">My Orders</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="page-content" class="page-wrapper">
            <div class="login-section mb-80">
                <div class="container">
                    <div class="row" ng-show="ordersFound">
                        <div class="col-md-12" ng-repeat="orderDetails in orders">
                            <div class="section-title text-left mb-40">
                            	<div class="panel panel-default">
								    <div class="panel-heading">
								    	Order Id : {{orderDetails.cartId}}
                                        <span style="float: right;">
                                            Date : {{orderDetails.orderDate.date.split(' ')[0]}}
                                        </span>
							    	</div>
								    <div class="panel-body">
								    	<div class="col-lg-12">
								    		<div class="col-lg-2" style="padding: 0px">
								    			<div ng-if="orderDetails.productDetails.length == 1" class="text-center">
								    				<img  src="{{orderDetails.productDetails[0].thumbnail}}" alt="{{orderDetails.productDetails[0].productName}}" style="height: 100px; width: inherit; max-width: 95%;">
							    				</div>
								    			<div ng-if="orderDetails.productDetails.length > 1" ng-repeat="details in orderDetails.productDetails" class="text-center">
								    				<img src="{{details.thumbnail}}" alt="{{details.productName}}" style="height: 100px; width: inherit; max-width: 95%;">
								    				<hr/>
							    				</div>
								    		</div>
								    		<div class="col-lg-10" style="text-align: left" ng-if="orderDetails.productDetails.length == 1">
								    			<h3>{{orderDetails.productDetails[0].productName}}</h3>
								    			<p>Color : {{orderDetails.productDetails[0].productColor}}</p>
								    			<p>Model : {{orderDetails.productDetails[0].productModel}}</p>
								    			<p>Quantity : {{orderDetails.productDetails[0].quantity}}</p>
								    		</div>
								    		<div class="col-lg-10" style="text-align: left" ng-if="orderDetails.productDetails.length > 1" >
								    			<div ng-repeat="details in orderDetails.productDetails">
									    			<h3>{{details.productName}}</h3>
									    			<p>Color : {{details.productColor}}</p>
									    			<p>Model : {{details.productModel}}</p>
									    			<p>Quantity : {{details.quantity}}</p>
									    			<hr/>
								    			</div>
								    		</div>
								    	</div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="col-md-6" style="padding-top: 10px;">
                                                    <textarea ng-show="showComplaintBox" class="custom-textarea" ng-model="complaintData.description" placeholder="Your comments here..."></textarea>
                                                </div>
                                                <div class="col-md-6" style="text-align: right;padding-top: 10px;">
                                                    <button ng-hide="showComplaintBox" class="submit-btn-1 btn-hover-1" type="button" ng-click="showComplaintBoxTextarea()">Complaint/feedback</button>
                                                    <button ng-show="showComplaintBox" class="submit-btn-1 btn-hover-1" type="button" ng-click="raiseComplaint(complaintData,orderDetails)">submit</button>
                                                    <button ng-show="showComplaintBox" class="submit-btn-1 btn-hover-1" type="button" ng-click="showComplaintBoxTextarea()">cancel</button>
                                                </div>
                                            </div>
                                        </div>
								    </div>
								</div>
                            </div>
                        </div>
                    </div>
                    <div class="row" ng-hide="ordersFound">
                    	<div class="col-lg-12 text-center">
                            <h1>There are no orders found!</h1>
                        </div>
                    </div>
                </div>
            </div>
        <div id="footer">
            <?php echo loadFooter();?>
        </div>
        </div>
        <div id="toaster"></div>
    </div>
   <?php echo loadScripts() ?>
</body>
</html>
