<?php
    include('header.php');
    include('plugins.php');
    include('footer.php');
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <title>Shopping Mall || Home</title>
    <?php echo loadPLugins();?>

    <link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100' rel='stylesheet' type='text/css'>

    <!-- styles -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/owl.theme.css" rel="stylesheet">

    <!-- theme stylesheet -->
    <link href="css/style.default.css" rel="stylesheet" id="theme-stylesheet">

    <!-- your stylesheet with modifications -->
    <link href="css/customobajustyle.css" rel="stylesheet">

    <script src="js/respond.min.js"></script>

    <link rel="shortcut icon" href="favicon.png">
</head>
<body ng-app="anantya" ng-controller="IndexController">
    <div class="wrapper ng-cloak">
        <?php echo loadHeader()?>



        <div class="slider-area  plr-185  mb-80">
            <div class="container-fluid">
                <div class="slider-content">
                    <div class="row ng-cloak">
                        <div class="active-slider-1 slick-arrow-1 slick-dots-1 ng-cloak">
                            <a href={{sliderImageDetails[0].productUrl}}>
                                <div class="col-md-12">
                                    <div class="layer-1">
                                        <div class="slider-img" ng-if="logoPresent">
                                            <img src="{{sliderImageDetails[0].sliderImagePath}}" class="slideImage" alt="">
                                        </div>
                                        <div class="slider-img" ng-if="logoPresent==false" class="slideImage">
                                            <img src="img/default-img/camera-icon.png" class="slideImage">
                                            <p class="imageHeading"> your image here </p>
                                        </div>
                                        <div class="slider-info gray-bg">
                                            <h1 ng-if="logoPresent==false" class="textDescription">your text goes here</h1>
                                            <div ng-if="logoPresent" class="slider-info-inner">
                                                <h1 class="slider-title-1 text-uppercase text-black-1">{{sliderImageDetails[0].sliderImageHeading}}</h1>
                                                <div class="slider-brief text-black-2">
                                                    <p>{{sliderImageDetails[0].sliderImageDescription}}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a href={{sliderImageDetails[1].productUrl}}>
                                <div class="col-md-12">
                                    <div class="layer-1">
                                        <div class="slider-img" ng-if="logoPresent">
                                            <img src="{{sliderImageDetails[1].sliderImagePath}}" class="slideImage" alt="">
                                        </div>
                                         <div class="slider-img" ng-if="logoPresent==false" class="slideImage">
                                        <img src="img/default-img/camera-icon.png" class="slideImage">
                                        <p class="imageHeading"> your image here </p>
                                        </div>
                                        <div class="slider-info gray-bg">
                                            <h1 ng-if="logoPresent==false" class="textDescription">your text goes here</h1>
                                            <div ng-if="logoPresent" class="slider-info-inner">
                                                <h1 class="slider-title-1 text-uppercase text-black-1">{{sliderImageDetails[1].sliderImageHeading}}</h1>
                                                <div class="slider-brief text-black-2">
                                                    <p>{{sliderImageDetails[1].sliderImageDescription}}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a href={{sliderImageDetails[2].productUrl}}>
                                <div class="col-md-12">
                                    <div class="layer-1">
                                        <div class="slider-img" ng-if="logoPresent">
                                            <img src="{{sliderImageDetails[2].sliderImagePath}}" class="slideImage" alt="">
                                        </div>
                                        <div class="slider-img" ng-if="logoPresent==false" class="slideImage">
                                        <img src="img/default-img/camera-icon.png" class="slideImage">
                                        <p class="imageHeading"> your image here </p>
                                        </div>
                                        <div class="slider-info gray-bg">
                                            <h1 ng-if="logoPresent" class="textDescription">your text goes here</h1>
                                            <div ng-if="logoPresent==false" class="slider-info-inner">
                                                <h1 class="slider-title-1 text-uppercase text-black-1">{{sliderImageDetails[2].sliderImageHeading}}</h1>
                                                <div class="slider-brief text-black-2">
                                                    <p>{{sliderImageDetails[2].sliderImageDescription}}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END SLIDER AREA -->

        <!-- Start page content -->
        <section id="page-content" class="page-wrapper">
            <!-- PRODUCT TAB SECTION START -->
            <div class="product-tab-section mb-50 ng-cloak">
                <div class="container">
                    <div class="row ng-cloak">
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="section-title text-left mb-40 ng-cloak">
                                <h2 class="uppercase ng-cloak">product list</h2>
                                <h6>There are many variations of passages of brands available,</h6>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="row">
                                <div class="col-lg-offset-9 col-lg-3">
                                      <div class="header-search f-left">
                                        <div class="header-search-inner">
                                           <button class="search-toggle">
                                            <i class="zmdi zmdi-search"></i>
                                           </button>
                                            <form action="#">
                                                <div class="top-search-box">
                                                    <input type="text" ng-model="search" minlength="2" ng-change="searchByAllProducts(search)" placeholder="Search here your product..."><!-- ---->
                                                    <button type="submit">
                                                        <i class="zmdi zmdi-search"></i>
                                                    </button>
                                                </div>
                                            </form> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                      <div class="container ng-cloak" ng-if="productsNotPresent" ng-hide="productsPresent">
                                        <div class="row">
                                            <div class="col-lg-12 text-center">
                                            <h3>There are no related contents found! please modify your search</h3>
                                            </div>
                                        </div>
                                    </div>
                    <div class="product-tab ng-cloak" ng-init="loadProducts()">
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <!-- popular-product start -->
                            <div class="tab-pane active" id="popular-product" ng-if="productsPresent">
                                <div class="row">
                                    <!-- product-item start -->
                                    <div class="col-md-3 col-sm-4 col-xs-12" ng-repeat="product in products">
                                        <div class="product-item">
                                            <div class="product-img text-center">
                                                <a href="singleProduct.php?productId={{product.productId}}">
                                                    <img src="{{product.productImages[0].thumbnail}}" alt="" class="productListImages"/>
                                                </a>
                                            </div>
                                            <div class="product-info">
                                                <h6 class="product-title">
                                                    <a href="singleProduct.php?productId={{product.productId}}">{{product.productName}}</a>
                                                </h6>
                                                <h3 class="pro-price"><i class="fa fa-rupee"></i>&nbsp;{{product.productPrice}}</h3>
                                                <ul class="action-button">
                                                    <li>
                                                        <a href="#" data-toggle="modal" data-target="#productModal" title="Quickview" ng-click="loadProductDetailInModal(product)"><i class="zmdi zmdi-zoom-in"></i></a>
                                                    </li>
                                                    <li>
                                                        <a title="Add to cart" ng-click="addProductToCart(product)"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- product-item end -->
                                </div>
                            </div>
                            <!-- popular-product end -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- PRODUCT TAB SECTION END -->
        </section>
        <div id="footer">
            <?php echo loadFooter();?>
        </div>
        <div id="quickview-wrapper">
            <div class="modal fade" id="productModal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content" style="margin-top: 20%">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                        <div class="modal-body">
                            <div class="modal-product clearfix">
                                <div class="product-images">
                                    <div class="text-center">
                                        <img alt="{{detailsOfProduct.productName}}" src="{{detailsOfProduct.productThumbnail}}">
                                    </div>
                                </div>
                                <div class="product-info">
                                    <h1>{{detailsOfProduct.productName}}</h1>
                                    <div class="price-box-3">
                                        <div class="s-price-box">
                                            <span class="new-price">
                                                <i class="fa fa-rupee"></i>&nbsp;{{detailsOfProduct.productPrice}}
                                            </span>
                                        </div>
                                    </div>
                                    <a href="singleProduct.php?productId={{detailsOfProduct.productId}}" class="see-all">See all features</a>
                                    <div class="quick-add-to-cart">
                                        <form method="post" class="cart">
                                            <button class="single_add_to_cart_button" ng-click="addProductToCart(detailsOfProduct)">Add to cart</button>
                                        </form>
                                    </div>
                                    <div class="quick-desc">
                                        {{detailsOfProduct.productDescription}}
                                    </div>
                                </div><!-- .product-info -->
                            </div><!-- .modal-product -->
                        </div><!-- .modal-body -->
                    </div><!-- .modal-content -->
                </div><!-- .modal-dialog -->
            </div>
        </div>
    </div>
    <?php echo loadScripts() ?>
</body>
</html>