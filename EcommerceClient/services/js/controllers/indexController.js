if(DEV_MODE == 1)
    BASE_URL = DEV_URL;
app
.filter('split', function() {
    return function(input, splitChar, splitIndex) {
        // do some bounds checking here to ensure it has that index
        return input.split(splitChar)[splitIndex];
    }
})
.controller('IndexController',function($scope,$http,LogService){
	//function to get the query parameter based on name
	$scope.getParameterByName = function(name, url) {
	    if (!url) {
	      url = window.location.href;
	    }
	    name = name.replace(/[\[\]]/g, "\\$&");
	    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
	        results = regex.exec(url);
	    if (!results) return null;
	    if (!results[2]) return '';
	    return decodeURIComponent(results[2].replace(/\+/g, " "));
	}
	if(localStorage.getItem("ecommerce_vendorId")){
		//do nothing
	}
	else
	{
		$scope.vendorId = $scope.getParameterByName('id');
		LogService.infoLog("vendorId is: "+$scope.vendorId);
		writeToLS('ecommerce_vendorId',$scope.vendorId);
	}

	$scope.vendorId = localStorage.getItem("ecommerce_vendorId");
	//function to get the logo and slider images
	$scope.loadVendorLogoAndImages = function(){
		$http({
			method : "GET",
			url : BASE_URL+GET_LOGO_URL,
			params : {
				vendorId : $scope.vendorId,
				filterType : 1
			}
		}).then(function success(response){
			LogService.infoLog(JSON.stringify(response));
			if(response.status == HTTP_STATUS_CODE){
				if(response.data.errorCode == GET_LOGO_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT && response.data.data != '' && response.data.data != null && typeof(response.data.data) !== undefined){
					$scope.vendorLogo = response.data.data[0].vendorLogo;
					$scope.facebookLink = response.data.data[0].facebookLink;
					$scope.twitterLink = response.data.data[0].twitterLink;
					$scope.googlePlusLink = response.data.data[0].googlePlusLink;
					localStorage.setItem("ecommerce_vendorLogo",$scope.vendorLogo);
					$scope.sliderImageDetails = response.data.data[0].sliderImageDetails;
					$scope.logoPresent = true;
				}
				else{
					$scope.logoPresent = false;
					LogService.infoLog("Unable to get the logo details..");

				}
			}
			else{
				LogService.errorLog("Encountered server error!");
			}
		},function failure(){
			LogService.errorLog("IndexController - loadVendorLogoAndImages - API not found!");
		});
	};
	if($scope.vendorId != null){
		$scope.loadVendorLogoAndImages();
	}
	
	//function to get the navbar header elements from server
	$scope.loadProductCategories = function(){
		$http({
			method : "GET",
			url : BASE_URL+GET_PRODUCT_CATEGORIES_URL,
			params : {
				vendorId : $scope.vendorId
			}
		}).then(function success(response){
			viviktaDebugConsole(JSON.stringify(response));
			if(response.status == HTTP_STATUS_CODE){
				if(response.data.errorCode == GET_PRODUCT_CATEGORIES_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT && response.data.data != '' && response.data.data != null && typeof(response.data.data) !== undefined){
					$scope.headerProductDetails = response.data.data;
				}
				else{
					viviktaDebugConsole("loadGenders elements not found!");
				}
			}
			else{
				viviktaDebugConsole("Encountered server error!");
			}
		},function failure(){
			viviktaDebugConsole("IndexController - loadGenders - API not found!");
		});
	};

	//function to load the data for header
	$scope.loadHeaders = function(){
		$scope.loginStatus = localStorage.getItem("loginStatus");
		if($scope.loginStatus == 1){
			$scope.isLoggedIn = true;
		}
		else{
			$scope.isLoggedIn = false;
		}
	};

	//function to check for cart items
    $scope.getCartItemsCount = function(){
        $scope.cartDetails = localStorage.getItem('anantya_cart');
        if($scope.cartDetails){
            $scope.cartDetails = JSON.parse(localStorage.getItem('anantya_cart'));
            $scope.cartCount = $scope.cartDetails.productDetails.length;
            document.getElementById('cart-count').innerHTML = $scope.cartCount;
            $scope.cartItemsPresent = true;
        }
    };
    $scope.getCartItemsCount();

    $scope.$watch(function (){ 
        return localStorage.getItem("anantya_cart"); 
    },function(newVal,oldVal){
        if(oldVal !== newVal && newVal === undefined){
            $scope.username = "";
        }
        else{
        	$scope.cartDetails = localStorage.getItem('anantya_cart');
        	if($scope.cartDetails){
	            $scope.cartDetails = JSON.parse(localStorage.getItem('anantya_cart'));
	            $scope.cartCount = $scope.cartDetails.productDetails.length;
	            document.getElementById('cart-count').innerHTML = $scope.cartCount;
	            $scope.cartItemsPresent = true;
	        }
	        else{
	        	document.getElementById('cart-count').innerHTML = 0;
	        }
        }
    })

	$scope.loadProducts = function(){
		$http({
			method : "GET",
			url : BASE_URL+LIST_PRODUCTS_URL,
			params : {
				vendorId : $scope.vendorId,
				filterType : 2
			}
		}).then(function success(response){
			viviktaDebugConsole(JSON.stringify(response));
			if(response.status == HTTP_STATUS_CODE){
				if(response.data.errorCode == LIST_PRODUCTS_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT && response.data.data != ''){
					$scope.products = response.data.data;
                    $scope.productsPresent = true;
				}
				else{
					viviktaDebugConsole("No products found!");
				}
			}
			else{
				viviktaDebugConsole("Encountered server error");
			}
		},function failure(){
			viviktaDebugConsole("IndexController - loadProducts - API not found!");
		});
	};

	//function to load the productDetails in modal quickview
	$scope.loadProductDetailInModal = function(productObject){
		$scope.detailsOfProduct = {
			productName : productObject.productName,
			productDescription : productObject.productDescription,
			productPrice : productObject.productPrice,
			productId : productObject.productId,
			productThumbnail : productObject.productImages[0].thumbnail
		};
	};

	//function to add the product to cart
    $scope.addProductToCart = function(productObject){
        viviktaDebugConsole("addProductToCart-->");
        $scope.cartDetails = JSON.parse(localStorage.getItem("anantya_cart"));
        $scope.cartObject = {
            productDetails : []
        };
        if($scope.cartDetails){
            $scope.cartDetails = JSON.parse(localStorage.getItem("anantya_cart"));
            for(var i=0;i<$scope.cartDetails.productDetails.length;i++){
                if(productObject.productId == $scope.cartDetails.productDetails[i].productId)
                    $scope.cartItemFound = true;
                else
                    $scope.cartItemFound = false;
            }
            if(!$scope.cartItemFound){
                $scope.cartDetails.productDetails.push({
                    productId : productObject.productId,
                    quantity : 1
                });
                localStorage.setItem("anantya_cart",JSON.stringify($scope.cartDetails));
            }
        }
        else{
            //if the cart contains nothing
            $scope.cartObject.productDetails.push({
                productId : productObject.productId,
                quantity : 1
            });
            localStorage.setItem("anantya_cart",JSON.stringify($scope.cartObject));
        }
    };
    
    //function to search by whole database
       $scope.searchByAllProducts = function(keyword){
        if(keyword == '' || keyword == undefined || typeof keyword == undefined){ 
             LogService.infoLog("HI");
            $scope.loadProducts(0,''); 
        }
        else{
            $scope.ajaxPromise = $http({
                method : "GET",
                url : BASE_URL+GET_SEARCH_FOR_PRODUCT_URL,
                params : {
                    vendorId : $scope.vendorId,
                    search : keyword
                }
            }).then(function success(response){
                LogService.infoLog(JSON.stringify(response));
                 
                  //$scope.products = response.data.data.data;
                if(response.status == HTTP_STATUS_CODE){
                    if(response.data.errorCode == GET_SUGGESTION_FOR_SEARCH_PRODUCT_SUCCESS_CODE ){
                       
                        $scope.products = response.data.data;
                        $scope.productsPresent = true; 
                    }
                    else if(response.data.errorCode == GET_SUGGESTION_FOR_SEARCH_PRODUCT_FAILURE_CODE && response.data.statusText == SUCCESS_STATUS_TEXT && response.data.data !='' && response.data.data != null && typeof(response.data.data) != undefined && response.data.data.length!=0){
                        $scope.productsPresent = false;
                        $scope.productsNotPresent = true;
                    }
                    else{   
                         $scope.productsPresent = false;
                        $scope.productsNotPresent = true;
                    }
                }
                else{
                    $scope.productsPresent = false; 
                }
           
            },function failure(){
                $scope.productsPresent = false;
                $scope.productsNotPresent = true;
            });
        }       
    };
    
})



.controller('FooterController',function($scope,$http){
	$scope.vendorLogo = localStorage.getItem("ecommerce_vendorLogo");
    $scope.vendorId = localStorage.getItem("ecommerce_vendorId");

	$scope.raiseQuery = function(queryData){
		viviktaDebugConsole(JSON.stringify(queryData));
		if(queryData.name == '' || queryData.name == null){
			$scope.showToaster("Please enter the name!");
		}
		else if(queryData.emailId == '' || queryData.emailId == null){
			$scope.showToaster("Please enter the email id!");
		}
		else if(queryData.message == '' || queryData.message == null){
			$scope.showToaster("Please enter the message!");
		}
		else{
			$http({
				method : "POST",
				url : BASE_URL+RAISE_QUERY_URL,
				params : {
					endUserName : queryData.name,
					endUserEmailId : queryData.emailId,
					suggestion : queryData.message,
					vendorId : $scope.vendorId
				}
			}).then(function success(response){
				viviktaDebugConsole(JSON.stringify(response));
				if(response.status == HTTP_STATUS_CODE){
					if(response.data.errorCode == RAISE_QUERY_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT){
						$scope.showToaster("Admin contacted successfully!");
						$scope.messageStatus = true;
					}
					else{
						$scope.showToaster("Could not get in touch with admin. Try again later!");
					}
				}
				else{
					$scope.showToaster("Encountered server error!");
				}
			},function failure(){
				viviktaDebugConsole("FooterController - raiseQuery - API not found!");
				$scope.showToaster("Could not contact the server!");
			});
		}
	};

	//function to show the toaster
	$scope.showToaster = function(message){
		$scope.toaster = document.getElementById("toaster");
		$scope.toaster.innerHTML = message;
        $scope.toaster.className = "show";
		setTimeout(function(){
			$scope.toaster.className = $scope.toaster.className.replace("show", "");
		}, 3000);
	};
})


