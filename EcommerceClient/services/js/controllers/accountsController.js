if(DEV_MODE == 1)
    BASE_URL = DEV_URL;
app
.controller('AccountsController',function($scope,$http){
	$scope.authToken = localStorage.getItem("anantya_token");
	$scope.vendorLogo = localStorage.getItem("ecommerce_vendorLogo");
    $scope.vendorId = localStorage.getItem("ecommerce_vendorId");
    
	//function to load the account details
	$scope.loadAccountDetails = function(){
		$scope.loadUserAddresses();
		$http({
            method : "GET",
            url : BASE_URL+GET_PROFILE_DETAILS_URL,
            params : {
                token : $scope.authToken
            }
        }).then(function success(response){
            viviktaDebugConsole(JSON.stringify(response));
            if(response.status == HTTP_STATUS_CODE){
                if(response.data.errorCode == GET_PROFILE_DETAILS__SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT && response.data.data != '' && response.data.data != null && typeof(response.data.data) !== undefined){
                    $scope.profileDetails = response.data.data;
                    $scope.detailsFound = true;
                }
                else{
                    $scope.detailsFound = false;
                }
            }
            else{
                viviktaDebugConsole("Encountered server error!");
                $scope.detailsFound = false;
            }
        },function failure(){
            viviktaDebugConsole("AccountsController - loadAccountDetails - API not found!");
            $scope.detailsFound = false;
        });
	};

	//function to update the personal information
	$scope.updatePersonalInfo = function(profileDetails){
		$http({
			method : "PUT",
			url : BASE_URL+UPDATE_PROFILE_URL,
			params : {
				token : $scope.authToken,
				userName : profileDetails.userName,
				userPhoneNumber : profileDetails.userPhoneNumber
			}
		}).then(function success(response){
			viviktaDebugConsole(JSON.stringify(response));
			if(response.status == HTTP_STATUS_CODE){
				if(response.data.errorCode == UPDATE_PROFILE_SUCCESS_CODE && response.data.statusText){
					$scope.showToaster("Account details updated successfully!");
				}
				else{
					$scope.showToaster("Account details could not be updated!");
				}
			}
			else{
				viviktaDebugConsole("Encounetered server error!");
				$scope.showToaster("Account details could not be updated!");
			}
		},function failure(){
			viviktaDebugConsole("AccountsController - updatePersonalInfo - API not found!");
			$scope.showToaster("Account details could not be updated!");
		});
	};

	//function to change the password
	$scope.changePassword = function(passwordData){
		if(passwordData.password !== passwordData.confirmPassword){
			$scope.showToaster("Password and cofirm password must be same!");
		}
		else{
			$http({
	            method : "POST",
	            url : BASE_URL+CHANGE_PASSWORD_URL,
	            params : {
	                token : $scope.authToken,
	                password : passwordData.password,
	                confirmPassword : passwordData.confirmPassword
	            }
	        }).then(function success(response){
	            viviktaDebugConsole(JSON.stringify(response));
	            if(response.status == HTTP_STATUS_CODE){
	                if(response.data.errorCode == CHANGE_PASSWORD_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT){
	                    viviktaDebugConsole("Password updated");
	          			$scope.showToaster("Password updated!");
	                }
	                else{
	                    $scope.showToaster("Password could not be updated!");
	                }
	            }
	            else{
	                viviktaDebugConsole("Encountered server error!");
	                $scope.showToaster("Password could not be updated!");
	            }
	        },function failure(){
	            viviktaDebugConsole("AccountsController - changePassword - API not found");
	            $scope.showToaster("Password could not be updated!");
	        });
		}
	};

	//function to load the user addresses
	$scope.loadUserAddresses = function(){
		$http({
            method : "GET",
            url : BASE_URL+GET_CHECKOUT_DETAILS_URL,
            params : {
                token : $scope.authToken
            }
        }).then(function success(response){
            viviktaDebugConsole(JSON.stringify(response));
            if(response.status == HTTP_STATUS_CODE){
                if(response.data.errorCode == GET_CHECKOUT_DETAILS_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT && response.data.data != '' && response.data.data != null && typeof(response.data.data) !== undefined){
                    $scope.addresses = response.data.data;
                    $scope.addressFound = true;
                }
                else{
                    $scope.addressFound = false;
                }
            }
            else{
                viviktaDebugConsole("Encountered server error!");
                $scope.addressFound = false;
            }
        },function failure(){
            viviktaDebugConsole("AccountsController - loadUserAddresses - API not found!");
            $scope.addressFound = false;
        });
	};

	//function to add the new address
	$scope.addNewAddress = function(addressData){
		$http({
            method : "POST",
            url : BASE_URL+ADD_ADDRESS_URL,
            params : {
                token : $scope.authToken,
                userAddress : addressData.address
            }
        }).then(function success(response){
            viviktaDebugConsole(JSON.stringify(response));
            if(response.status == HTTP_STATUS_CODE){
                if(response.data.errorCode == ADD_ADDRESS_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT){
                    $scope.addressAddStatus = true;
                    $scope.addAddress = false;
                    viviktaDebugConsole("Address added successfully!");
          			$scope.showToaster('Address added successfully!');
                    $scope.loadUserAddresses();
                }
                else{
                    $scope.addressAddStatus = false;
                    $scope.showToaster("Address could not be added!");
                }
            }
            else{
                viviktaDebugConsole("Encountered server error!");
                $scope.addressAddStatus = false;
                $scope.showToaster("Address could not be added!");
            }
        },function failure(){
            viviktaDebugConsole("AccountsController - addNewAddress - API not found");
            $scope.addressAddStatus = false;
            $scope.showToaster("Address could not be added!");
        });
	};

	//function to remove the existing address
	$scope.removeAddress = function(addressId){
		$scope.toaster = document.getElementById("toaster");
		$http({
			method : "DELETE",
			url : BASE_URL+REMOVE_ADDRESS_URL,
			params : {
				token : $scope.authToken,
				addressId : addressId
			}
		}).then(function success(response){
			viviktaDebugConsole(JSON.stringify(response));
			if(response.status == HTTP_STATUS_CODE){
				if(response.data.errorCode == REMOVE_ADDRESS_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT){
					$scope.showToaster("Address removed successfully!");
					viviktaDebugConsole("Address removed successfully!");
					$scope.loadUserAddresses();
				}
				else{
					$scope.showToaster("Address could not be removed!");
				}
			}
			else{
				viviktaDebugConsole("Encountered server error!");
				$scope.showToaster("Address could not be removed!");
			}
		},function failure(){
			viviktaDebugConsole("AccountsController - removeAddress - API not found!");
			$scope.showToaster("Address could not be removed!");
		});
	};

	//function to show the toaster
	$scope.showToaster = function(message){
		$scope.toaster = document.getElementById("toaster");
		$scope.toaster.innerHTML = message;
        $scope.toaster.className = "show";
		setTimeout(function(){
			$scope.toaster.className = $scope.toaster.className.replace("show", "");
		}, 3000);
	};
})