if(DEV_MODE == 1)
    BASE_URL = DEV_URL;
app
.controller('RegistrationLoginController',function($scope,$http,LogService){
	$scope.vendorLogo = localStorage.getItem("ecommerce_vendorLogo");
	$scope.vendorId = localStorage.getItem("ecommerce_vendorId");
	$scope.vendorId = localStorage.getItem("ecommerce_vendorId");
	//function to get the logo and slider images
	$scope.loadVendorLogoAndImages = function(){
		$http({
			method : "GET",
			url : BASE_URL+GET_LOGO_URL,
			params : {
				vendorId : $scope.vendorId,
				filterType : 1
			}
		}).then(function success(response){
			LogService.infoLog(JSON.stringify(response));
			if(response.status == HTTP_STATUS_CODE){
				if(response.data.errorCode == GET_LOGO_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT && response.data.data != '' && response.data.data != null && typeof(response.data.data) !== undefined){
					$scope.vendorLogo = response.data.data[0].vendorLogo;
					localStorage.setItem("ecommerce_vendorLogo",$scope.vendorLogo);
					$scope.sliderImageDetails = response.data.data[0].sliderImageDetails;
				}
				else{
					LogService.infoLog("Unable to get the logo details..");
				}
			}
			else{
				LogService.errorLog("Encountered server error!");
			}
		},function failure(){
			LogService.errorLog("IndexController - loadVendorLogoAndImages - API not found!");
		});
	};
	if($scope.vendorId != null){
		$scope.loadVendorLogoAndImages();
	}
	
	//function to handle the login
	$scope.doLogin = function(loginData){
		$http({
			method : "POST",
			url : BASE_URL+LOGIN_URL,
			params : {
				emailId : loginData.username,
				password : loginData.password,
				typeofuser : 2
			}
		}).then(function success(response){
			viviktaDebugConsole(JSON.stringify(response));
			if(response.status == HTTP_STATUS_CODE){
				if(response.data.errorCode == LOGIN_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT){
					viviktaDebugConsole("Login done successfully!");
					localStorage.setItem("anantya_token",response.data.data.token.token);
                    localStorage.setItem("anantya_name",response.data.data.name);
                    localStorage.setItem('anantya_profilePicturePath',response.data.data.profilePicturePath);
                    localStorage.setItem("loginStatus",1);
                    $scope.updateCartDetailsToServer();
					$scope.loginFailure = false;
				}
				else{
					viviktaDebugConsole("User not exists!");
					$scope.loginFailure = true;
				}
			}
			else{
				viviktaDebugConsole("Unable to login");
				$scope.loginFailure = true;
			}
		},function failure(){
			viviktaDebugConsole("RegistrationLoginController - doLogin - API not found!");
			$scope.loginFailure = true;
		});
	};

	//function to update the cart details to server
	$scope.vendorId = localStorage.getItem("ecommerce_vendorId");
	$scope.updateCartDetailsToServer = function(){
		viviktaDebugConsole("updateCartDetailsToServer after login success..");
		$scope.cartDetails = localStorage.getItem("anantya_cart");
		if($scope.cartDetails){
			$scope.cartDetails = JSON.parse($scope.cartDetails);
	        $scope.cartItems = $scope.cartDetails;
	        $scope.authToken = localStorage.getItem("anantya_token");
	        $http({
	            method : "POST",
	            url : BASE_URL+UPDATE_CART_DETAILS_TO_SERVER_URL,
	            params : {
	            	token : $scope.authToken,
	                cartDetails : $scope.cartItems,
	                vendorId : $scope.vendorId
	            }
	        }).then(function success(response){
	            viviktaDebugConsole(JSON.stringify(response));
	            if(response.status == HTTP_STATUS_CODE){
	                if(response.data.errorCode == UPDATE_CART_DETAILS_TO_SERVER_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT){
	                    viviktaDebugConsole("cart details updated!");
	                    LogService.infoLog("cardId is: "+response.data.data);
                        localStorage.setItem("ecommerce_cartId",response.data.data);
                        window.location.href = "cart.php";
	                }
	            }
	            else{
	                viviktaDebugConsole("Encountered server error! - updateCartDetailsToServer");
	            }
	        },function failure(){
	            viviktaDebugConsole("RegistrationLoginController - updateCartDetailsToServer - API not found!");
	        });
	    }
	    else{
	    	window.location.href = "index.php";
	    }
	};

	//function to handle the registration feature
	$scope.register = function(registrationData){
		viviktaDebugConsole("Registration Data: "+JSON.stringify(registrationData));
		$http({
			method : "POST",
			url : BASE_URL+REGISTRATION_URL,
			params : {
				userName : registrationData.name,
				userPhoneNumber : registrationData.phoneNumber,
				userEmailId : registrationData.emailId,
				userAddress : registrationData.address
			}
		}).then(function success(response){
			viviktaDebugConsole(JSON.stringify(response));
			if(response.status == HTTP_STATUS_CODE){
				if(response.data.errorCode == REGISTRATION_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT){
					viviktaDebugConsole("Registration done successfully!");
					$scope.registrationSuccess = true;
					$scope.registrationFailure = false;
				}
				else{
					viviktaDebugConsole("User already exists");
					$scope.registrationSuccess = false;
					$scope.registrationFailure = true;
				}
			}
			else{
				viviktaDebugConsole("Unable to register");
				$scope.registrationSuccess = false;
				$scope.registrationFailure = true;
			}
		},function failure(){
			viviktaDebugConsole("RegistrationLoginController - register - API not found!");
			$scope.registrationSuccess = false;
			$scope.registrationFailure = true;
		});
	};

	//function to get the query parameter based on name
	$scope.getParameterByName = function(name, url) {
	    if (!url) {
	      url = window.location.href;
	    }
	    name = name.replace(/[\[\]]/g, "\\$&");
	    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
	        results = regex.exec(url);
	    if (!results) return null;
	    if (!results[2]) return '';
	    return decodeURIComponent(results[2].replace(/\+/g, " "));
	}

	//function to create the password
	$scope.createPassword = function(passwordData){
		$scope.id = $scope.getParameterByName('id');
		if(passwordData.password == passwordData.confirmPassword){
        	$http({
        		method : "POST",
        		url : BASE_URL+CREATE_PASSWORD_URL,
        		params : {
        			id : $scope.id,
        			password : passwordData.password,
        			confirmPassword : passwordData.confirmPassword
        		}
        	}).then(function success(response){
        		viviktaDebugConsole(JSON.stringify(response));
        		if(response.status == HTTP_STATUS_CODE){
        			if(response.data.errorCode == CREATE_PASSWORD_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT){
        				viviktaDebugConsole("Password created successfully!");
        				$scope.createPasswordError = false;
        				window.location.href = "login.php";
        			}
        			else{
        				$scope.createPasswordError = true;
        			}
        		}
        		else{
        			$scope.createPasswordError = true;
        			viviktaDebugConsole("Encountered server error!");
        		}
        	},function failure(){
        		viviktaDebugConsole('PasswordController - createPassword - API not found!');
        		$scope.createPasswordError = true;
        	});
	    }
	    else{
	    	$scope.passwordMismatchError = true;
	    }
	};
})
.controller('LogoutController',function($scope,$http){
	localStorage.removeItem("anantya_token");
	localStorage.removeItem("loginStatus");
	window.location.href = "index.php";
})