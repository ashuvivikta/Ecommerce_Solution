app
.factory('LogService', [ '$http','$log', function($http,$log){
	var logs = {};
    logs.debugLog = function(message){
    	$log.debug(message);
    };
    logs.warningLog = function(message){
    	$log.warn(message);
    };
    logs.errorLog = function(message){
    	$log.error(message);
    };
    logs.infoLog = function(message){
    	$log.info(message);
    };
    logs.log = function(message){
    	$log.log(message);
    };
    return logs;
}])