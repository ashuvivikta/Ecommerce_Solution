if(DEV_MODE == 1)
    BASE_URL = DEV_URL;
app
.filter('split', function() {
    return function(input, splitChar, splitIndex) {
        // do some bounds checking here to ensure it has that index
        return input.split(splitChar)[splitIndex];
    }
})
.controller('ProductsController',function($scope,$http,LogService){
    $scope.vendorLogo = localStorage.getItem("ecommerce_vendorLogo");
    $scope.vendorId = localStorage.getItem("ecommerce_vendorId");
    
    $scope.$watch(function (){ 
        return localStorage.getItem("anantya_cart"); 
    },function(newVal,oldVal){
        if(oldVal !== newVal){
            $scope.updateCartDetailsToServer();
        }
    })

    $scope.vendorId = localStorage.getItem("ecommerce_vendorId");
    //function to get the logo and slider images
    $scope.loadVendorLogoAndImages = function(){
        $http({
            method : "GET",
            url : BASE_URL+GET_LOGO_URL,
            params : {
                vendorId : $scope.vendorId,
                filterType : 1
            }
        }).then(function success(response){
            LogService.infoLog(JSON.stringify(response));
            if(response.status == HTTP_STATUS_CODE){
                if(response.data.errorCode == GET_LOGO_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT && response.data.data != '' && response.data.data != null && typeof(response.data.data) !== undefined){
                    $scope.vendorLogo = response.data.data[0].vendorLogo;
                    $scope.facebookLink = response.data.data[0].facebookLink;
                    $scope.twitterLink = response.data.data[0].twitterLink;
                    $scope.googlePlusLink = response.data.data[0].googlePlusLink;
                    localStorage.setItem("ecommerce_vendorLogo",$scope.vendorLogo);
                    $scope.sliderImageDetails = response.data.data[0].sliderImageDetails;
                    $scope.logoPresent = true;
                }
                else{
                    $scope.logoPresent = false;
                    LogService.infoLog("Unable to get the logo details..");

                }
            }
            else{
                LogService.errorLog("Encountered server error!");
            }
        },function failure(){
            LogService.errorLog("IndexController - loadVendorLogoAndImages - API not found!");
        });
    };
    if($scope.vendorId != null){
        $scope.loadVendorLogoAndImages();
    }

    //function to update the cart details to server
    $scope.updateCartDetailsToServer = function(){
        viviktaDebugConsole("updateCartDetailsToServer-->");
        $scope.loginStatus = localStorage.getItem('loginStatus');
        if($scope.loginStatus == 1){
            $scope.cartDetails = localStorage.getItem("anantya_cart");
            if($scope.cartDetails){
                $scope.cartDetails = JSON.parse($scope.cartDetails);
                $scope.cartItems = $scope.cartDetails;
                $scope.authToken = localStorage.getItem("anantya_token");
                $http({
                    method : "POST",
                    url : BASE_URL+UPDATE_CART_DETAILS_TO_SERVER_URL,
                    params : {
                        token : $scope.authToken,
                        cartDetails : $scope.cartItems,
                        vendorId : $scope.vendorId
                    }
                }).then(function success(response){
                    viviktaDebugConsole(JSON.stringify(response));
                    if(response.status == HTTP_STATUS_CODE){
                        if(response.data.errorCode == UPDATE_CART_DETAILS_TO_SERVER_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT){
                            viviktaDebugConsole("cart details updated!");
                            LogService.infoLog("cardId is: "+response.data.data);
                            localStorage.setItem("ecommerce_cartId",response.data.data);
                        }
                    }
                    else{
                        viviktaDebugConsole("Encountered server error! - updateCartDetailsToServer");
                    }
                },function failure(){
                    viviktaDebugConsole("RegistrationLoginController - updateCartDetailsToServer - API not found!");
                });
            }
        }
    };

	//function to change the thumbnail image
    $scope.changeThumbnail = function(imageSource){
    	document.getElementById("zoom_03").setAttribute('src',imageSource);
    };
    
    //function to get the query parameter based on name
	$scope.getParameterByName = function(name, url) {
	    if (!url) {
	      url = window.location.href;
	    }
	    name = name.replace(/[\[\]]/g, "\\$&");
	    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
	        results = regex.exec(url);
	    if (!results) return null;
	    if (!results[2]) return '';
	    return decodeURIComponent(results[2].replace(/\+/g, " "));
	}

    //function to get the product details based on productId
	$scope.fetchProductDetails = function(){
		viviktaDebugConsole("loadProductDetailsBasedOnId-->");
		$scope.productId = $scope.getParameterByName('productId');
        $http({
            method : "GET",
            url : BASE_URL+PRODUCT_DETAILS_LIST_URL,
            params : {
                productId : $scope.productId,
                vendorId : $scope.vendorId
            }
        }).then(function success(response){
            viviktaDebugConsole(JSON.stringify(response));
            if(!response){
                viviktaDebugConsole("Encountered server error!");
                $scope.productDetailsPresent = true;
            }
            else{
                if(response.status == HTTP_STATUS_CODE){
                    if(response.data == "" || response.data == null){
                        $scope.productDetailsPresent = false;
                    }
                    else{
                        if(response.data.errorCode == PRODUCT_DETAILS_LIST_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT && response.data.data != ''){
                            $scope.productDetails = response.data.data;
                            $scope.productDetailsPresent =true;

                        }
                        else{
                            $scope.productDetailsPresent = false;
                        }
                    }
                }
                else{
                    $scope.productDetailsPresent = false;
                }
            }
        },function failure(){
            viviktaDebugConsole("IndexController - listProductDetails - API not found!");
            $scope.productDetailsPresent = false;
        });
	};
    
     //function to get the list of all products based on genderId
    $scope.loadProductsBasedOnProductCategory = function(paginateFlag,url){
     if(paginateFlag == 0)
     {
        $scope.url = BASE_URL+LIST_PRODUCTS_BY_CATEGORY_URL;
     }
     else
     {
        $scope.url = url;
     }
        $scope.productSubCategory = $scope.getParameterByName('category');
        viviktaDebugConsole("loadProductsBasedOnProductCategory-->");
        $http({
            method : "GET",
            url : $scope.url,
            params : {
                subCategoryId : $scope.productSubCategory,
                vendorId : $scope.vendorId,
                
            }
        }).then(function success(response){
            viviktaDebugConsole(JSON.stringify(response));
            if(response.status == HTTP_STATUS_CODE){
                if(response.data.errorCode == LIST_PRODUCTS_BY_CATEGORY_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT ){

                $scope.lastPage = response.data.data.lastPage;
                $scope.showPagination = true;
                if($scope.lastPage == "NULL")
                {
                    $scope.showPagination = false;
                    $scope.products = response.data.data.data;
                    $scope.productsPresent = true;
                    
                }
                else
                {
                    $scope.products = response.data.data.data;
                    $scope.productsPresent = true; 
                }

                $scope.previousPageUrl = response.data.data.previousPageUrl;
                $scope.nextPageUrl = response.data.data.nextPageUrl;
                $scope.currentPage = response.data.data.currentPage;
                    $scope.productsPresent = true; 
                }
                else{
                    viviktaDebugConsole("There are no products found!");
                }
            }
            else{
                viviktaDebugConsole("encouneterd server error!");
            }
        },function failure(){
            viviktaDebugConsole("ProductsController - loadProductsBasedOnProductCategory - API not found!");
        });
    };
    
    //function to load the productDetails in modal quickview
    $scope.loadProductDetailInModal = function(productObject){
        $scope.detailsOfProduct = {
            productName : productObject.productName,
            productDescription : productObject.productDescription,
            productPrice : productObject.productPrice,
            productId : productObject.productId,
            productThumbnail : productObject.productImages[0].thumbnail
        };
    };
    
    //function to add the product to cart
    $scope.addProductToCart = function(productObject){
        viviktaDebugConsole("addProductToCart-->");
        $scope.cartDetails = JSON.parse(localStorage.getItem("anantya_cart"));
        $scope.cartObject = {
            productDetails : []
        };
        if($scope.cartDetails){
            $scope.cartDetails = JSON.parse(localStorage.getItem("anantya_cart"));
            for(var i=0;i<$scope.cartDetails.productDetails.length;i++){
                if(productObject.productId == $scope.cartDetails.productDetails[i].productId)
                    $scope.cartItemFound = true;
                else
                    $scope.cartItemFound = false;
            }
            if(!$scope.cartItemFound){
                $scope.cartDetails.productDetails.push({
                    productId : productObject.productId,
                    quantity : 1
                });
                localStorage.setItem("anantya_cart",JSON.stringify($scope.cartDetails));
            }
        }
        else{
            //if the cart contains nothing
            $scope.cartObject.productDetails.push({
                productId : productObject.productId,
                quantity : 1
            });
            localStorage.setItem("anantya_cart",JSON.stringify($scope.cartObject));
        }
    };

    //function to remove the product from cart
    $scope.removeProductFromCart = function(productId){
        viviktaDebugConsole("removeProductFromCart for productId-->"+productId);
        $scope.cartDetails = JSON.parse(localStorage.getItem('anantya_cart'));
        for(var i=0;i<$scope.cartDetails.productDetails.length;i++){
            if($scope.cartDetails.productDetails[i].productId == productId){
                $scope.indexToBeRemoved = i;
                $scope.cartDetails.productDetails.splice($scope.indexToBeRemoved,1);
            }
        }
        localStorage.setItem("anantya_cart",JSON.stringify($scope.cartDetails));
        $scope.loadCartItems();
    };

    //function to change the quantity of the product
    $scope.changeQuantity = function(productId,quantity){
        if(quantity > 0){
            $scope.cartDetails = localStorage.getItem("anantya_cart");
            $scope.cartDetails = JSON.parse($scope.cartDetails);
            for(var i=0;i<$scope.cartDetails.productDetails.length;i++){
                if($scope.cartDetails.productDetails[i].productId == productId){
                    viviktaDebugConsole("item found in cart");
                    $scope.indexToBeRemoved = i;
                    $scope.cartDetails.productDetails.splice($scope.indexToBeRemoved,1);
                }
            }
            $scope.cartDetails.productDetails.push({
                productId : productId,
                quantity : quantity
            });
            localStorage.setItem("anantya_cart",JSON.stringify($scope.cartDetails));
            viviktaDebugConsole("changeQuantity<--");
            $scope.loadCartItems();
        }
    };

    //function to load the details of shopping cart
    $scope.loadCartItems = function(){
        $scope.cartDetails = localStorage.getItem("anantya_cart");
        if($scope.cartDetails){
            $scope.itemsFound = true;
            $scope.cartDetails = JSON.parse($scope.cartDetails);
            $scope.cartItems = $scope.cartDetails;
            viviktaDebugConsole("cartItems: "+JSON.stringify($scope.cartItems));
            $http({
                method : "GET",
                url : BASE_URL+GET_CART_DETAILS_URL,
                params : {
                    cartItems : $scope.cartItems,
                    vendorId : $scope.vendorId
                }
            }).then(function success(response){
                viviktaDebugConsole(JSON.stringify(response));
                if(response.status == HTTP_STATUS_CODE){
                    if(response.data.errorCode == GET_CART_DETAILS_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT && response.data.data != '' && response.data.data != null && typeof(response.data.data) !== undefined){
                        $scope.cartDetailsFound = true;
                        $scope.cartItemDetails = response.data.data.cartItemDetails;
                        $scope.totalPrice = response.data.data.totalAmount;
                    }
                    else{
                        $scope.cartDetailsFound = false;
                    }
                }
                else{
                    viviktaDebugConsole("Encountered server error!");
                    $scope.cartDetailsFound = false;
                }
            },function failure(){
                viviktaDebugConsole("ProductsController - loadCartItems - API not found!");
                $scope.cartDetailsFound = false;
            });
        }
        else{
            $scope.itemsFound = false;
        }
    };

    //function to place the order
    $scope.placeOrder = function(){
        $scope.loginStatus = localStorage.getItem("loginStatus");
        if($scope.loginStatus == 1){
            //go to checkout page
            $scope.updateCartDetailsToServer();
            window.location.href = "checkout.php";
        }
        else{
            //go to login page
            window.location.href = "login.php";
        }
    };

    //function to load the checkout details of user
    $scope.loadCheckoutDetails = function(){
        viviktaDebugConsole("loadCheckoutDetails-->");
        $scope.authToken = localStorage.getItem("anantya_token");
        $http({
            method : "GET",
            url : BASE_URL+GET_CHECKOUT_DETAILS_URL,
            params : {
                token : $scope.authToken,
                vendorId : $scope.vendorId
            }
        }).then(function success(response){
            viviktaDebugConsole(JSON.stringify(response));
            if(response.status == HTTP_STATUS_CODE){
                if(response.data.errorCode == GET_CHECKOUT_DETAILS_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT && response.data.data != '' && response.data.data != null && typeof(response.data.data) !== undefined){
                    $scope.checkoutDetails = response.data.data;
                    $scope.addressFound = true;
                }
                else{
                    $scope.addressFound = false;
                }
            }
            else{
                viviktaDebugConsole("Encountered server error!");
                $scope.addressFound = false;
            }
        },function failure(){
            viviktaDebugConsole("ProductsController - loadCheckoutDetails - API not found!");
            $scope.addressFound = false;
        });
    };

    //function to toggle the add new address
    $scope.addAddress = false;
    $scope.toggleAddAddress = function(){
        if($scope.addAddress == false){
            $scope.addAddress = true;
        }
        else{
            $scope.addAddress = false;
        }
    };

    //function to add new address
    $scope.addNewAddress = function(addressData){
        $scope.authToken = localStorage.getItem("anantya_token");
        $http({
            method : "POST",
            url : BASE_URL+ADD_ADDRESS_URL,
            params : {
                token : $scope.authToken,
                userAddress : addressData.address
            }
        }).then(function success(response){
            viviktaDebugConsole(JSON.stringify(response));
            if(response.status == HTTP_STATUS_CODE){
                if(response.data.errorCode == ADD_ADDRESS_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT){
                    $scope.addressAddStatus = true;
                    $scope.addAddress = false;
                    viviktaDebugConsole("address added successfully!");
                    $scope.loadCheckoutDetails();
                }
                else{
                    $scope.addressAddStatus = false;
                }
            }
            else{
                viviktaDebugConsole("Encountered server error!");
                $scope.addressAddStatus = false;
            }
        },function failure(){
            viviktaDebugConsole("ProductsController - addNewAddress - API not found");
            $scope.addressAddStatus = false;
        });
    };

    //function to proceed to payment
    $scope.proceedToPayment = function(){
        $scope.authToken = localStorage.getItem('anantya_token');
        $scope.shippingAddress = $('input[name=shippingAddress]:checked').val();
        if($scope.shippingAddress == "" || $scope.shippingAddress == null || typeof($scope.shippingAddress) === undefined){
            $scope.checkoutError = true;
        }
        else{
            viviktaDebugConsole("proceeding to payment..");
            overlay = $('<div class="blockDiv"><div class="blockInfoDiv">Please wait until your request is processed.<br/><p class="blockWarning">Please do not close the window or press refresh button</p><p><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></p></div></div>').prependTo('body').attr('id', 'overlay'); 
            $http({
                method : "PUT",
                url : BASE_URL+CART_CHECKOUT_URL,
                params : {
                    token : $scope.authToken,
                    addressId : $scope.shippingAddress,
                    vendorId : $scope.vendorId,
                    cartId : localStorage.getItem("ecommerce_cartId")
                }
            }).then(function success(response){
                viviktaDebugConsole(JSON.stringify(response));
                if(response.status == HTTP_STATUS_CODE){
                    if(response.data.errorCode == CART_CHECKOUT_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT){
                        localStorage.removeItem("anantya_cart");
                        setTimeout(function(){
                            overlay.remove();
                            window.location.href = "myOrders.php";
                        },10000);
                    }
                    else{
                        overlay.remove();
                        viviktaDebugConsole("could not buy the item!");
                    }
                }
                else{
                    overlay.remove();
                    viviktaDebugConsole("Encountered server error!");
                }
            },function failure(){
                overlay.remove();
                viviktaDebugConsole("ProductsController - proceedToPayment - API not found!");
            });
        }
    };

    //function to load the orders list
    $scope.loadOrders = function(){
        $scope.authToken = localStorage.getItem("anantya_token");
        $http({
            method : "GET",
            url : BASE_URL+GET_ORDERS_LIST_URL,
            params : {
                token : $scope.authToken,
                vendorId : $scope.vendorId
            }
        }).then(function success(response){
            viviktaDebugConsole(JSON.stringify(response));
            if(response.status == HTTP_STATUS_CODE){
                if(response.data.errorCode == GET_ORDERS_LIST_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT && response.data.data != '' && response.data.data != null && typeof(response.data.data) !== undefined){
                    $scope.ordersFound = true;
                    $scope.orders = response.data.data;
                }
                else{
                    $scope.ordersFound = false;
                }
            }
            else{
                viviktaDebugConsole("Encountered server error!");
                $scope.ordersFound = false;
            }
        },function failure(){
            viviktaDebugConsole("ProductsController - loadCheckoutDetails - API not found!");
            $scope.ordersFound = false;
        });
    };

    //function to display the textarea for writing the complaint
    $scope.showComplaintBox = false;
    $scope.showComplaintBoxTextarea = function(){
        if($scope.showComplaintBox == false){
            $scope.showComplaintBox = true;
        }
        else{
            $scope.showComplaintBox = false;
        }
    };

    //function to raise the complaint for an order
    $scope.raiseComplaint = function(complaintData,orderDetails){
        $scope.authToken = localStorage.getItem("anantya_token");
        $http({
            method : "POST",
            url : BASE_URL+RAISE_COMPLAINT_URL,
            params : {
                token : $scope.authToken,
                cartId : orderDetails.cartId,
                userComplaint : complaintData.description,
                vendorId : $scope.vendorId
            }
        }).then(function success(response){
            viviktaDebugConsole(JSON.stringify(response));
            if(response.status == HTTP_STATUS_CODE){
                if(response.data.errorCode == RAISE_COMPLAINT_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT){
                    $scope.showToaster("Complaint received!");
                    $scope.showComplaintBox = false;
                }
                else{
                    $scope.showToaster("Could not raise the complaint!");
                }
            }
            else{
                viviktaDebugConsole("Encountered server error!");
                $scope.showToaster("Could not raise the complaint!");
            }
        },function failure(){
            viviktaDebugConsole("ProductsController - raiseComplaint - API not found!");
            $scope.showToaster("Could not raise the complaint!");
        });
    };

    //function to show the toaster
    $scope.showToaster = function(message){
        $scope.toaster = document.getElementById("toaster");
        $scope.toaster.innerHTML = message;
        $scope.toaster.className = "show";
        setTimeout(function(){
            $scope.toaster.className = $scope.toaster.className.replace("show", "");
        }, 3000);
    };
        
    //function to search by subCategory
       $scope.searchProduct = function(keyword,productSubCategory){
        if(keyword == '' || keyword == undefined || typeof keyword == undefined){ 
             LogService.infoLog("HI");
            $scope.loadProductsBasedOnProductCategory(0,''); 
        }
        else{
            $scope.ajaxPromise = $http({
                method : "GET",
                url : BASE_URL+GET_SEARCH_FOR_PRODUCT_SUBCATEGORY_URL,
                params : {
                    subCategoryId : $scope.productSubCategory,
                    vendorId : $scope.vendorId,
                    search : keyword
                }
            }).then(function success(response){
                LogService.infoLog(JSON.stringify(response));
                 
                  //$scope.products = response.data.data.data;
                if(response.status == HTTP_STATUS_CODE){
                    if(response.data.errorCode == GET_SUGGESTION_FOR_SEARCH_PRODUCT_SUBCATEGORY_SUCCESS_CODE ){
                       
                        $scope.products = response.data.data;
                        $scope.productsPresent = true; 
                    }
                    else if(response.data.errorCode == GET_SUGGESTION_FOR_SEARCH_PRODUCT_SUBCATEGORY_FAILURE_CODE && response.data.statusText == SUCCESS_STATUS_TEXT && response.data.data !='' && response.data.data != null && typeof(response.data.data) != undefined && response.data.data.length!=0){
                        $scope.productsPresent = false;
                        $scope.productsNotPresent = true;
                    }
                    else{   
                         $scope.productsPresent = false;
                        $scope.productsNotPresent = true;
                    }
                }
                else{
                    $scope.productsPresent = false; 
                }
           
            },function failure(){
                $scope.productsPresent = false;
                $scope.productsNotPresent = true;
            });
        }       
    };

//function to get the products according to price range.
 $scope.getPriceRange = function(priceRange,paginateFlag,url){
       $scope.authToken = localStorage.getItem("anantya_token");
        $http({
            method : "GET",
            url : BASE_URL+LIST_PRODUCTS_BY_CATEGORY_URL,
            params : {
                token : $scope.authToken,
                vendorId : $scope.vendorId,
                subCategoryId : $scope.productSubCategory,
                minimumProductPrice : priceRange.minimumProductPrice,
                maximumProductPrice : priceRange.maximumProductPrice,
            }
        }).then(function success(response){
            viviktaDebugConsole(JSON.stringify(response));
            if(response.status == HTTP_STATUS_CODE){
                if(response.data.errorCode == LIST_PRODUCTS_BY_CATEGORY_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT ){

                $scope.lastPage = response.data.data.lastPage;
                $scope.showPagination = true;
                if($scope.lastPage == "NULL")
                {
                    $scope.showPagination = false;
                    $scope.products = response.data.data.data;
                    $scope.productsPresent = true;
                    
                }
                else
                {
                    $scope.products = response.data.data.data;
                    $scope.productsPresent = true; 
                }

                $scope.previousPageUrl = response.data.data.previousPageUrl;
                $scope.nextPageUrl = response.data.data.nextPageUrl;
                $scope.currentPage = response.data.data.currentPage;
                    $scope.productsPresent = true; 
                }
                else{
                    viviktaDebugConsole("There are no products found!");
                }
            }
            else{
                viviktaDebugConsole("encouneterd server error!");
            }
        },function failure(){
            viviktaDebugConsole("ProductsController - loadProductsBasedOnProductCategory - API not found!");
        });
     
    };
    

    
        $scope.loadColorsToFilter = function(color){
        viviktaDebugConsole("loadColorDetails-->");
        $http({
            method : "GET",
            url : BASE_URL+GET_COLOR_DETAILS_URL,
            params : {
                    vendorId : $scope.vendorId,
            }
        }).then(function success(response){
            viviktaDebugConsole(JSON.stringify(response));
            if(response.status == HTTP_STATUS_CODE){
                if(response.data.errorCode == GET_COLOR_DETAILS_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT && response.data.data != '' && response.data.data != null && typeof(response.data.data) !== undefined){
                    $scope.colors = response.data.data.data;
                }
                else{
                    
                }
            }
            else{
                viviktaDebugConsole("Encountered server error!");
            }
        },function failure(){
            viviktaDebugConsole("ProductsController - loadColorDetails - API not found!");
        });
    };
    
    
    
/*
    $scope.selectedColors = []; 
    $scope.toggleColor = function(checkbox,color) { 
        if($scope.SelectedColor(color)) {
            console.log("toggleColor: remove color " + color.productColor);
            $scope.selectedColors.splice($scope.selectedColors.indexOf(color.productColorId), 1);
        } else {
            console.log("toggleColor: add color " + color.productColor);
            $scope.selectedColors.push(color.productColorId);
        }    
    };
    
     $scope.SelectedColor = function(color) {
        return $scope.selectedColors.indexOf(color.productColorId) > -1;
    };
    
        */
    
    
        $scope.selectedColorsArray = [];
        localStorage.removeItem("selectedColorsArray");
       $scope.updateTicketStatus = function(checkboxStatus,color,productColorId){
        if(localStorage.getItem("selectedColorsArray")){
            $scope.selectedColorsArray = JSON.parse(localStorage.getItem("selectedColorsArray"));
            if(checkboxStatus == true){
                $scope.selectedColorsArray.push({
                    productColorId : color.productColorId
                });      
            }
            else{
                if($scope.selectedColorsArray.length>1)
                {
                    $scope.selectedColorsArray = $scope.selectedColorsArray.filter(function(item) { 
                        return item.productColorId !== color.productColorId;  
                    });
                }
                else{
                    $scope.selectedColorsArray = [];
                   
                }
            }
        }
        else{
            if(checkboxStatus == true){
                $scope.selectedColorsArray.push({
                    productColorId : color.productColorId,
                });      
            }
            
        }
        localStorage.setItem("selectedColorsArray",JSON.stringify($scope.selectedColorsArray));
             console.log(localStorage.getItem("selectedColorsArray"));
    }
       
       
       
       
        $scope.getColouredProduct = function(color,paginateFlag,url){
        $scope.authToken = localStorage.getItem("anantya_token");
        $http({
            method : "GET",
            url : BASE_URL+LIST_PRODUCTS_BY_CATEGORY_URL,
            params : {
                token : $scope.authToken,
                vendorId : $scope.vendorId,
                subCategoryId : $scope.productSubCategory,
                productColorId : ['EB63C850','EB0E4085','429E56AB']
            }
        }).then(function success(response){
            viviktaDebugConsole(JSON.stringify(response));
            if(response.status == HTTP_STATUS_CODE){
                if(response.data.errorCode == LIST_PRODUCTS_BY_CATEGORY_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT ){

                $scope.lastPage = response.data.data.lastPage;
                $scope.showPagination = true;
                if($scope.lastPage == "NULL")
                {
                    $scope.showPagination = false;
                    $scope.products = response.data.data.data;
                    $scope.productsPresent = true;
                    
                }
                else
                {
                    $scope.products = response.data.data.data;
                    $scope.productsPresent = true; 
                }

                $scope.previousPageUrl = response.data.data.previousPageUrl;
                $scope.nextPageUrl = response.data.data.nextPageUrl;
                $scope.currentPage = response.data.data.currentPage;
                    $scope.productsPresent = true; 
                }
                else{
                    viviktaDebugConsole("There are no products found!");
                }
            }
            else{
                viviktaDebugConsole("encouneterd server error!");
            }
        },function failure(){
            viviktaDebugConsole("ProductsController - getColouredProduct - API not found!");
        });
        
    };
    
    
    
})